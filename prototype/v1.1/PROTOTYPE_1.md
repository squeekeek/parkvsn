# Prototype v1
- The camera nodes would just send lists of data for Background Thresholding and HoG + SVMs
- If both are 1, then car
    - If BT is 1, but HS is 0, then obstruction
    - If BT is 0, but HS is 1, then unoccupied
- No initial operation states yet

# Normal operation states
## Central node states
- (DEFAULT) STATE #0: Cron job waiting for time to do STATE #1
    - Set time for example 3 mins, after STATE #4, before going back to STATE #1
- STATE #1:  Would send out command codes to the camera nodes, requesting parking lot information
    -  will send REQ-OCC
- STATE #2: Waiting for response from both nodes
    - Wait for Camera #1
        - "1-DATA-HOG: [0, 1, 1,]", 2-DATA-BT
    - Wait for Camera node #2
        - 1-DATA-HOG, 2-DATA-BT
- STATE #3: Process acquired data from State #2. Consolidate parking lot information. Finalize.
    - If both are 1, then car
    - If BT is 1, but HS is 0, then obstruction
    - If BT is 0, but HS is 1, then unoccupied
- STATE #4: Display parking lot info on terminal
    - Go back to State #0

## Camera node states
- (DEFAULT) STATE #0: Waitiing for request code from Central node
    - REQ-OCC
- STATE #1: Segment images into parking lots spaces
- STATE #2: Background thresholding detection 
- STATE #3: HoG + SVM detection
    - would consider just one occupied detection from the applied SVM models as occupied
    - or send all lists ba
- STATE #4: Send list results from State #2 and State #3
    - Go back to State #0
    - 1-DATA-BT, 2-DATA-BT
    - 1-DATA-HOG, 2-DATA-BT
    # insert into below list to send
    list_to_send = ""
    data_hog = "1-DATA-HOG: " + list_to_send 
    send(datta_hog)


## Future changes
- Take into account skewness to estimate 3d representation
    - This would affect taking into results for background thresholding and for HoG + SVM
- Better algorithm for occupancy detection
- Camera nodes should just send occupied or unoccupeid list to central node
- Do list of command codes