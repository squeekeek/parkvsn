###### Mon Mar 27 19:54:23 PHT 2017

pi@raspberrypi:~/parkvsn-rpi/prototype/v1.1 $ python prototype_hog.py
dataset_test/eeei_5th_solar/20170313-1346_seg6.jpg
Prediction time: 0:00:00.805610
dataset_test/eeei_5th_solar/20170313-1346_seg3.jpg
Prediction time: 0:00:01.120551
dataset_test/eeei_5th_solar/20170313-1346_seg2.jpg
Prediction time: 0:00:02.401507
dataset_test/eeei_5th_solar/20170313-1346_seg13.jpg
Prediction time: 0:00:00.358553
dataset_test/eeei_5th_solar/20170313-1346_seg11.jpg
Prediction time: 0:00:00.351612
dataset_test/eeei_5th_solar/20170313-1346_seg5.jpg
Prediction time: 0:00:00.322686
dataset_test/eeei_5th_solar/20170313-1346_seg1.jpg
Prediction time: 0:00:02.533647
dataset_test/eeei_5th_solar/20170313-1346_seg7.jpg
Prediction time: 0:00:00.950995
dataset_test/eeei_5th_solar/20170313-1346_seg4.jpg
Prediction time: 0:00:00.213559
dataset_test/eeei_5th_solar/20170313-1346_seg10.jpg
Prediction time: 0:00:00.370999
dataset_test/eeei_5th_solar/20170313-1346_seg9.jpg
Prediction time: 0:00:00.371571
dataset_test/eeei_5th_solar/20170313-1346_seg12.jpg
Prediction time: 0:00:02.368993
dataset_test/eeei_5th_solar/20170313-1346_seg8.jpg
Prediction time: 0:00:00.370199
HoG + SVM: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]
###############################

Parking Lot: EEEI Student's Parking Lot

#1: OCCUPIED
#2: OCCUPIED
#3: OCCUPIED
#4: OCCUPIED
#5: OCCUPIED
#6: OCCUPIED
#7: OCCUPIED
#8: OCCUPIED
#9: OCCUPIED
#10: OCCUPIED
#11: OCCUPIED
#12: EMPTY
#13: OCCUPIED

###############################

pi@raspberrypi2:~/parkvsn-rpi/prototype/v1.1 $ python prototype_hog.py
dataset_test/eeei_5th_solar/20170313-1346_seg6.jpg
Prediction time: 0:00:01.533682
dataset_test/eeei_5th_solar/20170313-1346_seg3.jpg
Prediction time: 0:00:02.137341
dataset_test/eeei_5th_solar/20170313-1346_seg2.jpg
Prediction time: 0:00:04.670256
dataset_test/eeei_5th_solar/20170313-1346_seg13.jpg
Prediction time: 0:00:00.701002
dataset_test/eeei_5th_solar/20170313-1346_seg11.jpg
Prediction time: 0:00:00.698764
dataset_test/eeei_5th_solar/20170313-1346_seg5.jpg
Prediction time: 0:00:00.632823
dataset_test/eeei_5th_solar/20170313-1346_seg1.jpg
Prediction time: 0:00:04.972361
dataset_test/eeei_5th_solar/20170313-1346_seg7.jpg
Prediction time: 0:00:01.904590
dataset_test/eeei_5th_solar/20170313-1346_seg4.jpg
Prediction time: 0:00:00.428911
dataset_test/eeei_5th_solar/20170313-1346_seg10.jpg
Prediction time: 0:00:00.701971
dataset_test/eeei_5th_solar/20170313-1346_seg9.jpg
Prediction time: 0:00:00.698176
dataset_test/eeei_5th_solar/20170313-1346_seg12.jpg
Prediction time: 0:00:04.623470
dataset_test/eeei_5th_solar/20170313-1346_seg8.jpg
Prediction time: 0:00:00.695545
HoG + SVM: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]
###############################

Parking Lot: EEEI Student's Parking Lot

#1: OCCUPIED
#2: OCCUPIED
#3: OCCUPIED
#4: OCCUPIED
#5: OCCUPIED
#6: OCCUPIED
#7: OCCUPIED
#8: OCCUPIED
#9: OCCUPIED
#10: OCCUPIED
#11: OCCUPIED
#12: EMPTY
#13: OCCUPIED

###############################


pi@raspberrypi3:~/parkvsn-rpi/prototype/v1.1 $ python prototype_hog.py
dataset_test/eeei_5th_solar/20170313-1346_seg6.jpg
Prediction time: 0:00:00.800779
dataset_test/eeei_5th_solar/20170313-1346_seg3.jpg
Prediction time: 0:00:01.082598
dataset_test/eeei_5th_solar/20170313-1346_seg2.jpg
Prediction time: 0:00:02.357005
dataset_test/eeei_5th_solar/20170313-1346_seg13.jpg
Prediction time: 0:00:00.351055
dataset_test/eeei_5th_solar/20170313-1346_seg11.jpg
Prediction time: 0:00:00.351399
dataset_test/eeei_5th_solar/20170313-1346_seg5.jpg
Prediction time: 0:00:00.321998
dataset_test/eeei_5th_solar/20170313-1346_seg1.jpg
Prediction time: 0:00:02.500843
dataset_test/eeei_5th_solar/20170313-1346_seg7.jpg
Prediction time: 0:00:00.948054
dataset_test/eeei_5th_solar/20170313-1346_seg4.jpg
Prediction time: 0:00:00.213595
dataset_test/eeei_5th_solar/20170313-1346_seg10.jpg
Prediction time: 0:00:00.353698
dataset_test/eeei_5th_solar/20170313-1346_seg9.jpg
Prediction time: 0:00:00.351176
dataset_test/eeei_5th_solar/20170313-1346_seg12.jpg
Prediction time: 0:00:02.340587
dataset_test/eeei_5th_solar/20170313-1346_seg8.jpg
Prediction time: 0:00:00.349804
HoG + SVM: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]
###############################

Parking Lot: EEEI Student's Parking Lot

#1: OCCUPIED
#2: OCCUPIED
#3: OCCUPIED
#4: OCCUPIED
#5: OCCUPIED
#6: OCCUPIED
#7: OCCUPIED
#8: OCCUPIED
#9: OCCUPIED
#10: OCCUPIED
#11: OCCUPIED
#12: EMPTY
#13: OCCUPIED

###############################