pi@raspberrypi2:~/parkvsn-rpi/prototype/v1.1 $ python prototype_hog.py
dataset_test/eeei_5th_solar/20170313-1346_seg6.jpg
/usr/local/lib/python2.7/dist-packages/skimage/feature/_hog.py:119: skimage_deprecation: Default value of `block_norm`==`L1` is deprecated and will be changed to `L2-Hys` in v0.15
  'be changed to `L2-Hys` in v0.15', skimage_deprecation)
Prediction time: 0:00:01.749506
dataset_test/eeei_5th_solar/20170313-1346_seg3.jpg
Prediction time: 0:00:00.805213
dataset_test/eeei_5th_solar/20170313-1346_seg2.jpg
Prediction time: 0:00:05.522647
dataset_test/eeei_5th_solar/20170313-1346_seg13.jpg
Prediction time: 0:00:00.825491
dataset_test/eeei_5th_solar/20170313-1346_seg11.jpg
Prediction time: 0:00:00.811982
dataset_test/eeei_5th_solar/20170313-1346_seg5.jpg
Prediction time: 0:00:00.729370
dataset_test/eeei_5th_solar/20170313-1346_seg1.jpg
Prediction time: 0:00:02.024240
dataset_test/eeei_5th_solar/20170313-1346_seg7.jpg
Prediction time: 0:00:02.214448
dataset_test/eeei_5th_solar/20170313-1346_seg4.jpg
Prediction time: 0:00:00.498307
dataset_test/eeei_5th_solar/20170313-1346_seg10.jpg
Prediction time: 0:00:00.824691
dataset_test/eeei_5th_solar/20170313-1346_seg9.jpg
Prediction time: 0:00:00.821491
dataset_test/eeei_5th_solar/20170313-1346_seg12.jpg
Prediction time: 0:00:05.438223
dataset_test/eeei_5th_solar/20170313-1346_seg8.jpg
Prediction time: 0:00:00.817498
HoG + SVM: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]
###############################

Parking Lot: EEEI Student's Parking Lot

#1: OCCUPIED
#2: OCCUPIED
#3: OCCUPIED
#4: OCCUPIED
#5: OCCUPIED
#6: OCCUPIED
#7: OCCUPIED
#8: OCCUPIED
#9: OCCUPIED
#10: OCCUPIED
#11: OCCUPIED
#12: EMPTY
#13: OCCUPIED

###############################

pi@raspberrypi3:~/parkvsn-rpi/prototype/v1.1 $ python prototype_hog.py
dataset_test/eeei_5th_solar/20170313-1346_seg6.jpg
/usr/local/lib/python2.7/dist-packages/skimage/feature/_hog.py:119: skimage_deprecation: Default value of `block_norm`==`L1` is deprecated and will be changed to `L2-Hys` in v0.15
  'be changed to `L2-Hys` in v0.15', skimage_deprecation)
Prediction time: 0:00:00.856838
dataset_test/eeei_5th_solar/20170313-1346_seg3.jpg
Prediction time: 0:00:00.410260
dataset_test/eeei_5th_solar/20170313-1346_seg2.jpg
Prediction time: 0:00:02.826700
dataset_test/eeei_5th_solar/20170313-1346_seg13.jpg
Prediction time: 0:00:00.419899
dataset_test/eeei_5th_solar/20170313-1346_seg11.jpg
Prediction time: 0:00:00.416177
dataset_test/eeei_5th_solar/20170313-1346_seg5.jpg
Prediction time: 0:00:00.378450
dataset_test/eeei_5th_solar/20170313-1346_seg1.jpg
Prediction time: 0:00:01.035040
dataset_test/eeei_5th_solar/20170313-1346_seg7.jpg
Prediction time: 0:00:01.138905
dataset_test/eeei_5th_solar/20170313-1346_seg4.jpg
Prediction time: 0:00:00.252547
dataset_test/eeei_5th_solar/20170313-1346_seg10.jpg
Prediction time: 0:00:00.420452
dataset_test/eeei_5th_solar/20170313-1346_seg9.jpg
Prediction time: 0:00:00.417309
dataset_test/eeei_5th_solar/20170313-1346_seg12.jpg
Prediction time: 0:00:02.795395
dataset_test/eeei_5th_solar/20170313-1346_seg8.jpg
Prediction time: 0:00:00.418231
HoG + SVM: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]
###############################

Parking Lot: EEEI Student's Parking Lot

#1: OCCUPIED
#2: OCCUPIED
#3: OCCUPIED
#4: OCCUPIED
#5: OCCUPIED
#6: OCCUPIED
#7: OCCUPIED
#8: OCCUPIED
#9: OCCUPIED
#10: OCCUPIED
#11: OCCUPIED
#12: EMPTY
#13: OCCUPIED

###############################


pi@raspberrypi2:~/parkvsn-rpi/prototype/v1.1 $ sudo python prototype_v1.1.py
================ SPI Configuration ================
CSN Pin  	 = CE0 (PI Hardware Driven)
CE Pin  	 = Custom GPIO17
Clock Speed	 = 8 Mhz
================ NRF Configuration ================
STATUS		 = 0x0e RX_DR=0 TX_DS=0 MAX_RT=0 RX_P_NO=7 TX_FULL=0
RX_ADDR_P0-1	 = 0xf0f0f0f0ab 0xf0f0f0f0c1
RX_ADDR_P2-5	 = 0xd1 0xc4 0xc5 0xc6
TX_ADDR		 = 0xf0f0f0f0ab
RX_PW_P0-6	 = 0x20 0x20 0x20 0x00 0x00 0x00
EN_AA		 = 0x3f
EN_RXADDR	 = 0x02
RF_CH		 = 0x4c
RF_SETUP	 = 0x01
CONFIG		 = 0x0e
DYNPD/FEATURE	 = 0x3f 0x06
Data Rate	 = 1MBPS
Model		 = nRF24L01+
CRC Length	 = 16 bits
PA Power	 = PA_MIN
 ************ Role Setup ***********
Choose a role: Enter 0 for controller, 1 for node1, 2 for node2 CTRL+C to exit) 0
Role: Controller, starting transmission
role: controller
Written to Node 1
Did not find Node 2
inside choose mode
 ************ Choose Mode ***********
Choose a mode:
 0: Measure metrics
 1: Send occupied array
 2: Send features
 3: exit
1
********** REQUESTED DATA *********
1-DATA-BT: [1,1,1,1,1,1,1,1,1,1,1,1,1]
bt_array_temp: [u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1', u'1']
Background Thresholding: 1,1,1,1,1,1,1,1,1,1,1,1,1
HOG+SVM: 1,1,1,1,0,1,1,1,1,1,0,1,1
Occupancy Array: [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1]
Obstruction Array: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0]
***********************************
********** REQUESTED DATA *********