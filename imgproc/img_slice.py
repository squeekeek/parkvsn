from __future__ import division

import os
import cv2
import xmltodict as xmld
import numpy as np
import math

from matplotlib import pyplot as plt

import img_math
import img_plot


def slice_one_img(img, polypts):
    polypts = np.int32(polypts)

    # Get each parking space by masking
    smask = np.zeros(img.shape, dtype=np.uint8)
    nchannels = img.shape[2] if (len(img.shape) == 3) else 1
    smask = cv2.fillPoly(smask, np.int32([polypts]), (255, ) * nchannels)
    pkspace = cv2.bitwise_and(smask, img)

    # Crop rectangles to masked size
    mincoords = map(min, zip(*polypts))
    maxcoords = map(max, zip(*polypts))
    normpts = map(tuple, np.subtract(polypts, mincoords))

    return pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]]


def slice_img_pklot(img, xmlboxes):
    #print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']

    pspaces = []
    pspacepts = []
    for eachspace in xmlboxes['parking']['space']:
        rectpts = img_math.sortfourcw([(int(a['@x']), int(a['@y'])) for a in eachspace['contour']['point']])

        # Slice single rectangle from image
        new_img = slice_one_img(img, rectpts)
        pspaces.append(new_img)
        pspacepts.append([int(eachspace['@occupied']) if '@occupied' in eachspace else -1, rectpts.tolist()])

    #print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return [pspaces, pspacepts]


def slice_img_json(img, jsonboxes, with_occupancy=True):
    #print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']

    pspaces = []
    pspacepts = []
    for eachspace in jsonboxes['spaces']:
        rectpts = img_math.sortfourcw([(int(a['x']), int(a['y'])) for a in eachspace['pts']])

        # Slice single rectangle from image
        new_img = slice_one_img(img, rectpts)
        pspaces.append(new_img)

        if with_occupancy:
            pspacepts.append([int(eachspace['occupied']) if 'occupied' in eachspace else -1, rectpts.tolist()])
        else:
            pspacepts.append(rectpts.tolist())

    #print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return [pspaces, pspacepts]


def slice_img_rects(img, pspace_rects):
    pspaces = []

    for eachspace in pspace_rects:
        eachspace = img_math.sortfourcw(eachspace)

        new_img = slice_one_img(img, eachspace)
        pspaces.append(new_img)

    return pspaces


def main():
    with open('../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626.xml') as fd:
        xmlf = xmld.parse(fd.read())
        img = cv2.imread('../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626.jpg', 1)
        pspaces, prect = slice_img(img, xmlf)

        img_plot.plot_img_with_segments(img, prect)
        img_plot.plot_spaces(pspaces)

    plt.show()


if __name__ == "__main__":
    main()