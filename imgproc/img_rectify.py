from __future__ import division

import cv2
import math
import numpy as np
import json

from matplotlib import pyplot as plt
from operator import itemgetter

import img_plot
import img_math
import img_slice

import sys


def get_affine_mat(alphabeta):
    if np.isclose(alphabeta[1], 0, atol=1e-6):
        return None

    affmat = np.eye(3)
    affmat[0, 0] = 1 / alphabeta[1]
    affmat[0, 1] = -alphabeta[0] / alphabeta[1]

    return affmat


def get_vanishing_point(pts):
    u"""
    From The 3-Point Method: A Fast, Accurate and Robust Solution to
    Vanishing Point Estimation

    A vanishing point is calculated using the algorithm developed by Saini
    et al (2013)

    :param pts: 3-point line for determining the vanishing point

    :type pts: numpy array of pairs of x and y coordinates

    :return: coordinates of vanishing point in homogeneous coordinates
    :rtype: numpy array
    """
    a, c, b = pts

    alp = ((c[0] - a[0]) * (0.5 - 1)) / ((c[0] - b[0]) * 0.5)
    vp_x, vp_y = (alp / (alp - 1)) * np.array([b[0] - a[0], b[1] - a[1]])

    return np.array([vp_x, vp_y, 1])


def get_new_pdim(img_rect, pmat):
    u"""Get the new bounding rectangle of an image dimension after some 3D
       transform

    :param img_rect: quadrilateral representing the bounds of the image
    :param pmat: 3x3 transformation matrix to apply to img_rect

    :type img_rect: numpy array of 4 pairs of x and y coordinates
    :type pmat: numpy array with shape 3x3

    :return: translation matrix and size of new image
    :rtype: 2-element tuple
    """
    img_pts = np.float32(img_rect).reshape(-1, 1, 2)
    img_pts_p = cv2.perspectiveTransform(img_pts, pmat)

    [xmin, ymin] = np.int32(img_pts_p.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = np.int32(img_pts_p.max(axis=0).ravel() + 0.5)

    tvec = [-xmin, -ymin]
    Ht = np.array([[1, 0, tvec[0]], [0, 1, tvec[1]], [0, 0, 1]])

    return Ht, (xmax - xmin, ymax - ymin)


def get_new_adim(dim, amat):
    h, w = dim

    img_pts = np.float32([[0, 0], [0, h], [w, h], [w, 0]]).reshape(-1, 1, 2)
    img_pts_a = cv2.transform(img_pts, amat)

    [xmin, ymin] = np.int32(img_pts_a.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = np.int32(img_pts_a.max(axis=0).ravel() + 0.5)

    tvec = [-xmin, -ymin]
    Ht = np.array([[1, 0, tvec[0]], [0, 1, tvec[1]], [0, 0, 1]])

    return Ht, (xmax - xmin, ymax - ymin)


def correct_perspective(img, img_rect, vert_pts, horiz_pts):
    u"""Correct perspective of image and show all visible pixels

    Lines made by vert_pts and horiz_pts should be perpendicular with each
    other in the world space. The points should be collinear.

    :param img: image to correct the perspective
    :param img_rect: bounding quadrilateral of the image
    :param vert_pts: 3-element list of points in the general vertical diretion
    :param horiz_pts: 3-element list of points in the general horizontal
                      direction

    :type img: BGR colored image
    :type img_rect: numpy list of pairs of x and y coordinates
    :type vert_pts: numpy list of pairs of x and y coordinates
    :type horiz_pts: numpy list of pairs of x and y coordinates

    :return: transformed image and perspective transform matrix used
    :rtype: 2-element tuple
    """
    vert_pts_pos = zip(*vert_pts)
    horiz_pts_pos = zip(*horiz_pts)

    # Fit points to a line via least squares
    line_mxb_horiz, line_mxb_vert = {}, {}
    line_mxb_horiz["m"], line_mxb_horiz["b"] = np.linalg.lstsq(np.vstack([horiz_pts_pos[0], np.ones(len(horiz_pts_pos[0]))]).T, horiz_pts_pos[1])[0]
    line_mxb_vert["m"], line_mxb_vert["b"] = np.linalg.lstsq(np.vstack([vert_pts_pos[0], np.ones(len(vert_pts_pos[0]))]).T, vert_pts_pos[1])[0]

    # Get vanishing points
    horiz_lstsq = img_math.get_closest_to_line(horiz_pts, line_mxb_horiz)
    vp_horiz = get_vanishing_point(horiz_lstsq)

    vert_lstsq = img_math.get_closest_to_line(vert_pts, line_mxb_vert)
    vp_vert = get_vanishing_point(vert_lstsq)

    # Set-up perspective matrix
    linf = img_math.linepts_to_abc([vp_vert, vp_horiz])
    pmat = np.vstack([np.eye(2, 3), linf])

    # Transform image
    Ht, new_dim = get_new_pdim(img_rect, pmat)
    img_p = cv2.warpPerspective(img, Ht.dot(pmat), new_dim)

    return img_p, Ht.dot(pmat), (vp_horiz, vp_vert)


def correct_affine(img, img_rect, linelist):
    u"""Correct affinity of image and show all visible pixels

    :param img: image to correct the perspective
    :param img_rect: bounding quadrilateral of image
    :param linelist: list of four lines as reference. The first two should be
                     be perpendicular to each other. Same should be the case
                     for the last two.

    :type img: BGR colored image
    :type img_rect: numpy list of pairs of x and y coordinates

    :return: transformed image and affine transform matrix used
    :rtype: 2-element tuple

    """
    # Transform points
    circ_a = img_math.linepair_to_circle_90deg(linelist[:2])
    circ_b = img_math.linepair_to_circle_90deg(linelist[2:])

    '''
    #print circ_a, circ_b

    plt.figure()
    ax = plt.gcf().gca()
    ax.set_xlim((-100, 100))
    ax.set_ylim((-100, 100))
    ax.add_artist(plt.Circle(circ_a[0], circ_a[1], color='r', fill=False))
    ax.add_artist(plt.Circle(circ_b[0], circ_b[1], color='g', fill=False))
    '''

    circ_intersect = img_math.circle_intersect(np.array([circ_a, circ_b]))
    amat = get_affine_mat(circ_intersect)

    #amat = np.eye(3)
    Ht, new_dim = get_new_pdim(img_rect, amat)
    newimg = cv2.warpPerspective(img, Ht.dot(amat), new_dim)

    # Returns corrected image
    return newimg, Ht.dot(amat)


def correct_similarity(img, img_rect, ref_pts, now_pts):
    u"""Correct Euclidean transform of image and show all visible pixels

    :param img: image to correct the Euclidean transform
    :param img_rect: bounding quadrilateral of image
    :param ref_pts: the bottom-most line in the untransformed image
                    represented in homogeneous coordinates
    :param now_pts: the bottom-most line in the transformed image
                    represented in homogeneous coordinates

    :type img: BGR colored image
    :type img_rect: numpy list of 4 pairs of x and y coordinates
    :type ref_pts: numpy list of 2 pairs of x and y coordinates
    :type now_pts: numpy list of 2 pairs of x and y coordinates

    :return: transformed image and similarity transform matrix used
    :rtype: 2-element tuple
    """
    ref_vec = ref_pts[1] - ref_pts[0]
    now_vec = now_pts[1] - now_pts[0]

    vec_arg = math.degrees(math.asin(np.cross(now_vec, ref_vec) / (np.linalg.norm(now_vec) * np.linalg.norm(ref_vec))))

    if vec_arg > 90:
        vec_arg -= 90
    elif vec_arg < -90:
        vec_arg += 90

    # Rotate in opposite direction
    rmat = np.vstack((cv2.getRotationMatrix2D(tuple(now_pts[0]), -vec_arg, 1), [0, 0, 1]))

    Ht, rdim = get_new_pdim(img_rect, rmat)
    img_r = cv2.warpPerspective(img, Ht.dot(rmat), rdim)

    return img_r, Ht.dot(rmat)


def rectify(img_color, jsonboxes, vert_pts, horiz_pts, line_vec, slice_spaces=False):
    u"""Rectify parking spaces in image into 3D

    :param img_color: colored image to transform
    :param jsonboxes: marked parking spaces formatted using the prescribed
                      JSON format, and read by json.loads()
    :param vert_pts: vertical 3-point calibration line for perspective
                     distortion correction
    :param horiz_pts: horizontal 3-point calibration line for perspective
                      distortion correction
    :param line_vec: two pairs of lines making 90-degree angles wrt each
                     other for affine distortion correction
    :param slice_spaces: flag to whether slice the image into its parking
                         spaces and transform them into upright rectangles

    :return: bounding rectangle of parallelepiped enclosing the parking
             space, convex hull of parallelepiped, and transformation
            matrix used
    :rtype: 3-element tuple
    """
    img_rect = np.float32([[0, 0], [img_color.shape[1], 0], [img_color.shape[1], img_color.shape[0]], [0, img_color.shape[0]]])

    # Perspective transform
    img_p, pmat, img_vpts = correct_perspective(img_color, img_rect, vert_pts, horiz_pts)
    p_lines = cv2.perspectiveTransform(line_vec, pmat)
    img_rect_p = cv2.perspectiveTransform(np.float32([img_rect]), pmat)[0]

    # Affine transform
    img_a, amat = correct_affine(img_p, img_rect_p, p_lines)
    a_lines = cv2.perspectiveTransform(p_lines, amat)
    img_rect_a = cv2.perspectiveTransform(np.float32([img_rect_p]), amat)[0]

    # Similarity transform
    # Get angle
    xvec = np.array([[0, img_color.shape[0]], img_color.shape[1::-1]])
    xvec_p = cv2.perspectiveTransform(np.float32([xvec]), pmat)
    xvec_a = cv2.perspectiveTransform(xvec_p, amat)[0]
    img_r, rmat = correct_similarity(img_a, img_rect_a, xvec, xvec_a)
    s_lines = cv2.perspectiveTransform(a_lines, rmat)
    img_rect_r = cv2.perspectiveTransform(np.float32([img_rect_a]), rmat)[0]

    # Get general transform matrix
    alltrans_mat = np.dot(rmat, np.dot(amat, pmat))

    # Estimate z-axis
    z_axis = img_rect_r[0] - img_rect_r[3]
    z_axis = z_axis / np.linalg.norm(z_axis)

    # Box rectangles
    rect_trans = [[], []]
    pspace_rects = []
    _, pspace_rects = img_slice.slice_img_json(img_color, jsonboxes)

    for isfull, eachrect in pspace_rects:
        eachrect = cv2.perspectiveTransform(np.float32([eachrect]), alltrans_mat)[0]
        rect_trans[0].append(eachrect)

        dr = img_math.get_length(eachrect)
        z_mat = np.float32([[1, 0, z_axis[0] * dr], [0, 1, z_axis[1] * dr], [0, 0, 1]])
        eachrect2 = cv2.perspectiveTransform(np.float32([eachrect]), z_mat)[0]
        rect_trans[1].append(eachrect2)

    chulls = []
    for d in zip(rect_trans[0], rect_trans[1]):
        chulls.append(np.reshape(cv2.convexHull(np.concatenate((d[0], d[1]))), (6, 2)))

    if slice_spaces:
        pspaces_bottom = img_slice.slice_img_rects(img_r, rect_trans[0])
        pspaces_top = img_slice.slice_img_rects(img_r, rect_trans[1])

        for k, (rb, rt) in enumerate(zip(*rect_trans)):
            # Similarity transform
            # Get angle
            rb = img_math.sortfourcw(rb)

            base_pt = np.min(zip(*rb), axis=1)

            rvec_spaces = np.array([rb[1], [rb[1][0], rb[0][1]]])
            rvec_spaces_p = np.array([rb[1], rb[0]])

            pspaces_bottom[k], ht = correct_similarity(pspaces_bottom[k], rb - base_pt, rvec_spaces, rvec_spaces_p)
            pspaces_top[k], _ = correct_similarity(pspaces_top[k], rb - base_pt, rvec_spaces, rvec_spaces_p)
            rect_new = cv2.perspectiveTransform(np.float32([rb - base_pt]), ht)

            # Get perspective transform
            rect_dims = np.float32([[0, 0], [0, pspaces_bottom[k].shape[0]], pspaces_bottom[k].shape[1::-1], [pspaces_bottom[k].shape[1], 0]])
            pt = cv2.getPerspectiveTransform(rect_new, rect_dims)

            pspaces_top[k] = cv2.warpPerspective(pspaces_top[k], pt, pspaces_top[k].shape[1::-1])
            pspaces_bottom[k] = cv2.warpPerspective(pspaces_bottom[k], pt, pspaces_bottom[k].shape[1::-1])

        return rect_trans, chulls, alltrans_mat, (pspaces_bottom, pspaces_top)
    else:
        return rect_trans, chulls, alltrans_mat

    #img_plot.plot_img_with_segments(img_color, pspace_rects)

    '''
    # Plot original image with calibration mark
    plt.figure()
    plt.plot(zip(*vert_pts)[0], zip(*vert_pts)[1], 'r-')
    plt.plot(zip(*horiz_pts)[0], zip(*horiz_pts)[1], 'g-')

    for d in line_vec:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')

    plt.imshow(img_color[:, :, ::-1])
    '''

    '''
    # Plot P corrected image with calibration mark
    plt.figure()
    for d in p_lines:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')

    plt.imshow(img_p[:, :, ::-1])
    '''

    '''
    # Plot A corrected image
    plt.figure()
    for d in a_lines:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')
    plt.imshow(img_a[:, :, ::-1])
    '''

    '''
    # Plot S corrected image
    plt.figure()
    plt.imshow(img_r[:, :, ::-1])

    for c in chulls:
        img_plot.plot_poly(c, 'b-')
    '''

    #img_plot.plot_img_with_segments(img_r, rect_trans[0])
    #img_plot.plot_img_with_segments(img_r, rect_trans[1]

    #plt.show()


def main():
    '''
    img_name = '../datasets/EEEI/eeei_5th_solar_20170329/20170330-0545.jpg'

    vert_pts = np.array([(433, 490), (473, 414), (512, 339)])
    horiz_pts = np.array([(546, 532), (389, 515), (234, 492)])
    line_vec = np.float32([[(433, 490), (512, 339)],
                           [(234, 492), (546, 532)],
                           [(804, 425), (863, 351)],
                           [(863, 351), (922, 400)]])
    '''

    '''
    img_name = '../datasets/EEEI/eeei_cnl_20170317/20170318-1638.jpg'
    img_json = '../datasets/EEEI/eeei_cnl_20170317/20170318-1638.json'
    '''

    #'''
    img_name = '../datasets/EEEI/eeei_5th_solar_20170414/20170414-1549.jpg'
    img_json = '../datasets/EEEI/eeei_5th_solar_20170414/20170414-1549.json'
    #'''

    '''
    img_name = '../datasets/EEEI/eeei_4th_bridge_20170313/20170315-1007.jpg'
    img_json = '../datasets/EEEI/eeei_4th_bridge_20170313/20170315-1007.json'
    '''

    '''
    img_name = '../datasets/EEEI/eeei_4th_bridge_20170504/20170504_1228.jpg'
    img_json = '../datasets/EEEI/eeei_4th_bridge_20170504/20170504_1334.json'
    '''

    with open(img_json) as jsonf:
        img_color = cv2.imread(img_name, 1)
        jsonboxes = json.loads(jsonf.read())
        vert_pts = np.float32(jsonboxes["calibration"]["vert_pts"])
        horiz_pts = np.float32(jsonboxes["calibration"]["horiz_pts"])
        line_vec = np.float32(jsonboxes["calibration"]["line_vec"])

        rectpts, chull, trans_mat, (pspaces_bot, pspaces_top) = rectify(img_color, jsonboxes, vert_pts, horiz_pts, line_vec, slice_spaces=True)

        img_rect = np.float32([[0, 0], [img_color.shape[1], 0], [img_color.shape[1], img_color.shape[0]], [0, img_color.shape[0]]])
        ht, new_dim = get_new_pdim(img_rect, trans_mat)
        img_trans = cv2.warpPerspective(img_color, ht.dot(trans_mat), new_dim)

        # Print 3D parking spaces
        cslices = []

        plt.figure()
        plt.yticks([])
        plt.xticks([])
        plt.imshow(img_trans[:, :, ::-1])
        for c in chull:
            img_plot.plot_poly(c, 'b-')
            cslices.append(img_slice.slice_one_img(img_trans, c))

        img_plot.plot_spaces(cslices)
        img_plot.plot_spaces(pspaces_bot)
        img_plot.plot_spaces(pspaces_top)

        plt.show()


if __name__ == "__main__":
    main()
