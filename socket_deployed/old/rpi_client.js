var http = require('http'),
    express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    errorhandler = require('errorhandler'),
    bodyParser = require('body-parser');

var app = express();

app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(logger('combined'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static('public'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(request, response) {
    response.render('index');
});

app.use(errorhandler());

var server = http.createServer(app);
var io = require('socket.io').listen(server);
/*
var io_cloud = require('socket.io-client');
socket_cloud = io_cloud.connect('localhost', {
    port: 4000
});
*/
var socket_cloud = require('socket.io-client')('http://localhost:4000');
socket_cloud.on('connect', function() { console.log("socket connected"); });
socket_cloud.emit('private message', { user: 'me', msg: 'whazzzup?' });



server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
/*
var socket = require('socket.io-client')('http://localhost');
socket.on('connect', function(){});
socket.on('event', function(data){});
socket.on('disconnect', function(){});

*/