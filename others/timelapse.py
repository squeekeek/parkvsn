'''
Code lifted from Instructables:
http://www.instructables.com/id/Time-Lapse-Photography-With-RPI-and-Pi-Camera/?ALLSTEPS
'''
import time
import picamera

NDAYS = 2
FPH = 20
NFRAMES = FPH * 24 * NDAYS
img_dir = "/home/pi/parkvsn-rpi/eeei_images"

def capture_frame(frame_id):
	with picamera.PiCamera(resolution='HD') as cam:
		time.sleep(2)
		cam.capture((img_dir + "/frame%03d.jpg") % frame_id)

def main():
	print 'Starting time-lapse photography for %d day(s)...' % NDAYS
	print 'Camera will take %d pictures per hour' % FPH
	print 'Pictures saved at %s' % img_dir

	while True:
		confirm_me = raw_input('WARNING: images with filename "frame%%d.jpg" in the directory will be overwritten.  Proceed? [y/N]')

		if len(confirm_me) >= 1:
			if confirm_me[0] != 'y' and confirm_me[0] != 'Y':
				print '\nTerminating script...'
				exit()

			break

	print '\nTerminate by pressing CTRL+D'
	print 'Opening camera preview to set view.'

	with picamera.PiCamera() as cam:
		cam.start_preview(fullscreen=False, window=(50, 50, 640, 480), alpha=225)

		while True:
			confirm_me = raw_input('Is the view OK? [y/N]')

			if len(confirm_me) >= 1 and (confirm_me[0] == 'y' or confirm_me[0] == 'Y'):
				break
		
		cam.stop_preview()

	print '\nCapturing images...'

	for i in range(int(NFRAMES)):
		start = time.time()
		capture_frame(i)
		print 'Frame %3d of %3d captured!' % (i + 1, NFRAMES)

		time.sleep((60 * 60 / FPH) - (time.time() - start))

	print 'Done capturing time-lapse image!'

if __name__ == "__main__":
	main()