# Web App

## Description
The web app was created on Node.JS, and is meant to be used as a progressive web app.
Hence, when using it on the cloud, the domain should have SSL certificates enabled, meaning it should be running on "https". However, when only testing on localhost, the web app should still work.


## Usage 
1. Run cloud_server.js on the cloud with "forever start cloud_server.js"





