var http = require('http'),
    express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    errorhandler = require('errorhandler'),
    bodyParser = require('body-parser');

var app = express();

app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(logger('combined'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static('public'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(request, response) {
    response.render('index');
});

app.use(errorhandler());

var server = http.createServer(app);


var io = require('socket.io-client'),
    socket = io.connect('http://localhost:4000');

var counter = 0;
var slots = [0, 0, 0, 0, 0, 0, 0, 0, 0];
var msg = "hello";
var empty = 0;
var total = 9;
socket.on('connect', function() {
    console.log("socket connected");
    //var value = "Hello";
    //socket.emit('messageChange', { message: value });

    setInterval(function() {
            // Change with actual slots
            if (counter == 0) {
                slots = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            } else if (counter == 1) {
                slots = [1, 0, 0, 0, 0, 0, 0, 0, 0];
            } else if (counter == 2) {
                slots = [1, 1, 0, 0, 0, 0, 0, 0, 0];
            } else if (counter == 3) {
                slots = [1, 1, 1, 0, 0, 0, 0, 0, 0];
            } else if (counter == 4) {
                slots = [1, 1, 1, 1, 0, 0, 0, 0, 0];
            } else if (counter == 5) {
                slots = [1, 1, 1, 1, 1, 0, 0, 0, 0];
            } else if (counter == 6) {
                slots = [1, 1, 1, 1, 1, 1, 0, 0, 0];
            } else if (counter == 7) {
                slots = [1, 1, 1, 1, 1, 1, 1, 0, 0];
            } else if (counter == 8) {
                slots = [1, 1, 1, 1, 1, 1, 1, 1, 0];
            } else if (counter == 9) {
                slots = [1, 1, 1, 1, 1, 1, 1, 1, 1];
            } else if (counter == 10) {
                slots = [1, 1, 1, 1, 1, 1, 1, 1, 0];
            } else if (counter == 11) {
                slots = [1, 1, 1, 1, 1, 1, 1, 0, 0];
            } else if (counter == 12) {
                slots = [1, 1, 1, 1, 1, 1, 0, 0, 0];
            } else if (counter == 13) {
                slots = [1, 1, 1, 1, 1, 0, 0, 0, 0];
            } else if (counter == 14) {
                slots = [1, 1, 1, 1, 0, 0, 0, 0, 0];
            } else if (counter == 15) {
                slots = [1, 1, 1, 0, 0, 0, 0, 0, 0];
            } else if (counter == 16) {
                slots = [1, 1, 0, 0, 0, 0, 0, 0, 0];
            } else if (counter == 17) {
                slots = [1, 0, 0, 0, 0, 0, 0, 0, 0];
                counter = 0;
            }
            total = slots.length;
            empty = 0;
            for (var i = 0; i < total; i++) {
                if (slots[i] == 0) {
                    empty = empty + 1;
                }
            }
            var msg = 'test_' + counter.toString();
            counter = counter + 1;
            socket.emit('rpi_msg', {
                message: msg,
                empty: empty,
                total: total,
                slots: slots,
            })

        },
        3000);
});

socket.on('from_rpi', function(data) {
    console.log(data);
});

server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});