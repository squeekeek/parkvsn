var http = require('http'),
    express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    errorhandler = require('errorhandler'),
    bodyParser = require('body-parser');

var app = express();

app.set('view engine', 'jade');
app.set('port', process.env.PORT || 4000);
app.use(logger('combined'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static('public'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(request, response) {
    response.render('index');
});

app.use(errorhandler());

var server = http.createServer(app);
var io = require('socket.io').listen(server);


var moment = require('moment');

var empty = 0;
var total = 9;
var slots = [0, 0, 0, 0, 0, 0, 0, 0, 0]
var RPI_data = { message: "proxy rpi data", empty: empty, total: total, slots: slots }

io.sockets.on('connection', function(socket) {

    // For browser clients
    socket.on('req_data', function(data) {
        console.log(data);
        socket.emit('from_rpi', RPI_data);
    });

    // For RPI interactions
    socket.on('rpi_msg', function(data) {
        var msg = "from rpi:" + data;
        RPI_data = data;
        RPI_data.message = moment().format('MMMM Do YYYY, h:mm:ss a');


    })
});


server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});