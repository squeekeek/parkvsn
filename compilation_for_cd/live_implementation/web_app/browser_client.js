var io = require('socket.io-client'),
    socket = io.connect('http://localhost:4000');
socket.on('connect', function() {
    console.log("socket connected");
    var value = "Hello";
    socket.emit('messageChange', { message: value });

    setInterval(function() {
            socket.emit('req_data', { message: 'test' })
        },
        3000);
});

socket.on('from_rpi', function(data) {
    console.log(data);
});