# An Implementation of an Open-space Visual Smart Parking System

This folder contains the source codes for implementing the image processing algorithm used in the study. The source codes are written in Python 2.7.

## Dependencies

The following Python libraries and modules are required to be able to run the source code:

* `xmltodict` - XML to dictionary converter (used for reading PKLot dataset XMLs)
* `json` - JSON to dictionary converter (used for reading EEEI dataset JSONs)
* `cv2` - OpenCV wrapper for Python
* `numpy` - Numerical library for Python with Matlab-like syntax
* `matplotlib` - For plotting marked parking lots and parking spaces, specifically `pyplot`.

## Organization

The source code is organized in a flat structure, with each Python script containg scripts for a specific algorithm. The scripts are the following:

* `img_main` - Main script used for occupancy detection
* `img_math` - Math functions used throughout the source code
* `img_plot` - Functions for plotting marked parking lots and parking spaces
* `img_rectify` - Functions for metric rectification and z-axis estimation
* `img_slice` - Functions for slicing parking spaces from an image of a parking lot

The source codes are fairly documented using docstrings. Sphinx may be used to autogenerate such documentation.

## Parking Lot Representation

Throughout the project, a parking lot image has a corresponding JSON file containing the coordinates for a parking space and optionally its calibration marks. The general layout of the JSON file is shown below.

```json
{
    "parking_id": "eeei_5th_solar_041017_a_morning_parkinglinebased", // Name/ID of parking lot
    "calibration": { // Calibraton marks
        "vert_pts": [ // Vertical calibration line (for perspective transform)
            [1135, 676],
            [1119, 547],
            [1100, 391]
        ],
        "horiz_pts": [ // Horizontal calibration line (for perspective transform)
            [384, 511],
            [549, 531],
            [727, 557]
        ],
        "line_vec": [ // Line pairs making an angle of 90 (for affine transform)
            [[142, 602], [120, 568]], // First two pairs
            [[120, 568], [227, 506]],
            [[632, 286], [671, 289]], // Last two pairs
            [[671, 289], [653, 340]]
        ]
    },
    "spaces": [{ // Parking spaces
            "id": 1, // Parking space ID
            "pts": [{ // Coordinates of four corners of each parking space (sorted CW or CCW)
                    "x": 139,
                    "y": 322
                },
                {
                    "x": 229,
                    "y": 327
                },
                {
                    "x": 123,
                    "y": 464
                },
                {
                    "x": 20,
                    "y": 457
                }
            ]
        },
        ...
```

## Running

The `img_main` module contains the encapsulated function `get_occupancy_list(img, spaces_dict, [use_calibration=False])` used to get an occupancy status for each parking space provided. It accepts an image of a parking lot `img`, dictionary of spaces from the JSON `spaces_dict` and an optional flag to whether use metric rectification `use_calibration`. This function uses an initial background reference image `parakeet_bs_ref.jpg`, which is assumed to be in the same location as the script. If it is not available, the script will generate one. A sample code is shown below.

```python
img_path = '..' # Path to parking lot image
json_path = '..' # Path to corresponding JSON file

with open(json_path) as data_file:
    test_img = cv2.imread(img_path, 1) # Load colored parking lot image
    spaces_dict = json.load(data_file) # Load json file containing parking lot spaces
    
    occ_list = img_main.get_occupancy_list(test_img, spaces_dict, use_calibration=False)
```

## Testing

Each script contains a small routine within `main()` to test its function, which can be run using either the EEEI datasets or the PKLot datasets. These routines can be edited with additional test functionality.

Accuracy and execution time tests were performed using the same scripts as that of the machine learning. Since the scripts are dependent on the tests sets for machine learning, these can instead be found at the `/experiments_ML/testing_UFPR04/src/_eval_scripts_w_det_w_scores_BS` folder.
# An Implementation of an Open-space Visual Smart Parking System

This folder contains the source codes for implementing the image processing algorithm used in the study. The source codes are written in Python 2.7.

## Dependencies

The following Python libraries and modules are required to be able to run the source code:

* `xmltodict` - XML to dictionary converter (used for reading PKLot dataset XMLs)
* `json` - JSON to dictionary converter (used for reading EEEI dataset JSONs)
* `cv2` - OpenCV wrapper for Python
* `numpy` - Numerical library for Python with Matlab-like syntax
* `matplotlib` - For plotting marked parking lots and parking spaces, specifically `pyplot`.

## Organization

The source code is organized in a flat structure, with each Python script containg scripts for a specific algorithm. The scripts are the following:

* `img_main` - Main script used for occupancy detection
* `img_math` - Math functions used throughout the source code
* `img_plot` - Functions for plotting marked parking lots and parking spaces
* `img_rectify` - Functions for metric rectification and z-axis estimation
* `img_slice` - Functions for slicing parking spaces from an image of a parking lot

The source codes are fairly documented using docstrings. Sphinx may be used to autogenerate such documentation.

## Parking Lot Representation

Throughout the project, a parking lot image has a corresponding JSON file containing the coordinates for a parking space and optionally its calibration marks. The general layout of the JSON file is shown below.

```json
{
    "parking_id": "eeei_5th_solar_041017_a_morning_parkinglinebased", // Name/ID of parking lot
    "calibration": { // Calibraton marks
        "vert_pts": [ // Vertical calibration line (for perspective transform)
            [1135, 676],
            [1119, 547],
            [1100, 391]
        ],
        "horiz_pts": [ // Horizontal calibration line (for perspective transform)
            [384, 511],
            [549, 531],
            [727, 557]
        ],
        "line_vec": [ // Line pairs making an angle of 90 (for affine transform)
            [[142, 602], [120, 568]], // First two pairs
            [[120, 568], [227, 506]],
            [[632, 286], [671, 289]], // Last two pairs
            [[671, 289], [653, 340]]
        ]
    },
    "spaces": [{ // Parking spaces
            "id": 1, // Parking space ID
            "pts": [{ // Coordinates of four corners of each parking space (sorted CW or CCW)
                    "x": 139,
                    "y": 322
                },
                {
                    "x": 229,
                    "y": 327
                },
                {
                    "x": 123,
                    "y": 464
                },
                {
                    "x": 20,
                    "y": 457
                }
            ]
        },
        ...
```

## Running

The `img_main` module contains the encapsulated function `get_occupancy_list(img, spaces_dict, [use_calibration=False])` used to get an occupancy status for each parking space provided. It accepts an image of a parking lot `img`, dictionary of spaces from the JSON `spaces_dict` and an optional flag to whether use metric rectification `use_calibration`. This function uses an initial background reference image `parakeet_bs_ref.jpg`, which is assumed to be in the same location as the script. If it is not available, the script will generate one. A sample code is shown below.

```python
img_path = '..' # Path to parking lot image
json_path = '..' # Path to corresponding JSON file

with open(json_path) as data_file:
    test_img = cv2.imread(img_path, 1) # Load colored parking lot image
    spaces_dict = json.load(data_file) # Load json file containing parking lot spaces
    
    occ_list = img_main.get_occupancy_list(test_img, spaces_dict, use_calibration=False)
```

## Testing

Each script contains a small routine within `main()` to test its function, which can be run using either the EEEI datasets or the PKLot datasets. These routines can be edited with additional test functionality.

Accuracy and execution time tests were performed using the same scripts as that of the machine learning. Since the scripts are dependent on the tests sets for machine learning, these can instead be found at the `/experiments_ML/testing_UFPR04/src/_eval_scripts_w_det_w_scores_BS` folder. On the other hand, tabulated data recorded are also saved in `imgproc_data.ods` and `imgproc_data.mat` for further scrutiny.
