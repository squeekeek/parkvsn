#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np

role = "controller"
inp_role = '0'

nrf.config()
nrf.controller_config()

'''
to do: insert found nodes in database
'''
nrf.findNodes()