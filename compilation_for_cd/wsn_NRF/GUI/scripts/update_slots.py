#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf

import shutil
import os

def moveFile(src, dest):
	try:
		shutil.move(src, dest)
	# eg. src and dest are the same file
	except shutil.Error as e:
		print('Error: %s' % e)
	# eg. source or destination doesn't exist
	except IOError as e:
		print('Error: %s' % e.strerror)

source1 = "/home/pi/Downloads/test1.json"
source2 = "/home/pi/Downloads/test2.json"
destination1 = "/home/pi/parkvsn-rpi/GUI/scripts/test1.json"
destination2 = "/home/pi/parkvsn-rpi/GUI/scripts/test2.json"

role = "controller"
inp_role = '0'


nrf.config()
nrf.controller_config()

moveFile(source1,destination1)
moveFile(source2,destination2)

os.chdir('/home/pi/parkvsn-rpi/GUI/scripts')

nrf.config_to_node1()
file  = open("test1.json", "r")
data_to_send1 = file.read()
file  = open("test2.json", "r")
data_to_send2 = file.read()

#node1
nrf.sendPacket("RECEIVE PARKING COOR 1")

ack_ping = nrf.recvPacket(1)
#print('Ack : {}'.format(ack_ping))
if(ack_ping == "NODE 1"):
	#print('*************************************')
	#print('Now accepting data from Node 1')
	nrf.sendTCP(data_to_send1)

print('Success')
'''
#node2
nrf.config_to_node2()

nrf.sendPacket("RECEIVE PARKING COOR 2")
ack_ping = nrf.recvPacket(2)
if(ack_ping == "NODE 2"):
	#print('*************************************')
	#print('Now accepting data from Node 2')
	nrf.sendTCP(data_to_send2)
'''
