#!/usr/bin/env python

'''
ATTN AGATHA: 
    line 34: sendOccupancyArray -> insert ML
    line 72: updateParkingCoordinates -> only prints the received json string
'''

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np
import picamera
from picamera.array import PiRGBArray
from base64 import b64decode, b64encode
import zlib
from PIL import Image

from _prototype_v3_wo_cam.sendOccupancyLists import sendOccupancyLists
import os
import io

role = "node"
inp_role = '1'

def waitCommand():
    while True:
        received = nrf.recvPacket(1)
        print('Received command: {}'.format(received))
        return received

def measureMetrics():
    nrf.sendPacket("NODE "+inp_role)

    counter = 0
    #PING
    while(counter < 10):
        received = nrf.recvPacket(1)
        nrf.sendPacket("PING")
        print("Ping done.")
        counter += 1
'''
    #THROUGHPUT
    with picamera.PiCamera(resolution='HD') as cam:
        time.sleep(2)
        time_begin = time.time()
        cam.capture("test_image.jpg")
        time_capture = time.time() - time_begin

    foo = Image.open('test_image.jpg')
    foo.save("compressed_opt.jpg",optimize=True,quality=55)

    with open("compressed_opt.jpg", "rb") as f:
        data = f.read()
        encoded_data = data.encode("base64")
    
    received = nrf.recvPacket(1)

    nrf.sendPic(encoded_data)
    print('Time to capture image: {} seconds'.format(time_capture))
'''
def sendOccupancyArray():
    nrf.sendPacket("NODE "+inp_role)

    # Load spaces dict

    spaces_path = "markings.json"
    with open(spaces_path) as data_file:
        spaces_dict = json.load(data_file)

    # Load hog setting
    hog_dict_path = os.path.join("_prototype_v3_wo_cam","config", "hog.json")
    with open(hog_dict_path) as data_file:
        hog_dict = json.load(data_file)

    hog_setting = None
    hog_option = "blk-2_cell-6"
    for option in hog_dict["hog"]:
        if hog_option == option["id"]:
            hog_setting = option
            break

    # Load detection dict
    detection_dict_path = os.path.join("_prototype_v3_wo_cam","config", "detection.json")
    with open(detection_dict_path) as data_file:
        detection_dict = json.load(data_file)

    # Load model folder and dict
    hog_models_folder = os.path.join("_prototype_v3_wo_cam","config", "models_hog")
    models_dict_path = os.path.join(hog_models_folder, "models.json")
    with open(models_dict_path) as data_file:
        hog_models_dict = json.load(data_file)



    # Load image (replace with cam version)
    #sample_image_path = os.path.join("_prototype_v3_wo_cam","sample_images", "*")
    with picamera.PiCamera(resolution='HD') as cam:
        time.sleep(2)
        cam.capture("current_capture.jpg")
    im_file = "current_capture.jpg"
    curr_directory = os.path.abspath(".")
    im_path = os.path.join(curr_directory, im_file)
    #for im_path in glob.glob(sample_image_path):
    occupancyList_BGS, occupancyList_HOG, occupancyList_final, time_evaluated = sendOccupancyLists(
        im_path,
        spaces_dict,
        hog_setting,
        detection_dict,
        hog_models_folder,
        hog_models_dict
    )


    print("Time evaluated: {}".format(time_evaluated))

    #occupancy_array = [1,0,1,0,0,0,1,0,1,0]
    #HOG_array = [0,0,1,0,1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,1,1,1]
    #BT_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1]
    occupancy_array = occupancyList_final
    data_to_send = str(occupancy_array)
    if(not nrf.sendTCP(data_to_send)):
        mode = nrf.chooseMode()
        runMode(mode)

def initSlots():
    nrf.sendPacket("NODE "+inp_role)

    with picamera.PiCamera(resolution='HD') as cam:
        time.sleep(2)
        cam.capture("test_image.jpg")

    foo = Image.open('test_image.jpg')
    foo.save("compressed_opt.jpg",optimize=True,quality=55)

    with open("compressed_opt.jpg", "rb") as f:
        data = f.read()
        encoded_data = data.encode("base64")

    print('{}'.format(encoded_data))
    nrf.sendPic(encoded_data)

def updateParkingCoordinates():
    nrf.sendPacket("NODE "+inp_role)

    received = nrf.recvTCP(1)
    print('Coordinates: {}'.format(received))
    # insert code to update parking coordinates here
    # store in json file
    parking_dict = json.loads(received)

    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    filename = "markings.json"
    with io.open(filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        parking_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

if __name__ == "__main__":
    nrf.config()
    nrf.node1_config()
    nrf.waitStartNormal()

    while True:
        cmd = waitCommand()
        if(cmd == ('SEND OCCUPANCY ARRAY '+inp_role)):
            print('Waiting for Occupancy Array')
            sendOccupancyArray()
            cmd = 'WAITING'
        if(cmd == ('INITIALIZE SLOTS '+inp_role)):
            print('Waiting for Parking Lot picture')
            initSlots()
            cmd = 'WAITING'
        if(cmd == ('RECEIVE PARKING COOR '+inp_role)):
            print('Receiving Parking Coordinates')
            updateParkingCoordinates()
            cmd = 'WAITING'
        if(cmd == ('SEND METRICS '+inp_role)):
            print('Measuring Metrics')
            measureMetrics()
            cmd = 'WAITING'