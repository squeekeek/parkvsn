from __future__ import division
import os
import cv2
import numpy as np
import xmltodict as xmld
import matplotlib
import math
from matplotlib import pyplot as plt

import img_rectify


def slice_one_img(img, rectpts):
    # Get each parking space by masking
    smask = np.zeros(img.shape, dtype=np.uint8)
    nchannels = img.shape[2] if (len(img.shape) == 3) else 1
    smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
    pkspace = cv2.bitwise_and(smask, img)

    # Crop rectangles to masked size
    mincoords = map(min, zip(*rectpts))
    maxcoords = map(max, zip(*rectpts))
    normpts = map(tuple, np.subtract(rectpts, mincoords))

    # Then rotate and perspective transform
    #new_img = trans_space(pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]], normpts)

    return new_img


def slice_img(img, xmlboxes):
    print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']

    pspaces = []
    pspacepts = []
    for eachspace in xmlboxes['parking']['space']:
        rectpts = sortfourcw([(int(a['@x']), int(a['@y'])) for a in eachspace['contour']['point']])

        # Slice single rectangle from image
        new_img = slice_one_img(img, rectpts)
        pspaces.append(new_img)
        pspacepts.append((int(eachspace['@occupied']) if '@occupied' in eachspace else -1, rectpts.tolist()))

    print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return (pspaces, pspacepts)


def plot_spaces(pspaces):
    # Plot each parking space
    plt.figure()
    plt.title('Segmented Parking Spaces')
    sqsize = np.ceil(np.sqrt(len(pspaces)))

    for k, eachimg in enumerate(pspaces):
        plt.subplot(sqsize, sqsize, k + 1)
        plt.title(str(k + 1), size=10, color='b', y=-0.01, weight='bold')
        plt.xticks([]), plt.yticks([])

        if len(eachimg.shape) == 3 and eachimg.shape[2] == 3:
            plt.imshow(eachimg[:, :, ::-1])
        else:
            plt.imshow(eachimg, 'gray')

    plt.subplots_adjust(wspace=0.05, hspace=0.05)


def plot_img_with_segments(img, ppts):
    # Show original image
    # Show with boxes
    plt.figure()
    plt.title('Original with ROI', fontsize=10)

    try:
        if len(img[0][0]) == 3:
            plt.imshow(img[:, :, ::-1])
        elif len(img[0][0]) < 3:
            plt.imshow(img, 'gray')
    except:
        plt.imshow(img, 'gray')

    plt.xticks([]), plt.yticks([])

    for k, (isfull, plotpts) in enumerate(ppts):
        plt.text(plotpts[0][0], plotpts[0][1], str(k + 1), size=10, color='b', weight='bold')

        for p in zip(plotpts + [plotpts[0]], \
                plotpts[1:] + [plotpts[0]]):
            if isfull == -1:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'b-', linewidth=1)
            elif isfull == 0:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'g-', linewidth=1)
            else:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'r-', linewidth=1)


def write_seg_img(dirname, basename, pspaces):
    for k, eachimg in enumerate(pspaces):
        cv2.imwrite("{}/{}_seg{}.jpg".format(dirname, basename, k + 1), eachimg)


def main():
    #filename = '../datasets/EEEI/eeei_samples/crop_830_1'
    #filename = '../datasets/EEEI/eeei_samples/crop_2pm3'
    #filename = '../datasets/PKLot/PUCPR/Cloudy/2012-09-12/2012-09-12_07_23_35'
    #filename = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/2013-01-16_09_25_05'
    #filename = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/2013-01-16_09_25_05'
    #filename = '../datasets/PKLot/PUCPR/Sunny/2012-09-11/2012-09-11_15_55_21'
    #filename = '../datasets/PKLot/UFPR05/Rainy/2013-03-19/2013-03-19_12_50_07'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170313-1346'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170311-1936'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170311-1626'
    #filename = '../datasets/EEEI/eeei_cnl/20170318-0558'
    filename = '../datasets/EEEI/eeei_cnl/20170318-1638'

    imgname = os.path.split(filename)[-1]

    with open(filename + '.xml') as fd:
        doc = xmld.parse(fd.read())

        print 'Reading filename ' + filename
        img = cv2.imread(filename + '.jpg', 1)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)
        #img_thresh = cv2.morphologyEx(img_thresh, cv2.MORPH_ERODE, np.ones([3, 3]))
        img_thresh2 = cv2.filter2D(img_thresh, -1, np.array([[0, 1/5, 0], [1/5, 1/5, 1/5], [0, 1/5, 0]]))

        #img_edge = cv2.blur(img_gray, (3, 3))
        img_edge = cv2.Canny(img, 50, 200)

        pspaces, ppts = slice_img(img, doc)
        #pspaces_gray, ppts_gray = slice_img(img_gray, doc)
        #pthresh, _ = slice_img(img_thresh, doc)

        # Show original image
        # Show with boxes
        ppts2 = map(cv2.boundingRect, np.array(map(lambda x: x[1], ppts)))

        plot_img_with_segments(img, ppts)
        #plot_img_with_segments(img, zip(map(lambda t: ppts[0], ppts), ppts2))

        # Plot each parking space
        plot_spaces(pspaces)

        '''
        d = []
        for eachpt, eachpt2 in zip(map(lambda t: t[1], ppts), ppts2):
            print "Processing one parking space"

            mask = np.zeros(img.shape[:2], np.uint8)
            cv2.fillConvexPoly(mask, np.array(eachpt), 1)
            mask_guide = cv2.GC_FGD * cv2.bitwise_and(mask, np.uint8(img_edge / np.amax(img_edge)))
            mask = cv2.bitwise_or(cv2.GC_PR_FGD * mask, mask_guide)

            bgdModel = np.zeros((1, 65), np.float64)
            fgdModel = np.zeros((1, 65), np.float64)

            cv2.grabCut(img, mask, eachpt2, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT | cv2.GC_INIT_WITH_MASK)
            cut_mask = np.where((mask==2) | (mask==0), 0, 1).astype('uint8')
            newimg = img * cut_mask[:, :, np.newaxis]
            newimg = slice_one_img(newimg, eachpt)
            _, newimg = cv2.threshold(cv2.cvtColor(newimg, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

            d.append(newimg)

            #plt.figure()
            #plt.imshow(newimg[:,:,::-1])

        plot_spaces(d)
        '''


        #linepair_to_circle([line_c, line_d])
        #linepair_to_circle([line_d, line_a])

        #plt.gca()

        #cv2.imwrite("{}_gray.jpg".format(imgname), img_gray)
        #cv2.imwrite("{}_edge.jpg".format(imgname), img_edge)
        #cv2.imwrite("{}_thresh.jpg".format(imgname), img_thresh)
        #cv2.imwrite("{}_thresh2.jpg".format(imgname), img_thresh2)

    '''
    #dataset_path = '../datasets/PKLot/PUCPR/Cloudy/2012-09-12/'
    #dataset_path = '../datasets/EEEI/eeei_5th_solar/'
    dataset_path = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/'

    fnames = [f for f in os.listdir(dataset_path) if f.endswith('.jpg')]
    fgmasks = []
    threshs = []
    bgsub = cv2.createBackgroundSubtractorMOG2()

    for k, eachfile in enumerate(fnames):
        if k < 20:
            continue

        if k >= 30:
            break

        imgname = os.path.splitext(eachfile)[0]
        filename = dataset_path + imgname

        with open(filename + '.xml') as fd:
            doc = xmld.parse(fd.read())
            img = cv2.imread(filename + '.jpg', 1)
            img_thresh = cv2.adaptiveThreshold(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)

            fgmask = bgsub.apply(img_thresh)
            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, np.ones([5, 5]))
            pspaces, ppts = slice_img(fgmask, doc)

            threshs.append(img_thresh)
            fgmasks.append(fgmask)

            #plot_img_with_segments(img, ppts)
            cv2.imwrite("{}_{}_thresh.jpg".format(imgname, k), img_thresh)
            cv2.imwrite("{}_{}_fgmask.jpg".format(imgname, k), fgmask)

            #plot_spaces(pspaces)
    '''

    # Show all plots
    plt.show()

if __name__ == "__main__":
    main()
