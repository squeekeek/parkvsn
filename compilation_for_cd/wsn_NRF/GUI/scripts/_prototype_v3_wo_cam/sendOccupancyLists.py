import os
from lib.improc_lib import slice_img_json
#from pred_BGS.pred_BGS import sendBGSOccupancyList
from pred_HOG.pred_HOG import sendHOGOccupancyList
import glob
import json
import cv2
import time


def sendOccupancyLists(im_path, spaces_dict, hog_setting, detection_dict,hog_models_folder, hog_models_dict):

    start_time = time.time()

    # Load image (replace with cam version)
    img_parkinglot = cv2.imread(im_path)
    pspaces, ppts = slice_img_json(img_parkinglot, spaces_dict)
    occupancyList_BGS = []

    #occupancyList_BGS = sendBGSOccupancyList(img_parkinglot, spaces_dict)
    occupancyList_HOG = sendHOGOccupancyList(
        pspaces, 
        detection_dict,
        hog_setting,
        hog_models_folder, 
        hog_models_dict
    )
    occupancyList_final = occupancyList_HOG
    #occupancyList_final = []
    """
    for i, status in enumerate(occupancyList_BGS):
        if status == 0:
            occupancyList_final.append(0)
        else:
            if occupancyList_HOG[i] == 1:
                occupancyList_final.append(1)
            else:
                occupancyList_final.append(2)
    """
    end_time = time.time()
    time_evaluated = end_time - start_time

    return (occupancyList_BGS, occupancyList_HOG, occupancyList_final, time_evaluated)

if __name__ == "__main__":


    # Load spaces dict
    spaces_path = os.path.join("config", "markings.json")
    with open(spaces_path) as data_file:
        spaces_dict = json.load(data_file)


    # Load hog setting
    hog_dict_path = os.path.join("config", "hog.json")
    with open(hog_dict_path) as data_file:
        hog_dict = json.load(data_file)

    hog_setting = None
    hog_option = "blk-2_cell-6"
    for option in hog_dict["hog"]:
        if hog_option == option["id"]:
            hog_setting = option
            break

    # Load detection dict
    detection_dict_path = os.path.join("config", "detection.json")
    with open(detection_dict_path) as data_file:
        detection_dict = json.load(data_file)

    # Load model folder and dict
    hog_models_folder = os.path.join("config", "models_hog")
    models_dict_path = os.path.join(hog_models_folder, "models.json")
    with open(models_dict_path) as data_file:
        hog_models_dict = json.load(data_file)



    # Load image (replace with cam version)
    sample_image_path = os.path.join("sample_images", "*")
    for im_path in glob.glob(sample_image_path):

        occupancyList_BGS, occupancyList_HOG, occupancyList_final, time_evaluated = sendOccupancyLists(
            im_path,
            spaces_dict,
            hog_setting,
            detection_dict,
            hog_models_folder,
            hog_models_dict
        )
    print occupancyList_BGS
    print occupancyList_HOG
    print occupancyList_final
    print time_evaluated