"""
    Input: pspaces list
    Output: occupancyList
"""
import os
import sys
import json
from lib.p1_hog_svm import hog_svm


def sendHOGOccupancyList(pspaces, detection_dict, hog_setting, models_folder, models_dict):

    occupancyList = []
    for i, eachspace in enumerate(pspaces):
        highest_value = hog_svm(eachspace, models_dict, models_folder, hog_setting, detection_dict)
        if highest_value > 0:
            occupancyList.append(1)
        else:
            occupancyList.append(0)
    return occupancyList

if __name__ == "__main__":
    print ("in main")
    # Load image of parking lot

    # Crops images to parking spaces

    # Calls classifier for each space

    # Returns array of occupancy for each id