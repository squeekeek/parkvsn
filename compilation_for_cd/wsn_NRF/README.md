#Description
This folder contains the codes to be used to set-up the network. Please see the video demonstration of the working implementation at EEEI at the 'live implementation' folder for a more visual instruction.

#Requirements
All Raspberry Pis must be installed with the Jessie OS then subsequently with Python, OpenCV, NodeJS, and Express. The libraries were used with TMRH20's python wrapper (tmrh20.github.com)
For easier deployment, use with Forever package.

#Set-up
NODE:
The Raspberry Pi nodes must be set-up first. Include the 'from lib' folder and under the GUI/scripts folder, you will need the pynrf24.py and a node script. Choose either node1.py or node2.py and run it with
    
$sudo python node1.py

    for example. Running as root is important. This can be extended to up to 5 nodes by simply changing the inp_role variable.

CONTROLLER:
The Raspberry Pi central controller must include both GUI and 'from lib' folders. You should install your favorite raspbery pi desktop mirror viewer. For the easiest installation, you can use tightvncviewer and run that on the controller. 

$tightvncserver

    Then from your computer's terminal, access the raspberry pi desktop by running

$open vnc://pi@ip_address:5901

    5901 is the default port. Then under GUI, run server.js

$forever start server.js

    From the desktop mirror, open up a web browser and go to localhost:3000 or the corresponding port. 
    The admin interface should appear. The following steps are needed for initial configuration. Simply click the corresponding button.

    -Initialize System
        An alert window should pop up confirming the detection of the nodes
    -Select Parking Spaces
        This needs to be done for each node. The controller will request for the node's current camera view and the GUI will enable you to select which parking spaces are to be observed. Please see the live demo video for more information
    -Update Parking Spaces
        When all the parking spaces from all the nodes are selected, use this command to send the updated parking slot coordinates to the corresponding nodes.
    -Start
    You can now begin normal operation by clicking this button. A summary of the occupancy information will be seen on the interface and if the controller is currently connected to a WiFi, this information can also be accessed through the web app: http://parakeet.sandnox.com

Other commands include:
    -Measure Network Metrics
        Simply outputs the ping, throughput, and time to process image. 
    -Time Interval
        The time between requesting for occupancy data can be changed through this drop-down button.

If the GUI is not working, you can also use the network by executing the scripts individually. ie.

$sudo python init_controller.py
$sudo python update_slots1.py
$sudo python send_occupancy_array.py
