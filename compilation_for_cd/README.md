# An Implementation for an Open-space Parking Lot System
## Last modified: May 30, 2017
## Authors
- Carl Dizon
- Liezl Magpayo
- Agatha Uy

## Folders and Files
The six folders and files included here are the following:

### 1. experiments_BS
#### Description
- This contains the scripts written and used for doing the background subtraction part of the project.
- It also contains the results for evaluating background subtraction

### 2. experiments_ML
#### Description
- This contains the scripts written and used for doing the machine learning part of the project.
- It also contains the results for evaluating the machine learning algorithms done.
- Zipped because file names get too long for Windows

### 3. wsn_NRF
#### Description
- This contains the scripts written for implementing the WSN part of the project using NRF24L01.
- It also contains the results for evaluating the WSN network created.

### 4. live_implementation
#### Description
- This contains the actual implementation that was run live.

### 5. docs
#### Description
- This contains relevant documentation files for the project, including the final paper, approval sheets, and overview video.

### 4. image_processing
#### Description
- This contains the scripts written for implementing the image processing part of the project
- It also contains the test results

## Notes
- Upon encountering folder paths/file paths in the code, please change them to the appropriate paths for your system.
