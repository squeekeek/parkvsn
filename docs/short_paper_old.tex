\documentclass[9pt, a4paper, oneside, journal]{IEEEtran}

\usepackage[pdftex]{graphicx}
\usepackage{notoccite}
\usepackage{stackengine}
\usepackage{multirow}
\usepackage{caption}   % to increase space between table and caption
\usepackage{subcaption}
\usepackage[final]{pdfpages}
\usepackage{amsmath}
\usepackage{cite}
\usepackage{tabulary}
\usepackage{tabularx}
\usepackage{float}

\usepackage[nokeyprefix]{refstyle}
\graphicspath{{images/}}
\DeclareGraphicsExtensions{.png, .jpg}



\newcommand\pesos{%
	\stackengine{-1.38ex}{P}{\stackengine{-1.2ex}{$-$}{$-$}{O}{c}{F}{F}{S}}{O}{c}{F}{T}{S}}


\begin{document}
	\title{An Implementation of an Open-space Visual Smart Parking System}
	\author{Carl C. Dizon, Liezl C. Magpayo, and Agatha C. Uy
		\thanks{C. Dizon, L. Magpayo, and A. Uy are with the Ubiquitous Computing Laboratory, Electrical and Electronics Engineering Institute, University of the Philippines, Diliman, Quezon City, Philippines. The corresponding author's email address is agatha.uy@eee.upd.edu.ph.
		}
	}
	
	\maketitle
	
	\begin{abstract}
		Parking has become an evident road traffic problem arising from the increase in the use of cars. To solve the continuous waste of time and energy in search of parking lots, we are presenting an implementation of a smart parking monitoring system for open-space parking lots. The system comprises of wireless sensor nodes equipped with cameras that monitor the status of each parking space in a parking lot. Each node is capable of running machine learning and image processing algorithms to check for occupancies. Finally, the results are sent to a central controller connected to the internet, which will then be disseminated through a GUI made for both web and mobile platforms.
	\end{abstract}
	
	\section{Introduction}
		With the increase use of cars in today`s time, new problems related to road traffic are appearing. One of the evident problems is the lack of parking spaces. According to Shoup \cite{shoup_2006}, 30\% of cars on average are cruising to find an open parking space. This percentage will greatly contribute to an already-congested and cramped roads of the country. Hence, there is a need for a system that makes finding available parking spaces easy and fast.
		
		The recent popularity of the Internet of Things (IoT) gave way to several solutions that address this parking problem in the form of smart parking systems. A smart parking system is used to automate the process of determining and counting parking spaces in a lot. This information is given to drivers who would want to park there. There have been numerous implementation of these kinds of systems, whether standalone or integrated in a smart transportation system. For example, SFPark \cite{sfpark} is a system developed in 2014 to improve parking in San Francisco, CA, USA. The system is still running as of this time of writing.
		
		Recent developments in this field include the use of machine learning  and cameras as a replacement for induction and light sensors. A camera is able to monitor a large number of parking spaces depending on its position and viewing angle. These can then be connected over a network to monitor multiple large parking lots. However, images itself expose a plethora of problems such as lighting variances, occlusions, and camera angle views, which researchers solve using either machine learning or traditional image processing. Additionally, networks such as these use a lot of energy because of handling of multimedia data, which can easily drain the small energy storage of IoT devices. These are the problems in monitoring parking lots with this technology that researchers and developers are trying to give solutions.
		
	\section{Related Work}
		The concept of ``smart'' parking has since been realized in 1982 when the city of Nara in Japan deployed a parking lot guidance system for four parking lots around the city \cite{sakai_etal_1995}. This concept has also been realized in several places such as Europe, the United States, and Japan. Today, sensor-based parking detection systems are widely implemented commercially such as in malls, airports, and offices.
		
		Nonvisual-based smart parking systems involve the use of environmental sensors, such as infrared, ultrasound, and inductive coils to determine the presence of a car in a parking space. Although non-intrusive sensors, such as magnetic and light, are effective sensors to determine a slot's occupancy, they are vulnerable to harsh weather conditions and lighting levels, and are difficult to scale. Hence, a natural choice for a parking system is a visual-based one \cite{Baroffio2016}. These systems are easier to scale, cheap, and can be integrated to existing CCTV systems. However, the problem has been shifted to image processing.
		
		Numerous parking space detection algorithms have been explored over the years in the hopes of increasing detection accuracy and speed. Image processing has been used to simply detect occupancy of parking spaces by detecting a fixed mark being covered by a car, for example \cite{yusnita_etal_2012}. Machine learning has also been a recent trend that it has also been incorporated into such algorithms. Bayesian networks \cite{huang_wang_2010} and linear support vector machine classifiers (SVM) \cite{sidla_lipetski_2014} have been used to reach accurate detection rates of up to 99.96\%.
		
		In addition to these algorithms, a network is required to be able to maximize the monitoring of several large parking spaces. Recently, visual sensor networks (VSNs) are used to monitor several areas of a certain location. They are ideally cheap, scalable, and flexible \cite{seema_reisslein_11}. With the use of VSNs arise the choice of compress-then-analyze (CTA) or analyze-then-compress (ATC) paradigms. It has been concludeed by Baroffio et al \cite{Baroffio2016} that ATC would be a suitable choice for such a parking lot monitoring system in terms of bandwidth usage, accuracy, and energy consumption.
		
		With the increasing number of researchers implementing smart parking systems as a solution to Shoup`s \cite{shoup_2006} surprising results, the country has yet to implement a smart parking system that can be used by people in their everyday lives. Hence, we are presenting an implementation of such a system incorporating all the features mentioned above.

	
	\section{System Implementation}
		Our proposed system is composed of a network of wireless camera sensor nodes running a parking lot occupancy algorithm, and an online GUI for monitoring and control of the system. Figure \ref{fig:sys_flow} shows a block diagram of the normal operation of the system. For this research, the system was deployed on the student parking lot of the institute.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.8\linewidth]{sysflow}
			\caption{Operation flow of the system during normal use}
			\label{fig:sys_flow}
		\end{figure}

		
		\subsection{System Overview}
			The system comprises of identical sensor nodes equipped with cameras and connected via a wireless mesh network. A Raspberry Pi (RPi) 3 single-board computer was used as a node to monitor the parking lot. It was also equipped with an RPi Camera Module v2 to take pictures of the parking lot, and an nRF24L01 receiver to be able to communicate among nodes within the network. The nodes were positioned on the fourth and fifth floors of the institute`s building to enable a clear view of the parking lot. The nodes take pictures of the parking lot at fixed periods and independently analyze each one for occupancy status using a parking lot occupancy algorithm.
			
			The system also has a central controller that aggregates all the occupancy data sent by the nodes and sends the aggregated data to an online server in the internet. It is an RPi 3 computer equipped with both an nRF24L01 and WiFi. The online server, on the other hand, sends data to the web and mobile interfaces and shows the status of the parking spaces in the lot.
			
			The parking lot occupancy algorithm was implemented in Python 2.7 and OpenCV on either an Anaconda 3 stack or the native Linux stack. The web and mobile front-ends were developed with a mobile-first paradigm in mind. The server for these front-ends runs on Node.js using several libraries that are responsible for the display and communication between the central controller and the front-end.
			
			Aside from being able to monitor parking space occupancy, the nodes also accept commands from any administrator, such as forcing to send an image, or measure network metrics. The commands are received via the central controller, which are then passed on to the recipient nodes.
			
		\subsection{Initial Calibration}
			An initial calibration is required for first-time use of the system. It consists of downloading an image of the parking lot, which will serve as a reference image. The user is required to manually mark each parking space as a four-sided convex polygon. The image should also contain a calibration mark that is used to generate a corresponding parallelepiped in each parking space, which the user should also manually draw. The calibration is largely based on the rectification performed by Liebowitz and Zisserman \cite{liebowitz_zisserman_1998} with a vanishing point estimation performed by Saini et al\cite{saini_etal_2013}. The height of a parallelepiped is about half the longest length of its corresponding marked parking space. The generated parallelepipeds, together with their convex hulls, are stored in a JSON file for use during normal opertion.
			
			
	\subsection{Parking Lot Occupancy Algorithm}
	A series of tests were done for working towards the parking lot occupancy algorithm that would be used for the system. These tests could be seen at Section \ref{section:dev_occ}. The target method was to have an integration of background-based detection algorithms, and vehicle-based detection algorithms, for determining three parking lot states of occupancy: empty, occupied, or obstruction. The over-all detector should also work images that it wasn't trained on.
	
	For background-based detection, the chosen method to explore was background subtraction with binary thresholding. For vehicle-based detection, Histogram of Oriented Gradients (HoG) and Local Binary Patterns (LBP) as feature descriptors for linear support vector machines (SVMs) were explored both individually, and integrated with each other. The aim was to have the prediction of the background-based detection predict whether a parking slot is occupied or not, and for the vehicle-based detection to determine whether the occupancy is caused by a vehicle or an obstruction. 


	\subsection{Wireless Sensor Network}
	Occupancy data independently processed by each node are sent via a wireless sensor network. The network was designed such that it is practical as a wireless video sensor network (WVSN) as outlined by Seema and Reisslein \cite{seema_reisslein_11} \textemdash~one that is cheap and consumes the least amount of energy. 
	
	\subsubsection{Network Type and Topology}
	A star topology was observed when the network was set-up because of its modularity, simplicity, and resilience to line failures. An analyze-then-compress (ATC) paradigm was also used, as suggested by Baroffio et al \cite{Baroffio2016}, to bring down the amount of data sent throughout the network to a few bytes during normal use. This is also in response to the lower data rate of the nRF24L01 module.
	
	\subsubsection{Node Commands}
	A feature of this system is the ability of the nodes to receive and process commands sent from the central controller. These commands are used to force initial configuration of the system, control the normal operation, or measure network metrics.

	\subsection{GUI}
		The admin panel can be used to change the configuration of the network and is accessible only through the central controller, but the central controller also uploads the occupancy information such that the web app is able to display it. Both interfaces were coded with NodeJS and Express.

		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.7\linewidth]{admin_interface}
			\caption[Admin interface]{Admin interface with command buttons}
			\label{fig:admininterface}
		\end{figure}
	
		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.3\linewidth]{web_mobile}
			\caption[Mobile]{Mobile app}
			\label{fig:web_mobile}
		\end{figure}
		
	\section{Development of the Parking Lot Occupancy Algorithm} \label{section:dev_occ}


\subsection{Datasets}
\subsubsection{UP EEEI Dataset}

For testing the system, a set of datasets, which we call the UP EEEI dataset, was compiled by taking images of all the parking lots around EEEI. This can be seen at Table \ref{table:dataset_eeei}.
\begin{table}[H]
	\centering
					\begin{tabulary}{\linewidth}{|l|l|L|l|l|l|}
		\hline
		\textbf{\#} & \textbf{Code} & \textbf{Description} & \textbf{Date} & \textbf{Empty} & \textbf{Occupied} \\
		\hline
		1 & 4THFLR & 4th flr. new bldg. & 05/12/2017 & 224 & 3304 \\
		2 & SOLAR 1 & 5th flr. new bldg. (Set A) & 04/02/2017 & 628 & 308 \\
		3 & SOLAR 2 & 5th flr. new bldg. (Set B) & 04/02/2017 & 895 & 140 \\
		4 & SOLAR 3 & 5th flr. new bldg. (Set A) & 04/05/2017 & 496 & 0 \\
		5 & SOLAR 4 & 5th flr. new bldg. (Set B) & 04/05/2017 & 3544 & 0 \\
		6 & SOLAR 5 & 5th flr. new bldg. (Set C) & 04/05/2017 & 1328 & 0 \\
		7 & SOLAR 6 & 5th flr. new bldg. & 04/10/2017 & 257 & 1471 \\
		8 & CNL & 2nd flr. old bldg. & 03/17/2017 & 278 & 14 \\
		9 & BRIDGE & 4th flr. bridge & 05/04/2017 & 257 & 1471 \\
		\hline               
	\end{tabulary}
	\caption{Summary of EEEI Datasets}
	\label{table:dataset_eeei}
\end{table}

\subsubsection{PKLot Dataset}
The PKLot dataset \cite{pklot} was used for training the machine learning algorithms evaluated in this study. It was chosen since the training set required by the detector should have samples for different weather conditions, views, vehicles, and parking lot backgrounds. It has three different views of parking lots from the 4th, 5th, and 10th floor angles. For the different methods tested in this research, the UFPR04 category (a 4th floor view) of the PKLot dataset was used as training data for the machine learning algorithms. The category was chosen since it was seen as most similar to the ``4THFLR'' and ``SOLAR'' datasets from our UP EEEI dataset, as well as with our target area of deployment, which is of the same view as the ``4THFLR'' dataset.

An example image from the UFPR04 dataset, as well as from the ``4THFLR'' dataset can be seen at Figure \ref{fig:datasets_4thflr}.

\begin{figure}[h!]
	\centering
	
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{images/UFPR04_ex_image}
		\caption[UFPR04 view of the PKLot Dataset]{UFPR04 dataset view}
		
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{4THFLR_view}
		\caption{``4THFLR'' dataset view}							
	\end{subfigure}
	
	\caption{Comparison of the 4th floor datasets}
	\label{fig:datasets_4thflr}
\end{figure}



\subsection{Background Subtraction}
The algorithm used was a fast background subtraction algorithm based on Nunes et al \cite{nunes_etal_2011}. It uses an adaptive median filter to get a running median of the few image frames fed into the algorithm. This running median serves as the background reference image, and is updated every time a new frame is fed. At this point, a binary image representing the foreground is returned, with the white pixels depicting such. Connected components are found within the binary image to get the area of each blob. Finally, each parking space is compared to blobs whose centroids are inside it to determine occupancy. The space is deemed occupied if the ratio of the area of the blob and the parking space is larger than a certain set threshold.


\subsection{PKLot Dataset Preprocessing as Training Set}
The first problem encountered with the analysis of the dataset was that the segmented parking spaces had different image sizes, and also different aspect ratios. This was something that needed to be given solution to since both HoG and LBP are scale-invariant and need fixed window sizes.  The solution was to make categorizations per view, and such is aptly called ``per view''. Another categorization was made upon observation that each view could be categorized into similar window sizes, where cropping from the upper left-corner could still cover most of a vehicle in the image. The categorizations done could be seen at Table \ref{table:UFPR04_views} and Figure \ref{fig:ufpr04_categories}. It is to be seen as well whether having a view-based model system would allow the system to have view-based classification, which would then help in estimating a 3D perspective for handling possible occlusions. 

\begin{table}[H]
	\centering
	
	\begin{tabulary}{\linewidth}{LLLLL}
		
		\hline
		\textbf{Per Window Size Category} & \textbf{New Window Size}  & \textbf{Per View Category} & \textbf{Old Window Size} & \textbf{\# of Occupied Samples} \\ \hline
		\multirow{3}{*}{left}             & \multirow{3}{*}{(80, 30)} & left-1                     & (80, 30)                 & 6393                            \\
		&                           & left-2                     & (80, 30)                 & 1469                            \\
		&                           & left-3                     & (80, 40)                 & 2112                            \\ \hline
		\multirow{3}{*}{center}           & \multirow{3}{*}{(50, 40)} & center-1                   & (50, 40)                 & 11528                           \\
		&                           & center-2                   & (50, 40)                 & 7434                            \\
		&                           & center-3                   & (50, 40)                 & 1699                            \\ \hline
		\multirow{3}{*}{right}            & \multirow{3}{*}{(80, 40)} & right-1                    & (80, 50)                 & 2001                            \\
		&                           & right-2                    & (80, 40)                 & 7473                            \\
		&                           & right-3                    & (80, 40)                 & 6016                          \\ \hline
	\end{tabulary}
	\caption{UFPR04: per view and per window categorization}
	\label{table:UFPR04_views}
\end{table}


\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\linewidth]{ufpr04_categories_horizontal}
	\caption{Categories made on the UFPR04 dataset}
	\label{fig:ufpr04_categories}
\end{figure}


\subsection{HoG with Linear SVM}
Histogram of Oriented Gradients (HoG) is a well-known textual descriptor proven with to be effective in people, object, and vehicle detection alike, even when used as the sole feature descriptor \cite{Dalal2005} \cite{Malisiewicz2011}.  
The HoG descriptor implementation used was from Scikit-Image \cite{scikit-image}. Their implementation follows Dalal and Triggs' implementation \cite{dalal_triggs_2005} for the most part, but does not allow for block-overlapping. Still, the descriptor was used for consistency with the usage of the Scikit-Learn \cite{scikit-learn} and Scikit-Image libraries for the whole of the machine learning part of this study.

\subsubsection{HoG Parameters}
The parameters used for the implementation of the HoG descriptor was followed from Dalal's study on HoG \cite{Dalal2005}. These parameters are: "orientations": 9, "normalize": True, "block normalization": L2-Hysteresis, and "window size" : depends on ``per view'' or ``per window size'' categories. The parameters for ``cells per block'' and ``pixels per cell'' are found by testing the performance of the resulting models for a list of combinations. 

\subsubsection{``Per View'': Finding Best Parameters"}
Testing for the ``per view'' categorization, it 

	\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\linewidth]{hogparams_view_test}
	\caption{F1-Scores for ``Occupied'' and ``Empty'' labels  with SOLAR\#7}
	\label{fig:hogparams_view_test_fs}
\end{figure}

\subsubsection{Part-based Detection}
			\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\linewidth]{partbased}
	\caption{Some part-based detections made by ``per window size'' models}
	\label{fig:partbased}
\end{figure}



\subsubsection{``Per Window Size'': Finding Best Parameters}
<include details about the top best parameters>

\subsection{LBP with Linear SVM}



\subsubsection{Multi-block LBP} 
For this experiment, a single level multi-block LBP descriptor is generated. This means that an image is split into $i$ x $j$ blocks, and then for each block a histogram of rotation invariant uniform LBP values is computed. These values are then concatenated to form the feature vector that would be used with the SVM. This method is inspired by Ahonen's work on facial recognition with using multi-block LBP \cite{Ahonen2004}. Although with their system, they used the simplistic approach of assigning higher weights to significant facial features like the eyes. With our experiment, we just assigned equal weights to the blocks, since unlike picture of faces, the images of vehicles are of different views and angles.

\subsubsection{Multi-block LBP: Finding Best Parameters}
<include summary of best parameters>

\subsection{HoG+LBP with Linear SVM}
Another experiment done was concatenating a single-level HoG feature and a single-level multi-block LBP feature, and then feeding this new feature into a linear SVM. The ``per window size'' categorization was used. The goal of the experiment is to see if doing this simple concatenation, would allow for the HoG descriptor to perform better than being used alone. 

		The chosen parameters for the HoG feature descriptor used are the same as the mentioned initial parameters we used for the HoG with linear SVM experiment, with the parameters for ``cells per block'' and ``pixels per cell'' as (3, 3) and (6, 6) respectively. For the ``left'' window size category, this gives us a feature vector of 2673-D. For LBP, we use the same parameters as the LBP with linear SVM experiment, with the parameters for the blocks division as  4 by 6 for rows and columns respectively. This gives us a feature vector of 60-D for the ``left'' category. In total, this gives us 2733-D for the ``left'' category, which is a fairly small feature vector, as is the same for the ``right'' and ``center''  feature vectors as well.

This experiment was inspired by Zeng et. al.'s implementation of multi-level and multi-block HoG and LBP \cite{Zeng2010}, where they concatenate the two features to feed as a new feature into an SVM, and to further improve their results, they also applied PCA to this new feature. 

\subsection{Detection Scheme for Vehicle Detection Algorithms}
		The detection scheme used for the system utilizes the marked parking spaces from the parking lot to be detected for. The flowchart for the scheme can be seen at Figure \ref{fig:detectionscheme}. For the detector, each model to be used is listed in sequence, sorted in an ascending order based on the width of the training input that was used for that model. The image passes through each model, and once a vehicle is detected, the detector outputs that a car is detected. A simplistic detection scheme was used in order to evaluate the performance of the feature descriptors and image processing used, since with doing so they would be the ones largely affecting the performance of the detector.
		
		
		\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\linewidth]{detection_scheme}
	\caption{Overview of Detection Scheme}
	\label{fig:detectionscheme}
\end{figure}


	\section{Experimental Results}

	\subsubsection{Background Subtraction}

	\subsubsection{HoG with Linear SVM}
	\begin{center}
		
		\includegraphics[width=0.8\linewidth]{hog_36_fs}
		\captionof{figure}{``cells per block'': (3, 3), ``pixels per cell'': (6, 6): F1-Score}
		\label{fig:hog_36_fs}
	\end{center}
		
	\subsubsection{LBP with Linear SVM}
		\begin{center}
		\includegraphics[width=0.8\linewidth]{lbp_46_fs}
		\captionof{figure}{``rows'': 4, ``columns'': 6: F1-Score}
		\label{fig:lbp_46_fs}
	\end{center}
	\subsubsection{HoG+LBP with Linear SVM}
	\begin{center}
		\includegraphics[width=0.8\linewidth]{hog_lbp_36_46_fs}
		\captionof{figure}{HoG+LBP: F1-Score}
		\label{fig:hog_lbp_36_46_fs}
	\end{center}


\begin{table}[]
	\centering
	\caption{My caption}
	\label{my-label}
	\begin{tabular}{l|ll|ll|ll|}
		\cline{2-7}
		& \multicolumn{2}{c|}{\textbf{HoG}}                                    & \multicolumn{2}{c|}{\textbf{LBP}}                                    & \multicolumn{2}{c|}{\textbf{HoG+LBP}}                                \\ \hline
		\multicolumn{1}{|l|}{\textbf{Dataset}} & \multicolumn{1}{c}{\textbf{FNR}} & \multicolumn{1}{c|}{\textbf{FPR}} & \multicolumn{1}{c}{\textbf{FNR}} & \multicolumn{1}{c|}{\textbf{FPR}} & \multicolumn{1}{c}{\textbf{FNR}} & \multicolumn{1}{c|}{\textbf{FPR}} \\ \hline
		\multicolumn{1}{|l|}{4THFLR}           & 1.76\%                           & 20.98\%                           & 18.61\%                          & 61.16\%                           & 4.12\%                           & 65.18\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#1}        & 0.00\%                           & 14.97\%                           & 11.36\%                          & 46.50\%                           & 0                                & 62.90\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#2}        & 0.00\%                           & 8.72\%                            & 0.71\%                           & 61.56\%                           & 0                                & 52.07\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#3}        & nan                              & 18.95\%                           & nan                              & 56.25\%                           & nan                              & 35.08\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#4}        & nan                              & 13.35\%                           & nan                              & 51.44\%                           & nan                              & 48.28\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#5}        & nan                              & 9.56\%                            & nan                              & 57.08\%                           & nan                              & 47.44\%                           \\
		\multicolumn{1}{|l|}{SOLAR \#6}        & 0.34\%                           & 17.51\%                           & 7.55\%                           & 80.16\%                           & 0.07\%                           & 47.08\%                           \\
		\multicolumn{1}{|l|}{CNL}              & 0.00\%                           & 42.81\%                           & 0.00\%                           & 100.00\%                          & 0                                & 14.03\%                           \\ \hline
	\end{tabular}
\end{table}
	
	\subsubsection{WSN}
		The power draw of the different modes, ping, throughput, image processing time, and image capture time were measured to describe the network metrics. The results can be seen below.
		
		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.8\linewidth]{pwr_results}
			\caption{Average power consumption}
			\label{fig:pwr_results}
		\end{figure}
		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.8\linewidth]{throughput_ave}
			\caption{Throughput measurements}
			\label{fig:throughput_ave}
		\end{figure}
		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.8\linewidth]{metrics}
			\caption{Network Metrics}
			\label{fig:metrics}
		\end{figure}
	
		It can be seen that the throughput varies greatly, exemplifying the looping of packets due to the TCP used. Also, the time for each process is greatly affected by the reduced throughput by the implementation of TCP especially in sending images which was essential for initial configuration of the system. It can take up to 50 seconds to transmit a single image. 
		
		The processing of the image to generate an occupancy array takes around 35 seconds. Adding the time it takes to send the image, this can shoot up to 40 seconds during a bad run when the radios fall out of sync. This fails as an implementation of a parking visual sensor network since a car may easily park at a space in much less than a minute but a user may see this in the interface as an empty slot. 
		
		Additionally, the nRF24L01 was also found to be very unreliable, often dropping packets. This became the bulk of the problem in designing the network protocols since most command codes are critical and need to be sent over the pipe, otherwise the network will freeze. The TCP implementation coupled with splitting the controller functions into scripts helped resolve these problems such that the controller can re-send command codes that were not executed by the node but this only reduced network failure to 16\% of the time when nodes are continuously switching between receiving and sending modes.
		
	\subsubsection{Final System Implementation}
	For the final system implementation used, the parking lot occupancy algorithm used was the HoG with SVM implementation, as it performed the best amongst the others. A single camera node was ran live, as it had the view of the 4THFLR, while the other camera node was ran with just simulations with the view of BRIDGE.  The two were controlled by a central controller, which sends the data received from those views to the cloud. The reason with just having one camera node live was that the parking lot occupancy algorithm used could not handle occlusions as of yet, and the BRIDGE view consists of vehicles being occluded by each other.

	\section{Conclusion and Recommendations}
	An implementation of an open-space camera-based parking system is feasible. With the work done in this research, we have successfully implemented a portable outdoors camera-based parking system.
		For future work on the vacancy detection algorithms, it is noted that 2D to 3D transform for handling occlusion and increasing accuracy of detection should still be explored, and that a more intelligent background subtraction and decision scheme for occupancy detection should be developed.
	
	For future work on the machine learning algorithms for vehicle detection, using the rest of the PKLot dataset as training samples should be explored, as well as in being selective about the training samples that would be used. Normalization of the parking lot should be explored if it would improve the detections made on parking lots with shadows. Multi-level HoG+LBP might improve accuracy as well. Porting to C++ would also be suggested for faster implementations of the algorithms once they are improved. And also, images from views of CCTV's should be tested. The use of NoIR cameras should also be explored for handling night time. Finding better negative images of objects regularly occurring at parking lots would improve the SVMs. And another is that the use of deep learning should be explored for better detection rate. 
	
	For the wireless sensor network, we suggest using more reliable modules for communication such as WiFi, LoRa, and etc. Our measurements show that the transmission power of nRF2FL01 are quite low, and the times that the RPis are processing the images consumes more power (with current input peaking at 300mA). And so improving processing time would be better for minimizing power consumption.


	\section*{Acknowledgements}
		The authors would like to thank the Ubiquitous Computing Laboratory of the University of the Philippines for helping in this project.

	\bibliographystyle{IEEEtran}
	\bibliography{IEEEabrv,refs}

\end{document}