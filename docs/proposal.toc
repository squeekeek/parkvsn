\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Background of the Project}{1}
\contentsline {section}{\numberline {1.2}Document Organization}{2}
\contentsline {chapter}{\numberline {2}Related Work}{3}
\contentsline {section}{\numberline {2.1}Sensor-based Systems}{3}
\contentsline {section}{\numberline {2.2}Visual-based Systems}{3}
\contentsline {subsection}{\numberline {2.2.1}Image Feature Extraction Methods}{4}
\contentsline {subsection}{\numberline {2.2.2}Classification Algorithms}{4}
\contentsline {section}{\numberline {2.3}Visual Sensor Networks}{5}
\contentsline {subsection}{\numberline {2.3.1}Hardware for VSN}{5}
\contentsline {subsection}{\numberline {2.3.2}VSN Operation Paradigms}{6}
\contentsline {subsection}{\numberline {2.3.3}Existing VSN Frameworks}{7}
\contentsline {chapter}{\numberline {3}Problem Statement and Objectives}{8}
\contentsline {section}{\numberline {3.1}Problem Statement}{8}
\contentsline {section}{\numberline {3.2}Objectives and Overview}{8}
\contentsline {chapter}{\numberline {4}Methodology}{9}
\contentsline {section}{\numberline {4.1}Testbed Realization}{9}
\contentsline {subsection}{\numberline {4.1.1}Microcontroller}{9}
\contentsline {subsection}{\numberline {4.1.2}Camera}{10}
\contentsline {subsection}{\numberline {4.1.3}Image Processing and Machine Learning}{10}
\contentsline {subsection}{\numberline {4.1.4}Web and Mobile Platform}{10}
\contentsline {subsection}{\numberline {4.1.5}Testing and Simulation}{10}
\contentsline {section}{\numberline {4.2}Framework Overview}{11}
\contentsline {subsection}{\numberline {4.2.1}Initial Set-up}{11}
\contentsline {subsection}{\numberline {4.2.2}System Flow During Usage}{11}
\contentsline {subsection}{\numberline {4.2.3}Modifications During Usage}{11}
\contentsline {section}{\numberline {4.3}Camera Sensor Node}{12}
\contentsline {subsection}{\numberline {4.3.1}Initial Parking Space Detection}{12}
\contentsline {subsection}{\numberline {4.3.2}Parking Lot Occupancy Detection}{12}
\contentsline {subsubsection}{Image Acquisition}{12}
\contentsline {subsubsection}{Image Processing}{12}
\contentsline {subsubsection}{Evaluation}{13}
\contentsline {subsubsection}{Send data to central controller}{13}
\contentsline {section}{\numberline {4.4}Central Controller}{13}
\contentsline {section}{\numberline {4.5}Wireless Sensor Network}{14}
\contentsline {subsection}{\numberline {4.5.1}System Architecture}{14}
\contentsline {subsection}{\numberline {4.5.2}Sensor Nodes}{16}
\contentsline {subsection}{\numberline {4.5.3}Transmission Protocol: IEEE 802.15.4}{17}
\contentsline {subsection}{\numberline {4.5.4}Network and Application Layers: Zigbee}{17}
\contentsline {section}{\numberline {4.6}Web and Mobile Platform}{17}
\contentsline {chapter}{\numberline {5}Project Schedule and Deliverables}{19}
\contentsline {section}{\numberline {5.1}Project Timeline}{19}
\contentsline {chapter}{Bibliography}{20}
