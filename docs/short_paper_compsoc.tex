\documentclass[10pt, a4paper, conference, compsocconf]{./IEEE_CS_LatexA4/IEEEtran}

\usepackage[pdftex]{graphicx}
\usepackage{caption}   % to increase space between table and caption
\usepackage{subcaption}
\usepackage{cite}

\usepackage[nokeyprefix]{refstyle}
\graphicspath{{images/}}
\DeclareGraphicsExtensions{.png, .jpg}

\begin{document}
	\title{Development of an Open-space Visual Smart Parking System}
	\author{\IEEEauthorblockN{Carl C. Dizon, Liezl C. Magpayo, Agatha C. Uy, and Nestor Michael C. Tiglao}
\IEEEauthorblockA{Electrical and Electronics Engineering Institute\\
University of the Philippines - Diliman\\
Quezon City, Philippines\\
Email: (carl.dizon, liezl.magpayo, agatha.uy, nestor.tiglao)@eee.upd.edu.ph}}
	
	\maketitle
	
	\begin{abstract}
		Parking has become an evident road traffic problem arising from the increase in the use of cars. To solve the continuous waste of time and energy in search of parking lots in the country, we have developed a visual smart parking monitoring system for open-space parking lots. The system comprises of wireless sensor nodes equipped with cameras that monitor the status of each parking space in a parking lot. Each node is capable of running a parking lot occupancy algorithm to check for occupancies. Finally, the results are sent to a central controller connected to the internet, which will then be disseminated through a GUI made for both web and mobile platforms. The system was proven to have a potential for robustness, energy-efficiency, and further improvement.
	\end{abstract}
	
	\section{Introduction}
		\IEEEPARstart{W}{ith} the increase use of cars in today`s time, new problems related to road traffic are appearing. One of the evident problems is the lack of parking spaces. According to Shoup \cite{shoup_2006}, 30\% of cars on average are cruising to find an open parking space. This percentage will greatly contribute to an already-congested and cramped roads of the country. Hence, there is a need for a system that makes finding available parking spaces easy and fast.
		
		The concept of ``smart'' parking has been realized since 1982 when the city of Nara in Japan deployed a parking lot guidance system for four parking lots around the city \cite{sakai_etal_1995}. Today, sensor-based parking detection systems are widely implemented commercially such as in malls, airports, and offices. The recent popularity of the Internet of Things (IoT) gave way to several solutions that address this parking problem in the form of smart parking systems. A smart parking system is used to automate the process of determining and counting parking spaces in a lot. This information is given to drivers who would want to park there. There have been numerous implementation of these kinds of systems, whether standalone or integrated in a smart transportation system. For example, on-trial parking monitoring systems \cite{sfpark} \cite{parkcbr_2016}, as of this time of writing, are already helping drivers find parking spaces.
		
		Recent developments in this field include the use of cameras and occupancy detection algorithms as a replacement for induction and light sensors. A camera is able to monitor a large number of parking spaces depending on its position and viewing angle. These can then be connected over a wireless network to monitor multiple large parking lots. However, images itself expose a plethora of problems such as lighting variances, occlusions, and camera angle views, which are solved using either machine learning or traditional image processing. Additionally, networks such as these use a lot of energy because of handling of multimedia data, which can easily drain the small energy storage of IoT devices.
		
		With the increasing number of countries implementing smart parking systems as a response to traffic congestion, the country has yet to implement one that can be used by people in their everyday lives. Hence, we have developed such a system aimed to be robust, energy-efficient, and affordable. Parking space occupancy detection was made using a combination of foreground-based and vehicle-based methods. The system was able to detect occupancy on a parking lot with prior training from other images not from that lot. Although the accuracy rates of the algorithms were lower than expected, several observations were made for further improvement. On the other hand, the sensor network for monitoring several large parking lots were proven to be feasible and within the sensible limits of such visual-based network. However, changes should be made on the communication devices to optimize throughput and timely arrival of data.
	
	\section{System Implementation}
		Our proposed system is composed of a network of wireless camera sensor nodes running a parking lot occupancy algorithm and an online GUI for monitoring and control of the system. \figurename~\ref{fig:sys_flow} shows a block diagram of the normal operation of the system. For this research, the system was deployed on the student parking lot of the institute.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.8\linewidth]{sysflow}
			\caption{Operation flow of the system during normal use}
			\label{fig:sys_flow}
		\end{figure}
		
		\subsection{System Overview}
			The system has identical sensor nodes equipped with cameras, and a single controller node interconnected via a wireless mesh network. The nodes used were Raspberry Pi (RPi) 3 single-board computers. The camera sensor nodes were equipped with an RPi Camera Module v2 to take pictures of the parking lot, and an nRF24L01 receiver to be able to communicate among nodes within the network. The nodes were positioned on the fourth and fifth floors of the institute building to enable a clear view of the parking lot. The nodes take HD size pictures of the parking lot at fixed periods and independently analyze each one for occupancy status using a parking lot occupancy algorithm. On the other hand, the central controller, equipped with an nRF24L01, aggregates all the occupancy data sent by the nodes and sends the aggregated data to an online server in the internet. The online server then sends data to the web and mobile interfaces and shows the status of the parking spaces in the lot. Aside from being able to monitor parking space occupancy, the nodes also accept commands from an administrator, such as forcing to send an image or measure network metrics. The commands are received via the central controller which are then sent to the recipient nodes.
			
			The parking lot occupancy algorithm was implemented in Python 2.7, OpenCV, and SciKits. The web and mobile front-ends were developed with a mobile-first paradigm in mind. The server for these front-ends runs on Node.js using several libraries that are responsible for the display and communication between the central controller and the front-end. 
			
		\subsection{Initial Calibration}
			An initial calibration is required for first-time use of the system. An image of the parking lot is first obtained, which will serve as a reference image. The user is required to manually mark each parking space as a four-sided convex polygon. The image should also contain a calibration mark drawn on the parking space, which is used to generate a corresponding parallelepiped in each parking space. Then, an image rectification algorithm largely based on \cite{liebowitz_zisserman_1998} with a vanishing point estimation performed by \cite{saini_etal_2013} is used. Finally, the z-axis of the image is estimated to be the direction of the leftmost bounding line of the transformed image. The height of a parallelepiped is then estimated to be the average length of the sides of its corresponding marked parking space. The generated parallelepipeds, together with their convex hulls, are stored in a JSON file for use during normal operation.
			
		\subsection{Parking Lot Occupancy Algorithm}
			A series of tests were done on datasets to test the parking lot occupancy algorithm that would be used for deployment. The algorithm is visioned to be an integration of a background-based detection algorithm and a vehicle-based detection algorithm for determining whether a parking space is empty, occupied, or obstructed. This integrated algorithm should also work on images not used for training.
			
			Background subtraction was explored for implementing a foreground-based detector. On the other hand, Histogram of Oriented Gradients (HoG) and Local Binary Patterns (LBP) as feature descriptors for linear support vector machines (SVM) were explored both individually and integrated with each other for implementing a vehicle-based detector. The background-based detection should be able to predict whether a parking space is occupied or not and the vehicle-based detection to determine whether the occupancy is caused by a vehicle or an obstruction. 

		\subsection{Wireless Sensor Network}
			Occupancy data independently processed by each node are sent via a wireless sensor network. The network was designed such that it is practical as a wireless VSN (WVSN) as outlined by \cite{seema_reisslein_11} \textemdash~one that is cheap and consumes the least amount of energy. 
			
			The network was set-up in a star topology layout because of its modularity, simplicity, and resilience to line failures. An analyze-then-compress (ATC) paradigm was also used, as suggested by \cite{Baroffio2016}, to bring down the amount of data sent throughout the network to a few bytes during normal use. This is also in response to the lower data rate of the nRF24L01 module.
			
			A feature of this system is the ability of the nodes to receive and process commands sent from the central controller. These commands are used to force initial configuration of the system, control the normal operation, and measure network metrics.

		\subsection{GUI}
			The admin panel can be used to change the configuration of the network and is accessible only through the central controller. The central controller then uploads the occupancy information on a cloud server for the web app to display it. Both interfaces, as shown in \figurename~\ref{fig:admininterface} and \figurename~\ref{fig:web_mobile}, were created with Node.js and Express.
	
			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.325\textwidth}
					\includegraphics[width=\linewidth]{admin_interface}
					\caption{}
					\label{fig:admininterface}
				\end{subfigure}
				~
				\begin{subfigure}[b]{0.125\textwidth}
					\includegraphics[width=\linewidth]{web_mobile}
					\caption{}
					\label{fig:web_mobile}
				\end{subfigure}
				
				\caption{Mock-up of (a) admin and (b) web app}
			\end{figure}
		
	\section{Development of the Parking Lot Occupancy Algorithm}
	\label{section:dev_occ}
		Parking space occupancy detection requires taking an image of a parking space, analyzing for features, and identifying occupancy states for each parking slot. In this section, the development and preliminary testing of such an algorithm is explored.

		\subsection{Datasets}
			Different parking lot datasets were used to verify the accuracy of the parking lot occupancy algorithm. These include self-built datasets and datasets from established research.

			\subsubsection{PKLot Dataset}
				The PKLot dataset \cite{pklot} was used for training the machine learning algorithms evaluated in this study. It was chosen since the training set required by the detector should have samples for different weather conditions, views, vehicles, and parking lot backgrounds. It has three different views of parking lots from the 4th (UFPR04), 5th (UFPR05), and 10th (PUCPR) floor angles. For the different methods tested in the research, the UFPR04 category was used as training data for the machine learning algorithms. It was chosen since its views are most similar to the 4THFLR and SOLAR datasets from the UP EEEI dataset, and to the target area of deployment, which is of the same view as the 4THFLR dataset.
		
			\subsubsection{UP EEEI Dataset}
				For testing the system, a set of datasets, which we call the UP EEEI dataset, was compiled by taking images of all the parking lots around the institute. The images were gathered at four locations around the institute buildings: 4th floor new bldg. (4THFLR), 5th floor new bldg (SOLAR), 2nd floor old bldg. (CNL), and 4th floor bridge (BRIDGE).
				
		\subsection{Test Environment}
			Accuracy tests were run to determine the feasibility of the algorithms to detect parking space occupancy. These were run over either or both datasets using a Dell Optiplex computer with an Intel i7 processor and 16 GB RAM. Both tests were done simultaneously using a Python script that determines the precision, recall, F1 score, and the confusion matrix of the algorithm.

		\subsection{Image Processing}
			Background subtraction was explored for foreground detection. The algorithm used is an adaptive median filter (AMF) implemented by \cite{nunes_etal_2011}. It saves a running median of the image frames taken and is treated as the background of the scene. For the first run, the initial reference image used is the grayscale of the first image fed into the algorithm. For the succeeding images, a binary image, as shown in \figurename~\ref{fig:bs_normal}, is returned with white pixels indicating foreground pixels and black pixels indicating background pixels.
		
			Occupancy status is then determined by first slicing the image into its corresponding parking spaces. The area of each foreground object in the each space is then calculated. Finally, two decision schemes, called simple and complex, are used to determine occupancy status. The first decision scheme predicts the space as occupied if the area of the foreground object exceeds a certain percentage of the area of the parking space. The second decision scheme predicts the parking space depending on the predictions for both top and bottom parts of the parking space parallelepiped. The parking space is deemed as occupied if either the top or bottom part is deemed as occupied using the first decision scheme.
			
			\begin{figure}[h!]
				\centering
			    \begin{subfigure}[b]{0.35\textwidth}
			        \includegraphics[width=\textwidth]{bs_img_orig}
			        \caption{Original}
			    \end{subfigure}
				
				\begin{subfigure}[b]{0.35\textwidth}
			        \includegraphics[width=\textwidth]{bs_img_bin}
			        \caption{Binarized}
			    \end{subfigure}
			    
				\caption{Background subtraction of an image from the SOLAR UP EEEI dataset}
				\label{fig:bs_normal}
			\end{figure}

		\subsection{Machine Learning Overview}
			Machine learning was explored for detecting vehicles within parking spaces. The decision scheme used for detection is performed by first getting an image of a parking space. It is then passed through each model. For each model, the image is initially resized to 100 px in height with aspect ratio being retained. Then, is scaled up by some increment after a sliding window pass is finished. The initial image size for each parking space was chosen in order to be fairly similar to the size and aspect ratios of the segmented images used to train the models.
			
			For each pass, the algorithm checks whether a car is detected. Non-maximum suppression is then ran to all the resulting detections after the pass. Finally, a car has been detected if at least one detection has been made. Each model to be used for detection is listed in sequence and sorted in ascending order based on the width of the training input that was used for that model. This scheme was also used to evaluate the performance of the feature descriptors and image processing used.

		\subsection{PKLot Dataset Preprocessing as Training Set}
			The provided segmented parking spaces had different image sizes and aspect ratios, which will create some problems with the feature extraction methods. HoG and LBP are scale-invariant and need fixed window sizes. The solution was to initially make categorizations ``per view''. Later, another categorization was made upon observation that each view could be categorized into similar window sizes where cropping from the upper left-corner could still cover most of a vehicle in the image. Sample of such categorizations are shown in \figurename~\ref{fig:ufpr04_categories}.
			
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.8\linewidth]{ufpr04_categories_horizontal}
				\caption{Categories made on the UFPR04 dataset}
				\label{fig:ufpr04_categories}
			\end{figure}


		\subsection{HoG with Linear SVM}
			HoG is a well-known textual descriptor proven to be effective in people, object, and vehicle detection even when used as the sole feature descriptor. The HoG descriptor implementation used was from Scikit-Image \cite{scikit-image} which follows the implementation of \cite{dalal_triggs_2005} except for block overlapping. Although OpenCV has support for such, this implementation is more compatible with the Scikit-Learn library. The default fixed parameters, such as normalization, were also taken from \cite{dalal_triggs_2005}. On the other hand, the number of orientations is 9 and the window size depends on what categorization is used.
			
			\subsubsection{Finding best parameters for ``per view'' categories}
				Testing for this categorization was done on the training set divided into 60\% training, 20\% cross-validation, and 20\% testing. The classification scores acquired were all consistently high with precision, recall, and F1 scores being as high as 99\%, but inconclusive due to the number of models. Next, each set was tested on a control test dataset SOLAR 7. The highest scores are those with the parameters [``cells per block'' (cpb), ``pixels per block'' (ppb)] as [(4, 4), (4, 4)], [(3, 3), (6, 6)], and [(2, 2), (6, 6)]. The F1 scores of these parameters for ``Empty'' and ``Occupied'' are 96.62\% and 74.94\%, 96.55\% and 74.33\%, and 97.25\% and 81.09\%, respectively.
		
				Interestingly, it was observed from testing that just parts of vehicles were detected as a vehicle. This part-based detection happens because the training data somewhat consists of parts of the vehicles and a view within the data looks like a part of a vehicle from a different view. Because of susceptibility to multiple detections of the same vehicle in a view, the ``per view'' categories were then regrouped into similar window sizes or ``per window size'' categories.
				
			\subsubsection{Finding best parameters for ``per window size'' categories}
				Testing for this categorization was done on the training set divided into 60\% training, 20\% cross-validation, and 20\% testing. The following cpb and ppb parameters have the highest classification scores: [(2, 2), (6, 6)], [(3, 3), (6, 6)], and [(4, 4), (4, 4)]. Each parameter has F1 scores of above 99\% for both the ``Empty'' and ``Occupied'' categories.

		\subsection{LBP with Linear SVM}
			LBP is another textual descriptor that is also proven to be as effective as HoG. A single level multi-block LBP descriptor inspired by \cite{Ahonen2004} was used, where an image is split into $i\times{}j$ blocks. These blocks were assigned equal weights since images of vehicles are of different views and angles.
			
			Preliminary tests were run to find the optimal $i\times{}j$ block to use. Integer values of $i$ and $j$ from 1 to 6 were evaluated. 4$\times$6, 5$\times$5, and 6$\times$6 block sizes performed the best, with F1 scores of between 92\% to 93\% for the ``Occupied'' label, and between 98\% to 99\% for the ``Empty'' label.
			
		\subsection{HoG+LBP with Linear SVM}
			Concatenating a single-level HoG feature and a single-level multi-block LBP feature, and then feeding this new feature into a linear SVM was also tested for accuracy. This method was inspired by \cite{Zeng2010} but without Principal Component Analysis (PCA). The ``per window size'' categorization was used. The goal of the experiment is to see if doing this simple concatenation, would allow for the HoG descriptor to perform better than being used alone. The chosen parameters for HoG are ``cells per block'' and ``pixels per cell'' of (3, 3) and (6, 6) respectively, and a block size of 4 $\times$ 6 is used for LBP.


	\section{Experimental Results}
		This section details the results and analysis relevant to the project. Project testing details and measurements are also discussed here.

		\subsubsection{Image Processing}
			The tests were run three times for each dataset with the first dataset having no initial background reference and the succeeding tests using a background reference from the previous test. Thresholds were varied from 30\% to 70\% for each test and each decision scheme used.
			
			F1 scores for the simple and complex decision schemes for different datasets are shown in \figurename~\ref{fig:imgproc_simple_f1} and \figurename~\ref{fig:imgproc_top3d_f1}. ``E'' represents empty spaces in the dataset and ``O'' represents occupied spaces. It is noticeable that the simple scheme performed worse as the threshold increases except when recalling empty spaces. This is because as the threshold increases, it becomes harder to mark a space as occupied because a larger foreground blob is required to do so. With this, more spaces are then marked as vacant, which makes the algorithm recall more of them. Intuitively, its precision goes down because a lot of supposedly occupied spaces were misclassified. Interestingly, the accuracy for the complex scheme slightly changes, if not at all, as threshold increases. Adding another view as a check made the prediction more robust to changes in the threshold. Also, the present implementation updates the reference image for every image fed into it. Hence, slow changes in the scenery, such as cars parked for hours, will cause the algorithm to adapt to it. Accuracy may increase if the reference image adapts to occupied spaces slowly.
			
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.75\linewidth]{result_datasets_simple_f1}
				\caption{F1 scores of image processing in different datasets for simple decision scheme}
				\label{fig:imgproc_simple_f1}
			\end{figure}

			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.75\linewidth]{result_datasets_top3d_f1}
				\caption{F1 scores of image processing in different datasets for fixed top complex decision scheme}
				\label{fig:imgproc_top3d_f1}
			\end{figure}
	
		\subsubsection{Machine Learning}
			The false positive and negative detection rates using HoG, LBP, and HoG+LBP with a linear SVM can be seen in Tables \ref{table:res_fnr} and \ref{table:res_fpr}. 
			
			The HoG implementation with parameters [(3, 3), (6, 6)] performs the best especially on the SOLAR datasets, which contain top views of vehicles. On the other hand, it performs worst on the CNL dataset, which contains front views of vehicles. It was also observed that the false positives are due to detections made by the ``center'' model. Upon examination of the data for that model, it was observed that most of the HoG representations do not look like vehicles because of the slant and blurry image in the training datasets. Flipping the images horizontally was also performed to add more views to the training dataset. With parameters [(3, 3), (6, 6)], the performance of the detector degraded, especially with a higher false positive rate due to the ``center'' category. 
			
			On the other hand, the LBP implementation performs worse than the single-level HoG with linear SVM implementation. This is because each block in the single multi-block layer has the same weight and each block does not actually seem to represent the distinctive features of a vehicle. The block size used is $4 \times 6$.
			
			Finally, for the HoG+LBP implementation, the accuracy also degraded compared to using the HoG descriptors alone as input for the linear SVM. Similar to the LBP with linear SVM algorithm, the features for LBP implementation done in the experiment do not represent the key features of a vehicle well enough. Hence, the inaccuracy of the LBP lowered the high accuracy acquired by HoG. The cpb and ppb parameters used are [(3, 3), (6, 6)] for HoG, and a block size of $4 \times 6$ for LBP.
		
			\begin{table}[h!]
				\centering
				\caption{False Negative Rates for linear SVM implementations}
				
				\begin{tabular}{lllll}
					\hline
					Dataset & HoG & HoG w/ flip & LBP & HoG+LBP \\ \hline
					4THFLR           & 1.76\%       & 0.64\%\%             & 18.61\%      & 4.12\%           \\
					SOLAR \#1        & 0.00\%       & 0.00\%               & 11.36\%      & 0                \\
					SOLAR \#2        & 0.00\%       & 0.00\%               & 0.71\%       & 0                \\
					SOLAR \#3        & nan          & nan                  & nan          & nan              \\
					SOLAR \#4        & nan          & nan                  & nan          & nan              \\
					SOLAR \#5        & nan          & nan                  & nan          & nan              \\
					SOLAR \#6        & 0.34\%       & 0.20\%               & 7.55\%       & 0.07\%           \\
					CNL              & 0.00\%       & 0.00\%               & 0.00\%       & 0                \\ \hline
				\end{tabular}
				\label{table:res_fnr}
			\end{table}
			
			
			\begin{table}[h!]
				\centering
				\caption{False Positive Rates for linear SVM implementations}
			
				\begin{tabular}{lllll}
					\hline
					Dataset & HoG & HoG w/ flip & LBP & HoG+LBP \\ \hline
					4THFLR           & 20.98\%      & 39.73\%              & 61.16\%      & 65.18\%          \\
					SOLAR \#1        & 14.97\%      & 35.99\%              & 46.50\%      & 62.90\%          \\
					SOLAR \#2        & 8.72\%       & 26.26\%              & 61.56\%      & 52.07\%          \\
					SOLAR \#3        & 18.95\%      & 26.01\%              & 56.25\%      & 35.08\%          \\
					SOLAR \#4        & 13.35\%      & 36.60\%              & 51.44\%      & 48.28\%          \\
					SOLAR \#5        & 9.56\%       & 18.90\%              & 57.08\%      & 47.44\%          \\
					SOLAR \#6        & 17.51\%      & 27.63\%              & 80.16\%      & 47.08\%          \\
					CNL              & 42.81\%      & 25.18\%              & 100.00\%     & 14.03\%          \\ \hline
				\end{tabular}
				
				\label{table:res_fpr}
			\end{table}

		\subsubsection{WSN}
			Several tests were made to determine the feasibility of the WSN network in providing communication throughout the system. Power consumption, throughput, and processing times were measured for this system.
			
			The power consumption of each node was determined by placing a multimeter in series with the node and the power supply. Camera nodes consume about 0.2 W on idle and about 1.5 W both on processing and transmission. On the other hand, the central controller consumes about 1.25 W both on idle and transmission. All of these measurements fall comfortably within the range of a sensible VWSN according to \cite{seema_reisslein_11} who suggested a maximum of 2W on average per node.
		
			The nRF24L01 was also found to be very unreliable since it often drops packets. This was the primary problem in designing the network protocols since most commands are critical and need to be sent over the pipe. Otherwise, the network will freeze since both the central controller and camera nodes are waiting for responses from each other. The TCP implementation and splitting of controller functions into scripts helped resolve these problems in exchange for a much lower throughput of about 1.059 kBps without an antenna from the observed 10 kBps without TCP. It was observed that switching a node between sending and receiving modes increases the likelihood of dropping packets to 16\%.
			
			Table \ref{table:wsn_metrics_stat} shows a summary of the network metrics observed. It can be seen that the throughput varies greatly because of the inherent nature of TCP to resend unacknowledged packets. Processing times are greatly affected by this reduced throughput. This is more pronounced especially in sending images during initial configuration of the system, which can take up to 50 seconds to transmit an image. On the other hand, the processing of the image to generate an occupancy array takes around 35 seconds. Adding the time it takes to send the image, this can take at most 40 seconds during a run when the radios fall out of sync and need to resend acknowledgments. This means that the safe minimum interval between occupancy requests would be at around one minute. Unfortunately, this implementation is not suitable for a parking VSN since a car may easily park at a space in much less than a minute but a user may not see this in the interface as an empty slot until the next minute.

			\begin{table}[h!]
				\centering
				\caption{Network metrics observed in the WSN}

				\begin{tabular}{lllll}
					\hline
					~ & Ping (s) & Capture (s) & Processing (s) & Throughput (Bps) \\
					\hline
					Min & 0.00305 & 0.49683 & 6.76523 & 526.18687 \\
					Max & 0.01003 & 0.56331 & 35.29975 & 1948.05643 \\
					Mean & 0.00476 & 0.53340 & 15.14657 & 1059.34464 \\
					Stdev & 0.00167 & 0.01573 & 8.43804 & 359.31659 \\
					\hline
				\end{tabular}
				
				\label{table:wsn_metrics_stat}
			\end{table}

		
		\subsubsection{Final System Implementation}
			A prototype smart parking system was set-up in the student parking of the institute for a week. The parking lot occupancy algorithm only contains the HoG with SVM implementation as it performed the best among the others. The cpb and ppb parameters used are [(3, 3), (6, 6)]. A single camera node was ran live viewing the same view as that of the 4THFLR, while the other camera node was ran with just simulations with the view of BRIDGE. The two were controlled by a central controller, which sends the data received from those views to the cloud. Only one node was live because the parking lot occupancy algorithm used could not handle occlusions as of this time of writing, and the BRIDGE view consists of vehicles occluding each other.
		
	\section{Conclusion and Recommendations}
		A final prototype of an open-space visual smart parking system has been implemented using HoG and SVM with ``per-window size'' categories as the vehicle detection algorithm used, Raspberry Pi 3s as microcontrollers used for ATC processing, nRF24L01s as the means for wireless communication between the nodes, and a progressive web app as the means of viewing the parking lot tested on. Tests for both the algorithms and the network showed that there is a large room for improvement for such a system. Nevertheless, the final prototype has shown that a camera-based smart parking monitoring system is in fact feasible and implementable.
	
	In terms of the vehicle detection algorithms explored, the following conclusions were made: The usability of the rectification algorithm should be further explored for handling occlusion and increasing accuracy of detection. A more intelligent background subtraction and decision scheme for occupancy detection should also be developed because the present decision schemes used were too simple. Training the machine learning algorithms using the entirety of the PKLot dataset, being selective about the training data, and using images from a CCTV should be considered. Also, image enhancement and the use of other camera modules may enable the algorithm to detect parking spaces in the presence of shadows and at night. The accuracy of the machine learning algorithm may also increase if deep learning, multi-level HoG + LBP, and using C++ for coding are further explored.
	
	Finally, in terms of the wireless sensor network developed, we recommend using communication modules more reliable than nRF24L01 in terms of two-way communication, e.g. WiFi and LoRa. The power measurements from our tests also show that improving the processing time would further minimize power consumption of the nodes.

	\bibliographystyle{IEEEtran}
	\bibliography{IEEEabrv,refs}

\end{document}