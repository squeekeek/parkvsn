\documentclass[10pt, a4paper, oneside, journal]{IEEEtran}

\usepackage[pdftex]{graphicx}
\usepackage{notoccite}
\usepackage{stackengine}
\usepackage{multirow}
\usepackage{caption}   % to increase space between table and caption
\usepackage{subcaption}
\usepackage[final]{pdfpages}
\usepackage{amsmath}
\usepackage{cite}
\usepackage{tabulary}
\usepackage{tabularx}
\usepackage{float}

\usepackage[nokeyprefix]{refstyle}
\graphicspath{{images/}}
\DeclareGraphicsExtensions{.png, .jpg}

\newcommand\pesos{%
	\stackengine{-1.38ex}{P}{\stackengine{-1.2ex}{$-$}{$-$}{O}{c}{F}{F}{S}}{O}{c}{F}{T}{S}}

\begin{document}
	\title{An Implementation of an Open-space Visual Smart Parking System}
	\author{Carl C. Dizon, Liezl C. Magpayo, and Agatha C. Uy%
\thanks{C. Dizon, L. Magpayo, and A. Uy were with the Electrical and Electronics Engineering Institute, University of the Philippines, Diliman, Quezon City, Philippines. The corresponding author's email address is agatha.uy@eee.upd.edu.ph. Manuscript submitted on June 1, 2017.
}
}
	
	\maketitle
	
	\begin{abstract}
		Parking has become an evident road traffic problem arising from the increase in the use of cars. To solve the continuous waste of time and energy in search of parking lots, we are presenting an implementation of a smart parking monitoring system for open-space parking lots. The system comprises of wireless sensor nodes equipped with cameras that monitor the status of each parking space in a parking lot. Each node is capable of running machine learning and image processing algorithms to check for occupancies. Finally, the results are sent to a central controller connected to the internet, which will then be disseminated through a GUI made for both web and mobile platforms.
	\end{abstract}
	
	\section{Introduction}
		\IEEEPARstart{W}{ith} the increase use of cars in today`s time, new problems related to road traffic are appearing. One of the evident problems is the lack of parking spaces. According to Shoup \cite{shoup_2006}, 30\% of cars on average are cruising to find an open parking space. This percentage will greatly contribute to an already-congested and cramped roads of the country. Hence, there is a need for a system that makes finding available parking spaces easy and fast.
		
		The recent popularity of the Internet of Things (IoT) gave way to several solutions that address this parking problem in the form of smart parking systems. A smart parking system is used to automate the process of determining and counting parking spaces in a lot. This information is given to drivers who would want to park there. There have been numerous implementation of these kinds of systems, whether standalone or integrated in a smart transportation system. For example, on-trial parking monitoring systems like SFPark \cite{sfpark} last 2014 and ParkCBR \cite{parkcbr_2016} last 2016 are already helping drivers find parking spaces. Both systems are still running as of this time of writing.
		
		Recent developments in this field include the use of cameras and occupancy detection algorithms as a replacement for induction and light sensors. A camera is able to monitor a large number of parking spaces depending on its position and viewing angle. These can then be connected over a wireless network to monitor multiple large parking lots. However, images itself expose a plethora of problems such as lighting variances, occlusions, and camera angle views, which researchers solve using either machine learning or traditional image processing. Additionally, networks such as these use a lot of energy because of handling of multimedia data, which can easily drain the small energy storage of IoT devices. These are the problems in monitoring parking lots with this technology that researchers and developers are trying to give solutions.
		
	\section{Related Work}
		The concept of ``smart'' parking has since been realized in 1982 when the city of Nara in Japan deployed a parking lot guidance system for four parking lots around the city \cite{sakai_etal_1995}. This concept has also been realized in several places such as Europe, the United States, and Japan. Today, sensor-based parking detection systems are widely implemented commercially such as in malls, airports, and offices.
		
		Nonvisual-based smart parking systems involve the use of environmental sensors, such as infrared, ultrasound, and inductive coils to determine the presence of a car in a parking space. Although non-intrusive sensors, such as magnetic and light, are effective sensors to determine occupancy, they are vulnerable to harsh weather conditions and lighting levels, and are difficult to scale. Hence, a natural choice for a parking system is a visual-based one \cite{Baroffio2016}. These systems are easier to scale, cheap, and can be integrated to existing CCTV systems. However, the problem has been shifted to image processing.
		
		Numerous parking lot occupancy algorithms have been explored over the years in the hopes of increasing detection accuracy and speed. Image processing has been used to simply detect occupancy of parking spaces by detecting a fixed mark being covered by a car, for example \cite{yusnita_etal_2012}. Machine learning has also been a recent trend and was already incorporated into such algorithms. Bayesian networks \cite{huang_wang_2010} and linear support vector machine (SVM) classifiers \cite{sidla_lipetski_2014} have been used to reach accurate detection rates of up to 99.96\%.
		
		In addition to these algorithms, a network is required to be able to maximize the monitoring of several large parking spaces. Recently, visual sensor networks (VSNs) are used to monitor several areas of a certain location. They are ideally cheap, scalable, and flexible \cite{seema_reisslein_11}. With the use of VSNs arise the choice of compress-then-analyze (CTA) or analyze-then-compress (ATC) paradigms. It has been concluded by \cite{Baroffio2016} that ATC would be a suitable choice for such a network in terms of bandwidth usage, accuracy, and energy consumption.
		
		With the increasing number of researchers implementing smart parking systems as a solution to Shoup`s surprising results, the country has yet to implement a smart parking system that can be used by people in their everyday lives. Hence, we are presenting an implementation of such a system.
	
	\section{System Implementation}
		Our proposed system is composed of a network of wireless camera sensor nodes running a parking lot occupancy algorithm and an online GUI for monitoring and control of the system. \figurename~\ref{fig:sys_flow} shows a block diagram of the normal operation of the system. For this research, the system was deployed on the student parking lot of the institute.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.8\linewidth]{sysflow}
			\caption{Operation flow of the system during normal use}
			\label{fig:sys_flow}
		\end{figure}

		
		\subsection{System Overview}
			The system comprises of identical sensor nodes equipped with cameras and connected via a wireless mesh network. A Raspberry Pi (RPi) 3 single-board computer was used as a node to monitor the parking lot. It was also equipped with an RPi Camera Module v2 to take pictures of the parking lot, and an nRF24L01 receiver to be able to communicate among nodes within the network. The nodes were positioned on the fourth and fifth floors of the institute building to enable a clear view of the parking lot. The nodes take HD size pictures of the parking lot at fixed periods and independently analyze each one for occupancy status using a parking lot occupancy algorithm.
			
			The system also has a central controller that aggregates all the occupancy data sent by the nodes and sends the aggregated data to an online server in the internet. It is an RPi 3 computer equipped with both an nRF24L01 and WiFi. The online server, on the other hand, sends data to the web and mobile interfaces and shows the status of the parking spaces in the lot.
			
			The parking lot occupancy algorithm was implemented in Python 2.7, OpenCV, and SciKits on either an Anaconda 3 stack or a native Linux stack. The web and mobile front-ends were developed with a mobile-first paradigm in mind. The server for these front-ends runs on Node.js using several libraries that are responsible for the display and communication between the central controller and the front-end.
			
			Aside from being able to monitor parking space occupancy, the nodes also accept commands from an administrator, such as forcing to send an image, or measure network metrics. The commands are received via the central controller which are then passed on to the recipient nodes.
			
		\subsection{Initial Calibration}
			An initial calibration is required for first-time use of the system. An image of the parking lot is first obtained, which will serve as a reference image. The user is required to manually mark each parking space as a four-sided convex polygon. The image should also contain a calibration mark drawn on the parking space, which is used to generate a corresponding parallelepiped in each parking space. Then, an image rectification algorithm largely based on \cite{liebowitz_zisserman_1998} with a vanishing point estimation performed by \cite{saini_etal_2013} is used. Finally, the z-axis of the image is estimated to be the direction of the leftmost bounding line of the transformed image. The height of a parallelepiped is then estimated to be the average length of the sides of its corresponding marked parking space. The generated parallelepipeds, together with their convex hulls, are stored in a JSON file for use during normal operation. \figurename~\ref{fig:imgacq_calib} shows a rectification of a parking lot from the EEEI dataset.

			\begin{figure}[h!]
				\centering
			    \begin{subfigure}[b]{0.35\textwidth}
			        \includegraphics[width=\textwidth]{imgacq_calib_live}
			        \caption{Original}
			    \end{subfigure}
				
				\begin{subfigure}[b]{0.35\textwidth}
			        \includegraphics[width=\textwidth]{imgacq_rectified}
			        \caption{Rectified and Marked}
			    \end{subfigure}
				
				\caption{Image rectification applied to an image from the EEEI dataset}
				\label{fig:imgacq_calib}
			\end{figure}
			
		\subsection{Parking Lot Occupancy Algorithm}
			A series of tests were done on datasets to test the parking lot occupancy algorithm that would be used for deployment. The algorithm is visioned to be an integration of a background-based detection algorithm and a vehicle-based detection algorithm for determining whether a parking space is empty, occupied, or obstructed. This integrated algorithm should also work on images not used for training.
			
			Background subtraction was explored for implementing a foreground-based detector. On the other hand, Histogram of Oriented Gradients (HoG) and Local Binary Patterns (LBP) as feature descriptors for linear support vector machines (SVMs) were explored both individually and integrated with each other for implementing a vehicle-based detector. The background-based detection should be able to predict whether a parking space is occupied or not and the vehicle-based detection to determine whether the occupancy is caused by a vehicle or an obstruction. 

		\subsection{Wireless Sensor Network}
			Occupancy data independently processed by each node are sent via a wireless sensor network. The network was designed such that it is practical as a wireless VSN (WVSN) as outlined by \cite{seema_reisslein_11} \textemdash~one that is cheap and consumes the least amount of energy. 
			
			The network was set-up in a star topology layout because of its modularity, simplicity, and resilience to line failures. An ATC paradigm was also used, as suggested by \cite{Baroffio2016}, to bring down the amount of data sent throughout the network to a few bytes during normal use. This is also in response to the lower data rate of the nRF24L01 module.
			
			A feature of this system is the ability of the nodes to receive and process commands sent from the central controller. These commands are used to force initial configuration of the system, control the normal operation, and measure network metrics.

		\subsection{GUI}
			The admin panel can be used to change the configuration of the network and is accessible only through the central controller. The central controller then uploads the occupancy information on a cloud server for the web app to display it. Both interfaces, as shown in \figurename~\ref{fig:admininterface} and \figurename~\ref{fig:web_mobile}, were created with Node.js and Express.
	
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.7\linewidth]{admin_interface}
				\caption[Admin interface]{Mock-up of admin interface with command buttons}
				\label{fig:admininterface}
			\end{figure}
		
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.3\linewidth]{web_mobile}
				\caption[Mobile]{Mock-up of web app on a mobile phone}
				\label{fig:web_mobile}
			\end{figure}
		
	\section{Development of the Parking Lot Occupancy Algorithm}
	\label{section:dev_occ}
		Parking space occupancy detection requires taking an image of a parking space, analyzing for features, and identifying occupancy states for each parking slot. In this section, the development and preliminary testing of such an algorithm is explored.

		\subsection{Datasets}
			Different parking lot datasets were used to verify the accuracy of the parking lot occupancy algorithm. These include self-built datasets and datasets from established research. An example image from each dataset is shown in \figurename~\ref{fig:datasets_4thflr}.

			\subsubsection{PKLot Dataset}
				The PKLot dataset \cite{pklot} was used for training the machine learning algorithms evaluated in this study. It was chosen since the training set required by the detector should have samples for different weather conditions, views, vehicles, and parking lot backgrounds. It has three different views of parking lots from the 4th, 5th, and 10th floor angles. For the different methods tested in the research, the UFPR04 category, a 4th floor view, was used as training data for the machine learning algorithms. It was chosen since its views are most similar to the 4THFLR and SOLAR datasets from the UP EEEI dataset, and to the target area of deployment, which is of the same view as the 4THFLR dataset.
		
			\subsubsection{UP EEEI Dataset}
				For testing the system, a set of datasets, which we call the UP EEEI dataset, was compiled by taking images of all the parking lots around the institute. A summary of the datasets are shown on Table \ref{table:dataset_eeei}, with the empty and occupied columns depicting the number of total parking spaces of that category counted in the dataset.
			\begin{table}[H]
				\centering
								\begin{tabulary}{\linewidth}{|l|l|L|l|l|l|}
					\hline
					\textbf{\#} & \textbf{Code} & \textbf{Description} & \textbf{Date} & \textbf{Empty} & \textbf{Occupied} \\
					\hline
					1 & 4THFLR & 4th flr. new bldg. & 05/12/2017 & 224 & 3304 \\
					2 & SOLAR 1 & 5th flr. new bldg. (Set A) & 04/02/2017 & 628 & 308 \\
					3 & SOLAR 2 & 5th flr. new bldg. (Set B) & 04/02/2017 & 895 & 140 \\
					4 & SOLAR 3 & 5th flr. new bldg. (Set A) & 04/05/2017 & 496 & 0 \\
					5 & SOLAR 4 & 5th flr. new bldg. (Set B) & 04/05/2017 & 3544 & 0 \\
					6 & SOLAR 5 & 5th flr. new bldg. (Set C) & 04/05/2017 & 1328 & 0 \\
					7 & SOLAR 6 & 5th flr. new bldg. & 04/10/2017 & 257 & 1471 \\
					8 & CNL & 2nd flr. old bldg. & 03/17/2017 & 278 & 14 \\
					9 & BRIDGE & 4th flr. bridge & 05/04/2017 & 257 & 1471 \\
					\hline               
				\end{tabulary}
				\caption{Summary of EEEI Datasets}
				\label{table:dataset_eeei}
			\end{table}
			
				\begin{figure}[h!]
					\centering
					
					\begin{subfigure}[b]{0.35\textwidth}
						\includegraphics[width=\textwidth]{images/UFPR04_ex_image}
						\caption[PKLot dataset UFPR04]{UFPR04 dataset view}
						
					\end{subfigure}
					\begin{subfigure}[b]{0.35\textwidth}
						\includegraphics[width=\textwidth]{4THFLR_view}
						\caption{UP EEEI dataset 4THFLR}							
					\end{subfigure}
					
					\caption{Comparison of the 4th floor datasets}
					\label{fig:datasets_4thflr}
				\end{figure}

		\subsection{Detection Scheme for Vehicle Detection Algorithms}		
			The detection scheme used is shown in \figurename~\ref{fig:detectionscheme}. Each model to be used for detection is listed in sequence and sorted in ascending order based on the width of the training input that was used for that model. An image of a parking space first passes through each model and marks the space when a car is detected. A simple detection scheme was used in order to evaluate the performance of the feature descriptors and image processing used.

			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.75\linewidth]{detection_scheme}
				\caption{Overview of detection scheme}
				\label{fig:detectionscheme}
			\end{figure}
				
		\subsection{Test Environment}
			Accuracy tests were run to determine the feasibility of the algorithms to detect parking spaces. The tests were run over the either or both datasets using a Dell Optiplex computer with an Intel i7 processor and 16 GB RAM. Both tests were done simultaneously using a Python script that determines the precision, recall, F1 score, and the confusion matrix of the algorithm.

		\subsection{Image Processing}
			Background subtraction was first used to determine which parts of the image change over a certain time frame. The algorithm used is an adaptive median filter (AMF) implemented by \cite{nunes_etal_2011}. It saves a running median of the image frames taken and is treated as the background of the scene. For the first run, the initial reference image used is the grayscale of the first image fed into the algorithm. For the succeeding images, a binary image, as shown in \figurename~\ref{fig:bs_normal}, is returned with white pixels indicating foreground pixels and black pixels indicating background pixels.
		
			Occupancy status is then determined by first slicing the image into its corresponding parking spaces. The area of each foreground object in the each space is then calculated. Finally, two decision schemes, called simple and complex, are used to determine occupancy status. The first decision scheme predicts the space as occupied if the area of the foreground object exceeds a certain percentage of the area of the parking space. The second decision scheme predicts the parking space depending on the predictions for both top and bottom parts of the parking space parallelepiped. The parking space is deemed as occupied if either the top or bottom part is deemed as occupied using the first decision scheme.

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.35\textwidth}
					\includegraphics[width=\textwidth]{bs_img_orig}
					\caption{Original}
				\end{subfigure}
				
				\begin{subfigure}[b]{0.35\textwidth}
					\includegraphics[width=\textwidth]{bs_img_bin}
					\caption{Foreground}
				\end{subfigure}
				
				\caption{Background subtraction of an image from the SOLAR UP EEEI dataset}
				\label{fig:bs_normal}
			\end{figure}

		\subsection{PKLot Dataset Preprocessing as Training Set}
			The provided segmented parking spaces had different image sizes and aspect ratios, which will create some problems with the feature extraction methods. HoG and LBP are scale-invariant and need fixed window sizes. The solution was to make categorizations ``per view''. Another categorization was made upon observation that each view could be categorized into similar window sizes where cropping from the upper left-corner could still cover most of a vehicle in the image. Sample of such categorizations are shown in \figurename~\ref{fig:ufpr04_categories}. It is to be determined whether having a view-based model system would allow the system to have view-based classification. This will help in estimating a 3D view of the parking lot for handling occlusions in scenery. 
			
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.8\linewidth]{ufpr04_categories_horizontal}
				\caption{Categories made on the UFPR04 dataset}
				\label{fig:ufpr04_categories}
			\end{figure}


		\subsection{HoG with Linear SVM}
			HoG is a well-known textual descriptor proven to be effective in people, object, and vehicle detection even when used as the sole feature descriptor \cite{dalal_triggs_2005} \cite{Malisiewicz2011}. The HoG descriptor implementation used was from Scikit-Image \cite{scikit-image} which follows the implementation of \cite{dalal_triggs_2005} except for block overlapping. Although OpenCV has support for such, this implementation is more compatible with the Scikit-Learn library.
			
			\subsubsection{HoG parameters}
				The parameters used for the implementation of the HoG descriptor was followed from \cite{dalal_triggs_2005}. These parameters are: orientations: 9, normalize: true, block normalization: L2-Hysteresis, and window size: depends on ``per view'' or ``per window size'' category. The parameters for ``cells per block'' and ``pixels per cell'' are determined by testing the performance of the resulting models for a list of combinations. 
			
			\subsubsection{Finding best parameters for ``per view'' categories}
				Testing for the ``per view'' categorization was done on the training set divided into 60\% training, 20\% cross-validation, and 20\% testing. The classification scores acquired were all consistently high with precision, recall, and F1 scores being as high as 99\%, but inconclusive due to the number of models. Next, each set was tested on a control test dataset, which was SOLAR 7. The highest scores are those with the parameters for ``cells per block'' and ``pixels per block'' as (4, 4) and (4, 4), (3, 3) and (6, 6), and (2, 2) and (6, 6). The F1 scores of these parameters for ``Empty'' and ``Occupied'' are 96.62\% and 74.94\%, 96.55\% and 74.33\%, and 97.25\% and 81.09\%, respectively.
			
			\subsubsection{Part-based detection}
				It was observed from testing using ``per view'' categorization that just parts of vehicles were detected as a vehicle as shown in \figurename~\ref{fig:partbased}. This part-based detection happens because the training data somewhat consists of parts of the vehicles and a view within the data looks like a part of a vehicle from a different view. Because of susceptibility to multiple detections of the same vehicle in a view, the ``per view'' categories were then regrouped into similar window sizes or ``per window size'' categories.
				
				\begin{figure}[h!]
					\centering
					\includegraphics[width=0.8\linewidth]{partbased}
					\caption{Some part-based detections made by ``per window size'' models}
					\label{fig:partbased}
				\end{figure}
			
			\subsubsection{Finding best parameters for ``per window size'' categories}
				Testing for the ``per window size'' categorization was done on the training set divided into 60\% training, 20\% cross-validation, and 20\% testing. The following parameters for ``cells per block'' and ``pixels per block'' have the highest classification scores: (2, 2) and (6, 6), (3, 3) and (6, 6), and (4, 4) and (4, 4). Each parameter has F1 scores of above 99\% for both the ``Empty'' and ``Occupied'' categories.

		\subsection{LBP with Linear SVM}
			LBP is another textual descriptor that is also proven to be effective and in similar terms with HoG \cite{Almeida2013} \cite{Delibaltov2013} \cite{Ahonen2004}. This is used on grayscale images where for each pixel, a parameter $(p,~r)$ is used, with $p$ neighbors at $r$ radius away from it are considered. The neighbors with values greater than the center pixel are assigned a ``1'' while those having a lower value are assigned a ``0''. The resulting binary code sequence is concatenated and then converted into a decimal value representing the LBP code value for the center pixel in consideration. 

			\subsubsection{Multi-block LBP} 
				A single level multi-block LBP descriptor inspired by \cite{Ahonen2004} is used. An image is first split into $i\times{}j$ blocks. Then, for each block, a histogram of rotation invariant uniform LBP values is computed. These values are then concatenated to form the feature vector that would be used with the SVM. These blocks were assigned equal weights since images of vehicles are of different views and angles.
			
			\subsubsection{Finding best parameters for multi-block LBP}
			Preliminary tests were run to find the optimal $i\times{}j$ block to use. Integer values of $i$ and $j$ from 1 to 6 were evaluated. 4$\times$6, 5$\times$5, and 6$\times$6 block sizes performed the best, with F1 scores of between 92\% to 93\% for the ``Occupied'' label, and between 98\% to 99\% for the ``Empty'' label.
			
		\subsection{HoG+LBP with Linear SVM}
			Concatenating a single-level HoG feature and a single-level multi-block LBP feature, and then feeding this new feature into a linear SVM was also tested for accuracy. This method was inspired by \cite{Zeng2010} but without Principal Component Analysis (PCA). The ``per window size'' categorization was used. The goal of the experiment is to see if doing this simple concatenation, would allow for the HoG descriptor to perform better than being used alone. The chosen parameters for HoG are ``cells per block'' and ``pixels per cell'' of (3, 3) and (6, 6) respectively, and a block size of 4 $\times$ 6 is used for LBP.


	\section{Experimental Results}
		This section details the results and analysis relevant to the project. Project testing details and measurements are also discussed here.

		\subsubsection{Image Processing}
			The tests were run three times for each dataset with the first dataset having no initial background reference and the succeeding tests using a background reference from the previous test. Thresholds were varied from 30\% to 70\% for each test and each decision scheme used.
			
			F1 scores for the simple and complex decision schemes for different datasets are shown in \figurename~\ref{fig:imgproc_simple_f1} and \figurename~\ref{fig:imgproc_top3d_f1}. ``E'' represents empty spaces in the dataset and ``O'' represents occupied spaces. It is noticeable from these results that the simple scheme performed worse as the threshold increases except when recalling empty spaces. This is because as the threshold increases, it becomes harder to mark a space as occupied because a larger foreground blob is required to do so. With this, more spaces are then marked as vacant, which makes the algorithm recall more of them. Intuitively, its precision goes down because a lot of supposedly occupied spaces were misclassified. Interestingly, the accuracy for the complex scheme slightly changes, if not at all, as threshold increases. Adding another view as a check made the prediction more robust to changes in the threshold. The choice of the background subtraction algorithm also dictates the accuracy of the predictions. Although the AMF is robust, the present implementation updates the reference image for every image fed into it. Hence, slow changes in the scenery, such as cars parked for hours, will cause the algorithm to adapt to it. Accuracy may increase if the segments of the image do not update the reference image easily when that segment contains an occupied parking space.
			
			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.75\linewidth]{result_datasets_simple_f1}
				\caption{F1 scores of image processing in different datasets for simple decision scheme}
				\label{fig:imgproc_simple_f1}
			\end{figure}

			\begin{figure}[h!]
				\centering
				\includegraphics[width=0.75\linewidth]{result_datasets_top3d_f1}
				\caption{F1 scores of image processing in different datasets for fixed top complex decision scheme}
				\label{fig:imgproc_top3d_f1}
			\end{figure}
	
		\subsubsection{Machine Learning}
			The false positive and negative detection rates using HoG, LBP, and HoG+LBP with a linear SVM can be seen in Tables \ref{table:res_fnr} and \ref{table:res_fpr}. 
			
			The HoG implementation with ``cells per block'' and ``pixels per cell'' as (3, 3) and (6, 6) respectively performs the best especially on the SOLAR datasets, which contain top views of vehicles. On the other hand, it performs worst on the CNL dataset, which contains front views of vehicles. It was also observed that the false positives are due to detections made by the ``center'' model. Upon examination of the data for that model, it was observed that most of the HoG representations do not look like vehicles because of the slant and blurry image in the training datasets.
			
			Flipping the images horizontally for HoG with SVM was also performed to add more views to the training dataset. With ``cells per block'' of (3, 3), and ``pixels per cell'' of (6, 6), the performance of the detector degraded, especially with a higher false positive rate due to the ``center'' category. 
			
			On the other hand, the LBP implementation performs worse than the single-level HoG with linear SVM implementation. This is because each block in the single multi-block layer has the same weight and each block does not actually seem to represent the distinctive features of a vehicle. The block size used is $4 \times 6$.
			
			Finally, for the HoG+LBP implementation, the accuracy also degraded compared to using the HoG descriptors alone as input for the linear SVM. Similar to the LBP with linear SVM algorithm, the features for LBP implementation done in the experiment do not represent the key features of a vehicle well enough. Hence, the inaccuracy of the LBP lowered the high accuracy acquired by HoG. The parameters used for this are ``cells per block'' of (3, 3) and ``pixels per cell'' of (6, 6) for HoG, and a block size of $4 \times 6$ for LBP.
		
			\begin{table}[h!]
				\centering
			
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					\textbf{Dataset} & \textbf{HoG} & \textbf{HoG w/ flip} & \textbf{LBP} & \textbf{HoG+LBP} \\ \hline
					4THFLR           & 1.76\%       & 0.64\%\%             & 18.61\%      & 4.12\%           \\
					SOLAR \#1        & 0.00\%       & 0.00\%               & 11.36\%      & 0                \\
					SOLAR \#2        & 0.00\%       & 0.00\%               & 0.71\%       & 0                \\
					SOLAR \#3        & nan          & nan                  & nan          & nan              \\
					SOLAR \#4        & nan          & nan                  & nan          & nan              \\
					SOLAR \#5        & nan          & nan                  & nan          & nan              \\
					SOLAR \#6        & 0.34\%       & 0.20\%               & 7.55\%       & 0.07\%           \\
					CNL              & 0.00\%       & 0.00\%               & 0.00\%       & 0                \\ \hline
				\end{tabular}
				\caption{False Negative Rates for linear SVM implementations}
			\label{table:res_fnr}
			\end{table}
			
			
			\begin{table}[h!]
				\centering
			
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					\textbf{Dataset} & \textbf{HoG} & \textbf{HoG w/ flip} & \textbf{LBP} & \textbf{HoG+LBP} \\ \hline
					4THFLR           & 20.98\%      & 39.73\%              & 61.16\%      & 65.18\%          \\
					SOLAR \#1        & 14.97\%      & 35.99\%              & 46.50\%      & 62.90\%          \\
					SOLAR \#2        & 8.72\%       & 26.26\%              & 61.56\%      & 52.07\%          \\
					SOLAR \#3        & 18.95\%      & 26.01\%              & 56.25\%      & 35.08\%          \\
					SOLAR \#4        & 13.35\%      & 36.60\%              & 51.44\%      & 48.28\%          \\
					SOLAR \#5        & 9.56\%       & 18.90\%              & 57.08\%      & 47.44\%          \\
					SOLAR \#6        & 17.51\%      & 27.63\%              & 80.16\%      & 47.08\%          \\
					CNL              & 42.81\%      & 25.18\%              & 100.00\%     & 14.03\%          \\ \hline
				\end{tabular}
				\caption{False Positive Rates for linear SVM implementations}
			\label{table:res_fpr}
			\end{table}

		\subsubsection{WSN}
			Several tests were made to determine the feasibility of the WSN network in providing communication throughout the system. Power consumption, throughput, and processing times were measured for this system.
			
			The power consumption of each node was determined by placing a multimeter in series with the node and the power supply. All measurements for both the camera nodes and central controller fall comfortably within the range of a sensible VWSN according to \cite{seema_reisslein_11} who suggested a maximum of 2W on average per node, as shown in Table \ref{table:wsn_power}.
			
			\begin{table}[h!]
				\centering
				\begin{tabular}{|l|l|l|l|}
					\hline
					Node & Idle & Processing & rx/tx \\
					\hline
					Controller & 1.25 & - & 1.28 \\
					Sensor & 0.2 & 1.5 & 1.6 \\
					\hline
				\end{tabular}
				
				\caption{Average power consumption of WSN nodes in watts}
				\label{table:wsn_power}
			\end{table}
		
			The nRF24L01 was also found to be very unreliable since it often drops packets, as shown in Table \ref{table:wsn_throughput_avg}. This was the primary problem in designing the network protocols since most commands are critical and need to be sent over the pipe. Otherwise, the network will freeze since both the central controller and camera nodes are waiting for responses from each other. The TCP implementation and splitting of controller functions into scripts helped resolve these problems. It was observed that switching a node between sending and receiving modes increases the likelihood of dropping packets to 16\%.
			
			\begin{table}[h!]
				\centering
				\begin{tabular}{|l|l|l|l|}
					\hline
					Documented & w/o TCP & w/ TCP & w/ TCP + antenna \\
					\hline
					31.25 & ~10 & 1.059 & 1.405 \\
					\hline
				\end{tabular}
				
				\caption{Throughput of WSN nodes in kBps}
				\label{table:wsn_throughput_avg}
			\end{table}
			
			Table \ref{table:wsn_metrics_stat} shows a summary of the network metrics observed. It can be seen that the throughput varies greatly because of the inherent nature of TCP to resend unacknowledged packets. Processing times are greatly affected by this reduced throughput. This is more pronounced especially in sending images during initial configuration of the system. It can take up to 50 seconds to transmit a single image. On the other hand, the processing of the image to generate an occupancy array takes around 35 seconds. Adding the time it takes to send the image, this can shoot up to 40 seconds during a run when the radios fall out of sync and need to resend acknowledgments. This means that the safe minimum interval between occupancy requests would be at around one minute. Unfortunately, this implementation is not suitable for a parking VSN since a car may easily park at a space in much less than a minute but a user may see this in the interface as an empty slot until the next minute.

			\begin{table}[h!]
				\centering
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					~ & Ping (s) & Capture (s) & Processing (s) & Throughput (Bps) \\
					\hline
					Min & 0.00305 & 0.49683 & 6.76523 & 526.18687 \\
					Max & 0.01003 & 0.56331 & 35.29975 & 1948.05643 \\
					Mean & 0.00476 & 0.53340 & 15.14657 & 1059.34464 \\
					Stdev & 0.00167 & 0.01573 & 8.43804 & 359.31659 \\
					\hline
				\end{tabular}
				
				\caption{Network metrics observed in the WSN}
				\label{table:wsn_metrics_stat}
			\end{table}

		
		\subsubsection{Final System Implementation}
			The parking lot occupancy algorithm only contains the HoG with SVM implementation as it performed the best among the others. The parameters used were a $3 \times 3$ ``cell per block'' and $6 \times 6$ ``pixels per cell''. A single camera node was ran live viewing the same view as that of the 4THFLR, while the other camera node was ran with just simulations with the view of BRIDGE. The two were controlled by a central controller, which sends the data received from those views to the cloud. Only one node was live because the parking lot occupancy algorithm used could not handle occlusions as of this time of writing, and the BRIDGE view consists of vehicles occluding each other.
		
	\section{Conclusion and Recommendations}
		An open-space visual smart parking system has been successfully implemented albeit some concerns. The system as of this time of writing was deployed using two RPi camera nodes and one central controller mounted from the 4th floor of the institute. A unified web app that enables people to view the available parking spaces from that view was also created. Integration of both image processing and machine learning algorithms was not performed because the image processing algorithm implemented at that time performed worse.

			There are several future work the authors hope to explore to further improve the system. The usability of the rectification algorithm should be further explored for handling occlusion and increasing accuracy of detection. A more intelligent background subtraction and decision scheme for occupancy detection should also be developed because the present decision schemes used were too simple. Training the machine learning algorithms using the entirety of the PKLot dataset, being selective about the training data, and using images from a CCTV should be considered. Also, histogram normalization of images and the use of RPi NoIR cameras for monitoring the parking lots may enable the algorithm to detect parking spaces in the presence of shadows and at night. The accuracy of the machine learning algorithm may also increase if deep learning, multi-level HoG + LBP, and using C++ for coding are further explored. It is also suggested to use communication modules more reliable than nRF24L01, such as WiFi and LoRa, for the WSN. The measurements also show that processing the images consume more power than transmission using the nRF24L01. Hence, improving the processing time would further minimize power consumption of the nodes.

	\section*{Acknowledgements}
		The authors would like to thank the Ubiquitous Computing Laboratory of the University of the Philippines for helping in this project.

	\bibliographystyle{IEEEtran}
	\bibliography{IEEEabrv,refs}

\end{document}