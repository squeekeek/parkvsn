\contentsline {chapter}{List of Figures}{iv}
\contentsline {chapter}{List of Tables}{vi}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Background of the Project}{1}
\contentsline {section}{\numberline {1.2}Document Organization}{2}
\contentsline {chapter}{\numberline {2}Related Work}{3}
\contentsline {section}{\numberline {2.1}Smart Parking Systems}{3}
\contentsline {subsection}{\numberline {2.1.1}Nonvisual-based Systems}{3}
\contentsline {subsection}{\numberline {2.1.2}Visual-based Systems}{4}
\contentsline {subsection}{\numberline {2.1.3}Local Development}{4}
\contentsline {section}{\numberline {2.2}Parking Space Detection Algorithms}{5}
\contentsline {subsection}{\numberline {2.2.1}Image Processing}{5}
\contentsline {subsection}{\numberline {2.2.2}Machine Learning}{5}
\contentsline {section}{\numberline {2.3}Visual Sensor Networks}{6}
\contentsline {subsection}{\numberline {2.3.1}Hardware for VSN}{7}
\contentsline {subsection}{\numberline {2.3.2}VSN Operation Paradigms}{7}
\contentsline {subsection}{\numberline {2.3.3}Existing VSN Frameworks}{8}
\contentsline {chapter}{\numberline {3}Problem Statement and Objectives}{9}
\contentsline {section}{\numberline {3.1}Problem Statement}{9}
\contentsline {section}{\numberline {3.2}Objectives and Overview}{9}
\contentsline {chapter}{\numberline {4}Methodology}{10}
\contentsline {section}{\numberline {4.1}Testbed Implementation}{10}
\contentsline {subsection}{\numberline {4.1.1}Parking Lot}{10}
\contentsline {subsection}{\numberline {4.1.2}Camera Sensor Nodes}{10}
\contentsline {subsection}{\numberline {4.1.3}Central Controller}{12}
\contentsline {subsection}{\numberline {4.1.4}Cloud Server}{12}
\contentsline {subsection}{\numberline {4.1.5}Web and Mobile Platform}{12}
\contentsline {subsection}{\numberline {4.1.6}Algorithm Development Platforms}{12}
\contentsline {subsection}{\numberline {4.1.7}Initial Set-up}{13}
\contentsline {subsection}{\numberline {4.1.8}Simulation and Testing}{13}
\contentsline {section}{\numberline {4.2}System Use}{13}
\contentsline {subsection}{\numberline {4.2.1}Initial Configuration}{14}
\contentsline {subsection}{\numberline {4.2.2}Normal Use}{14}
\contentsline {subsection}{\numberline {4.2.3}System Customization and Control}{15}
\contentsline {section}{\numberline {4.3}Image Acquisition}{15}
\contentsline {subsection}{\numberline {4.3.1}Initial Configuration}{15}
\contentsline {subsection}{\numberline {4.3.2}Normal Use}{17}
\contentsline {subsection}{\numberline {4.3.3}Datasets}{17}
\contentsline {section}{\numberline {4.4}Parking Lot Occupancy Detection}{18}
\contentsline {subsection}{\numberline {4.4.1}Image Processing}{19}
\contentsline {subsection}{\numberline {4.4.2}Machine Learning}{19}
\contentsline {subsubsection}{Training Dataset Analysis and Preprocessing}{20}
\contentsline {subsubsection}{Binary Classification with Linear Support Vector Machines}{23}
\contentsline {subsubsection}{Histogram of Oriented Gradients (HoG) with Linear SVM}{24}
\contentsline {subsubsection}{Local Binary Patterns (LBP) with Linear SVM}{31}
\contentsline {subsubsection}{HoG+LBP Linear SVM}{32}
\contentsline {subsubsection}{Detection Scheme}{34}
\contentsline {subsubsection}{Test Datasets}{35}
\contentsline {section}{\numberline {4.5}Wireless Sensor Network}{36}
\contentsline {subsection}{\numberline {4.5.1}nRF24L01}{37}
\contentsline {subsection}{\numberline {4.5.2}Network Topology}{37}
\contentsline {subsection}{\numberline {4.5.3}Network Paradigm}{38}
\contentsline {subsection}{\numberline {4.5.4}System Flow}{39}
\contentsline {subsection}{\numberline {4.5.5}Administrative Interface}{39}
\contentsline {subsubsection}{Initialization}{40}
\contentsline {subsubsection}{Select and Update Parking Spaces}{40}
\contentsline {subsubsection}{Change Time Interval}{41}
\contentsline {subsubsection}{Network Metrics}{41}
\contentsline {subsubsection}{Start and Stop}{41}
\contentsline {subsection}{\numberline {4.5.6}Network Protocols}{42}
\contentsline {subsubsection}{Transmission Control Protocol}{42}
\contentsline {subsubsection}{Time-slotted Routing}{43}
\contentsline {section}{\numberline {4.6}Web Interface}{43}
\contentsline {chapter}{\numberline {5}Results and Analysis}{45}
\contentsline {section}{\numberline {5.1}Image Preprocessing}{45}
\contentsline {subsection}{\numberline {5.1.1}Image Preparation}{45}
\contentsline {subsection}{\numberline {5.1.2}Future Work}{46}
\contentsline {section}{\numberline {5.2}Machine Learning}{46}
\contentsline {subsection}{\numberline {5.2.1}HoG with Linear SVM}{46}
\contentsline {subsubsection}{Test \#1: ``cells per block'': (2, 2), ``pixels per cell'': (6, 6)}{47}
\contentsline {subsubsection}{Test \#2: ``cells per block'': (3, 3), ``pixels per cell'': (6, 6)}{49}
\contentsline {subsubsection}{Test \#3: ``cells per block'': (4, 4), ``pixels per cell'': (4, 4)}{50}
\contentsline {subsection}{\numberline {5.2.2}HoG (with Horizontal Flipping) with Linear SVM}{51}
\contentsline {subsubsection}{Test \#1: ``cells per block'': (2, 2), ``pixels per cell'': (6, 6)}{51}
\contentsline {subsubsection}{Test \#2: ``cells per block'': (3, 3), ``pixels per cell'': (6, 6)}{53}
\contentsline {subsubsection}{Test \#3: ``cells per block'': (4, 4), ``pixels per cell'': (4, 4)}{54}
\contentsline {subsection}{\numberline {5.2.3}LBP with Linear SVM}{55}
\contentsline {subsubsection}{Test \#1: (rows, columns): (3, 6)}{55}
\contentsline {subsubsection}{Test \#2: (rows, columns): (4, 6)}{56}
\contentsline {subsubsection}{Test \#3: (rows, columns): (5, 6)}{57}
\contentsline {subsubsection}{Test \#4: (rows, columns): (6, 6)}{58}
\contentsline {subsection}{\numberline {5.2.4}HoG + LBP with Linear SVM}{59}
\contentsline {subsubsection}{Testing on Training Datasets}{59}
\contentsline {subsubsection}{Testing on Test Datasets}{60}
\contentsline {subsection}{\numberline {5.2.5}Comparison to other Camera-based Vehicle Detectors.}{61}
\contentsline {section}{\numberline {5.3}Wireless Sensor Network}{61}
\contentsline {subsection}{\numberline {5.3.1}NRF24L01}{61}
\contentsline {subsection}{\numberline {5.3.2}Power}{62}
\contentsline {subsection}{\numberline {5.3.3}Camera}{63}
\contentsline {subsection}{\numberline {5.3.4}Network Metrics}{63}
\contentsline {chapter}{\numberline {6}Conclusions and Recommendations}{65}
\contentsline {section}{\numberline {6.1}Over-all System Implementation}{65}
\contentsline {chapter}{\numberline {A}Tables for Results}{67}
\contentsline {section}{\numberline {A.1}HoG with Linear SVM}{67}
\contentsline {subsection}{\numberline {A.1.1}``cells per block'': (2, 2), ``pixels per cell'': (6, 6)}{67}
\contentsline {subsubsection}{Without Flipping Horizontally}{67}
\contentsline {subsubsection}{With Flipping Horizontally}{69}
\contentsline {subsection}{\numberline {A.1.2}``cells per block": (3, 3), ``pixels per cell'': (6, 6)}{70}
\contentsline {subsubsection}{Without Flipping Horizontally}{70}
\contentsline {subsubsection}{With Flipping Horizontally}{72}
\contentsline {subsection}{\numberline {A.1.3}``cells per block": (4, 4), ``pixels per cell'': (4 , 4)}{73}
\contentsline {subsubsection}{Without Flipping Horizontally}{73}
\contentsline {subsubsection}{With Flipping Horizontally}{75}
\contentsline {section}{\numberline {A.2}LBP with Linear SVM}{76}
\contentsline {subsection}{\numberline {A.2.1}``rows'': 3, ``columns'': 6}{76}
\contentsline {subsection}{\numberline {A.2.2}``rows'': 4, ``columns'': 6}{78}
\contentsline {subsection}{\numberline {A.2.3}``rows'': 5, ``columns'' : 6}{79}
\contentsline {subsection}{\numberline {A.2.4}``rows'': 6, ``columns'': 6}{81}
\contentsline {section}{\numberline {A.3}HoG+LBP with Linear SVM}{82}
\contentsline {subsection}{\numberline {A.3.1}Testing on Training Dataset}{82}
\contentsline {subsection}{\numberline {A.3.2}Training on Test Datasets}{83}
\contentsline {chapter}{Bibliography}{85}
