\BOOKMARK [0][-]{chapter*.2}{List of Figures}{}% 1
\BOOKMARK [0][-]{chapter*.3}{List of Tables}{}% 2
\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 3
\BOOKMARK [1][-]{section.1.1}{Background of the Project}{chapter.1}% 4
\BOOKMARK [1][-]{section.1.2}{Document Organization}{chapter.1}% 5
\BOOKMARK [0][-]{chapter.2}{Related Work}{}% 6
\BOOKMARK [1][-]{section.2.1}{Smart Parking Systems}{chapter.2}% 7
\BOOKMARK [2][-]{subsection.2.1.1}{Nonvisual-based Systems}{section.2.1}% 8
\BOOKMARK [2][-]{subsection.2.1.2}{Visual-based Systems}{section.2.1}% 9
\BOOKMARK [2][-]{subsection.2.1.3}{Local Development}{section.2.1}% 10
\BOOKMARK [1][-]{section.2.2}{Parking Space Detection Algorithms}{chapter.2}% 11
\BOOKMARK [2][-]{subsection.2.2.1}{Image Processing}{section.2.2}% 12
\BOOKMARK [2][-]{subsection.2.2.2}{Machine Learning}{section.2.2}% 13
\BOOKMARK [1][-]{section.2.3}{Visual Sensor Networks}{chapter.2}% 14
\BOOKMARK [2][-]{subsection.2.3.1}{Hardware for VSN}{section.2.3}% 15
\BOOKMARK [2][-]{subsection.2.3.2}{VSN Operation Paradigms}{section.2.3}% 16
\BOOKMARK [2][-]{subsection.2.3.3}{Existing VSN Frameworks}{section.2.3}% 17
\BOOKMARK [0][-]{chapter.3}{Problem Statement and Objectives}{}% 18
\BOOKMARK [1][-]{section.3.1}{Problem Statement}{chapter.3}% 19
\BOOKMARK [1][-]{section.3.2}{Objectives and Overview}{chapter.3}% 20
\BOOKMARK [0][-]{chapter.4}{Methodology}{}% 21
\BOOKMARK [1][-]{section.4.1}{Testbed Implementation}{chapter.4}% 22
\BOOKMARK [2][-]{subsection.4.1.1}{Parking Lot}{section.4.1}% 23
\BOOKMARK [2][-]{subsection.4.1.2}{Camera Sensor Nodes}{section.4.1}% 24
\BOOKMARK [2][-]{subsection.4.1.3}{Central Controller}{section.4.1}% 25
\BOOKMARK [2][-]{subsection.4.1.4}{Cloud Server}{section.4.1}% 26
\BOOKMARK [2][-]{subsection.4.1.5}{Web and Mobile Platform}{section.4.1}% 27
\BOOKMARK [2][-]{subsection.4.1.6}{Algorithm Development Platforms}{section.4.1}% 28
\BOOKMARK [2][-]{subsection.4.1.7}{Initial Set-up}{section.4.1}% 29
\BOOKMARK [2][-]{subsection.4.1.8}{Simulation and Testing}{section.4.1}% 30
\BOOKMARK [1][-]{section.4.2}{System Use}{chapter.4}% 31
\BOOKMARK [2][-]{subsection.4.2.1}{Initial Configuration}{section.4.2}% 32
\BOOKMARK [2][-]{subsection.4.2.2}{Normal Use}{section.4.2}% 33
\BOOKMARK [2][-]{subsection.4.2.3}{System Customization and Control}{section.4.2}% 34
\BOOKMARK [1][-]{section.4.3}{Image Acquisition}{chapter.4}% 35
\BOOKMARK [2][-]{subsection.4.3.1}{Initial Configuration}{section.4.3}% 36
\BOOKMARK [2][-]{subsection.4.3.2}{Normal Use}{section.4.3}% 37
\BOOKMARK [2][-]{subsection.4.3.3}{Datasets}{section.4.3}% 38
\BOOKMARK [1][-]{section.4.4}{Parking Lot Occupancy Detection}{chapter.4}% 39
\BOOKMARK [2][-]{subsection.4.4.1}{Image Processing}{section.4.4}% 40
\BOOKMARK [2][-]{subsection.4.4.2}{Machine Learning}{section.4.4}% 41
\BOOKMARK [1][-]{section.4.5}{Wireless Sensor Network}{chapter.4}% 42
\BOOKMARK [2][-]{subsection.4.5.1}{nRF24L01}{section.4.5}% 43
\BOOKMARK [2][-]{subsection.4.5.2}{Network Topology}{section.4.5}% 44
\BOOKMARK [2][-]{subsection.4.5.3}{Network Paradigm}{section.4.5}% 45
\BOOKMARK [2][-]{subsection.4.5.4}{System Flow}{section.4.5}% 46
\BOOKMARK [2][-]{subsection.4.5.5}{Administrative Interface}{section.4.5}% 47
\BOOKMARK [2][-]{subsection.4.5.6}{Network Protocols}{section.4.5}% 48
\BOOKMARK [1][-]{section.4.6}{Web Interface}{chapter.4}% 49
\BOOKMARK [0][-]{chapter.5}{Results and Analysis}{}% 50
\BOOKMARK [1][-]{section.5.1}{Image Preprocessing}{chapter.5}% 51
\BOOKMARK [2][-]{subsection.5.1.1}{Image Preparation}{section.5.1}% 52
\BOOKMARK [2][-]{subsection.5.1.2}{Future Work}{section.5.1}% 53
\BOOKMARK [1][-]{section.5.2}{HoG with Linear SVM}{chapter.5}% 54
\BOOKMARK [1][-]{section.5.3}{LBP with Linear SVM}{chapter.5}% 55
\BOOKMARK [1][-]{section.5.4}{Concatenated HoG + LBP with Linear SVM}{chapter.5}% 56
\BOOKMARK [1][-]{section.5.5}{Wireless Sensor Network}{chapter.5}% 57
\BOOKMARK [2][-]{subsection.5.5.1}{NRF24L01}{section.5.5}% 58
\BOOKMARK [2][-]{subsection.5.5.2}{Power}{section.5.5}% 59
\BOOKMARK [2][-]{subsection.5.5.3}{Camera}{section.5.5}% 60
\BOOKMARK [2][-]{subsection.5.5.4}{Network Metrics}{section.5.5}% 61
\BOOKMARK [0][-]{chapter.6}{Conclusions and Recommendations}{}% 62
\BOOKMARK [1][-]{section.6.1}{Future Work}{chapter.6}% 63
\BOOKMARK [0][-]{chapter*.43}{Bibliography}{}% 64
