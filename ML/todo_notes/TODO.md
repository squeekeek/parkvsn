<!-- TOC -->

- [Mon Mar 20 08:57:57 PHT 2017](#mon-mar-20-085757-pht-2017)
- [Mon Mar 20 09:41:58 PHT 2017](#mon-mar-20-094158-pht-2017)
- [Mon Mar 20 10:45:33 PHT 2017](#mon-mar-20-104533-pht-2017)
- [Mon Mar 20 10:45:55 PHT 2017](#mon-mar-20-104555-pht-2017)
- [Mon Mar 20 10:58:04 PHT 2017](#mon-mar-20-105804-pht-2017)
- [Mon Mar 20 11:52:12 PHT 2017](#mon-mar-20-115212-pht-2017)
- [Mon Mar 20 14:16:10 PHT 2017](#mon-mar-20-141610-pht-2017)
- [Mon Mar 20 17:05:13 PHT 2017](#mon-mar-20-170513-pht-2017)
- [Tue Mar 21 13:19:37 PHT 2017](#tue-mar-21-131937-pht-2017)
- [Tue Mar 21 16:25:11 PHT 2017](#tue-mar-21-162511-pht-2017)
- [Tue Mar 21 18:55:44 PHT 2017](#tue-mar-21-185544-pht-2017)
- [Thu Mar 23 13:27:22 PHT 2017](#thu-mar-23-132722-pht-2017)
- [Fri Mar 24 16:12:03 PHT 2017](#fri-mar-24-161203-pht-2017)
- [Thu Apr 13 19:40:34 PHT 2017 [TODOS]](#thu-apr-13-194034-pht-2017-todos)
- [Fri Apr 14 20:01:56 PHT 2017 [TODOS]](#fri-apr-14-200156-pht-2017-todos)
- [Mon Apr 17 09:10:37 PHT 2017 [TODOS]](#mon-apr-17-091037-pht-2017-todos)
- [Fri Apr 21 08:17:33 PHT 2017 [TODOS]](#fri-apr-21-081733-pht-2017-todos)
- [Wed May 3 14:31:19 PHT 2017](#wed-may-3-143119-pht-2017)
- [Fri May 5 14:54:21 PHT 2017](#fri-may-5-145421-pht-2017)
- [Sat May 6 18:32:45 PHT 2017](#sat-may-6-183245-pht-2017)
- [Sun May 7 13:34:40 PHT 2017](#sun-may-7-133440-pht-2017)
- [Mon May 8 17:32:17 PHT 2017](#mon-may-8-173217-pht-2017)
- [Tue May 9 07:27:10 PHT 2017](#tue-may-9-072710-pht-2017)
- [Wed May 10 05:27:10 PHT 2017](#wed-may-10-052710-pht-2017)
- [Thurs May 12 16:25:59 PHT 2017](#thurs-may-12-162559-pht-2017)
- [Fri May 12 16:25:59 PHT 2017](#fri-may-12-162559-pht-2017)
- [Sat May 13 19:34:36 PHT 2017](#sat-may-13-193436-pht-2017)
- [Mon May 15 21:06:27 PHT 2017](#mon-may-15-210627-pht-2017)
- [Tue May 16 18:46:39 PHT 2017](#tue-may-16-184639-pht-2017)
                    - [Wed May 17 20:23:10 PHT 2017](#wed-may-17-202310-pht-2017)
- [Thu May 18 16:52:06 PHT 2017](#thu-may-18-165206-pht-2017)
        - [Web app](#web-app)
        - [26 testing on perwinsize](#26-testing-on-perwinsize)
        - [About doing plots in Matplotlib](#about-doing-plots-in-matplotlib)
                    - [Fri May 19 20:22:10 PHT 2017](#fri-may-19-202210-pht-2017)
        - [210 212 addition to perview hogparams](#210-212-addition-to-perview-hogparams)
- [Mon May 22 09:13:56 PHT 2017](#mon-may-22-091356-pht-2017)
    - [Scripts to run in Server](#scripts-to-run-in-server)
        - [Batch A](#batch-a)
        - [Batch B](#batch-b)
        - [Batch C](#batch-c)
        - [Batch D [NO TIME, SAY UFPR04 SINCE CLOSEST TO EEEI DATASETS]](#batch-d-no-time-say-ufpr04-since-closest-to-eeei-datasets)
- [Batch E](#batch-e)
    - [Results to create graphs/charts](#results-to-create-graphscharts)
        - [HoG](#hog)
            - [perwinsize](#perwinsize)
        - [LBP](#lbp)
    - [Scripts to code](#scripts-to-code)
- [Experiment Notes (for Methodology)](#experiment-notes-for-methodology)
    - [Experiment #0](#experiment-0)
    - [Experiment #1](#experiment-1)
- [Methodology F](#methodology-f)

<!-- /TOC -->
# Mon Mar 20 08:57:57 PHT 2017
- [X] Create adjust for window size script for nums-to-paths.py
    - generate-window.py

# Mon Mar 20 09:41:58 PHT 2017
- [X]Create a script that will generate roi JSON from window JSON
    - Can select if will crop from:
        - upper-left (for now ito muna, since it's just from 0,0)
        - upper-center
        - upper-right
        - lower-left
        - lower-center
        - lower-right
    - generate-window-pos.py

# Mon Mar 20 10:45:33 PHT 2017
- [X] Create extract-hog script that would load from generate-window-pos.py

# Mon Mar 20 10:45:55 PHT 2017
- [ ] Test training and testing with train-svm-trainingset.py
    - [X] Test with left-1 occupied and unoccupied
    - [ ] Test with left-1 occupied and ALL unoccupied
    - [X] Test with left-1 occupied and neg: unoccupied UFPR04-left-1 and other categories
    - [X] Test with left-1 occupied and neg: unoccupied UFPR04 and other categories
    

# Mon Mar 20 10:58:04 PHT 2017
- [X] Create script that would generate partial neg paths file from pos paths
    - generate-occ-to-neg.py

# Mon Mar 20 11:52:12 PHT 2017
Need to go back to window sizes of UFPR04 and see if ok na sila for testing.

# Mon Mar 20 14:16:10 PHT 2017
- [X] Test for entire of UFPR04
    - [x] left-1
    - [x] left-2
    - [x] left-3
    - [X] center-1
    - [X] center-2
    - [X] center-3
    - [X] right-1
    - [X] right-2
    - [X] right-3

# Mon Mar 20 17:05:13 PHT 2017
- [X] Created automated pipeline script of up to HoG

# Tue Mar 21 13:19:37 PHT 2017
- [X] Create a script that would separate a given category into train, cv and test paths. Use the output JSON of generate-window-pos.py
    - sep-pos-json.py
- [ ] Modify train-svm-trainingset-mult to accomodate for these different foldered training, test, and cv HoG features


# Tue Mar 21 16:25:11 PHT 2017
- [ ] Create a version of train-svm-mult that would output a model
    - use with the segregated version.
    - so... use the train-svm-traingingset mult instead :))
- [X] Create a version of train-svm-mult that would load a model, a folder of hog features, and an output
    - test-model-hog.py
- [X] Create a version of train-svm-mult that would load a trained model, and pos and neg folders and output the same things as train-svm-mult
    - test-svm-trainingset.py       
- [X] Modify train-svm-mult to include manual percentages


# Tue Mar 21 18:55:44 PHT 2017
- [X] Create script that would evaluate a parking space image
    - For now, use the UFPR04 models and dataset as test input.
- [X] attach window size to name of model file. 
    - [X] change script of training to reflect this

# Thu Mar 23 13:27:22 PHT 2017
- [X] Type out prototype 1 plan, fitting in our components
- [X] Code an integratable part for HoG + SVM
    - [X] use json(?) for making a dictionary of models to use


# Fri Mar 24 16:12:03 PHT 2017
- [X] Install packages on RPi
     - [ ] RPi 1
     - [X] RPi 2
     - [X] RPi 3
- [X] Test runing testing on RPi
    - [X] Note seconds run
- [X] Prepare a few data samples for testing HoG+SVM
    - [X] label data samples
    - [X] store log into json(?)
- [X] Test the whole prototype
    - [X] HoG + SVM part

- [ ] Better categorization for UFPR04
- [ ] Create an automated training script for doing doing all 9 models for UFPR04
- [ ] Train larger models on Carl's PC
- [ ] Categorize UFPR05s
- [ ] Categorize PUC
- [ ] See if the 2 sets are ready for training
    - [ ] Train UFPR05 as its own set
    - [ ] Train PUC as its own set
- [ ] Analyze the different categories for the 3 sets
    - [ ] See which could be included as except sets etc


# Thu Apr 13 19:40:34 PHT 2017 [TODOS]
- [X] Create script that would allow for marking of segmented parking images, from each image in a certain dataset (that has same parking lot space coordinates), as either occupied or unoccupied.
    - mark-dataset.py
    - # Thu Apr 13 20:55:59 PHT 2017
- [X] Create script for testing a classfier, given a testing dataset
    - eval-classifier.py
    - # Thu Apr 13 23:56:17 PHT 2017
- [X] Create script that would show results based on json script from above
    - eval-results.py
    - # Fri Apr 14 00:59:23 PHT 2017
    - [ ] Do evaluation for each parking ID
- [X] Create script that would output folder of images with marked parking lots and predictions
    - images-results.py
    - # Fri Apr 14 00:59:45 PHT 2017
- [X] Create linking script for above
    - eval_all.py
    - # Fri Apr 14 02:17:11 PHT 2017
- [X] Mark EEEI solar dataset (the one on USB red drive)
    - [X] Mark the first batch (parking line based)
        - eeei_5th_flr_solar_a_morning_parkinglinebased.json
        - # Fri Apr 14 02:29:09 PHT 2017
    - [X] Mark the first batch (top of car based)
        - eeei_5th_flr_solar_a_morning_topcarbased.json
        - # Fri Apr 14 03:17:18 PHT 2017
- [X] Script for generating marked and segmented images
    - segmented_images.py
    - # Fri Apr 14 03:18:03 PHT 2017

# Fri Apr 14 20:01:56 PHT 2017 [TODOS]
- [X] Look into Linear SVMs and how it could be improved from Real World Machine Learning (Prateek)
    - [X] Read on SVMs from Real World Machine Learning (Prateek) # Sat Apr 15 14:31:39 PHT 2017
        - Done: *** Sat Apr 15 16:10:37 PHT 2017 ***

- [X] Reading papers
    - Start time: *** Sat Apr 15 16:10:37 PHT 2017 ***
    - End time: *** Sun Apr 16 17:01:42 +08 2017 ***
- [X] Read about using Boosting algorithms for classification from Prateek book
    - Start time: *** Sun Apr 16 17:22:00 +08 2017 ***
    - End time: *** Sun Apr 16 23:03:39 +08 2017 ***

# Mon Apr 17 09:10:37 PHT 2017 [TODOS]
- [X] Clean up folder system and naming system, too cluttered and even I forget what to do 
    - [X] test-on-dataset-scripts
        - [X] clean up folder structure
        - [X] adjust scripts
        - [X] Test scripts to finalize
        - check for reproducibility
        - check for capability of handling mulitple experiments
        - End time: *** Mon Apr 17 17:02:43 +08 2017 ***
    - [X] categorize scripts
        - [X] clean up folder structure
        - [X] adjust scripts
        - [X] Test scripts to finalize
        - check for reproducibility
        - check for capability of handling mulitple experiments
        - Start time: *** Mon Apr 17 18:34:47 +08 2017 ***
        - End time: *** Tue Apr 18 00:16:27 +08 2017 ***
    - [X] training scripts
        - [X] clean up folder structure
        - [X] adjust scripts
        - [X] Test scripts to finalize
        - check for reproducibility
        - check for capability of handling mulitple experiments
        - [X] turn the scripts into functions
        - Start time: *** Tue Apr 18 00:21:31 +08 2017 ***
        - End time:  *** Thu Apr 20 21:08:47 +08 2017 ***
    - Start time: *** Mon Apr 17 09:54:57 +08 2017 ***
    - End time: *** Thu Apr 20 21:08:47 +08 2017 ***

- [X] Clean up plan for training testing and evaluating (most of which I already have code for)
    - [X] Create a README for how to do the procedures that need to be done



- [X] Fix the algo for cropping the images

# Fri Apr 21 08:17:33 PHT 2017 [TODOS]
- hog_svm_script fixes/add-ons
    - [X] pipeline-per-category.py
        - [X] Add save to text file for command results for pipleine-per-category.py
        - [X] Make argument based, so that running on smartwire server would be smooth
    - [X] Fix code for separating list of paths into partition with equal distribution (Must work with ALL)
- [X] Write up plan for training with Smartwire server
    - [X] List down the diff tests I would need to do
- [X] Train for optimal hyperparameters

# Wed May 3 14:31:19 PHT 2017
- [X]  Do modifications for the first eval_scripts where scores and detection extractions are being done, but uses the first triggered algo
    - [X] Do modification where dict of hog settings is being passed by function
    - Start time (time i started tracking i need to get work done): *** Wed May 3 19:33:47 +08 2017 ***

- [X] Test if results of changing the threshold to 0.7 for window width is ok for eval_scripts with parking line based, see if detections would improve
    - Results
        - Improved for whole car detections
        - However, due to the size of the image, some are being missed.
        - If moved to 0.6, will detect parts of car as a "whole car" due to similar HoG
- [x] Test one model 40x60 with solar morning a parking lot and top car based set
    - [X] test with first triggered algo
        - there's really just one model anyway
        - [X] put in fix that puts in break for 70% width
- [X] Test UFPR04 multi-model with solar mornign a parking lot and top car based set
    - [X] test with first triggered algo, extract relevant pics
        - [X] w_except
        - [X] wo_except
    - [ ] test with best score algo, extract relevant pics


# Fri May 5 14:54:21 PHT 2017
- [X] Rerun blk-2_cell-6 and blk-4_cell-4 testing and sort detections into folders by which is triggered
    - [X] Modify saving of detections to sort into folders by model triggered
    - [X] Run test for 5th solar morning parkingline
- [X] Group left, center, and right together (since they have similar window sizes if not the same)
    - [X] Run training for blk-2_cell-6
    - [X] Run training for blk-4_cell-4


[Note]: I noticed a discrepancy, I've actually used w_div for evaluating the models for hog_params :O


# Sat May 6 18:32:45 PHT 2017
- [X] Test for blk-4_cell-4
    - [X] perview
        - [X] 0.7 width threshold
            - [X] ready script
        - [X] 0.8 width threshold   
            - [X] ready script
        - [X] running
             - start time: *** Sun May 7 00:31:03 +08 2017 ***
        - [X] gather results
- [X] Test for blk-2_cell-6
    - [X] perview
        - [X] 0.7 width threshold
            - [X] ready script
        - [X] 0.8 width thresholds
            - [X] ready script
        - [X] running
             - start time: *** Sun May 7 00:31:03 +08 2017 ***
        - [X] gather results

- [X] Test for blk-3_cell-6
    - [X] perview
        - [X] 0.6 width threshold
            - [x] ready script
        - [X] 0.7 width threshold
            - [X] ready script
        - [X] 0.8 width thresholds
            - [X] ready script
    - [X] perwinsize
        - [X] Train models
            - [X] ready script
            - [X] running
                 - start time: *** Sun May 7 00:31:03 +08 2017 ***
        - [X] 0.6 width threshold
            - [X] ready script
            - [X] running
        - [X] 0.7 width threshold
            - [X] ready script
            - [X] running
        - [X] gather results

- [X] Run current tests with removing the "balanced" parameter for LinearSVC
    - [X] for per view
        - [X] ready scripts
        - [X] running 
            - start time: *** Sun May 7 00:31:03 +08 2017 ***
        - [X] test
            - test with 0.70
            - [X] ready script
            - [X] running
      - [X] gather results
    - [X] for per window size
        - [X] ready scripts
        - [X] running
             - start time: *** Sun May 7 00:31:03 +08 2017 ***
        - [X] test
            - test with 0.70
            - [X] ready script
            - [X] running
        - [X] gather results


# Sun May 7 13:34:40 PHT 2017
- [X] Change the format of the models into a list so that order can be considered

- [X] Do script for extracting a sample of HoG given a setting sample from a certain views
    - just uses directly from sorted nums
    - does not use window size
    - start: *** Sun May 7 15:29:35 +08 2017 ***
    - end: *** Sun May 7 16:32:33 +08 2017 ***

# Mon May 8 17:32:17 PHT 2017

- [X] Check for best SVM parameters 
    - check with.... itself lol

- [X] Test training perwinsize 36 with orientations 18 and C=1.0
    - [X] train
        - [X] runnning script
    - [X] test
        - [X] running script
- [X] Test training perwinsize 36 with orientations 18 and C=0.1
    - [X] train
        - [X] runnning script
    - [X] test
        - [X] running script
- [X] Test training perwinsize 36 with C = 0.1
    - [X] train
        - [X] runnning script
    - [X] test
        - [X] running script

- [X] Test perwinsize 36 on random picture
    - Result: it was kind of noisy, like with the picture of the model it detected basically parts of the model


# Tue May 9 07:27:10 PHT 2017
- [X] Change training script such that json for hog is passed on. Then eliminate need for having multiple folders -- for perwinsize and lib1

- [X] Create training script that would flip samples horizontally as well
    - [X] Run this script while doing LBP
    - start time: *** Tue May 9 17:13:54 +08 2017 ***
    - end time: *** Tue May 9 17:37:53 +08 2017 ***
    - [X] Test this with solar dataset
        - [X] running test

- [X] Test LBP
- [X] Implement a method of training with SVM with LBP


# Wed May 10 05:27:10 PHT 2017
- [X] Mark lines for the other 5th solar data
- [X] Mark lines for CNL dataset
- [X] Run tests for solar data
- [X] Run tests for CNL dataset


# Thurs May 12 16:25:59 PHT 2017
- [X] Do the sockets thing for web


# Fri May 12 16:25:59 PHT 2017 

- [X] While doing below, implement the testing of the latest implementation of perwinsize 3-6 with the datasets
    - [X] create json file for each dataset


# Sat May 13 19:34:36 PHT 2017
- [X] Ran script for testing perview 36 on all datasets
- [X] Run 36 perwinsize with having the other winsize as except. And then have C=0.1
    - [X] running script
    - [X] test script with all datasets

- [X] Find a way to do the LBP part lol.



- [ ] Sort UFPR05 and see if it can be used to better svm
- [ ] Sort PUC and see if it can be used to better svm



- [ ] Create script that would test for individual views


- [ ] Find learning curve lol. Or plot it. Get parameters.
    - *** A ***
    - [ ] So find a way to merge training and testing
- [ ] Try to do PCA for the thing
    - can perhaps do


- [ ] See if it would be posible to feed representative examples from each view into the models instead
- [ ] Test the methods with Exemplar to see if it would be better to pick a few choices from each view, and the rest be negative examples
    - [ ] unoccupied examples
    - [ ] unoccupied examples and from other major categories


- [ ] Do json file for each of the datasets.
- [ ] Test implemneted version on dataset 


- [ ] Modify training script for HoG only, LBP only, and HoG+LBP cascade
    - [X] HoG Only (fix the scripts for the views)
    - [X] adapt for LBP only
    - [ ] Merged HoG + LBP
- [ ] Testing script modifications


- [ ] Do HoG+LBP cascade

- [ ] Test multiplying a representative from each view with its SVM to see what parts the model is acquiring



- [ ] Check if it would be better to normalize the entire parking space before croppping and evaluating each space
    - Check with 0.7 width threshold

# Mon May 15 21:06:27 PHT 2017
Figures to generate
- [X] hog params (3d) [for the testing on eeei_5th_solar_041017_a_morning]
    - [X] precision
    - [X] recall
    - [X] f1-score
    - [!] There's something wrong with evaluating models for 2-10 and 2-12
        - Should also evaluate 2-6
- [X] lbp params (3d) [for testing within models]
    - [X] left
        - [X] precision
        - [X] recall
        - [X] f1-score
    - [X] center
        - [ ] precision
        - [ ] recall
        - [X] f1-score
    - [X] right
        - [ ] precision
        - [ ] recall
        - [X] f1-score
- [ ] figure to show that for per view, hog and attached car part is the same (photoshop) [!]
    - [X] had a version of this for 1st draft
    - [ ] do a more accurate version
- [ ] figure to show that for per window size, hog and attached car part is the same (photoshop) [!]

- [ ] table of results for perwinsize flip (table generator)  [!]
- [ ] table of results for perwinsize not flip (table generator) 
- [X] table of results for testing LBP with eeei_5th_solar_041017_a_morning  [!]
- [X] mock up of mobile (photoshop)  [!]
- [X] mock up of web version (photoshop)

if has time do results for
- [ ] table of results for perview 
- [ ] table of results for perwinsize no flip w except
- [ ] hog params for models only (3d)


# Tue May 16 18:46:39 PHT 2017
- [X] Study how to do a progressive web app
    - [X] Do google's codelab tutorial on PWA
    - [X] Read on how callbacks and javascript's functions actually work
- [X] Read google's tutorial on promises
- [X] Do a simplistic web app for Parakeet hosted at parakeet.sandnox.com
- [X] See how to do the sockets, using my laptop as the client

###### Wed May 17 20:23:10 PHT 2017
- [X] Design website for parakeet
- [X] Simulate usage with proxy data from my laptop

- [X] Do mock-up for mobile site on Sketch
    - [X] mobile view
    - [X] desktop view


# Thu May 18 16:52:06 PHT 2017
### Web app
- [X] Do fix for adding streets and other markers for 4th solar view
- [X] Integrate with implemented node server for RPI 
    - Test tomorrow!

### 26 testing on perwinsize
- [X] Run w flip on all datasets to compare with 36 w flip wo except
    - [ ] log results

### About doing plots in Matplotlib
- Do data analysis in excel first, then do plots in matplotlib after final results have been gathered


###### Fri May 19 20:22:10 PHT 2017
### 210 212 addition to perview hogparams
- [ ] Add results to perview w except and wo except notes
- [X] Update excel file for 210-212 data on perview wo except

# Mon May 22 09:13:56 PHT 2017
## Scripts to run in Server
### Batch A
-  Train 44 w flip C=0.01
    - [X] ran
    - [X] done
- Test 44 w except wo except all datasets
    - [X] ran
    - [ ] done
- Test 36 w except wo except all datasets 
    - [X] ran
    - [X] done

### Batch B
- Test 44 w flip all datasets
    - [X] ran
    - [X] done
- Train 36 w flip C=0.01
    - [X] ran
    - [X] done [use old results!]

### Batch C
- Test 36 w flip
    - [X] ran [use old results!]
    - [X] done [use old results!]


### Batch D [NO TIME, SAY UFPR04 SINCE CLOSEST TO EEEI DATASETS]
- [ ] Analyze UFPR05 to integrate with existing window sizes
- [ ] Analyze PUC to integrate with existing window sizes
- [ ] Train UFPR05, and PUC with UFPR04, using only select samples from each

# Batch E
- [ ] Test Batch D final HoG on all datasets


## Results to create graphs/charts


### HoG
- [ ] perview hogparams
- [ ] perview hogparams on eeei 5th solar

#### perwinsize
- [X] perwinsize hogparams
- [ ] perwinsize (2, 6), (3, 6), (4, 4) on all datasets


### LBP
- [ ] LBP params
- [ ] LBP results


## Scripts to code
- [ ] Analyze UFPR05 to integrate with existing window sizes
- [ ] Analyze PUC to integrate with existing window sizes
- [ ] Train UFPR05, and PUC with UFPR04, using only select samples from each

- [ ] Concat hog+lbp


- [ ] Integrate 2d to 3d for bridge dataset with img_rectify. Test HoG classifier



- [ ] Multi-level LBP
- [ ] hog + multi-level lbp concat


# Experiment Notes (for Methodology)
## Experiment #0
## Experiment #1
Testing perview with 

# Methodology F