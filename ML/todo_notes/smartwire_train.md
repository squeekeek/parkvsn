

## Plan for training with Smartwire server
### Considerations

####[HALTED, test with view-based SVM training first with UFPR04]
#### When using a folder of images as unoccupied space (and with the usual positive images from PKLot):
- [ ] Create modified scripts to use for folders of images (to be used with pedestrian images, for negative features)
    - [ ] 
    - In the end, only needs to have extracted features of HoG of a certain window size
    - Basically, create a version of the scripts similar to the process of using PKLot for training and testing.
    - For analysis:
        - Check the dimensions of the cropped images to get an idea of their relative size
        - Generate paths file same format as generate_unsorted.py
        - Get a 
    - For 

#### [Halted, test with view-based SVM training first with UFPR04]
#### When using a folder of images as occupied and unoccupied space:

### Discarded ToDo: *** Sat Apr 29 23:36:30 +08 2017 ***
- [ ] Change hog evaluation to opencv. (This first) [CANCELLED DUE TO IT ALSO PROHIBITING USING LINEARSVM BY SKLEARN]
    - [ ] Hard code implementation first (similar to current params)
        - [ ] For training
        - [ ] For testing
        - [ ] Test on dataset with top 4 best results so far with the sklearn hog extraction
            - [ ] Time comparison test
            - [ ] Compare with:
                -
                -
                -
                -
            - Results:

### UFPR04 only, view-based
- [X] Do for each view, e.g. "left-1", and the rest as well as unoccupied as negatives


-------------------------------------------------------------------------------


# Tests done such that if one identifier says that something is a car, then it is a car, regardless of confidence level.

### Halfway models results
041417_halfwaymodels_results_eeei_5th_solar_041017_a_morning_parkinglinebased.txt
Features in dataset (100%):
Count: 1728

Average time in pred: 0.167400186123
Max time in pred: 2.34418487549
Min time in pred: 0.0111699104309

             precision    recall  f1-score   support

      Empty       0.82      0.40      0.54       258
   Occupied       0.90      0.98      0.94      1470

avg / total       0.89      0.90      0.88      1728


041417_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased.txt
Features in dataset (100%):
Count: 1728

Average time in pred: 0.219008786397
Max time in pred: 2.56608200073
Min time in pred: 0.0306260585785

             precision    recall  f1-score   support

      Empty       0.99      0.29      0.45       258
   Occupied       0.89      1.00      0.94      1470

avg / total       0.90      0.89      0.87      1728

### UFPR04, left-1 
#### left-1, against all
one-view-against-all_results_eeei_5th_solar_041017_a_morning_topcarbased04212017_1631.txt
Features in dataset (100%):
Count: 1728

Average time in pred: 0.143229080709
Max time in pred: 0.239885091782
Min time in pred: 0.0642189979553

             precision    recall  f1-score   support

      Empty       0.29      0.71      0.41       258
   Occupied       0.93      0.69      0.80      1470

avg / total       0.84      0.70      0.74      1728


#### left-1, against all except left
one-view-against-all_left_results_eeei_5th_solar_041017_a_morning_topcarbased04212017_1716.txt
Features in dataset (100%):
Count: 1728


Average time in pred: 0.148360634568
Max time in pred: 0.246634960175
Min time in pred: 0.0662457942963

             precision    recall  f1-score   support

      Empty       0.65      0.74      0.69       258
   Occupied       0.95      0.93      0.94      1470

avg / total       0.91      0.90      0.90      1728


### UFPR04 only, major view-based
- [X] Do for each view, e.g. "left-1", and the rest as well as unoccupied as negatives

- [X] Train all models, for major view, w div
    - Start time: *** Sat Apr 22 15:46:48 +08 2017 ***
- [X] Testing all models, for major view, w div
    - [X] one-view-against-all_except-major_results_eeei_5th_solar_041017_a_morning_topcarbased04222017_1749.txt
        - Start time: *** Sat Apr 22 ***
        - End time: *** Sat Apr 22 ***
        - Results:
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.488775969793
            Max time in pred: 2.90299701691
            Min time in pred: 0.0599460601807

                        precision    recall  f1-score   support

               Empty       0.93      0.60      0.73       258
            Occupied       0.93      0.99      0.96      1470

            avg / total       0.93      0.93      0.93      1728

    - [X] one-view-against-all_except-major_results_eeei_5th_solar_041017_a_morning_parkinglinebased04222017_1918.txt
        - Start Time: *** Sat Apr 22 ***
        - End Time: *** Sat Apr 22 ***
        - Results:
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.420865031442
            Max time in pred: 4.76777005196
            Min time in pred: 0.0257158279419

                        precision    recall  f1-score   support

                Empty       0.76      0.64      0.70       258
             Occupied       0.94      0.96      0.95      1470

            avg / total       0.91      0.92      0.91      1728
- [X] Readied HoG script
- [X] Extracted HoG
    - Start time: *** Sun Apr 23 ***
    - End time: *** Sun Apr 23 15:57:07 +08 2017 ***


- [X] Train all models, for major view w except, wo div
    - [X] Readied script
        - Start time: *** Sun Apr 23 16:56:59 +08 2017 ***
        - End time: *** Sun Apr 23 17:41:11 +08 2017 ***
    - Start time: *** Sun Apr 23 17:41:29 +08 2017 ***
    - End time: *** Sun Apr 23 19:51:35 +08 2017 ***
- [X] ^ Testing 
    - [X] eeei_5th_solar_041017_a_morning_parkinglinebased
        -  [X] Readied script
            ovaa_except-major_wo_div-parking_line_results_eeei_5th_solar_041017_a_morning_parkinglinebased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.353593044121
            Max time in pred: 4.91400504112
            Min time in pred: 0.0259759426117

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.600775193798
            Occupied: 0.972108843537

            Confusion Report:
            [[ 155  103]
            [  41 1429]]
            True Negative: 155
            False Negative: 41
            False Positive: 103 
            True Positive: 1429 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.0278911564626
            False Positive Rate: 0.399224806202

                        precision    recall  f1-score   support

                Empty     0.7908    0.6008    0.6828       258
            Occupied     0.9328    0.9721    0.9520      1470

            avg / total     0.9116    0.9167    0.9118      1728


    - [X] eeei_5th_solar_041017_a_morning_topcarbased
        - [X] Readied script
            Features in dataset (100%):
            ovaa_except-major_wo_div-top_car_results_eeei_5th_solar_041017_a_morning_topcarbased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.44154671759
            Max time in pred: 3.36283898354
            Min time in pred: 0.0672540664673

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.604651162791
            Occupied: 0.997278911565

            Confusion Report:
            [[ 156  102]
            [   4 1466]]
            True Negative: 156
            False Negative: 4
            False Positive: 102 
            True Positive: 1466 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.00272108843537
            False Positive Rate: 0.395348837209

                        precision    recall  f1-score   support

                Empty     0.9750    0.6047    0.7464       258
            Occupied     0.9349    0.9973    0.9651      1470

            avg / total     0.9409    0.9387    0.9325      1728




- [X] Train all models per view, pos cars, neg unoccupied, w div
    - [X] Readied script
    - Start time: *** Sun Apr 23 17:41:31 +08 2017 ***
    - End time:*** Sun Apr 23 19:55:31 +08 2017 ***
- [X] ^ Testing 
    - [X] eeei_5th_solar_041017_a_morning_parkinglinebased
        - [X] Readied script
            ovaa_wo_except-parking_line_results_eeei_5th_solar_041017_a_morning_parkinglinebased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.621407820947
            Max time in pred: 5.15539383888
            Min time in pred: 0.0306069850922

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.593023255814
            Occupied: 1.0

            Confusion Report:
            [[ 153  105]
            [   0 1470]]
            True Negative: 153
            False Negative: 0
            False Positive: 105 
            True Positive: 1470 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.0
            False Positive Rate: 0.406976744186

                        precision    recall  f1-score   support

                Empty     1.0000    0.5930    0.7445       258
            Occupied     0.9333    1.0000    0.9655      1470

            avg / total     0.9433    0.9392    0.9325      1728



    - [X] eeei_5th_solar_041017_a_morning_topcarbased
        - [X] Readied script
            ovaa_wo_except-top_car_results_eeei_5th_solar_041017_a_morning_topcarbased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.528134858443
            Max time in pred: 3.72187495232
            Min time in pred: 0.067578792572

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.294573643411
            Occupied: 0.999319727891

            Confusion Report:
            [[  76  182]
            [   1 1469]]
            True Negative: 76
            False Negative: 1
            False Positive: 182 
            True Positive: 1469 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.000680272108844
            False Positive Rate: 0.705426356589

                        precision    recall  f1-score   support

                Empty     0.9870      0.2946    0.4537       258
             Occupied     0.8898    0.9993    0.9414      1470

            avg / total     0.9043    0.8941    0.8686      1728



- [X] Train all models per view, pos cars, neg unoccupied, wo div
    - [X] Readied script
    - Start time: *** Sun Apr 23 17:41:35 +08 2017 ***
    - End time: *** Sun Apr 23 19:55:35 +08 2017 ***

- [X] ^ Testing 
    - [X] eeei_5th_solar_041017_a_morning_parkinglinebased
        - [X] Readied script
            ovaa_wo_except_wo_div-parking_line_results_eeei_5th_solar_041017_a_morning_parkinglinebased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.343334735544
            Max time in pred: 4.9453959465
            Min time in pred: 0.0256597995758

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.612403100775
            Occupied: 1.0

            Confusion Report:
            [[ 158  100]
            [   0 1470]]
            True Negative: 158
            False Negative: 0
            False Positive: 100 
            True Positive: 1470 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.0
            False Positive Rate: 0.387596899225

                        precision    recall  f1-score   support

                Empty     1.0000    0.6124    0.7596       258
            Occupied     0.9363    1.0000    0.9671      1470

            avg / total     0.9458    0.9421    0.9361      1728



    - [X] eeei_5th_solar_041017_a_morning_topcarbased
        - [X] Readied script
            ovaa_wo_except_wo_div-top_car_results_eeei_5th_solar_041017_a_morning_topcarbased04252017_0852
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.352528833681
            Max time in pred: 3.35829091072
            Min time in pred: 0.0619049072266

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.383720930233
            Occupied: 1.0

            Confusion Report:
            [[  99  159]
            [   0 1470]]
            True Negative: 99
            False Negative: 0
            False Positive: 159 
            True Positive: 1470 

            (aim to reduce FN rate - misclassify occupied as empty)
            False Negative Rate: 0.0
            False Positive Rate: 0.616279069767

                        precision    recall  f1-score   support

                Empty     1.0000    0.3837    0.5546       258
            Occupied     0.9024    1.0000    0.9487      1470

            avg / total     0.9170    0.9080    0.8899      1728




-------------------------------------------------------------------------------
## For one-model_w40h60

- [X] Readied HoG Script
- [X] Extracted HoG
    - Start time: *** Sun Apr 23 17:41:40 +08 2017 ***
    - End time: *** Sun Apr 23 19:51:17 +08 2017 ***

- [X] Train a model, with w40, h60 for all views, w div
    - [X] Readied script
    - Training
        - Start time: *** Mon Apr 24 00:26:36 +08 2017 ***
        - End time: *** Mon Apr 24 00:26:36 +08 2017 ***
- [X] ^ Testing 
    - [X] eeei_5th_solar_041017_a_morning_parkinglinebased
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.396283033407
            Max time in pred: 1.55664896965
            Min time in pred: 0.00071382522583

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.294573643411
            Occupied: 0.777551020408

                        precision    recall  f1-score   support

                Empty     0.1886    0.2946    0.2300       258
             Occupied     0.8626    0.7776    0.8179      1470

            avg / total     0.7620    0.7054    0.7301      1728


    - [X] eeei_5th_solar_041017_a_morning_topcarbased
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.26071789502
            Max time in pred: 0.837333917618
            Min time in pred: 0.000777006149292

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.093023255814
            Occupied: 0.885034013605

                        precision    recall  f1-score   support

                Empty     0.1244    0.0930    0.1064       258
             Occupied     0.8476    0.8850    0.8659      1470

            avg / total     0.7396    0.7668    0.7525      1728



- [X] Train a model, with w40, h60 for all views, wo div
    - [X] Readied script
    - Start time: *** Mon Apr 24 00:26:36 +08 2017 ***
    - End time: *** Mon Apr 24 00:26:36 +08 2017 ***
- [X] ^ Testing 
    - [X] eeei_5th_solar_041017_a_morning_parkinglinebased
    - Results:
            Features in dataset (100%):
            Count: 1728

            Average time in pred: 0.380696986285
            Max time in pred: 1.64439082146
            Min time in pred: 0.000712156295776

            Accuracies for each class (predicted_of_class/actual_of_class):
            Empty: 0.313953488372
            Occupied: 0.777551020408

                        precision    recall  f1-score   support

                Empty     0.1985    0.3140    0.2432       258
             Occupied     0.8659    0.7776    0.8194      1470

            avg / total     0.7663    0.7083    0.7333      1728


    - [X] eeei_5th_solar_041017_a_morning_topcarbased
        Features in dataset (100%):
        Count: 1728

        Average time in pred: 0.260533451482
        Max time in pred: 0.889959812164
        Min time in pred: 0.000774145126343

        Accuracies for each class (predicted_of_class/actual_of_class):
        Empty: 0.104651162791
        Occupied: 0.885034013605

                    precision    recall  f1-score   support

            Empty     0.1378    0.1047    0.1189       258
         Occupied     0.8492    0.8850    0.8668      1470

        avg / total     0.7430    0.7685    0.7551      1728




-------------------------------------------------------------------------------

## Smartwire Folders

PKlot folder
/home/agatha/Documents/datasets/PKLot/PKLotSegmented

-------------------------------------------------------------------------------

# ToDo's for Improvement

###### Sun Apr 23 19:58:58 PHT 2017
## Notes
Need to see how the behavior of top car and parkingline based is, I'm getting conflicting results for it. The topcar with the except-major also gets 60% for recall.

Also see about getting more negative images.

Need to see extracted images later.

###### Mon Apr 24 14:07:19 PHT 2017
## Notes
Currently I'm getting a 60% error rate for misclassifying occupied cars as empty.

Btw need to check on sliding window size. And if I'm resizing the images to a certian scale before doing the sliding window.

Hard mine from negative images with sliding window.
Add negative images with objects commonly found on parking lots.
- Can use dataset from caltech with the pedestrians.
- Would include people, buidlings, etc.


Double data by flipping on vertical and axis.
- So by doing this, I therefore disregard the views

###### Tue Apr 25 08:30:14 PHT 2017
## Immediate changes
- [X] Put change in eval_results.py that also counts true positive, false positive, true negative, false negative
- [X] Create script that extracts marked images from dataset (for checking if marking is correct, huhu I just discovered a mistake I made)
- [X] Re-run image result scripts
     - [X] Create script
     - [X] Run script on smartwire server
- [X] Create central configs with json
    - [X] apply to relevant scripts
- [X] Change from pyramid_gaussian to pyramid from helpers (to all relevant scripts)


### For UFPR04 for now

#### Done:
- [X] Create script for finding hog parameters
    - [X] the preprocess-training script
    - [X] the pipeline script
    - [X] apply the horizontal and vertical for negative mining
    - [ ] apply image pyramid to except occ-to-neg with horizontal and vertical (skipped for now)
    - [X] make training scripts into functions
- [X] Make sure above script is working
    - start time of testing preprocess script: *** Sun Apr 30 09:33:50 +08 2017 ***
    - end time of testing preprocess script: 
- [X] Run training script
- [X] Create script generator for producing to be filled .sh with different testing sets.
    - Start time *** Sun Apr 30 09:34:03 +08 2017 ***
    - End Time: *** Sun Apr 30 13:44:37 +08 2017 ***

- [X] Create pipeline script: extracting, training

- [X] Train for finding optimal HoG Parameters
- [X] Test for finding optimal HoG parameters

- [X] Put timing script for the pipeline script
- [X] Put timing script for training script

- [ ] Create script for finding svm parameters
    - [ ] One optimal svm parameters are found, 

- [X] Create script that extracts images with the detections on it

- [X] For future testing and training, put in thing that if folder exists, do not redo it.

## Extract Hog changes
- [ ] Flip each sample vertically for more data. Also flip horizontally.

#### After these above things, I could train, and then move on to changing things for testing

## Testing script changes
### Notes
Instead of just breaking once an occupancy has been detected, try to see which models are triggered.

Although, it would be better if the models were more accurate such that they stick to their views.
Will try this.
Then see what detections each models triggers and see if there's sense.

Also try about mixing views into same window size after the above.

Also put in script that write the detection in the outputs when triggered.

### Todos
- [X] Put identifier that determines which model is triggered for an identification
- [ ] Create version of images_results.py from pipeline testing that also puts marks of the identified cars at the segmented pictures
- [ ] Expose confidence score and log for every identification
- [X] Create new classifying script that would pick classification of classifier among the 9 that has highest confidence

## For test data
- [ ] Mark parking spaces for the empty spaces dataset
- [ ] Mark parking spaces for CNL dataset

## HoG Gameplan
- [ ] With current models, see if it would be better to rely on confidence scores instead ()
- [ ] Find optimal HoG parameters (using UFPR04)
- [ ] Test result when merged with same window sizes (using UFPR04)
- [ ] Get a conclusion from this
- [ ] Use setting with optimal HoG parameters, to test for optimal SVM parameters
- [ ] Finalize Ho

- [X] Analyze PUC
    - [X] See if it could be run with just one window size
    - Result:

## Testing for finding HoG parameters


## Testing for the one model PUC dataset 40x60



## [ ] Move on to LBP after doing what I can with HoG for Prototype V2
## Applying LBP
### Notes:
I have some pdfs on Mendeley for this.

Utilize my scripts for HoG. Just replace with LBP 



# Notes on combining HoG and LBP
Lipetski uses two classifiers, HoG and LBP to detect the presence of a vehicle.
If both are ok, then it is a car.
They also use a time ring, like if there's 5 consecutive same detections, then the prediction must be ok.

In a previous paper, Lipetski also implements an HoG cascade, with each stage increasing in number of features. It's said that this would be faster. My understanding about this is that one would use a lesser/weaker classifer that may instead detect an obstruction, and once an obstruction is detected, a feature vector with more features would be used instead. 

http://ieeexplore.ieee.org.ezproxy.engglib.upd.edu.ph/document/5597274/references
There'a another paper using HoG and LBP for head detection. Also uses PCA towards the end to filter out noise and have a smaller feature vector.


The LBP is an excellent texture descriptor for its invariance to gray-scale and rotation. We extend the work of [11], and use multilevel block structured LBP to describe the head-shoulders of people. The same as Figure 3(c), we divide the gray image of the input image into blocks at three levels. Then, we calculate the histograms of the LBP patterns for each block. The LBP patterns we used is LBP28,1 (Figure 4), which denotes that 8 points with radius 1 are sampled for each pixel, and the number of 0−1 transitions is at most 2. A binary pattern is called uniform pattern if the binary pattern contains at most two 0−1 transitions.

For each block at one level, pixels in the block with different uniform patterns are voted into different bins and all of the nonuniform patterns are voted into one bin. We then use L2-Hys normalization scheme for the histograms of the blocks, which is better performance than L1-sqrt normalization scheme used in [4] (by 5% with a false alarm of 10−4). Finally, the three LBP feature vector corresponding to the three levels are concatenated into the final 4956-D multilevel LBP feature vector.

## Notes on implementation
For now will test the HoG then LBP approach.
Would first test LBP with the dataset.
Then the HoG-LBP cascade.

Then will try the PCA version.

Also would see the results of counting on the part based handling for occlusions.

