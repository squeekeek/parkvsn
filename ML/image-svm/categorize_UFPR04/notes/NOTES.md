
python nums-to-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -j UFPR04_sort_nums.json > UFPR04_paths.txt

python view-sorted.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -j UFPR04_sort_nums.json

python sizes.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c ALL -j UFPR04_pos_paths.json > UFPR04_sizes.txt

Can combine center-1 and center-2

- [X] left-2: 80 x 30
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c left-2 -j UFPR04_pos_paths.json -w 30 -ht 80 -crop y

- [X] left-1: 80 x 30
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c left-1 -j UFPR04_pos_paths.json -w 30 -ht 80 -crop y

10 - 80 x 40 (moved to left-3)
11 - 80 x 30
12 - 80 x 30 
13 - 80 x 30
14 - 80 x 30 

- [X] left-3: 80 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c left-3 -j UFPR04_pos_paths.json -w 40 -ht 80 -crop y

center-1: 50 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c center-1 -j UFPR04_pos_paths.json -w 40 -ht 50 -crop y

consider moving 25 to 28 to center-2

center-2: 50 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c center-2 -j UFPR04_pos_paths.json -w 40 -ht 50 -crop y

center-3: 50 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c center-3 -j UFPR04_pos_paths.json -w 40 -ht 50 -crop y

right-2: 80 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c right-2 -j UFPR04_pos_paths.json -w 40 -ht 80 -crop y

right-3: 80 x 40
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c right-3 -j UFPR04_pos_paths.json -w 40 -ht 80 -crop y

right-1: 80x50
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c right-1 -j UFPR04_pos_paths.json -w 50 -ht 80 -crop y


python extract-roi.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c left-1 -j UFPR04_pos_paths.json -w 30 -ht 80 -cont n














Category Window Sizes:
left-1: 80 x 30
left-2: 80 x 30
left-3: 80 x 40
right-1: 80 x 50
right-2: 80 x 40
right-3: 80 x 40
center-1: 50 x 40
center-2: 50 x 40
center-3: 50 x 40


















