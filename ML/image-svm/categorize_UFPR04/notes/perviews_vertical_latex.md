\begin{table}[]
\centering
\caption{My caption}
\label{my-label}
\begin{tabular}{|lcc|}
\hline
\multicolumn{1}{|c|}{Views} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}Window Size\\ (rows, columns)\end{tabular}} & \# of occupied images \\ \hline
left-1                      & (80, 30)                                                                                   & 6393                  \\
left-2                      & (80, 30)                                                                                   & 1469                  \\
left-3                      & (80, 40)                                                                                   & 2112                  \\
center-1                    & (50, 40)                                                                                   & 11528                 \\
center-2                    & (50, 40)                                                                                   & 7434                  \\
center-3                    & (50, 40)                                                                                   & 1699                  \\
right-1                     & (80, 50)                                                                                   & 2001                  \\
right-2                     & (80, 40)                                                                                   & 7473                  \\
right-3                     & (80, 40)                                                                                   & 6016                  \\ \hline
\end{tabular}
\end{table}