"""
    Outputs JSON files to positive and negative paths, categorized according 
    to the JSON file of sorted numbers with sort-nums.py

    Example Usage:
    python nums-to-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -j UFPR04_sort_nums.py


"""
import argparse 
import glob
import os
import json
import io
import sys


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05, ALL",
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to list of numbers to parse",
        required=True)
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]

    # load json dict
    with open(json_path) as data_file:
        category_dict = json.load(data_file)

    print category_dict
    # Create paths dict
    seq = []
    for key, values in category_dict.iteritems():
        seq.append(key)

    paths_pos_dict = category_dict.fromkeys(seq)
    print "paths_pos_dict: {}".format(paths_pos_dict) 

    for key, values in paths_pos_dict.iteritems():
        paths_pos_dict[key] = []

    #paths_neg_dict = paths_pos_dict
    paths_neg_dict = category_dict.fromkeys(seq)

    for key, values in paths_neg_dict.iteritems():
        paths_neg_dict[key] = []

    if set_option == "ALL":
        neg_path = os.path.join(str(pklot_path), "*", "*", "*", "Empty", "*")
        pos_path = os.path.join(str(pklot_path), "*", "*", "*", "Occupied", "*")
    else:
        neg_path = os.path.join(str(pklot_path), str(set_option), "*", "*", "Empty", "*")
        pos_path = os.path.join(str(pklot_path),  str(set_option), "*", "*", "Occupied", "*")

    print neg_path
    print pos_path
    neg_num = 0
    pos_num = 0

    for im_path in glob.glob(neg_path):
        check_num = im_path.split('#')[1].split('.')[0]
        check_num_int = int(check_num)

        # check where the number is categorized
        for key, value in category_dict.iteritems():
            if check_num_int in category_dict[key]:
                paths_neg_dict[key].append(im_path.split("PKLotSegmented")[1])

        neg_num = neg_num + 1
        if neg_num % 1000 == 0:
            print neg_num

    for im_path in glob.glob(pos_path):
        # parse path for image number
        check_num = im_path.split('#')[1].split('.')[0]
        check_num_int = int(check_num)

        # check where the number is categorized
        for key, value in category_dict.iteritems():
            if check_num_int in category_dict[key]:
                paths_pos_dict[key].append(im_path.split("PKLotSegmented")[1])

        pos_num = pos_num + 1
        if pos_num % 1000 == 0:
            print pos_num

    print "Option chosen: " + str(set_option)
    print "Number of Neg images: " + str(neg_num)
    print "Number of Pos images: " + str(pos_num)
    print ""
 
    print "Number of negative images per category: "
    for key, value in paths_neg_dict.iteritems():
        print "{}: {}".format(key, len(paths_neg_dict[key]))
    print ""
    print "Number of positive images per category: "
    for key, value in paths_pos_dict.iteritems():
        print "{}: {}".format(key, len(paths_pos_dict[key]))

    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    # Write JSON file
    filename_neg = set_option + "_neg_paths.json"
    filename_pos = set_option + "_pos_paths.json"
    with io.open(filename_neg, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        paths_neg_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    with io.open(filename_pos, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        paths_pos_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))