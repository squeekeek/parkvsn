"""
    Use this to sort the parking lot spaces into similar views
"""

import argparse
import glob
import os
import cv2
import json
import io
import sys

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk',
        "--pklot_path", help="Path to PkLot dataset",
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    args = vars(ap.parse_args())

    # Parsing pathnames to datasets
    pklot_path = args["pklot_path"]
    set_option = args["set_option"]

    neg_path = os.path.join(str(pklot_path), set_option, "*", "*", "Empty", "*")
    pos_path = os.path.join(str(pklot_path), set_option, "*", "*", "Occupied", "*")

    set_length = 0
    if set_option == "UFPR04":
        set_length = 28
    elif set_option == "UFPR05":
        # set_length = 45
        set_length = 40
    elif set_option == "PUC":
        set_length = 100
    else:
        print "Invalid -s argument."
        print ""
        sys.exit()


    CATEGORY = {
        1: "left-1",
        2: "center-1",
        3: "right-1",
        4: "left-2",
        5: "center-2",
        6: "right-2",
        7: "left-3",
        8: "center-3",
        9: "right-3"
    }

    desc = (
        "1: {}\n"
        "2: {}\n"
        "3: {}\n"
        "4: {}\n"
        "5: {}\n"
        "6: {}\n"
        "7: {}\n"
        "8: {}\n"
        "9: {}\n"
    ).format(
        CATEGORY[1], CATEGORY[2], CATEGORY[3],
        CATEGORY[4], CATEGORY[5], CATEGORY[6],
        CATEGORY[7], CATEGORY[8], CATEGORY[9]
    )




    KEY_1 = ord('1')
    KEY_2 = ord('2')
    KEY_3 = ord('3')
    KEY_4 = ord('4')
    KEY_5 = ord('5')
    KEY_6 = ord('6')
    KEY_7 = ord('7')
    KEY_8 = ord('8')
    KEY_9 = ord('9')
    


    cat_1 = "1: {}".format(CATEGORY[1])
    cat_2 = "2: {}".format(CATEGORY[2])
    cat_3 = "3: {}".format(CATEGORY[3])
    cat_4 = "4: {}".format(CATEGORY[4])
    cat_5 = "5: {}".format(CATEGORY[5])
    cat_6 = "6: {}".format(CATEGORY[6])
    cat_7 = "7: {}".format(CATEGORY[7])
    cat_8 = "8: {}".format(CATEGORY[8])
    cat_9 = "9: {}".format(CATEGORY[9])

    cv2.namedWindow(cat_1)
    cv2.namedWindow(cat_2)
    cv2.namedWindow(cat_3)
    cv2.namedWindow(cat_4)
    cv2.namedWindow(cat_5)
    cv2.namedWindow(cat_6)
    cv2.namedWindow(cat_7)
    cv2.namedWindow(cat_8)
    cv2.namedWindow(cat_9)

    # Load json file for storing skipped image paths
    num_sorted = []
    sorted_filename = "skipped_" +  set_option + "_sort_nums.json"

    if os.path.exists(sorted_filename):
        print "Continuing sorting from {}".format(sorted_filename)
        with open(sorted_filename) as data_file:
            dict = json.load(data_file)
            for i, key in enumerate(dict):
                for j, num in enumerate(dict[key]):
                    num_sorted.append(num)
            print "Lots sorted before: {}".format(len(num_sorted))
    else:
        print "Creating new json file for sorting"
        dict = {
            CATEGORY[1]: [],
            CATEGORY[2]: [],
            CATEGORY[3]: [],
            CATEGORY[4]: [],
            CATEGORY[5]: [],
            CATEGORY[6]: [],
            CATEGORY[7]: [],
            CATEGORY[8]: [],
            CATEGORY[9]: []
        }
 
    skipped = 0

    # only look at occupied cars
    print pos_path
    for im_path in glob.glob(pos_path):
        if len(num_sorted) == set_length:
            break

        # parse path for image number
        check_num = im_path.split('#')[1].split('.')[0]
        check_num_int = int(check_num)

        # check if image number was already encountered
        if num_sorted.count(check_num_int) > 0:
            continue
        else:
            num_sorted.append(check_num_int)

            image = cv2.imread(im_path)
            sep_path = im_path.split(pklot_path)[1]

            while 1:
                print im_path
                print desc
                cv2.imshow(check_num, image)
                key = cv2.waitKey(0)
                cv2.destroyWindow(check_num)

                if key == KEY_1:
                    dict[CATEGORY[1]].append(check_num_int)
                    cv2.imshow(cat_1, image)
                    break
                elif key == KEY_2:
                    dict[CATEGORY[2]].append(check_num_int)
                    cv2.imshow(cat_2, image)
                    break
                elif key == KEY_3:
                    dict[CATEGORY[3]].append(check_num_int)
                    cv2.imshow(cat_3, image)
                    break
                elif key == KEY_4:
                    dict[CATEGORY[4]].append(check_num_int)
                    cv2.imshow(cat_4, image)
                    break
                elif key == KEY_5:
                    dict[CATEGORY[5]].append(check_num_int)
                    cv2.imshow(cat_5, image)
                    break
                elif key == KEY_6:
                    dict[CATEGORY[6]].append(check_num_int)
                    cv2.imshow(cat_6, image)
                    break
                elif key == KEY_7:
                    dict[CATEGORY[7]].append(check_num_int)
                    cv2.imshow(cat_7, image)
                    break
                elif key == KEY_8:
                    dict[CATEGORY[8]].append(check_num_int)
                    cv2.imshow(cat_8, image)
                    break
                elif key == KEY_9:
                    dict[CATEGORY[9]].append(check_num_int)
                    cv2.imshow(cat_9, image)
                    break
                elif key == ord("q"):
                    print "You would need to restart sorting numbers."
                    sys.exit()
                elif key == ord("0"):
                    # skip
                    skipped  = 1
                    print "Skip"
                    break
                else:
                    print "Invalid key. Press 1~9, or 0 only."

            # print details so far
            print "Current Details"
            print "Current image path: {}".format(im_path)
            print "Images sorted: {} / {}".format(
                len(num_sorted), set_length)
            print "Sort count:"

            desc = (
                "1: {}: {}\n"
                "2: {}: {}\n"
                "3: {}: {}\n"
                "4: {}: {}\n"
                "5: {}: {}\n"
                "6: {}: {}\n"
                "7: {}: {}\n"
                "8: {}: {}\n"
                "9: {}: {}\n"
            ).format(
                CATEGORY[1], len(dict[CATEGORY[1]]),
                CATEGORY[2], len(dict[CATEGORY[2]]),
                CATEGORY[3], len(dict[CATEGORY[3]]),
                CATEGORY[4], len(dict[CATEGORY[4]]),
                CATEGORY[5], len(dict[CATEGORY[5]]),
                CATEGORY[6], len(dict[CATEGORY[6]]),
                CATEGORY[7], len(dict[CATEGORY[7]]),
                CATEGORY[8], len(dict[CATEGORY[8]]),
                CATEGORY[9], len(dict[CATEGORY[9]])
            )
    print desc
    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    # Write JSON file
    skipped_filename = "skipped_" +  set_option + "_sort_nums.json"
    if skipped == 1:
        filename = skipped_filename
    else:
        filename = set_option + "_sort_nums.json"
        if os.path.exists(skipped_filename):
            os.remove(skipped_filename)
    with io.open(filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))
