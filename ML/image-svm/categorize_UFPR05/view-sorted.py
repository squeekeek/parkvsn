"""
Save all sorted images with sort-nums.py into distinct folders for viewiing.
"""

import argparse
import glob
import os
import cv2
import json
import io
import sys
import shutil


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk',
        "--pklot_path", help="Path to PkLot dataset",
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of sorted numbers to load",
        required=True)
    args = vars(ap.parse_args())

    test_pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    set_option = args["set_option"]

    if os.path.exists(json_path):
        print "Viewing images from: {}".format(json_path)
        with open(json_path) as data_file:
            sorted_dict = json.load(data_file)
    else:
        print "Json path does not exist"

    pklot_path = test_pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    pos_path = os.path.join(str(pklot_path), set_option, "*", "*", "Occupied", "*")
    save_path = "sorted_nums_" + set_option

    set_length = 0
    if set_option == "UFPR04":
        set_length = 28
    elif set_option == "UFPR05":
        # set_length = 45
        set_length = 40
    elif set_option == "PUC":
        set_length = 100
    else:
        print "Invalid -s argument."
        print ""
        sys.exit()

    num_sorted = []
    sorted_directory = "sorted_" + set_option + "_images"
    if os.path.isdir(sorted_directory):
        shutil.rmtree(sorted_directory)

    os.makedirs(sorted_directory)
    for i, category in enumerate(sorted_dict):
        if len(sorted_dict[category]) > 0:
            new_dir = os.path.join(sorted_directory, category)
            os.makedirs(new_dir)

    for im_path in glob.glob(pos_path):
        if len(num_sorted) == set_length:
            break

        # parse path for image number
        check_num = im_path.split('#')[1].split('.')[0]
        check_num_int = int(check_num)  

        if check_num_int in num_sorted:
            continue
        else:
           
            num_sorted.append(check_num_int)
            for i, category in enumerate(sorted_dict):
                if check_num_int in sorted_dict[category]:
                    print("{}: {}".format(category, check_num_int))
                    print("{}/{} copied".format(len(num_sorted), set_length))
                    dest_path = os.path.join(sorted_directory, category)
                    copyFile(im_path, dest_path)
