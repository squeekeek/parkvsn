"""
    - Use this to have info on the minimum, maximum and
    average dimensions of each category, or all categories.
    - Use this with the JSON file containing positive/occupied lots.
"""
import argparse 
import glob
import os
import json
import io
import cv2
import sys

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05, ALL",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL"
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to list of paths",
        required=True)
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]

    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []

    if category == "ALL":
        to_extract = json_paths.keys()
    else:
        to_extract.append(category)


    # instance variables
    num_images = 0
    min_dim = [float('Inf'), float('Inf')]
    max_dim = [0, 0]
    ave_dim = [0, 9]

    actual_pklot = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    print actual_pklot
    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        if len(json_paths[key]) == 0:
            continue
        for j, partial_path in enumerate(json_paths[key]):
            # do something
            im_path = actual_pklot + partial_path
            #im_path = os.path.join(actual_pklot, partial_path)
          
            image = cv2.imread(im_path)

            width = image.shape[1]
            height = image.shape[0]
            dim = width*height
            if dim < (min_dim[0]*min_dim[1]):
                min_dim[0] = height
                min_dim[1] = width 
                min_path = im_path
            if dim > (max_dim[0]*max_dim[1]):
                max_dim[0] = height
                max_dim[1] = width 
                max_path = im_path
            ave_dim[0] = ave_dim[0] + height
            ave_dim[1] = ave_dim[1] + width

            num_images = num_images + 1

        ave_dim[0] = ave_dim[0]/num_images
        ave_dim[1] = ave_dim[1]/num_images
        print "########################"
        print "Category: " + str(key)
        print "Number of images: " + str(num_images)
        print "Image Dimensions:"
        print ("Min. Dim: [{}, {}]").format(min_dim[0], min_dim[1])
        print ("Min Path: {}").format(min_path)
        print ("Max. Dim: [{}, {}]").format(max_dim[0], max_dim[1])
        print ("Max Path: {}").format(max_path)
        print ("Ave. Dim: [{}, {}]").format(ave_dim[0], ave_dim[1])
        print ""
        print "########################"
        # Reset all variables
        ave_dim[0] = 0
        ave_dim[1] = 0
        num_images = 0
        min_dim = [float('Inf'), float('Inf')]
        max_dim = [0, 0]
        ave_dim = [0, 9]




