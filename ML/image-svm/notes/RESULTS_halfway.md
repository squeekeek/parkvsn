#####################################################

### UFPR04: left-1 
positive features: left-1 occupied
negative features: left-1 unoccupied

Features in dataset (100%):
Count: 15073

Features in training dataset (60%):
Count: 9043
Counter({0: 5220, 1: 3823})

Features in cross-validation dataset (20%):
Count: 3015
Counter({0: 1731, 1: 1284})

Features in test dataset (20%):
Count: 3015
Counter({0: 1729, 1: 1286})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1731
   Occupied       1.00      1.00      1.00      1284

avg / total       1.00      1.00      1.00      3015

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1729
   Occupied       1.00      1.00      1.00      1286

avg / total       1.00      1.00      1.00      3015

#####################################################

### UFPR04: left-1 
positive features: left-1 occupied
negative features: left-1 unoccupied, center and right occupied categories of UFPR04

Features in dataset (100%):
Count: 51224

Features in training dataset (60%):
Count: 30734
Counter({0: 26937, 1: 3797})

Features in cross-validation dataset (20%):
Count: 10245
Counter({0: 8941, 1: 1304})

Features in test dataset (20%):
Count: 10245
Counter({0: 8953, 1: 1292})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8941
   Occupied       1.00      1.00      1.00      1304

avg / total       1.00      1.00      1.00     10245

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8953
   Occupied       1.00      1.00      1.00      1292

avg / total       1.00      1.00      1.00     10245

#####################################################

### UFPR04: left-2 
positive features: left-2 occupied
negative features: left-2 unoccupied

Features in dataset (100%):
Count: 3764

Features in training dataset (60%):
Count: 2258
Counter({0: 1366, 1: 892})

Features in cross-validation dataset (20%):
Count: 753
Counter({0: 467, 1: 286})

Features in test dataset (20%):
Count: 753
Counter({0: 462, 1: 291})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       467
   Occupied       1.00      1.00      1.00       286

avg / total       1.00      1.00      1.00       753

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       462
   Occupied       1.00      1.00      1.00       291

avg / total       1.00      1.00      1.00       753

#####################################################

### UFPR04: left-2
positive features: left-2 occupied
negative features: left-2 unoccupied, center and right occupied categories of UFPR04

Features in dataset (100%):
Count: 39915

Features in training dataset (60%):
Count: 23949
Counter({0: 23057, 1: 892})

Features in cross-validation dataset (20%):
Count: 7983
Counter({0: 7692, 1: 291})

Features in test dataset (20%):
Count: 7983
Counter({0: 7697, 1: 286})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7692
   Occupied       1.00      0.99      0.99       291

avg / total       1.00      1.00      1.00      7983

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7697
   Occupied       1.00      1.00      1.00       286

avg / total       1.00      1.00      1.00      7983

#####################################################

### UFPR04: left-3
positive features: left-3 occupied
negative features: left-3 unoccupied

Features in dataset (100%):
Count: 3751

Features in training dataset (60%):
Count: 2250
Counter({1: 1281, 0: 969})

Features in cross-validation dataset (20%):
Count: 751
Counter({1: 414, 0: 337})

Features in test dataset (20%):
Count: 750
Counter({1: 417, 0: 333})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       337
   Occupied       1.00      1.00      1.00       414

avg / total       1.00      1.00      1.00       751

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       333
   Occupied       1.00      1.00      1.00       417

avg / total       1.00      1.00      1.00       750

#####################################################

### UFPR04: left-3
positive features: left-3 occupied
negative features: left-3 unoccupied, center and right occupied categories of UFPR04

Features in dataset (100%):
Count: 39902

Features in training dataset (60%):
Count: 23941
Counter({0: 22668, 1: 1273})

Features in cross-validation dataset (20%):
Count: 7981
Counter({0: 7543, 1: 438})

Features in test dataset (20%):
Count: 7980
Counter({0: 7579, 1: 401})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7543
   Occupied       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00      7981

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7579
   Occupied       1.00      1.00      1.00       401

avg / total       1.00      1.00      1.00      7980

#####################################################

### UFPR04: right-1
positive features: right-1 occupied
negative features: right-1 unoccupied

Features in dataset (100%):
Count: 3790

Features in training dataset (60%):
Count: 2274
Counter({1: 1214, 0: 1060})

Features in cross-validation dataset (20%):
Count: 758
Counter({1: 410, 0: 348})

Features in test dataset (20%):
Count: 758
Counter({0: 381, 1: 377})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       348
   Occupied       1.00      1.00      1.00       410

avg / total       1.00      1.00      1.00       758

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       381
   Occupied       1.00      1.00      1.00       377

avg / total       1.00      1.00      1.00       758

#####################################################

### UFPR04: right-1
positive features: right-1 occupied
negative features: right-1 unoccupied, center and left occupied categories of UFPR04

Features in dataset (100%):
Count: 34425

Features in training dataset (60%):
Count: 20655
Counter({0: 19446, 1: 1209})

Features in cross-validation dataset (20%):
Count: 6885
Counter({0: 6496, 1: 389})

Features in test dataset (20%):
Count: 6885
Counter({0: 6482, 1: 403})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6496
   Occupied       1.00      1.00      1.00       389

avg / total       1.00      1.00      1.00      6885

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6482
   Occupied       1.00      1.00      1.00       403

avg / total       1.00      1.00      1.00      6885

#####################################################

### UFPR04: right-2
positive features: right-2 occupied
negative features: right-2 unoccupied

Features in dataset (100%):
Count: 15150

Features in training dataset (60%):
Count: 9090
Counter({0: 4612, 1: 4478})

Features in cross-validation dataset (20%):
Count: 3030
Counter({1: 1524, 0: 1506})

Features in test dataset (20%):
Count: 3030
Counter({0: 1559, 1: 1471})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1506
   Occupied       1.00      1.00      1.00      1524

avg / total       1.00      1.00      1.00      3030

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1559
   Occupied       1.00      1.00      1.00      1471

avg / total       1.00      1.00      1.00      3030

#####################################################

### UFPR04: right-2
positive features: right-2 occupied
negative features: right-2 unoccupied, center and left occupied categories of UFPR04

Features in dataset (100%):
Count: 34425

Features in training dataset (60%):
Count: 20655
Counter({0: 19446, 1: 1209})

Features in cross-validation dataset (20%):
Count: 6885
Counter({0: 6496, 1: 389})

Features in test dataset (20%):
Count: 6885
Counter({0: 6482, 1: 403})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6496
   Occupied       1.00      1.00      1.00       389

avg / total       1.00      1.00      1.00      6885

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6482
   Occupied       1.00      1.00      1.00       403

avg / total       1.00      1.00      1.00      6885

#####################################################

### UFPR04: right-3
positive features: right-3 occupied
negative features: right-3 unoccupied

Features in dataset (100%):
Count: 15099

Features in training dataset (60%):
Count: 9059
Counter({0: 5456, 1: 3603})

Features in cross-validation dataset (20%):
Count: 3020
Counter({0: 1791, 1: 1229})

Features in test dataset (20%):
Count: 3020
Counter({0: 1836, 1: 1184})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1791
   Occupied       1.00      1.00      1.00      1229

avg / total       1.00      1.00      1.00      3020

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1836
   Occupied       1.00      1.00      1.00      1184

avg / total       1.00      1.00      1.00      3020

#####################################################

### UFPR04: right-3 
positive features: right-3 occupied
negative features: right-3 unoccupied, center and left occupied catergories of UFPR04

Features in dataset (100%):
Count: 45734

Features in training dataset (60%):
Count: 27440
Counter({0: 23849, 1: 3591})

Features in cross-validation dataset (20%):
Count: 9147
Counter({0: 7929, 1: 1218})

Features in test dataset (20%):
Count: 9147
Counter({0: 7940, 1: 1207})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7929
   Occupied       1.00      1.00      1.00      1218

avg / total       1.00      1.00      1.00      9147

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7940
   Occupied       1.00      1.00      1.00      1207

avg / total       1.00      1.00      1.00      9147

#####################################################

#### UFPR04: center-1
positive features: center-1 occupied
negative features: center-1 unoccupied

Features in dataset (100%):
Count: 26509

Features in training dataset (60%):
Count: 15905
Counter({0: 8991, 1: 6914})

Features in cross-validation dataset (20%):
Count: 5302
Counter({0: 2954, 1: 2348})

Features in test dataset (20%):
Count: 5302
Counter({0: 3036, 1: 2266})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2954
   Occupied       1.00      1.00      1.00      2348

avg / total       1.00      1.00      1.00      5302

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      3036
   Occupied       1.00      1.00      1.00      2266

avg / total       1.00      1.00      1.00      5302

#####################################################

### UFPR04: center-1
positive features: center-1 occupied
negative features: center-1 unoccupied, left and right occupied categories of UFPRO4

Features in dataset (100%):
Count: 51973

Features in training dataset (60%):
Count: 31183
Counter({0: 24273, 1: 6910})

Features in cross-validation dataset (20%):
Count: 10395
Counter({0: 8096, 1: 2299})

Features in test dataset (20%):
Count: 10395
Counter({0: 8076, 1: 2319})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8096
   Occupied       1.00      1.00      1.00      2299

avg / total       1.00      1.00      1.00     10395

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8076
   Occupied       1.00      1.00      1.00      2319

avg / total       1.00      1.00      1.00     10395

#####################################################

### UFPR04: center-2
positive features: center-2 occupied
negative features: center-2 unoccupied

Features in dataset (100%):
Count: 18919

Features in training dataset (60%):
Count: 11351
Counter({0: 6893, 1: 4458})

Features in cross-validation dataset (20%):
Count: 3784
Counter({0: 2264, 1: 1520})

Features in test dataset (20%):
Count: 3784
Counter({0: 2328, 1: 1456})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2264
   Occupied       1.00      1.00      1.00      1520

avg / total       1.00      1.00      1.00      3784

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2328
   Occupied       1.00      1.00      1.00      1456

avg / total       1.00      1.00      1.00      3784

#####################################################

### UFPR04: center-2
positive features: center-2 occupied
negative features: center-2 unoccupied, left and right occupied categories of UFPR04

Features in dataset (100%):
Count: 44383

Features in training dataset (60%):
Count: 26629
Counter({0: 22202, 1: 4427})

Features in cross-validation dataset (20%):
Count: 8877
Counter({0: 7378, 1: 1499})

Features in test dataset (20%):
Count: 8877
Counter({0: 7369, 1: 1508})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7378
   Occupied       1.00      1.00      1.00      1499

avg / total       1.00      1.00      1.00      8877

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7369
   Occupied       1.00      1.00      1.00      1508

avg / total       1.00      1.00      1.00      8877

#####################################################

### UFPR04: center-3
positive features: center-3 occupied
negative features: center-3 unoccupied

Features in dataset (100%):
Count: 29252

Features in training dataset (60%):
Count: 17551
Counter({0: 16521, 1: 1030})

Features in cross-validation dataset (20%):
Count: 5851
Counter({0: 5482, 1: 369})

Features in test dataset (20%):
Count: 5850
Counter({0: 5550, 1: 300})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5482
   Occupied       1.00      1.00      1.00       369

avg / total       1.00      1.00      1.00      5851

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5550
   Occupied       0.99      1.00      1.00       300

avg / total       1.00      1.00      1.00      5850

#####################################################

### UFPR04: center-3
positive features: center-3 occupied
negative features: center-3 occupied, left and right categories of UFPR04

Features in dataset (100%):
Count: 29252

Features in training dataset (60%):
Count: 17551
Counter({0: 16521, 1: 1030})

Features in cross-validation dataset (20%):
Count: 5851
Counter({0: 5482, 1: 369})

Features in test dataset (20%):
Count: 5850
Counter({0: 5550, 1: 300})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5482
   Occupied       1.00      1.00      1.00       369

avg / total       1.00      1.00      1.00      5851

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5550
   Occupied       0.99      1.00      1.00       300

avg / total       1.00      1.00      1.00      5850

#####################################################




