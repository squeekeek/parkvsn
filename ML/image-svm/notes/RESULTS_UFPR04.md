###### Sun Mar 19 21:48:50 PHT 2017
**Test with exemplar**
Output: 
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_scripts$ python test-feats.py -f feats_roi_UFPR04_left-1 -m single14.model
feats_roi_UFPR04_left-1/2012-12-12_10_00_05#011.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_00_05#012.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_00_05#013.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_00_05#014.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_05_05#011.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_05_05#012.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_05_05#013.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_05_05#014.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_10_05#011.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_10_05#012.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_10_05#013.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_10_05#014.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_15_05#011.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_15_05#012.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_15_05#013.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_15_05#014.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_20_05#011.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_20_05#012.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_20_05#013.feat
Pred: [0]

feats_roi_UFPR04_left-1/2012-12-12_10_20_05#014.feat
Pred: [1]

feats_roi_UFPR04_left-1/2012-12-12_10_25_05#011.feat
Pred: [0]


###### Mon Mar 20 10:53:56 PHT 2017
 **Test with left-1 occupied and unoccupied**

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$  python train-svm-trainingset.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -m pos-left-1_neg-left-1
Training a Linear SVM Classifier
Classifier saved to pos-left-1_neg-left-1.model
Features in dataset (100%):
Count: 15073

Features in training dataset (60%):
Count: 9043
Counter({0: 5220, 1: 3823})

Features in cross-validation dataset (20%):
Count: 3015
Counter({0: 1731, 1: 1284})

Features in test dataset (20%):
Count: 3015
Counter({0: 1729, 1: 1286})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1731
   Occupied       1.00      1.00      1.00      1284

avg / total       1.00      1.00      1.00      3015

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1729
   Occupied       1.00      1.00      1.00      1286

avg / total       1.00      1.00      1.00      3015

###### Mon Mar 20 11:33:45 PHT 2017
**Pos: UFPR04 left-1 Neg: UFPR04 all except left**

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$     python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1_neg_left-1_except-left
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-1_neg_left-1_except-left.model
Features in dataset (100%):
Count: 51224

Features in training dataset (60%):
Count: 30734
Counter({0: 26937, 1: 3797})

Features in cross-validation dataset (20%):
Count: 10245
Counter({0: 8941, 1: 1304})

Features in test dataset (20%):
Count: 10245
Counter({0: 8953, 1: 1292})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8941
   Occupied       1.00      1.00      1.00      1304

avg / total       1.00      1.00      1.00     10245

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8953
   Occupied       1.00      1.00      1.00      1292

avg / total       1.00      1.00      1.00     10245

###### Mon Mar 20 14:31:06 PHT 2017
**UFPR04 left-2 self and unocc all**
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80  -m pos_left-2_neg_unocc
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-2_neg_unocc.model
Features in dataset (100%):
Count: 37620

Features in training dataset (60%):
Count: 22572
Counter({0: 21668, 1: 904})

Features in cross-validation dataset (20%):
Count: 7524
Counter({0: 7242, 1: 282})

Features in test dataset (20%):
Count: 7524
Counter({0: 7241, 1: 283})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7242
   Occupied       1.00      0.99      1.00       282

avg / total       1.00      1.00      1.00      7524

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7241
   Occupied       1.00      1.00      1.00       283

avg / total       1.00      1.00      1.00      7524


**UFPR04 left-2 self and unocc all, and except left**

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -n2  feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-2_neg_unocc_exceptleft
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-2_neg_unocc_exceptleft.model
Features in dataset (100%):
Count: 73771

Features in training dataset (60%):
Count: 44262
Counter({0: 43380, 1: 882})

Features in cross-validation dataset (20%):
Count: 14755
Counter({0: 14465, 1: 290})

Features in test dataset (20%):
Count: 14754
Counter({0: 14457, 1: 297})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     14465
   Occupied       1.00      1.00      1.00       290

avg / total       1.00      1.00      1.00     14755

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     14457
   Occupied       1.00      0.99      1.00       297

avg / total       1.00      1.00      1.00     14754 


### Left-1 unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -m pos_left-1neg_unocc_left-1
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-1neg_unocc_left-1.model
Features in dataset (100%):
Count: 15073

Features in training dataset (60%):
Count: 9043
Counter({0: 5220, 1: 3823})

Features in cross-validation dataset (20%):
Count: 3015
Counter({0: 1731, 1: 1284})

Features in test dataset (20%):
Count: 3015
Counter({0: 1729, 1: 1286})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1731
   Occupied       1.00      1.00      1.00      1284

avg / total       1.00      1.00      1.00      3015

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1729
   Occupied       1.00      1.00      1.00      1286

avg / total       1.00      1.00      1.00      3015


### Left-1 unocc and others

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1neg_unocc_left-1_left
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-1neg_unocc_left-1_left.model
Features in dataset (100%):
Count: 51224

Features in training dataset (60%):
Count: 30734
Counter({0: 26937, 1: 3797})

Features in cross-validation dataset (20%):
Count: 10245
Counter({0: 8941, 1: 1304})

Features in test dataset (20%):
Count: 10245
Counter({0: 8953, 1: 1292})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8941
   Occupied       1.00      1.00      1.00      1304

avg / total       1.00      1.00      1.00     10245

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8953
   Occupied       1.00      1.00      1.00      1292

avg / total       1.00      1.00      1.00     10245


### Left-2 Unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_left-2_UFPR04_neg_paths_2295_w-30h-80 -m pos_left-2neg_unocc_left-2
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-2neg_unocc_left-2.model
Features in dataset (100%):
Count: 3764

Features in training dataset (60%):
Count: 2258
Counter({0: 1366, 1: 892})

Features in cross-validation dataset (20%):
Count: 753
Counter({0: 467, 1: 286})

Features in test dataset (20%):
Count: 753
Counter({0: 462, 1: 291})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       467
   Occupied       1.00      1.00      1.00       286

avg / total       1.00      1.00      1.00       753

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       462
   Occupied       1.00      1.00      1.00       291

avg / total       1.00      1.00      1.00       753


#### Left-2 unocc and others

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_left-2_UFPR04_neg_paths_2295_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-2neg_unocc_left-2_left
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-2neg_unocc_left-2_left.model
Features in dataset (100%):
Count: 39915

Features in training dataset (60%):
Count: 23949
Counter({0: 23057, 1: 892})

Features in cross-validation dataset (20%):
Count: 7983
Counter({0: 7692, 1: 291})

Features in test dataset (20%):
Count: 7983
Counter({0: 7697, 1: 286})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7692
   Occupied       1.00      0.99      0.99       291

avg / total       1.00      1.00      1.00      7983

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7697
   Occupied       1.00      1.00      1.00       286

avg / total       1.00      1.00      1.00      7983


**UFPR04 left-3: unocc (left-3) and self**
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$  python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 -n feats_pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80 -m pos_left-3_neg_unocc
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-3_neg_unocc.model
Features in dataset (100%):
Count: 3751

Features in training dataset (60%):
Count: 2250
Counter({1: 1281, 0: 969})

Features in cross-validation dataset (20%):
Count: 751
Counter({1: 414, 0: 337})

Features in test dataset (20%):
Count: 750
Counter({1: 417, 0: 333})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       337
   Occupied       1.00      1.00      1.00       414

avg / total       1.00      1.00      1.00       751

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       333
   Occupied       1.00      1.00      1.00       417

avg / total       1.00      1.00      1.00       750

**UFPR04 left-3: unocc (left-3), except left and self**

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$  python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 -n feats_pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80 -m pos_left-3_neg_unocc_exceptleft
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_left-3_neg_unocc_exceptleft.model
Features in dataset (100%):
Count: 39902

Features in training dataset (60%):
Count: 23941
Counter({0: 22668, 1: 1273})

Features in cross-validation dataset (20%):
Count: 7981
Counter({0: 7543, 1: 438})

Features in test dataset (20%):
Count: 7980
Counter({0: 7579, 1: 401})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7543
   Occupied       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00      7981

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7579
   Occupied       1.00      1.00      1.00       401

avg / total       1.00      1.00      1.00      7980

**UFPR04 right-1 and unocc**
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 -n feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80  -m pos_right-1_neg_unocc_right-1

No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-1_neg_unocc_right-1.model
Features in dataset (100%):
Count: 3790

Features in training dataset (60%):
Count: 2274
Counter({1: 1214, 0: 1060})

Features in cross-validation dataset (20%):
Count: 758
Counter({1: 410, 0: 348})

Features in test dataset (20%):
Count: 758
Counter({0: 381, 1: 377})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       348
   Occupied       1.00      1.00      1.00       410

avg / total       1.00      1.00      1.00       758

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       381
   Occupied       1.00      1.00      1.00       377

avg / total       1.00      1.00      1.00       758

**UFPR04 right, occupied and others**
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 -n feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-1_neg_unocc_right-1_except-right
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-1_neg_unocc_right-1_except-right.model
Features in dataset (100%):
Count: 34425

Features in training dataset (60%):
Count: 20655
Counter({0: 19446, 1: 1209})

Features in cross-validation dataset (20%):
Count: 6885
Counter({0: 6496, 1: 389})

Features in test dataset (20%):
Count: 6885
Counter({0: 6482, 1: 403})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6496
   Occupied       1.00      1.00      1.00       389

avg / total       1.00      1.00      1.00      6885

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6482
   Occupied       1.00      1.00      1.00       403

avg / total       1.00      1.00      1.00      6885

### right-2 unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 -n feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80 -m pos_right-2neg_unocc_right-2

No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-2neg_unocc_right-2.model
Features in dataset (100%):
Count: 15150

Features in training dataset (60%):
Count: 9090
Counter({0: 4612, 1: 4478})

Features in cross-validation dataset (20%):
Count: 3030
Counter({1: 1524, 0: 1506})

Features in test dataset (20%):
Count: 3030
Counter({0: 1559, 1: 1471})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1506
   Occupied       1.00      1.00      1.00      1524

avg / total       1.00      1.00      1.00      3030

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1559
   Occupied       1.00      1.00      1.00      1471

avg / total       1.00      1.00      1.00      3030

## right-2 unocc and others
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 -n feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-1_neg_unocc_right-1_except-right
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-1_neg_unocc_right-1_except-right.model
Features in dataset (100%):
Count: 34425

Features in training dataset (60%):
Count: 20655
Counter({0: 19446, 1: 1209})

Features in cross-validation dataset (20%):
Count: 6885
Counter({0: 6496, 1: 389})

Features in test dataset (20%):
Count: 6885
Counter({0: 6482, 1: 403})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6496
   Occupied       1.00      1.00      1.00       389

avg / total       1.00      1.00      1.00      6885

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      6482
   Occupied       1.00      1.00      1.00       403

avg / total       1.00      1.00      1.00      6885



### Sanity check
Digits in dataset (100%):
Count: 70000

Digits in training dataset (60%):
Count: 42000
Counter({1: 4711, 7: 4432, 3: 4296, 2: 4221, 9: 4160, 8: 4145, 6: 4097, 0: 4090, 4: 4081, 5: 3767})

Digits in cross-validation dataset (20%):
Count: 14000
Counter({1: 1542, 3: 1462, 7: 1424, 2: 1416, 9: 1407, 4: 1382, 6: 1378, 0: 1371, 8: 1355, 5: 1263})

Digits in test dataset (20%):
Count: 14000
Counter({1: 1624, 0: 1442, 7: 1437, 6: 1401, 9: 1391, 3: 1383, 4: 1361, 2: 1353, 8: 1325, 5: 1283})

For cross-validation set (20%):
             precision    recall  f1-score   support

    Class-0       0.98      1.00      0.99      1371
    Class-1       0.99      0.98      0.99      1542
    Class-2       0.97      0.98      0.97      1416
    Class-3       0.98      0.97      0.98      1462
    Class-4       0.98      0.98      0.98      1382
    Class-5       0.98      0.98      0.98      1263
    Class-6       0.99      0.99      0.99      1378
    Class-7       0.97      0.98      0.98      1424
    Class-8       0.96      0.97      0.96      1355
    Class-9       0.97      0.96      0.97      1407

avg / total       0.98      0.98      0.98     14000

For test set (20%):
             precision    recall  f1-score   support

    Class-0       0.98      0.99      0.99      1442
    Class-1       0.99      0.99      0.99      1624
    Class-2       0.97      0.97      0.97      1353
    Class-3       0.98      0.98      0.98      1383
    Class-4       0.99      0.97      0.98      1361
    Class-5       0.98      0.98      0.98      1283
    Class-6       0.99      0.99      0.99      1401
    Class-7       0.98      0.98      0.98      1437
    Class-8       0.96      0.96      0.96      1325
    Class-9       0.96      0.97      0.96      1391

avg / total       0.98      0.98      0.98     14000


#### Right-3 unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 -n feats
_pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80 -m pos_right-3neg_unocc_right-3
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-3neg_unocc_right-3.model
Features in dataset (100%):
Count: 15099

Features in training dataset (60%):
Count: 9059
Counter({0: 5456, 1: 3603})

Features in cross-validation dataset (20%):
Count: 3020
Counter({0: 1791, 1: 1229})

Features in test dataset (20%):
Count: 3020
Counter({0: 1836, 1: 1184})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1791
   Occupied       1.00      1.00      1.00      1229

avg / total       1.00      1.00      1.00      3020

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      1836
   Occupied       1.00      1.00      1.00      1184

avg / total       1.00      1.00      1.00      3020

#### Right-3 unocc and others
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 -n feats
_pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-3neg_unocc_right-3_right
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_right-3neg_unocc_right-3_right.model
Features in dataset (100%):
Count: 45734

Features in training dataset (60%):
Count: 27440
Counter({0: 23849, 1: 3591})

Features in cross-validation dataset (20%):
Count: 9147
Counter({0: 7929, 1: 1218})

Features in test dataset (20%):
Count: 9147
Counter({0: 7940, 1: 1207})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7929
   Occupied       1.00      1.00      1.00      1218

avg / total       1.00      1.00      1.00      9147

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7940
   Occupied       1.00      1.00      1.00      1207

avg / total       1.00      1.00      1.00      9147


#### Center-1 unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50 -n feats_pos_upper-left_genwin_center-1_UFPR04_neg_paths_14981_w-40h-50 -m pos_center-1neg_unocc_center-1
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-1neg_unocc_center-1.model
Features in dataset (100%):
Count: 26509

Features in training dataset (60%):
Count: 15905
Counter({0: 8991, 1: 6914})

Features in cross-validation dataset (20%):
Count: 5302
Counter({0: 2954, 1: 2348})

Features in test dataset (20%):
Count: 5302
Counter({0: 3036, 1: 2266})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2954
   Occupied       1.00      1.00      1.00      2348

avg / total       1.00      1.00      1.00      5302

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      3036
   Occupied       1.00      1.00      1.00      2266

avg / total       1.00      1.00      1.00      5302

### center-1 unocc and others
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50 -n feats_pos_upper-left_genwin_center-1_UFPR04_neg_paths_14981_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-1neg_unocc_center-1_center
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-1neg_unocc_center-1_center.model
Features in dataset (100%):
Count: 51973

Features in training dataset (60%):
Count: 31183
Counter({0: 24273, 1: 6910})

Features in cross-validation dataset (20%):
Count: 10395
Counter({0: 8096, 1: 2299})

Features in test dataset (20%):
Count: 10395
Counter({0: 8076, 1: 2319})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8096
   Occupied       1.00      1.00      1.00      2299

avg / total       1.00      1.00      1.00     10395

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      8076
   Occupied       1.00      1.00      1.00      2319

avg / total       1.00      1.00      1.00     10395


#### Center-2 unocc

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50 -n feats_pos_upper-left_genwin_center-2_UFPR04_neg_paths_11485_w-40h-50 -m pos_center-2neg_unocc_center-2
No neg paths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-2neg_unocc_center-2.model
Features in dataset (100%):
Count: 18919

Features in training dataset (60%):
Count: 11351
Counter({0: 6893, 1: 4458})

Features in cross-validation dataset (20%):
Count: 3784
Counter({0: 2264, 1: 1520})

Features in test dataset (20%):
Count: 3784
Counter({0: 2328, 1: 1456})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2264
   Occupied       1.00      1.00      1.00      1520

avg / total       1.00      1.00      1.00      3784

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      2328
   Occupied       1.00      1.00      1.00      1456

avg / total       1.00      1.00      1.00      3784

#### Center-2 unocc and others

(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50 -n feats_pos_upper-left_genwin_center-2_UFPR04_neg_paths_11485_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-2neg_unocc_center-2_center
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-2neg_unocc_center-2_center.model
Features in dataset (100%):
Count: 44383

Features in training dataset (60%):
Count: 26629
Counter({0: 22202, 1: 4427})

Features in cross-validation dataset (20%):
Count: 8877
Counter({0: 7378, 1: 1499})

Features in test dataset (20%):
Count: 8877
Counter({0: 7369, 1: 1508})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7378
   Occupied       1.00      1.00      1.00      1499

avg / total       1.00      1.00      1.00      8877

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      7369
   Occupied       1.00      1.00      1.00      1508

avg / total       1.00      1.00      1.00      8877


#### Center-3 Unocc
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50 -n feats_pos_upper-left_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-3neg_unocc_center-3_center
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-3neg_unocc_center-3_center.model
Features in dataset (100%):
Count: 29252

Features in training dataset (60%):
Count: 17551
Counter({0: 16521, 1: 1030})

Features in cross-validation dataset (20%):
Count: 5851
Counter({0: 5482, 1: 369})

Features in test dataset (20%):
Count: 5850
Counter({0: 5550, 1: 300})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5482
   Occupied       1.00      1.00      1.00       369

avg / total       1.00      1.00      1.00      5851

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5550
   Occupied       0.99      1.00      1.00       300

avg / total       1.00      1.00      1.00      5850

#### Center-3 unocc and others
(cv) agatha:~/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests$ python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50 -n feats_pos_upper-left_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-3neg_unocc_center-3_center
Has 2nd negpaths folder
Training a Linear SVM Classifier
Classifier saved to pos_center-3neg_unocc_center-3_center.model
Features in dataset (100%):
Count: 29252

Features in training dataset (60%):
Count: 17551
Counter({0: 16521, 1: 1030})

Features in cross-validation dataset (20%):
Count: 5851
Counter({0: 5482, 1: 369})

Features in test dataset (20%):
Count: 5850
Counter({0: 5550, 1: 300})

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5482
   Occupied       1.00      1.00      1.00       369

avg / total       1.00      1.00      1.00      5851

For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5550
   Occupied       0.99      1.00      1.00       300

avg / total       1.00      1.00      1.00      5850



