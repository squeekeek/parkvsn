<!-- TOC -->

- [blk-1_cell-4](#blk-1_cell-4)
- [blk-1_cell-6](#blk-1_cell-6)
    - [134 123, 1 1470](#134-123-1-1470)
- [blk-1_cell-8](#blk-1_cell-8)
    - [142 115, 1 1470](#142-115-1-1470)
- [blk-1_cell-10](#blk-1_cell-10)
- [blk-1_cell-12](#blk-1_cell-12)
    - [149 108, 0 1471](#149-108-0-1471)
- [blk-2_cell-4](#blk-2_cell-4)
    - [137 120, 1 1470](#137-120-1-1470)
- [blk-2_cell-6](#blk-2_cell-6)
    - [178 79, 4 1476](#178-79-4-1476)
    - [Competing](#competing)
- [blk-2_cell-8](#blk-2_cell-8)
    - [148 109, 2 1469](#148-109-2-1469)
- [blk-2_cell-10](#blk-2_cell-10)
- [blk-2_cell-12](#blk-2_cell-12)
- [blk-3_cell-4](#blk-3_cell-4)
- [blk-3_cell-6](#blk-3_cell-6)
    - [152 105, 0 1471](#152-105-0-1471)
- [blk-3_cell-8](#blk-3_cell-8)
- [blk-3_cell-10](#blk-3_cell-10)
- [blk-4_cell-4](#blk-4_cell-4)
    - [154 103, 0 1471](#154-103-0-1471)
    - [Competing](#competing-1)
- [blk-4_cell-6](#blk-4_cell-6)

<!-- /TOC -->

# blk-1_cell-4
wo_div_blk-1_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1235
Features in dataset (100%):
Count: 1728

Average time in pred: 1.66624154675
Max time in pred: 11.9262039661
Min time in pred: 0.182722091675

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 786
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 812
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 5
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 6
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.463035019455
Occupied: 1.0

Confusion Report:
[[ 119  138]
 [   0 1471]]
True Negative: 119
False Negative: 0
False Positive: 138 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.536964980545

             precision    recall  f1-score   support

      Empty     1.0000    0.4630    0.6330       257
   Occupied     0.9142    1.0000    0.9552      1471

avg / total     0.9270    0.9201    0.9073      1728


# blk-1_cell-6
## 134 123, 1 1470

wo_div_blk-1_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1312
Features in dataset (100%):
Count: 1728

Average time in pred: 0.571886520005
Max time in pred: 5.80896687508
Min time in pred: 0.101139068604

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 756
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 20
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 385
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 420
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 12

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.52140077821
Occupied: 0.999320190347

Confusion Report:
[[ 134  123]
 [   1 1470]]
True Negative: 134
False Negative: 1
False Positive: 123 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.47859922179

             precision    recall  f1-score   support

      Empty     0.9926    0.5214    0.6837       257
   Occupied     0.9228    0.9993    0.9595      1471

avg / total     0.9332    0.9282    0.9185      1728



# blk-1_cell-8
## 142 115, 1 1470

wo_div_blk-1_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1343
Features in dataset (100%):
Count: 1728

Average time in pred: 0.45168345325
Max time in pred: 4.34126400948
Min time in pred: 0.021418094635

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 823
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 1
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 336
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 329
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 8
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 88
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.552529182879
Occupied: 0.999320190347

Confusion Report:
[[ 142  115]
 [   1 1470]]
True Negative: 142
False Negative: 1
False Positive: 115 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.447470817121

             precision    recall  f1-score   support

      Empty     0.9930    0.5525    0.7100       257
   Occupied     0.9274    0.9993    0.9620      1471

avg / total     0.9372    0.9329    0.9246      1728


# blk-1_cell-10
wo_div_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1031
Features in dataset (100%):
Count: 1728

Average time in pred: 0.329702920246
Max time in pred: 3.40609622002
Min time in pred: 0.0244331359863

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 856
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 402
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 339
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 4
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 2

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.47859922179
Occupied: 1.0

Confusion Report:
[[ 123  134]
 [   0 1471]]
True Negative: 123
False Negative: 0
False Positive: 134 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.52140077821

             precision    recall  f1-score   support

      Empty     1.0000    0.4786    0.6474       257
   Occupied     0.9165    1.0000    0.9564      1471

avg / total     0.9289    0.9225    0.9105      1728


# blk-1_cell-12
## 149 108, 0 1471
wo_div_blk-1_cell-12_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1049
Features in dataset (100%):
Count: 1728

Average time in pred: 0.281035536831
Max time in pred: 2.72056293488
Min time in pred: 0.0131158828735

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 822
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 390
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 2
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 4
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 3
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 19
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 333
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 6

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.579766536965
Occupied: 1.0

Confusion Report:
[[ 149  108]
 [   0 1471]]
True Negative: 149
False Negative: 0
False Positive: 108 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.420233463035

             precision    recall  f1-score   support

      Empty     1.0000    0.5798    0.7340       257
   Occupied     0.9316    1.0000    0.9646      1471

avg / total     0.9418    0.9375    0.9303      1728


# blk-2_cell-4
## 137 120, 1 1470
wo_div_blk-2_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1649
Features in dataset (100%):
Count: 1728

Average time in pred: 1.89418352878
Max time in pred: 12.2885370255
Min time in pred: 0.0765240192413

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 1105
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 477
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 4
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 2

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.533073929961
Occupied: 0.999320190347

Confusion Report:
[[ 137  120]
 [   1 1470]]
True Negative: 137
False Negative: 1
False Positive: 120 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.466926070039

             precision    recall  f1-score   support

      Empty     0.9928    0.5331    0.6937       257
   Occupied     0.9245    0.9993    0.9605      1471

avg / total     0.9347    0.9300    0.9208      1728


# blk-2_cell-6
## 178 79, 4 1476
## Competing
wo_div_blk-2_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1052
Features in dataset (100%):
Count: 1728

Average time in pred: 0.635565601979
Max time in pred: 6.38122296333
Min time in pred: 0.0269341468811

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 47
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 301
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 379
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 8
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 4
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 807

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.692607003891
Occupied: 0.997280761387

Confusion Report:
[[ 178   79]
 [   4 1467]]
True Negative: 178
False Negative: 4
False Positive: 79 
True Positive: 1467 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00271923861319
False Positive Rate: 0.307392996109

             precision    recall  f1-score   support

      Empty     0.9780    0.6926    0.8109       257
   Occupied     0.9489    0.9973    0.9725      1471

avg / total     0.9532    0.9520    0.9485      1728


# blk-2_cell-8
## 148 109, 2 1469
wo_div_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1132
Features in dataset (100%):
Count: 1728

Average time in pred: 0.646377028552
Max time in pred: 3.9736199379
Min time in pred: 0.0540390014648

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 715
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 742
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 13
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 61
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 27
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 14
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 4

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.575875486381
Occupied: 0.998640380693

Confusion Report:
[[ 148  109]
 [   2 1469]]
True Negative: 148
False Negative: 2
False Positive: 109 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00135961930659
False Positive Rate: 0.424124513619

             precision    recall  f1-score   support

      Empty     0.9867    0.5759    0.7273       257
   Occupied     0.9309    0.9986    0.9636      1471

avg / total     0.9392    0.9358    0.9284      1728


# blk-2_cell-10
051717_210_woexcept_a_results_eeei_5th_solar_041017_a_morning_parkinglinebased05182017_1314
Features in dataset (100%):
Count: 1728

Average time in pred: 1.41566451211
Max time in pred: 15.0185399055
Min time in pred: 0.246682882309

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 692
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 12
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 874
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 15
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 33
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 1
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 25

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.291828793774
Occupied: 0.999320190347

Confusion Report:
[[  75  182]
 [   1 1470]]
True Negative: 75
False Negative: 1
False Positive: 182 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.708171206226

             precision    recall  f1-score   support

      Empty     0.9868    0.2918    0.4505       257
   Occupied     0.8898    0.9993    0.9414      1471

avg / total     0.9043    0.8941    0.8684      1728



# blk-2_cell-12
051717_210_woexcept_a_results_eeei_5th_solar_041017_a_morning_parkinglinebased05182017_1257
Features in dataset (100%):
Count: 1728

Average time in pred: 0.800065096192
Max time in pred: 9.4429371357
Min time in pred: 0.134560108185

List of model detections:
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 16
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 5
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 670
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 2
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 4
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-12_wo_div.model: 883

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.482490272374
Occupied: 0.984364377974

Confusion Report:
[[ 124  133]
 [  23 1448]]
True Negative: 124
False Negative: 23
False Positive: 133 
True Positive: 1448 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0156356220258
False Positive Rate: 0.517509727626

             precision    recall  f1-score   support

      Empty     0.8435    0.4825    0.6139       257
   Occupied     0.9159    0.9844    0.9489      1471

avg / total     0.9051    0.9097    0.8991      1728




# blk-3_cell-4
wo_div_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1113
Features in dataset (100%):
Count: 1728

Average time in pred: 0.74561771171
Max time in pred: 4.70672011375
Min time in pred: 0.0995140075684

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 849
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 703
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 78
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 69
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 21
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 5
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0077519379845
Occupied: 1.0

Confusion Report:
[[   2  256]
 [   0 1470]]
True Negative: 2
False Negative: 0
False Positive: 256 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.992248062016

             precision    recall  f1-score   support

      Empty     1.0000    0.0078    0.0154       258
   Occupied     0.8517    1.0000    0.9199      1470

avg / total     0.8738    0.8519    0.7849      1728


# blk-3_cell-6
## 152 105, 0 1471
wo_div_blk-3_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1434
Features in dataset (100%):
Count: 1728

Average time in pred: 0.509602941573
Max time in pred: 4.55172991753
Min time in pred: 0.0768070220947

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 27
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 747
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 30
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 26
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 355
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 7
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 384

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.591439688716
Occupied: 1.0

Confusion Report:
[[ 152  105]
 [   0 1471]]
True Negative: 152
False Negative: 0
False Positive: 105 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.408560311284

             precision    recall  f1-score   support

      Empty     1.0000    0.5914    0.7433       257
   Occupied     0.9334    1.0000    0.9655      1471

avg / total     0.9433    0.9392    0.9325      1728


# blk-3_cell-8
wo_div_blk-3_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1454
Features in dataset (100%):
Count: 1728

Average time in pred: 0.327966745805
Max time in pred: 3.11611890793
Min time in pred: 0.0163590908051

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 865
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 3
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 329
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 316
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 10
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 73
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 2
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 2
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 28

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.389105058366
Occupied: 1.0

Confusion Report:
[[ 100  157]
 [   0 1471]]
True Negative: 100
False Negative: 0
False Positive: 157 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.610894941634

             precision    recall  f1-score   support

      Empty     1.0000    0.3891    0.5602       257
   Occupied     0.9036    1.0000    0.9493      1471

avg / total     0.9179    0.9091    0.8915      1728


# blk-3_cell-10
wo_div_blk-3_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1159
Features in dataset (100%):
Count: 1728

Average time in pred: 0.37608409604
Max time in pred: 2.43932795525
Min time in pred: 0.0171830654144

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 1159
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 9
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 142
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 354
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 7
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 31
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 4
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0739299610895
Occupied: 0.99796057104

Confusion Report:
[[  19  238]
 [   3 1468]]
True Negative: 19
False Negative: 3
False Positive: 238 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00203942895989
False Positive Rate: 0.926070038911

             precision    recall  f1-score   support

      Empty     0.8636    0.0739    0.1362       257
   Occupied     0.8605    0.9980    0.9241      1471

avg / total     0.8610    0.8605    0.8070      1728


# blk-4_cell-4
## 154 103, 0 1471
## Competing
wo_div_blk-4_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1629
Features in dataset (100%):
Count: 1728

Average time in pred: 1.38247109963
Max time in pred: 9.08059382439
Min time in pred: 0.12838602066

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 20
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 744
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 29
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 26
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 751
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 4

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.599221789883
Occupied: 1.0

Confusion Report:
[[ 154  103]
 [   0 1471]]
True Negative: 154
False Negative: 0
False Positive: 103 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.400778210117

             precision    recall  f1-score   support

      Empty     1.0000    0.5992    0.7494       257
   Occupied     0.9346    1.0000    0.9662      1471

avg / total     0.9443    0.9404    0.9339      1728


# blk-4_cell-6
wo_div_blk-4_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1652
Features in dataset (100%):
Count: 1728

Average time in pred: 0.33367649452
Max time in pred: 3.57672715187
Min time in pred: 0.0599920749664

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 777
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 400
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 431
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 26
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.354085603113
Occupied: 1.0

Confusion Report:
[[  91  166]
 [   0 1471]]
True Negative: 91
False Negative: 0
False Positive: 166 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.645914396887

             precision    recall  f1-score   support

      Empty     1.0000    0.3541    0.5230       257
   Occupied     0.8986    1.0000    0.9466      1471

avg / total     0.9137    0.9039    0.8836      1728

