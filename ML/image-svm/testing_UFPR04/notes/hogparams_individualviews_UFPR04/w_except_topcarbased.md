<!-- TOC -->

- [blk-1_cell-4](#blk-1_cell-4)
- [blk-1_cell-6](#blk-1_cell-6)
- [blk-1_cell-8](#blk-1_cell-8)
- [blk-1_cell-10](#blk-1_cell-10)
- [blk-1_cell-12](#blk-1_cell-12)
- [blk-2_cell-4](#blk-2_cell-4)
- [blk-2_cell-6](#blk-2_cell-6)
- [blk-2_cell-8](#blk-2_cell-8)
- [blk-2_cell-10](#blk-2_cell-10)
- [blk-2_cell-12](#blk-2_cell-12)
- [blk-3_cell-4](#blk-3_cell-4)
- [blk-3_cell-6](#blk-3_cell-6)
- [blk-3_cell-8](#blk-3_cell-8)
- [blk-3_cell-10](#blk-3_cell-10)
- [blk-4_cell-4](#blk-4_cell-4)
- [blk-4_cell-6](#blk-4_cell-6)

<!-- /TOC -->
# blk-1_cell-4
wo_div_w_except_blk-1_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1219
Features in dataset (100%):
Count: 1728

Average time in pred: 2.31140961788
Max time in pred: 23.345074892
Min time in pred: 0.336879014969

List of model detections:
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 670
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 338
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 317
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 83
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 164
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 99
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 34

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0891472868217
Occupied: 1.0

Confusion Report:
[[  23  235]
 [   0 1470]]
True Negative: 23
False Negative: 0
False Positive: 235 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.910852713178

             precision    recall  f1-score   support

      Empty     1.0000    0.0891    0.1637       258
   Occupied     0.8622    1.0000    0.9260      1470

avg / total     0.8827    0.8640    0.8122      1728


# blk-1_cell-6
wo_div_w_except_blk-1_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1335
Features in dataset (100%):
Count: 1728

Average time in pred: 1.04924846468
Max time in pred: 11.5149781704
Min time in pred: 0.175158023834

List of model detections:
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 699
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 474
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 372
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 2
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 11
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 3
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 134
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.116279069767
Occupied: 0.998639455782

Confusion Report:
[[  30  228]
 [   2 1468]]
True Negative: 30
False Negative: 2
False Positive: 228 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00136054421769
False Positive Rate: 0.883720930233

             precision    recall  f1-score   support

      Empty     0.9375    0.1163    0.2069       258
   Occupied     0.8656    0.9986    0.9274      1470

avg / total     0.8763    0.8669    0.8198      1728


# blk-1_cell-8
wo_div_w_except_blk-1_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1428
Features in dataset (100%):
Count: 1728

Average time in pred: 1.05803015483
Max time in pred: 8.54289484024
Min time in pred: 0.117285966873

List of model detections:
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 1126
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 9
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 196
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 305
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 9
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 5
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 11
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 22

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.174418604651
Occupied: 1.0

Confusion Report:
[[  45  213]
 [   0 1470]]
True Negative: 45
False Negative: 0
False Positive: 213 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.825581395349

             precision    recall  f1-score   support

      Empty     1.0000    0.1744    0.2970       258
   Occupied     0.8734    1.0000    0.9324      1470

avg / total     0.8923    0.8767    0.8376      1728


# blk-1_cell-10
wo_div_w_except_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1033
Features in dataset (100%):
Count: 1728

Average time in pred: 0.730845831335
Max time in pred: 4.26369905472
Min time in pred: 0.103639125824

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 918
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 586
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 37
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 13
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 67
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 21
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 5
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 3
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.294573643411
Occupied: 0.998639455782

Confusion Report:
[[  76  182]
 [   2 1468]]
True Negative: 76
False Negative: 2
False Positive: 182 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00136054421769
False Positive Rate: 0.705426356589

             precision    recall  f1-score   support

      Empty     0.9744    0.2946    0.4524       258
   Occupied     0.8897    0.9986    0.9410      1470

avg / total     0.9023    0.8935    0.8681      1728


# blk-1_cell-12
wo_div_w_except_blk-1_cell-12_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1103
Features in dataset (100%):
Count: 1728

Average time in pred: 0.417604963813
Max time in pred: 2.76182198524
Min time in pred: 0.0277569293976

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 753
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 414
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 163
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 87
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 7
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 251
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 4
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 12
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.135658914729
Occupied: 0.999319727891

Confusion Report:
[[  35  223]
 [   1 1469]]
True Negative: 35
False Negative: 1
False Positive: 223 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.864341085271

             precision    recall  f1-score   support

      Empty     0.9722    0.1357    0.2381       258
   Occupied     0.8682    0.9993    0.9292      1470

avg / total     0.8837    0.8704    0.8260      1728


# blk-2_cell-4
wo_div_w_except_blk-2_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1640
Features in dataset (100%):
Count: 1728

Average time in pred: 2.47252260562
Max time in pred: 18.8416769505
Min time in pred: 0.347035169601

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 933
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 654
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 18
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 48
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 1
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 3
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.271317829457
Occupied: 1.0

Confusion Report:
[[  70  188]
 [   0 1470]]
True Negative: 70
False Negative: 0
False Positive: 188 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.728682170543

             precision    recall  f1-score   support

      Empty     1.0000    0.2713    0.4268       258
   Occupied     0.8866    1.0000    0.9399      1470

avg / total     0.9035    0.8912    0.8633      1728


# blk-2_cell-6
wo_div_w_except_blk-2_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1037
Features in dataset (100%):
Count: 1728

Average time in pred: 0.892430876002
Max time in pred: 7.69871211052
Min time in pred: 0.165035009384

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 1166
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 22
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 343
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 36
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 23
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 59
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 30
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.186046511628
Occupied: 0.999319727891

Confusion Report:
[[  48  210]
 [   1 1469]]
True Negative: 48
False Negative: 1
False Positive: 210 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.813953488372

             precision    recall  f1-score   support

      Empty     0.9796    0.1860    0.3127       258
   Occupied     0.8749    0.9993    0.9330      1470

avg / total     0.8906    0.8779    0.8404      1728


# blk-2_cell-8
wo_div_w_except_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1116
Features in dataset (100%):
Count: 1728

Average time in pred: 0.677990294441
Max time in pred: 5.17267799377
Min time in pred: 0.0986840724945

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 1054
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 7
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 620
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 11
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 15
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 2
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 3

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.062015503876
Occupied: 1.0

Confusion Report:
[[  16  242]
 [   0 1470]]
True Negative: 16
False Negative: 0
False Positive: 242 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.937984496124

             precision    recall  f1-score   support

      Empty     1.0000    0.0620    0.1168       258
   Occupied     0.8586    1.0000    0.9239      1470

avg / total     0.8797    0.8600    0.8034      1728


# blk-2_cell-10
wo_div_w_except_blk-2_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1502
Features in dataset (100%):
Count: 1728

Average time in pred: 0.374029691986
Max time in pred: 3.2965900898
Min time in pred: 0.0930070877075

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 1231
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 372
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 72
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 15
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 14
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 8

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.062015503876
Occupied: 1.0

Confusion Report:
[[  16  242]
 [   0 1470]]
True Negative: 16
False Negative: 0
False Positive: 242 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.937984496124

             precision    recall  f1-score   support

      Empty     1.0000    0.0620    0.1168       258
   Occupied     0.8586    1.0000    0.9239      1470

avg / total     0.8797    0.8600    0.8034      1728


# blk-2_cell-12
wo_div_w_except_blk-2_cell-12_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1521
Features in dataset (100%):
Count: 1728

Average time in pred: 0.293430299533
Max time in pred: 4.6551759243
Min time in pred: 0.0623278617859

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 839
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 12
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 173
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 44
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 67
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 8
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 10
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 520
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 7

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.139534883721
Occupied: 0.991836734694

Confusion Report:
[[  36  222]
 [  12 1458]]
True Negative: 36
False Negative: 12
False Positive: 222 
True Positive: 1458 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00816326530612
False Positive Rate: 0.860465116279

             precision    recall  f1-score   support

      Empty     0.7500    0.1395    0.2353       258
   Occupied     0.8679    0.9918    0.9257      1470

avg / total     0.8503    0.8646    0.8226      1728


# blk-3_cell-4
wo_div_w_except_blk-3_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1300
Features in dataset (100%):
Count: 1728

Average time in pred: 2.49128498427
Max time in pred: 21.1287419796
Min time in pred: 0.290114879608

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 1047
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 23
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 540
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 16
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 30
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 11
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 4
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 8

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.186046511628
Occupied: 0.999319727891

Confusion Report:
[[  48  210]
 [   1 1469]]
True Negative: 48
False Negative: 1
False Positive: 210 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.813953488372

             precision    recall  f1-score   support

      Empty     0.9796    0.1860    0.3127       258
   Occupied     0.8749    0.9993    0.9330      1470

avg / total     0.8906    0.8779    0.8404      1728


# blk-3_cell-6
wo_div_w_except_blk-3_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1429
Features in dataset (100%):
Count: 1728

Average time in pred: 1.17976174655
Max time in pred: 8.97008109093
Min time in pred: 0.141313076019

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 847
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 520
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 168
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 118
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 5
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 14
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 22

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.127906976744
Occupied: 1.0

Confusion Report:
[[  33  225]
 [   0 1470]]
True Negative: 33
False Negative: 0
False Positive: 225 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.872093023256

             precision    recall  f1-score   support

      Empty     1.0000    0.1279    0.2268       258
   Occupied     0.8673    1.0000    0.9289      1470

avg / total     0.8871    0.8698    0.8241      1728


# blk-3_cell-8
wo_div_w_except_blk-3_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1525
Features in dataset (100%):
Count: 1728

Average time in pred: 1.03878735378
Max time in pred: 6.28117418289
Min time in pred: 0.0720598697662

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 422
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 841
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 426
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 7
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 10
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 2
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0736434108527
Occupied: 0.999319727891

Confusion Report:
[[  19  239]
 [   1 1469]]
True Negative: 19
False Negative: 1
False Positive: 239 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.926356589147

             precision    recall  f1-score   support

      Empty     0.9500    0.0736    0.1367       258
   Occupied     0.8601    0.9993    0.9245      1470

avg / total     0.8735    0.8611    0.8069      1728


# blk-3_cell-10
wo_div_w_except_blk-3_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1142
Features in dataset (100%):
Count: 1728

Average time in pred: 0.303065943359
Max time in pred: 2.19467711449
Min time in pred: 0.0684812068939

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 1187
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 203
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 84
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 208
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 1
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 12
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 8

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0968992248062
Occupied: 1.0

Confusion Report:
[[  25  233]
 [   0 1470]]
True Negative: 25
False Negative: 0
False Positive: 233 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.903100775194

             precision    recall  f1-score   support

      Empty     1.0000    0.0969    0.1767       258
   Occupied     0.8632    1.0000    0.9266      1470

avg / total     0.8836    0.8652    0.8146      1728


# blk-4_cell-4
wo_div_w_except_blk-4_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1649
Features in dataset (100%):
Count: 1728

Average time in pred: 2.16479387714
Max time in pred: 8.110049963
Min time in pred: 0.240514039993

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 98
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 239
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 76
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 51
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 70
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 1149
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 1
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.166666666667
Occupied: 1.0

Confusion Report:
[[  43  215]
 [   0 1470]]
True Negative: 43
False Negative: 0
False Positive: 215 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.833333333333

             precision    recall  f1-score   support

      Empty     1.0000    0.1667    0.2857       258
   Occupied     0.8724    1.0000    0.9319      1470

avg / total     0.8915    0.8756    0.8354      1728


# blk-4_cell-6

wo_div_w_except_blk-4_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1751
Features in dataset (100%):
Count: 1728

Average time in pred: 0.58068443614
Max time in pred: 5.56891894341
Min time in pred: 0.111416101456

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 1113
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 98
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 30
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 259
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 1
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 57
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 11
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 129
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.112403100775
Occupied: 1.0

Confusion Report:
[[  29  229]
 [   0 1470]]
True Negative: 29
False Negative: 0
False Positive: 229 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.887596899225

             precision    recall  f1-score   support

      Empty     1.0000    0.1124    0.2021       258
   Occupied     0.8652    1.0000    0.9277      1470

avg / total     0.8853    0.8675    0.8194      1728

