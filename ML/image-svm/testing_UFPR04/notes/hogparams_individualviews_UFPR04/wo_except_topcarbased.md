# blk-1_cell-4
wo_div_blk-1_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1147
Features in dataset (100%):
Count: 1728

Average time in pred: 1.99040067044
Max time in pred: 12.3370099068
Min time in pred: 0.328966856003

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 1004
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 709
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 9
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0193798449612
Occupied: 0.999319727891

Confusion Report:
[[   5  253]
 [   1 1469]]
True Negative: 5
False Negative: 1
False Positive: 253 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.980620155039

             precision    recall  f1-score   support

      Empty     0.8333    0.0194    0.0379       258
   Occupied     0.8531    0.9993    0.9204      1470

avg / total     0.8501    0.8530    0.7887      1728

# blk-1_cell-6
wo_div_blk-1_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1256
Features in dataset (100%):
Count: 1728

Average time in pred: 0.691872174679
Max time in pred: 3.64016580582
Min time in pred: 0.185837984085

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 921
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 96
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 584
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 105
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 2
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-6.model: 3

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.062015503876
Occupied: 1.0

Confusion Report:
[[  16  242]
 [   0 1470]]
True Negative: 16
False Negative: 0
False Positive: 242 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.937984496124

             precision    recall  f1-score   support

      Empty     1.0000    0.0620    0.1168       258
   Occupied     0.8586    1.0000    0.9239      1470

avg / total     0.8797    0.8600    0.8034      1728


# blk-1_cell-8
wo_div_blk-1_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1328
Features in dataset (100%):
Count: 1728

Average time in pred: 0.519639641736
Max time in pred: 4.83903598785
Min time in pred: 0.119295120239

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 1383
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 26
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 221
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 20
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 8
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 31
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 4

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.131782945736
Occupied: 0.999319727891

Confusion Report:
[[  34  224]
 [   1 1469]]
True Negative: 34
False Negative: 1
False Positive: 224 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.868217054264

             precision    recall  f1-score   support

      Empty     0.9714    0.1318    0.2321       258
   Occupied     0.8677    0.9993    0.9289      1470

avg / total     0.8832    0.8698    0.8248      1728



# blk-1_cell-10
wo_div_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1022
Features in dataset (100%):
Count: 1728

Average time in pred: 0.354680522724
Max time in pred: 4.16900801659
Min time in pred: 0.035315990448

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 892
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 586
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 5
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 193
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 8
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 6

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.143410852713
Occupied: 1.0

Confusion Report:
[[  37  221]
 [   0 1470]]
True Negative: 37
False Negative: 0
False Positive: 221 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.856589147287

             precision    recall  f1-score   support

      Empty     1.0000    0.1434    0.2508       258
   Occupied     0.8693    1.0000    0.9301      1470

avg / total     0.8888    0.8721    0.8287      1728


# blk-1_cell-12
wo_div_blk-1_cell-12_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1041
Features in dataset (100%):
Count: 1728

Average time in pred: 0.32346577625
Max time in pred: 4.91809892654
Min time in pred: 0.0774579048157

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 1348
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 99
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 48
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 26
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 190
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-12.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0503875968992
Occupied: 1.0

Confusion Report:
[[  13  245]
 [   0 1470]]
True Negative: 13
False Negative: 0
False Positive: 245 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.949612403101

             precision    recall  f1-score   support

      Empty     1.0000    0.0504    0.0959       258
   Occupied     0.8571    1.0000    0.9231      1470

avg / total     0.8785    0.8582    0.7996      1728


# blk-2_cell-4
wo_div_blk-2_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1554
Features in dataset (100%):
Count: 1728

Average time in pred: 2.59271775131
Max time in pred: 24.0651230812
Min time in pred: 0.343485116959

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 1386
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 276
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 12
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 8
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 1
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-4.model: 4

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.158914728682
Occupied: 1.0

Confusion Report:
[[  41  217]
 [   0 1470]]
True Negative: 41
False Negative: 0
False Positive: 217 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.841085271318

             precision    recall  f1-score   support

      Empty     1.0000    0.1589    0.2742       258
   Occupied     0.8714    1.0000    0.9313      1470

avg / total     0.8906    0.8744    0.8332      1728


# blk-2_cell-6
wo_div_blk-2_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1033
Features in dataset (100%):
Count: 1728

Average time in pred: 0.750868588962
Max time in pred: 10.9170629978
Min time in pred: 0.166292905807

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 1294
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 225
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 9
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 122
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 34
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model: 2

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.158914728682
Occupied: 1.0

Confusion Report:
[[  41  217]
 [   0 1470]]
True Negative: 41
False Negative: 0
False Positive: 217 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.841085271318

             precision    recall  f1-score   support

      Empty     1.0000    0.1589    0.2742       258
   Occupied     0.8714    1.0000    0.9313      1470

avg / total     0.8906    0.8744    0.8332      1728


# blk-2_cell-8
wo_div_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1113
Features in dataset (100%):
Count: 1728

Average time in pred: 0.74561771171
Max time in pred: 4.70672011375
Min time in pred: 0.0995140075684

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 849
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 703
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 78
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 69
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 21
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 5
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0077519379845
Occupied: 1.0

Confusion Report:
[[   2  256]
 [   0 1470]]
True Negative: 2
False Negative: 0
False Positive: 256 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.992248062016

             precision    recall  f1-score   support

      Empty     1.0000    0.0078    0.0154       258
   Occupied     0.8517    1.0000    0.9199      1470

avg / total     0.8738    0.8519    0.7849      1728


# blk-2_cell-10
wo_div_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1113
Features in dataset (100%):
Count: 1728

Average time in pred: 0.74561771171
Max time in pred: 4.70672011375
Min time in pred: 0.0995140075684

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 849
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 703
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 78
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 69
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 21
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 5
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0077519379845
Occupied: 1.0

Confusion Report:
[[   2  256]
 [   0 1470]]
True Negative: 2
False Negative: 0
False Positive: 256 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.992248062016

             precision    recall  f1-score   support

      Empty     1.0000    0.0078    0.0154       258
   Occupied     0.8517    1.0000    0.9199      1470

avg / total     0.8738    0.8519    0.7849      1728


# blk-2_cell-12
wo_div_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1113
Features in dataset (100%):
Count: 1728

Average time in pred: 0.74561771171
Max time in pred: 4.70672011375
Min time in pred: 0.0995140075684

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 849
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 703
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 78
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 69
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 21
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 5
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0077519379845
Occupied: 1.0

Confusion Report:
[[   2  256]
 [   0 1470]]
True Negative: 2
False Negative: 0
False Positive: 256 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.992248062016

             precision    recall  f1-score   support

      Empty     1.0000    0.0078    0.0154       258
   Occupied     0.8517    1.0000    0.9199      1470

avg / total     0.8738    0.8519    0.7849      1728


# blk-3_cell-4
wo_div_blk-3_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1306
Features in dataset (100%):
Count: 1728

Average time in pred: 2.30838937395
Max time in pred: 19.0266001225
Min time in pred: 0.284645795822

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 1389
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 19
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 51
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 188
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 24
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 16
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-4.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.15503875969
Occupied: 1.0

Confusion Report:
[[  40  218]
 [   0 1470]]
True Negative: 40
False Negative: 0
False Positive: 218 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.84496124031

             precision    recall  f1-score   support

      Empty     1.0000    0.1550    0.2685       258
   Occupied     0.8709    1.0000    0.9310      1470

avg / total     0.8901    0.8738    0.8321      1728


# blk-3_cell-6
wo_div_blk-3_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1419
Features in dataset (100%):
Count: 1728

Average time in pred: 0.685763652402
Max time in pred: 8.03309082985
Min time in pred: 0.138423919678

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 882
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 632
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 39
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 49
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 30
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 8
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 21
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.259689922481
Occupied: 1.0

Confusion Report:
[[  67  191]
 [   0 1470]]
True Negative: 67
False Negative: 0
False Positive: 191 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.740310077519

             precision    recall  f1-score   support

      Empty     1.0000    0.2597    0.4123       258
   Occupied     0.8850    1.0000    0.9390      1470

avg / total     0.9022    0.8895    0.8604      1728


# blk-3_cell-8
wo_div_blk-3_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1445
Features in dataset (100%):
Count: 1728

Average time in pred: 0.353617097769
Max time in pred: 3.4683868885
Min time in pred: 0.0740308761597

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 1434
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 12
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 1
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 180
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 11
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 56
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model: 16

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0697674418605
Occupied: 1.0

Confusion Report:
[[  18  240]
 [   0 1470]]
True Negative: 18
False Negative: 0
False Positive: 240 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.93023255814

             precision    recall  f1-score   support

      Empty     1.0000    0.0698    0.1304       258
   Occupied     0.8596    1.0000    0.9245      1470

avg / total     0.8806    0.8611    0.8060      1728


# blk-3_cell-10
wo_div_blk-3_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1148
Features in dataset (100%):
Count: 1728

Average time in pred: 0.530527699325
Max time in pred: 2.59630680084
Min time in pred: 0.0696020126343

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 1432
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 45
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 26
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 169
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 3
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 25
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-10.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0968992248062
Occupied: 1.0

Confusion Report:
[[  25  233]
 [   0 1470]]
True Negative: 25
False Negative: 0
False Positive: 233 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.903100775194

             precision    recall  f1-score   support

      Empty     1.0000    0.0969    0.1767       258
   Occupied     0.8632    1.0000    0.9266      1470

avg / total     0.8836    0.8652    0.8146      1728


# blk-4_cell-4
wo_div_blk-4_cell-4_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1549
Features in dataset (100%):
Count: 1728

Average time in pred: 1.87679942035
Max time in pred: 17.2433111668
Min time in pred: 0.246861934662

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 875
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 704
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 21
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 18
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 25
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 12

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.279069767442
Occupied: 1.0

Confusion Report:
[[  72  186]
 [   0 1470]]
True Negative: 72
False Negative: 0
False Positive: 186 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.720930232558

             precision    recall  f1-score   support

      Empty     1.0000    0.2791    0.4364       258
   Occupied     0.8877    1.0000    0.9405      1470

avg / total     0.9045    0.8924    0.8652      1728


# blk-4_cell-6
wo_div_blk-4_cell-6_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1642
Features in dataset (100%):
Count: 1728

Average time in pred: 0.452568050198
Max time in pred: 3.80718398094
Min time in pred: 0.11426281929

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 884
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 674
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 125
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 1
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 7
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.131782945736
Occupied: 1.0

Confusion Report:
[[  34  224]
 [   0 1470]]
True Negative: 34
False Negative: 0
False Positive: 224 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.868217054264

             precision    recall  f1-score   support

      Empty     1.0000    0.1318    0.2329       258
   Occupied     0.8678    1.0000    0.9292      1470

avg / total     0.8875    0.8704    0.8252      1728

