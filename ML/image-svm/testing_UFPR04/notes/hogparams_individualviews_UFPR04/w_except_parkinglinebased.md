<!-- TOC -->

- [blk-1_cell-4](#blk-1_cell-4)
    - [138-119, 6-1465](#138-119-6-1465)
- [blk-1_cell-6](#blk-1_cell-6)
    - [144-113, 21-1450](#144-113-21-1450)
- [blk-1_cell-8](#blk-1_cell-8)
    - [142-115, 11-1460](#142-115-11-1460)
- [blk-1_cell-10](#blk-1_cell-10)
    - [172-85, 5, 1466](#172-85-5-1466)
- [blk-1_cell-12](#blk-1_cell-12)
- [blk-2_cell-4](#blk-2_cell-4)
- [blk-2_cell-6](#blk-2_cell-6)
- [blk-2_cell-8](#blk-2_cell-8)
- [blk-2_cell-10](#blk-2_cell-10)
- [blk-2_cell-12](#blk-2_cell-12)
- [blk-3_cell-4](#blk-3_cell-4)
- [blk-3_cell-6](#blk-3_cell-6)
- [blk-3_cell-8](#blk-3_cell-8)
- [blk-3_cell-10](#blk-3_cell-10)
- [blk-4_cell-4](#blk-4_cell-4)
- [blk-4_cell-6](#blk-4_cell-6)

<!-- /TOC -->

# blk-1_cell-4
## 138-119, 6-1465
wo_div_w_except_blk-1_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1303
Features in dataset (100%):
Count: 1728

Average time in pred: 1.49639105962
Max time in pred: 11.968132019
Min time in pred: 0.0580220222473

List of model detections:
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 320
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 446
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 104
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 31
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 254
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-4.model: 420
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-4.model: 9

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.536964980545
Occupied: 0.99592114208

Confusion Report:
[[ 138  119]
 [   6 1465]]
True Negative: 138
False Negative: 6
False Positive: 119 
True Positive: 1465 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00407885791978
False Positive Rate: 0.463035019455

             precision    recall  f1-score   support

      Empty     0.9583    0.5370    0.6883       257
   Occupied     0.9249    0.9959    0.9591      1471

avg / total     0.9299    0.9277    0.9188      1728


# blk-1_cell-6
## 144-113, 21-1450
wo_div_w_except_blk-1_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1357
Features in dataset (100%):
Count: 1728

Average time in pred: 0.782230649557
Max time in pred: 5.82251882553
Min time in pred: 0.0278601646423

List of model detections:
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 336
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 423
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 343
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 317
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 12
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 4
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-6.model: 127
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-6.model: 1
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.560311284047
Occupied: 0.985723997281

Confusion Report:
[[ 144  113]
 [  21 1450]]
True Negative: 144
False Negative: 21
False Positive: 113 
True Positive: 1450 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0142760027192
False Positive Rate: 0.439688715953

             precision    recall  f1-score   support

      Empty     0.8727    0.5603    0.6825       257
   Occupied     0.9277    0.9857    0.9558      1471

avg / total     0.9195    0.9225    0.9152      1728


# blk-1_cell-8
## 142-115, 11-1460
wo_div_w_except_blk-1_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1451
Features in dataset (100%):
Count: 1728

Average time in pred: 0.789060253236
Max time in pred: 4.358481884
Min time in pred: 0.0292508602142

List of model detections:
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 1041
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 94
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 351
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 47
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-8.model: 9
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-8.model: 10
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 7
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-8.model: 16

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.552529182879
Occupied: 0.992522093814

Confusion Report:
[[ 142  115]
 [  11 1460]]
True Negative: 142
False Negative: 11
False Positive: 115 
True Positive: 1460 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00747790618627
False Positive Rate: 0.447470817121

             precision    recall  f1-score   support

      Empty     0.9281    0.5525    0.6927       257
   Occupied     0.9270    0.9925    0.9586      1471

avg / total     0.9272    0.9271    0.9191      1728


# blk-1_cell-10
## 172-85, 5, 1466
wo_div_w_except_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1050
Features in dataset (100%):
Count: 1728

Average time in pred: 0.59857445155
Max time in pred: 3.50204515457
Min time in pred: 0.0566489696503

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 695
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 771
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 13
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 41
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 27
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-10.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-10.model: 1
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-10.model: 2

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.669260700389
Occupied: 0.996600951734

Confusion Report:
[[ 172   85]
 [   5 1466]]
True Negative: 172
False Negative: 5
False Positive: 85 
True Positive: 1466 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00339904826649
False Positive Rate: 0.330739299611

             precision    recall  f1-score   support

      Empty     0.9718    0.6693    0.7926       257
   Occupied     0.9452    0.9966    0.9702      1471

avg / total     0.9491    0.9479    0.9438      1728


# blk-1_cell-12
wo_div_w_except_blk-1_cell-12_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1112
Features in dataset (100%):
Count: 1728

Average time in pred: 0.333336715897
Max time in pred: 2.68978786469
Min time in pred: 0.0197510719299

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 796
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 369
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 32
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 20
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-1_cell-12.model: 343
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-12.model: 1
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-1_cell-12.model: 13

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.466926070039
Occupied: 0.976886471788

Confusion Report:
[[ 120  137]
 [  34 1437]]
True Negative: 120
False Negative: 34
False Positive: 137 
True Positive: 1437 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0231135282121
False Positive Rate: 0.533073929961

             precision    recall  f1-score   support

      Empty     0.7792    0.4669    0.5839       257
   Occupied     0.9130    0.9769    0.9438      1471

avg / total     0.8931    0.9010    0.8903      1728


# blk-2_cell-4
wo_div_w_except_blk-2_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1737
Features in dataset (100%):
Count: 1728

Average time in pred: 1.93275842278
Max time in pred: 12.4172699451
Min time in pred: 0.185413122177

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 693
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 718
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 18
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 74
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-4.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-4.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.571984435798
Occupied: 0.951053704963

Confusion Report:
[[ 147  110]
 [  72 1399]]
True Negative: 147
False Negative: 72
False Positive: 110 
True Positive: 1399 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0489462950374
False Positive Rate: 0.428015564202

             precision    recall  f1-score   support

      Empty     0.6712    0.5720    0.6176       257
   Occupied     0.9271    0.9511    0.9389      1471

avg / total     0.8890    0.8947    0.8911      1728


# blk-2_cell-6
wo_div_w_except_blk-2_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1056
Features in dataset (100%):
Count: 1728

Average time in pred: 0.633742079001
Max time in pred: 5.35126519203
Min time in pred: 0.0257399082184

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 785
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 282
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 25
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 12
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-6.model: 295
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 100
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-6.model: 4
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-6.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.509727626459
Occupied: 0.936777702243

Confusion Report:
[[ 131  126]
 [  93 1378]]
True Negative: 131
False Negative: 93
False Positive: 126 
True Positive: 1378 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0632222977566
False Positive Rate: 0.490272373541

             precision    recall  f1-score   support

      Empty     0.5848    0.5097    0.5447       257
   Occupied     0.9162    0.9368    0.9264      1471

avg / total     0.8669    0.8733    0.8696      1728


# blk-2_cell-8
wo_div_w_except_blk-2_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1133
Features in dataset (100%):
Count: 1728

Average time in pred: 0.577192108112
Max time in pred: 3.89108705521
Min time in pred: 0.0536630153656

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 757
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 2
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 753
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 15
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 28
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-8.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-8.model: 1
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.330739299611
Occupied: 0.941536369816

Confusion Report:
[[  85  172]
 [  86 1385]]
True Negative: 85
False Negative: 86
False Positive: 172 
True Positive: 1385 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0584636301835
False Positive Rate: 0.669260700389

             precision    recall  f1-score   support

      Empty     0.4971    0.3307    0.3972       257
   Occupied     0.8895    0.9415    0.9148      1471

avg / total     0.8312    0.8507    0.8378      1728


# blk-2_cell-10
wo_div_w_except_blk-2_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1512
Features in dataset (100%):
Count: 1728

Average time in pred: 0.313676477444
Max time in pred: 3.01604604721
Min time in pred: 0.0152790546417

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 838
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 370
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 264
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 150
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-10.model: 2
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 17
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-10.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-10.model: 3

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.147859922179
Occupied: 0.968728755948

Confusion Report:
[[  38  219]
 [  46 1425]]
True Negative: 38
False Negative: 46
False Positive: 219 
True Positive: 1425 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0312712440517
False Positive Rate: 0.852140077821

             precision    recall  f1-score   support

      Empty     0.4524    0.1479    0.2229       257
   Occupied     0.8668    0.9687    0.9149      1471

avg / total     0.8052    0.8466    0.8120      1728


# blk-2_cell-12
wo_div_w_except_blk-2_cell-12_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1528
Features in dataset (100%):
Count: 1728

Average time in pred: 0.263395855686
Max time in pred: 2.32555389404
Min time in pred: 0.0348761081696

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 782
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 1
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 67
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 46
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 80
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-12.model: 2
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 3
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-2_cell-12.model: 662
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-2_cell-12.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.272373540856
Occupied: 0.989802855201

Confusion Report:
[[  70  187]
 [  15 1456]]
True Negative: 70
False Negative: 15
False Positive: 187 
True Positive: 1456 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0101971447995
False Positive Rate: 0.727626459144

             precision    recall  f1-score   support

      Empty     0.8235    0.2724    0.4094       257
   Occupied     0.8862    0.9898    0.9351      1471

avg / total     0.8769    0.8831    0.8569      1728


# blk-3_cell-4
wo_div_w_except_blk-3_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1355
Features in dataset (100%):
Count: 1728

Average time in pred: 1.85001074026
Max time in pred: 10.813710928
Min time in pred: 0.0664598941803

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 905
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 2
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 492
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-4.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 30
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 82
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-4.model: 5
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-4.model: 24

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.513618677043
Occupied: 0.961930659415

Confusion Report:
[[ 132  125]
 [  56 1415]]
True Negative: 132
False Negative: 56
False Positive: 125 
True Positive: 1415 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0380693405846
False Positive Rate: 0.486381322957

             precision    recall  f1-score   support

      Empty     0.7021    0.5136    0.5933       257
   Occupied     0.9188    0.9619    0.9399      1471

avg / total     0.8866    0.8953    0.8883      1728


# blk-3_cell-6
wo_div_w_except_blk-3_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1455
Features in dataset (100%):
Count: 1728

Average time in pred: 0.892644043046
Max time in pred: 4.53058409691
Min time in pred: 0.0294959545135

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 687
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 505
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 160
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 2
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 8
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 163
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6.model: 44
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6.model: 25

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.428015564202
Occupied: 0.984364377974

Confusion Report:
[[ 110  147]
 [  23 1448]]
True Negative: 110
False Negative: 23
False Positive: 147 
True Positive: 1448 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0156356220258
False Positive Rate: 0.571984435798

             precision    recall  f1-score   support

      Empty     0.8271    0.4280    0.5641       257
   Occupied     0.9078    0.9844    0.9446      1471

avg / total     0.8958    0.9016    0.8880      1728


# blk-3_cell-8
wo_div_w_except_blk-3_cell-8_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1546
Features in dataset (100%):
Count: 1728

Average time in pred: 0.718450548886
Max time in pred: 3.10618400574
Min time in pred: 0.021476984024

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 470
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 607
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 511
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 4
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-8.model: 5
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 38
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-8.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-8.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.233463035019
Occupied: 0.978925900748

Confusion Report:
[[  60  197]
 [  31 1440]]
True Negative: 60
False Negative: 31
False Positive: 197 
True Positive: 1440 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0210740992522
False Positive Rate: 0.766536964981

             precision    recall  f1-score   support

      Empty     0.6593    0.2335    0.3448       257
   Occupied     0.8797    0.9789    0.9266      1471

avg / total     0.8469    0.8681    0.8401      1728


# blk-3_cell-10
wo_div_w_except_blk-3_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1148
Features in dataset (100%):
Count: 1728

Average time in pred: 0.219350986321
Max time in pred: 1.79637312889
Min time in pred: 0.0129780769348

List of model detections:
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 850
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 424
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 134
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 34
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-10.model: 2
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-10.model: 2
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-10.model: 265

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0544747081712
Occupied: 0.99796057104

Confusion Report:
[[  14  243]
 [   3 1468]]
True Negative: 14
False Negative: 3
False Positive: 243 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00203942895989
False Positive Rate: 0.945525291829

             precision    recall  f1-score   support

      Empty     0.8235    0.0545    0.1022       257
   Occupied     0.8580    0.9980    0.9227      1471

avg / total     0.8529    0.8576    0.8007      1728


# blk-4_cell-4
wo_div_w_except_blk-4_cell-4_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1732
Features in dataset (100%):
Count: 1728

Average time in pred: 1.46726901126
Max time in pred: 6.80152320862
Min time in pred: 0.134238004684

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 170
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 306
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 99
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 57
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 940
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-4_cell-4.model: 1
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-4_cell-4.model: 2
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-4_cell-4.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.459143968872
Occupied: 0.976886471788

Confusion Report:
[[ 118  139]
 [  34 1437]]
True Negative: 118
False Negative: 34
False Positive: 139 
True Positive: 1437 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0231135282121
False Positive Rate: 0.540856031128

             precision    recall  f1-score   support

      Empty     0.7763    0.4591    0.5770       257
   Occupied     0.9118    0.9769    0.9432      1471

avg / total     0.8917    0.8999    0.8888      1728


# blk-4_cell-6
wo_div_w_except_blk-4_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1804
Features in dataset (100%):
Count: 1728

Average time in pred: 0.447222224678
Max time in pred: 2.87268996239
Min time in pred: 0.0184710025787

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 738
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 22
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 276
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 163
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 275
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 10
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-4_cell-6.model: 2
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-4_cell-6.model: 180
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-4_cell-6.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.225680933852
Occupied: 0.99796057104

Confusion Report:
[[  58  199]
 [   3 1468]]
True Negative: 58
False Negative: 3
False Positive: 199 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00203942895989
False Positive Rate: 0.774319066148

             precision    recall  f1-score   support

      Empty     0.9508    0.2257    0.3648       257
   Occupied     0.8806    0.9980    0.9356      1471

avg / total     0.8911    0.8831    0.8507      1728

