# parkinglinebased
puc_onemodel_wdiv_40x60_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_0314
Features in dataset (100%):
Count: 1728

Average time in pred: 1.00263176169
Max time in pred: 3.32707285881
Min time in pred: 0.000972986221313

List of model detections:
w40_h60_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 1281

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.470817120623
Occupied: 0.778382053025

Confusion Report:
[[ 121  136]
 [ 326 1145]]
True Negative: 121
False Negative: 326
False Positive: 136 
True Positive: 1145 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.221617946975
False Positive Rate: 0.529182879377

             precision    recall  f1-score   support

      Empty     0.2707    0.4708    0.3437       257
   Occupied     0.8938    0.7784    0.8321      1471

avg / total     0.8012    0.7326    0.7595      1728


# topcarbased
puc_onemodel_wdiv_40x60_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_0126
Features in dataset (100%):
Count: 1728

Average time in pred: 1.67348350516
Max time in pred: 3.79025816917
Min time in pred: 0.00125908851624

List of model detections:
w40_h60_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4.model: 1536

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.093023255814
Occupied: 0.885714285714

Confusion Report:
[[  24  234]
 [ 168 1302]]
True Negative: 24
False Negative: 168
False Positive: 234 
True Positive: 1302 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.114285714286
False Positive Rate: 0.906976744186

             precision    recall  f1-score   support

      Empty     0.1250    0.0930    0.1067       258
   Occupied     0.8477    0.8857    0.8663      1470

avg / total     0.7398    0.7674    0.7529      1728

