<!-- TOC -->

- [Notes](#notes)
- [blk-2_cell-6](#blk-2_cell-6)
    - [0.6 width threshold](#06-width-threshold)
        - [178-79, 2-1469](#178-79-2-1469)
    - [0.7 width threshold](#07-width-threshold)
        - [197-60, 7-1464](#197-60-7-1464)
    - [0.8 width threshold](#08-width-threshold)
        - [228-29, 330-1141](#228-29-330-1141)
- [blk-3_cell-6](#blk-3_cell-6)
    - [0.6 width threshold](#06-width-threshold-1)
        - [134-123, 0-1471](#134-123-0-1471)
    - [0.7 width threshold](#07-width-threshold-1)
        - [157-100, 0-1471](#157-100-0-1471)
    - [0.8 threshold](#08-threshold)
        - [216-41, 330-1141](#216-41-330-1141)
- [blk-4_cell-4](#blk-4_cell-4)
    - [0.6 width threshold](#06-width-threshold-2)
        - [170-87, 1-1470](#170-87-1-1470)
    - [0.7 width threshold](#07-width-threshold-2)
        - [187-70, 1-1470](#187-70-1-1470)

<!-- /TOC -->

# Notes
- W/o Except
- W/o div
- First triggered

# blk-2_cell-6
## 0.6 width threshold
### 178-79, 2-1469
sorted_blk-2_cell-6_first_triggered_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_0634
Features in dataset (100%):
Count: 1728

Average time in pred: 4.47218585332
Max time in pred: 35.8073310852
Min time in pred: 0.164133787155

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 327
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 14
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 308
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 9
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 41
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 793
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 38
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 4
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 14

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.692607003891
Occupied: 0.998640380693

Confusion Report:
[[ 178   79]
 [   2 1469]]
True Negative: 178
False Negative: 2
False Positive: 79 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00135961930659
False Positive Rate: 0.307392996109

             precision    recall  f1-score   support

      Empty     0.9889    0.6926    0.8146       257
   Occupied     0.9490    0.9986    0.9732      1471

avg / total     0.9549    0.9531    0.9496      1728

## 0.7 width threshold
### 197-60, 7-1464
perview_blk-2_cell-6_070_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_0405
Features in dataset (100%):
Count: 1728

Average time in pred: 3.02066740752
Max time in pred: 40.4311420918
Min time in pred: 0.165301799774

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 323
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 27
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 304
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 33
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 36
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 567
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 203
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 12
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 19

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.766536964981
Occupied: 0.995241332427

Confusion Report:
[[ 197   60]
 [   7 1464]]
True Negative: 197
False Negative: 7
False Positive: 60 
True Positive: 1464 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00475866757308
False Positive Rate: 0.233463035019

             precision    recall  f1-score   support

      Empty     0.9657    0.7665    0.8547       257
   Occupied     0.9606    0.9952    0.9776      1471

avg / total     0.9614    0.9612    0.9593      1728

## 0.8 width threshold
### 228-29, 330-1141
perview_blk-2_cell-6_080_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_0442
Features in dataset (100%):
Count: 1728

Average time in pred: 1.29081554118
Max time in pred: 17.1451408863
Min time in pred: 0.0256741046906

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 371
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 1
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 299
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 22
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 36
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 411
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 19
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 8
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 3

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.887159533074
Occupied: 0.775662814412

Confusion Report:
[[ 228   29]
 [ 330 1141]]
True Negative: 228
False Negative: 330
False Positive: 29 
True Positive: 1141 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.224337185588
False Positive Rate: 0.112840466926

             precision    recall  f1-score   support

      Empty     0.4086    0.8872    0.5595       257
   Occupied     0.9752    0.7757    0.8641      1471

avg / total     0.8909    0.7922    0.8188      1728

# blk-3_cell-6
## 0.6 width threshold
### 134-123, 0-1471
perview_blk-3_cell-6_060_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_2207
Features in dataset (100%):
Count: 1728

Average time in pred: 4.27118169158
Max time in pred: 34.8547320366
Min time in pred: 0.219440937042

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 359
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 24
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 14
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 394
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 796
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 6
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.52140077821
Occupied: 1.0

Confusion Report:
[[ 134  123]
 [   0 1471]]
True Negative: 134
False Negative: 0
False Positive: 123 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.47859922179

             precision    recall  f1-score   support

      Empty     1.0000    0.5214    0.6854       257
   Occupied     0.9228    1.0000    0.9599      1471

avg / total     0.9343    0.9288    0.9191      1728


## 0.7 width threshold
### 157-100, 0-1471
perview_blk-3_cell-6_070_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_2124
Features in dataset (100%):
Count: 1728

Average time in pred: 2.76391204336
Max time in pred: 32.7556638718
Min time in pred: 0.217642068863

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 533
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 31
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 14
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 394
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 595
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.610894941634
Occupied: 1.0

Confusion Report:
[[ 157  100]
 [   0 1471]]
True Negative: 157
False Negative: 0
False Positive: 100 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.389105058366

             precision    recall  f1-score   support

      Empty     1.0000    0.6109    0.7585       257
   Occupied     0.9363    1.0000    0.9671      1471

avg / total     0.9458    0.9421    0.9361      1728

## 0.8 threshold
### 216-41, 330-1141
perview_blk-3_cell-6_080_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_2049
Features in dataset (100%):
Count: 1728

Average time in pred: 1.53847576567
Max time in pred: 14.1149628162
Min time in pred: 0.025691986084

List of model detections:
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 257
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 45
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 14
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 339
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 54
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 467
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 6
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.84046692607
Occupied: 0.775662814412

Confusion Report:
[[ 216   41]
 [ 330 1141]]
True Negative: 216
False Negative: 330
False Positive: 41 
True Positive: 1141 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.224337185588
False Positive Rate: 0.15953307393

             precision    recall  f1-score   support

      Empty     0.3956    0.8405    0.5380       257
   Occupied     0.9653    0.7757    0.8602      1471

avg / total     0.8806    0.7853    0.8122      1728



# blk-4_cell-4
## 0.6 width threshold
### 170-87, 1-1470
sorted_blk-4_cell-4_first_triggered_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1118
Features in dataset (100%):
Count: 1728

Average time in pred: 14.3226153804
Max time in pred: 82.4037151337
Min time in pred: 0.50141787529

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 1133
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 15
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 333
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 33
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 25
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 17
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.661478599222
Occupied: 0.999320190347

Confusion Report:
[[ 170   87]
 [   1 1470]]
True Negative: 170
False Negative: 1
False Positive: 87 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.338521400778

             precision    recall  f1-score   support

      Empty     0.9942    0.6615    0.7944       257
   Occupied     0.9441    0.9993    0.9709      1471

avg / total     0.9516    0.9491    0.9447      1728

## 0.7 width threshold
### 187-70, 1-1470
perview_div_blk-4_cell-4_070_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1532
Features in dataset (100%):
Count: 1728

Average time in pred: 7.88343562876
Max time in pred: 92.5419158936
Min time in pred: 0.513369083405

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 1108
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 17
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 332
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 31
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 26
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 25
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.727626459144
Occupied: 0.999320190347

Confusion Report:
[[ 187   70]
 [   1 1470]]
True Negative: 187
False Negative: 1
False Positive: 70 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.272373540856

             precision    recall  f1-score   support

      Empty     0.9947    0.7276    0.8404       257
   Occupied     0.9545    0.9993    0.9764      1471

avg / total     0.9605    0.9589    0.9562      1728



