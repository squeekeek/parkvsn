<!-- TOC -->

- [blk-2_cell-6](#blk-2_cell-6)
    - [0.7 width threshold](#07-width-threshold)
        - [197-60, 7-1464](#197-60-7-1464)
            - [Time elapsed: 1:39:31.226782](#time-elapsed-13931226782)
- [blk-3_cell-6](#blk-3_cell-6)
    - [0.7 width threshold](#07-width-threshold-1)
        - [152-105, 0-1471](#152-105-0-1471)
            - [Time elapsed: 1:18:24.344068](#time-elapsed-11824344068)
- [blk-4_cell-4](#blk-4_cell-4)
    - [0.7 width threshold](#07-width-threshold-2)
        - [186-71, 1-1470](#186-71-1-1470)
            - [Time elapsed: 3:24:54.276133](#time-elapsed-32454276133)

<!-- /TOC -->

# blk-2_cell-6
## 0.7 width threshold
### 197-60, 7-1464
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:39:31.226782

perview_blk-2_cell-6_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1806
Features in dataset (100%):
Count: 1728

Average time in pred: 3.44383402941
Max time in pred: 34.5024030209
Min time in pred: 0.620491981506

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 1
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 636
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 13
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 9
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 864

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.766536964981
Occupied: 0.995241332427

Confusion Report:
[[ 197   60]
 [   7 1464]]
True Negative: 197
False Negative: 7
False Positive: 60 
True Positive: 1464 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00475866757308
False Positive Rate: 0.233463035019

             precision    recall  f1-score   support

      Empty     0.9657    0.7665    0.8547       257
   Occupied     0.9606    0.9952    0.9776      1471

avg / total     0.9614    0.9612    0.9593      1728


# blk-3_cell-6
## 0.7 width threshold
### 152-105, 0-1471
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:18:24.344068

perview_blk-3_cell-6_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1745
Features in dataset (100%):
Count: 1728

Average time in pred: 2.71120148497
Max time in pred: 32.7770321369
Min time in pred: 0.608077049255

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 921
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 4
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 640
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 11

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.591439688716
Occupied: 1.0

Confusion Report:
[[ 152  105]
 [   0 1471]]
True Negative: 152
False Negative: 0
False Positive: 105 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.408560311284

             precision    recall  f1-score   support

      Empty     1.0000    0.5914    0.7433       257
   Occupied     0.9334    1.0000    0.9655      1471

avg / total     0.9433    0.9392    0.9325      1728


# blk-4_cell-4
## 0.7 width threshold
### 186-71, 1-1470
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 3:24:54.276133

perview_blk-4_cell-4_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_2325
Features in dataset (100%):
Count: 1728

Average time in pred: 7.10252128735
Max time in pred: 78.9560370445
Min time in pred: 1.22269511223

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 8
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 8
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 634
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 891
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.72373540856
Occupied: 0.999320190347

Confusion Report:
[[ 186   71]
 [   1 1470]]
True Negative: 186
False Negative: 1
False Positive: 71 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.27626459144

             precision    recall  f1-score   support

      Empty     0.9947    0.7237    0.8378       257
   Occupied     0.9539    0.9993    0.9761      1471

avg / total     0.9600    0.9583    0.9555      1728

