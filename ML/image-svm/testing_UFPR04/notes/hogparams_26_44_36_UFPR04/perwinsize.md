<!-- TOC -->

- [Notes](#notes)
- [blk-2_cell-6](#blk-2_cell-6)
    - [0.6 width threshold](#06-width-threshold)
        - [179-78, 0-1471](#179-78-0-1471)
    - [0.7 width threshold](#07-width-threshold)
        - [200-57, 7-1464](#200-57-7-1464)
    - [0.8 width threshold](#08-width-threshold)
        - [230-27, 332-1139](#230-27-332-1139)
- [blk-3_cell-6](#blk-3_cell-6)
    - [0.6 width threshold](#06-width-threshold-1)
        - [167-90, 0-1471](#167-90-0-1471)
            - [Time elapsed: 2:15:28.526702](#time-elapsed-21528526702)
    - [0.7 width](#07-width)
        - [185-72, 0-1471](#185-72-0-1471)
            - [Time elapsed: 1:13:00.797665](#time-elapsed-11300797665)
    - [0.8 width threshold](#08-width-threshold-1)
- [blk-4_cell-4](#blk-4_cell-4)
    - [0.6 width threshold](#06-width-threshold-2)
        - [158-99, 1-1470](#158-99-1-1470)
    - [0.7 width threshold](#07-width-threshold-1)
        - [177-80, 1-1470](#177-80-1-1470)
    - [0.8 width threshold](#08-width-threshold-2)
        - [215-42, 333-1138](#215-42-333-1138)

<!-- /TOC -->
# Notes
- W/o Except
- W/o div
- First triggered

# blk-2_cell-6
## 0.6 width threshold
### 179-78, 0-1471
perwinsize_blk-2_cell-6_first_triggered_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_0552
Features in dataset (100%):
Count: 1728

Average time in pred: 2.9954965385
Max time in pred: 11.8867790699
Min time in pred: 0.62317609787

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 770
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 392
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 387

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.696498054475
Occupied: 1.0

Confusion Report:
[[ 179   78]
 [   0 1471]]
True Negative: 179
False Negative: 0
False Positive: 78 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.303501945525

             precision    recall  f1-score   support

      Empty     1.0000    0.6965    0.8211       257
   Occupied     0.9496    1.0000    0.9742      1471

avg / total     0.9571    0.9549    0.9514      1728


## 0.7 width threshold
### 200-57, 7-1464
perwinsize_blk-2_cell-6_first_triggered70_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1517
Features in dataset (100%):
Count: 1728

Average time in pred: 1.72490765748
Max time in pred: 12.4400629997
Min time in pred: 0.607089996338

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 635
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 330
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 556

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.778210116732
Occupied: 0.995241332427

Confusion Report:
[[ 200   57]
 [   7 1464]]
True Negative: 200
False Negative: 7
False Positive: 57 
True Positive: 1464 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00475866757308
False Positive Rate: 0.221789883268

             precision    recall  f1-score   support

      Empty     0.9662    0.7782    0.8621       257
   Occupied     0.9625    0.9952    0.9786      1471

avg / total     0.9631    0.9630    0.9613      1728


## 0.8 width threshold
### 230-27, 332-1139
perwinsize_blk-2_cell-6_first_triggered80_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1448
Features in dataset (100%):
Count: 1728

Average time in pred: 0.710761605314
Max time in pred: 3.47467398643
Min time in pred: 0.00895500183105

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 470
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 297
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 399

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.894941634241
Occupied: 0.774303195105

Confusion Report:
[[ 230   27]
 [ 332 1139]]
True Negative: 230
False Negative: 332
False Positive: 27 
True Positive: 1139 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.225696804895
False Positive Rate: 0.105058365759

             precision    recall  f1-score   support

      Empty     0.4093    0.8949    0.5617       257
   Occupied     0.9768    0.7743    0.8639      1471

avg / total     0.8924    0.7922    0.8189      1728

# blk-3_cell-6
## 0.6 width threshold
### 167-90, 0-1471
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 2:15:28.526702

perwinsize_blk-3_cell-6_060_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1843
Features in dataset (100%):
Count: 1728

Average time in pred: 4.69251340117
Max time in pred: 13.7890748978
Min time in pred: 0.597934961319

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 4
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 781
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 776

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.649805447471
Occupied: 1.0

Confusion Report:
[[ 167   90]
 [   0 1471]]
True Negative: 167
False Negative: 0
False Positive: 90 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.350194552529

             precision    recall  f1-score   support

      Empty     1.0000    0.6498    0.7877       257
   Occupied     0.9423    1.0000    0.9703      1471

avg / total     0.9509    0.9479    0.9432      1728


## 0.7 width 
### 185-72, 0-1471
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:13:00.797665

perwinsize_blk-3_cell-6_070_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1740
Features in dataset (100%):
Count: 1728

Average time in pred: 2.52362672819
Max time in pred: 16.1103689671
Min time in pred: 0.615898132324

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 4
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 901
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 638

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.719844357977
Occupied: 1.0

Confusion Report:
[[ 185   72]
 [   0 1471]]
True Negative: 185
False Negative: 0
False Positive: 72 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.280155642023

             precision    recall  f1-score   support

      Empty     1.0000    0.7198    0.8371       257
   Occupied     0.9533    1.0000    0.9761      1471

avg / total     0.9603    0.9583    0.9554      1728


## 0.8 width threshold


# blk-4_cell-4
## 0.6 width threshold
### 158-99, 1-1470
perwinsize_blk-4_cell-4_first_triggered_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_0750
Features in dataset (100%):
Count: 1728

Average time in pred: 7.02849627162
Max time in pred: 28.6289198399
Min time in pred: 0.40535902977

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 842
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 397
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 330

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.614785992218
Occupied: 0.999320190347

Confusion Report:
[[ 158   99]
 [   1 1470]]
True Negative: 158
False Negative: 1
False Positive: 99 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.385214007782

             precision    recall  f1-score   support

      Empty     0.9937    0.6148    0.7596       257
   Occupied     0.9369    0.9993    0.9671      1471

avg / total     0.9454    0.9421    0.9362      1728


## 0.7 width threshold
### 177-80, 1-1470
perwinsize_blk-4_cell-4_first_triggered70_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1614
Features in dataset (100%):
Count: 1728

Average time in pred: 3.69140350487
Max time in pred: 26.2257959843
Min time in pred: 0.400476932526

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 650
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 570
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 330

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.688715953307
Occupied: 0.999320190347

Confusion Report:
[[ 177   80]
 [   1 1470]]
True Negative: 177
False Negative: 1
False Positive: 80 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.311284046693

             precision    recall  f1-score   support

      Empty     0.9944    0.6887    0.8138       257
   Occupied     0.9484    0.9993    0.9732      1471

avg / total     0.9552    0.9531    0.9495      1728


## 0.8 width threshold
### 215-42, 333-1138
perwinsize_blk-4_cell-4_first_triggered80_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1512
Features in dataset (100%):
Count: 1728

Average time in pred: 1.52133414549
Max time in pred: 9.28687286377
Min time in pred: 0.00891995429993

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 453
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 397
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 330

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.836575875486
Occupied: 0.773623385452

Confusion Report:
[[ 215   42]
 [ 333 1138]]
True Negative: 215
False Negative: 333
False Positive: 42 
True Positive: 1138 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.226376614548
False Positive Rate: 0.163424124514

             precision    recall  f1-score   support

      Empty     0.3923    0.8366    0.5342       257
   Occupied     0.9644    0.7736    0.8585      1471

avg / total     0.8793    0.7830    0.8103      1728


