<!-- TOC -->

- [blk-2_cell-6](#blk-2_cell-6)
    - [0.7 width threshold](#07-width-threshold)
        - [200-57, 9-1462](#200-57-9-1462)
            - [Time elapsed: 1:17:52.677124](#time-elapsed-11752677124)
- [blk-3_cell-6](#blk-3_cell-6)
    - [0.7 width threshold](#07-width-threshold-1)
        - [186-71, 0-1471](#186-71-0-1471)
            - [Time elapsed: 1:10:43.317804](#time-elapsed-11043317804)
- [blk-4_cell-4](#blk-4_cell-4)
    - [0.7 width threshold](#07-width-threshold-2)
        - [175-82, 1-1470](#175-82-1-1470)
            - [Time elapsed: 2:53:06.826866](#time-elapsed-25306826866)

<!-- /TOC -->
# blk-2_cell-6
## 0.7 width threshold
### 200-57, 9-1462
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:17:52.677124
perwinsize_blk-2_cell-6_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1745
Features in dataset (100%):
Count: 1728

Average time in pred: 2.69238980639
Max time in pred: 11.3607041836
Min time in pred: 0.649103164673

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 636
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 1
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 882

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.778210116732
Occupied: 0.99388171312

Confusion Report:
[[ 200   57]
 [   9 1462]]
True Negative: 200
False Negative: 9
False Positive: 57 
True Positive: 1462 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00611828687967
False Positive Rate: 0.221789883268

             precision    recall  f1-score   support

      Empty     0.9569    0.7782    0.8584       257
   Occupied     0.9625    0.9939    0.9779      1471

avg / total     0.9617    0.9618    0.9601      1728



# blk-3_cell-6
## 0.7 width threshold
### 186-71, 0-1471
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:10:43.317804

perwinsize_blk-3_cell-6_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1738
Features in dataset (100%):
Count: 1728

Average time in pred: 2.44403137198
Max time in pred: 10.4330320358
Min time in pred: 0.597592830658

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 4
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 899
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 639

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.72373540856
Occupied: 1.0

Confusion Report:
[[ 186   71]
 [   0 1471]]
True Negative: 186
False Negative: 0
False Positive: 71 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.27626459144

             precision    recall  f1-score   support

      Empty     1.0000    0.7237    0.8397       257
   Occupied     0.9540    1.0000    0.9764      1471

avg / total     0.9608    0.9589    0.9561      1728



# blk-4_cell-4
## 0.7 width threshold
### 175-82, 1-1470
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 2:53:06.826866
perwinsize_blk-4_cell-4_070_notbalanced_results_eeei_5th_solar_041017_a_morning_parkinglinebased05072017_1920
Features in dataset (100%):
Count: 1728

Average time in pred: 5.99947308252
Max time in pred: 26.5830750465
Min time in pred: 1.26404285431

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 13
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 905
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 634

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.68093385214
Occupied: 0.999320190347

Confusion Report:
[[ 175   82]
 [   1 1470]]
True Negative: 175
False Negative: 1
False Positive: 82 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.31906614786

             precision    recall  f1-score   support

      Empty     0.9943    0.6809    0.8083       257
   Occupied     0.9472    0.9993    0.9725      1471

avg / total     0.9542    0.9520    0.9481      1728

