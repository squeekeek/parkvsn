<!-- TOC -->

- [blk-3_cell-6](#blk-3_cell-6)
    - [orientations: 9, C=0.1](#orientations-9-c01)
        - [207-50, 2-1469](#207-50-2-1469)
            - [Time elapsed: 1:13:47.759631](#time-elapsed-11347759631)
    - [orientations: 18, C=0.1](#orientations-18-c01)
        - [202-55, 2-1469](#202-55-2-1469)
            - [Time elapsed: 2:12:09.847031](#time-elapsed-21209847031)
    - [orientations: 18, C=1](#orientations-18-c1)
        - [196-61, 0-1471](#196-61-0-1471)
            - [Time elapsed: 2:11:27.689735](#time-elapsed-21127689735)

<!-- /TOC -->

# blk-3_cell-6
## orientations: 9, C=0.1
### 207-50, 2-1469
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:13:47.759631

perwinsize_36_9_01_results_eeei_5th_solar_041017_a_morning_parkinglinebased05082017_2127
Features in dataset (100%):
Count: 1728

Average time in pred: 2.5341246829
Max time in pred: 10.6325440407
Min time in pred: 0.605401039124

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 3
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 880
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 636

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.805447470817
Occupied: 0.998640380693

Confusion Report:
[[ 207   50]
 [   2 1469]]
True Negative: 207
False Negative: 2
False Positive: 50 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00135961930659
False Positive Rate: 0.194552529183

             precision    recall  f1-score   support

      Empty     0.9904    0.8054    0.8884       257
   Occupied     0.9671    0.9986    0.9826      1471

avg / total     0.9706    0.9699    0.9686      1728


## orientations: 18, C=0.1
### 202-55, 2-1469
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 2:12:09.847031

perwinsize_36_18_01_results_eeei_5th_solar_041017_a_morning_parkinglinebased05082017_2225
Features in dataset (100%):
Count: 1728

Average time in pred: 4.57812244749
Max time in pred: 19.1759898663
Min time in pred: 1.09619116783

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 9
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 875
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 640

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.785992217899
Occupied: 0.998640380693

Confusion Report:
[[ 202   55]
 [   2 1469]]
True Negative: 202
False Negative: 2
False Positive: 55 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00135961930659
False Positive Rate: 0.214007782101

             precision    recall  f1-score   support

      Empty     0.9902    0.7860    0.8764       257
   Occupied     0.9639    0.9986    0.9810      1471

avg / total     0.9678    0.9670    0.9654      1728


## orientations: 18, C=1
### 196-61, 0-1471
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 2:11:27.689735

perwinsize_36_18_1_results_eeei_5th_solar_041017_a_morning_parkinglinebased05082017_2225
Features in dataset (100%):
Count: 1728

Average time in pred: 4.5535423906
Max time in pred: 23.8258647919
Min time in pred: 1.09076094627

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 9
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 881
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 642

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.762645914397
Occupied: 1.0

Confusion Report:
[[ 196   61]
 [   0 1471]]
True Negative: 196
False Negative: 0
False Positive: 61 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.237354085603

             precision    recall  f1-score   support

      Empty     1.0000    0.7626    0.8653       257
   Occupied     0.9602    1.0000    0.9797      1471

avg / total     0.9661    0.9647    0.9627      1728

