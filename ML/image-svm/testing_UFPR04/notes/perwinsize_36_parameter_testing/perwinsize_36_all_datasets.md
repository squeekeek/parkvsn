<!-- TOC -->

- [051217_36_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05122017_2148](#051217_36_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05122017_2148)
    - [514-114, 0-308](#514-114-0-308)
        - [Time elapsed: 0:38:16.686090](#time-elapsed-03816686090)
- [051217_36_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05122017_2153](#051217_36_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05122017_2153)
    - [805-90, 0-140](#805-90-0-140)
        - [Time elapsed: 0:42:42.239003](#time-elapsed-04242239003)
- [051217_36_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05122017_2130](#051217_36_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05122017_2130)
    - [370-126, 0-0](#370-126-0-0)
        - [Time elapsed: 0:20:10.929428](#time-elapsed-02010929428)
- [051217_36_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05122017_2334](#051217_36_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b05122017_2334)
    - [2925-619, 0-0](#2925-619-0-0)
        - [Time elapsed: 2:24:23.022327](#time-elapsed-22423022327)
- [051217_36_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05122017_2215](#051217_36_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05122017_2215)
    - [1165-163, 0-0](#1165-163-0-0)
        - [Time elapsed: 1:05:28.947317](#time-elapsed-10528947317)
- [051217_36_all_tests_6_results_eeei_CNL_031717_morning05122017_2134](#051217_36_all_tests_6_results_eeei_cnl_031717_morning05122017_2134)
    - [144-134, 0-14](#144-134-0-14)
        - [Time elapsed: 0:23:49.537045](#time-elapsed-02349537045)

<!-- /TOC -->

# 051217_36_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05122017_2148
## 514-114, 0-308
Parking lot images: 104
Segmented spaces: 936
### Time elapsed: 0:38:16.686090

Features in dataset (100%):
Count: 936

Average time in pred: 2.43735430893
Max time in pred: 10.5155098438
Min time in pred: 0.155346155167

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 1
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 198
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 223

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.81847133758
Occupied: 1.0

Confusion Report:
[[514 114]
 [  0 308]]
True Negative: 514
False Negative: 0
False Positive: 114 
True Positive: 308 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.18152866242

             precision    recall  f1-score   support

      Empty     1.0000    0.8185    0.9002       628
   Occupied     0.7299    1.0000    0.8438       308

avg / total     0.9111    0.8782    0.8816       936



# 051217_36_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05122017_2153
## 805-90, 0-140
Parking lot images: 115
Segmented spaces: 1035
### Time elapsed: 0:42:42.239003

Features in dataset (100%):
Count: 1035

Average time in pred: 2.46186615741
Max time in pred: 10.6286270618
Min time in pred: 0.137377023697

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 1
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 85
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 144

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.899441340782
Occupied: 1.0

Confusion Report:
[[805  90]
 [  0 140]]
True Negative: 805
False Negative: 0
False Positive: 90 
True Positive: 140 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.100558659218

             precision    recall  f1-score   support

      Empty     1.0000    0.8994    0.9471       895
   Occupied     0.6087    1.0000    0.7568       140

avg / total     0.9471    0.9130    0.9213      1035

# 051217_36_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05122017_2130
## 370-126, 0-0
Parking lot images: 62
Segmented spaces: 496
### Time elapsed: 0:20:10.929428
Features in dataset (100%):
Count: 496

Average time in pred: 2.42795336919
Max time in pred: 12.3204200268
Min time in pred: 0.00257897377014

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 1
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 101
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 24

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.745967741935
Occupied: None

Confusion Report:
[[370 126]
 [  0   0]]
True Negative: 370
False Negative: 0
False Positive: 126 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.254032258065

             precision    recall  f1-score   support

      Empty     1.0000    0.7460    0.8545       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7460    0.8545       496

#051217_36_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05122017_2334
## 2925-619, 0-0
Parking lot images: 443
Segmented spaces: 3544
### Time elapsed: 2:24:23.022327
Features in dataset (100%):
Count: 3544

Average time in pred: 2.43092356827
Max time in pred: 10.6525640488
Min time in pred: 0.00209784507751

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 3
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 581
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 35

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.825338600451
Occupied: None

Confusion Report:
[[2925  619]
 [   0    0]]
True Negative: 2925
False Negative: 0
False Positive: 619 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.174661399549

             precision    recall  f1-score   support

      Empty     1.0000    0.8253    0.9043      3544
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.8253    0.9043      3544

# 051217_36_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05122017_2215
## 1165-163, 0-0
Parking lot images: 166
Segmented spaces: 1328
### Time elapsed: 1:05:28.947317

Features in dataset (100%):
Count: 1328

Average time in pred: 2.94535218449
Max time in pred: 12.2882628441
Min time in pred: 0.00210809707642

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 2
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 142
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 19

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.877259036145
Occupied: None

Confusion Report:
[[1165  163]
 [   0    0]]
True Negative: 1165
False Negative: 0
False Positive: 163 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.122740963855

             precision    recall  f1-score   support

      Empty     1.0000    0.8773    0.9346      1328
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.8773    0.9346      1328

# 051217_36_all_tests_6_results_eeei_CNL_031717_morning05122017_2134
## 144-134, 0-14
Parking lot images: 73
Segmented spaces: 292
### Time elapsed: 0:23:49.537045
Features in dataset (100%):
Count: 292

Average time in pred: 4.86829360054
Max time in pred: 9.50536417961
Min time in pred: 0.15350484848

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 86
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 20
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 42

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.517985611511
Occupied: 1.0

Confusion Report:
[[144 134]
 [  0  14]]
True Negative: 144
False Negative: 0
False Positive: 134 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.482014388489

             precision    recall  f1-score   support

      Empty     1.0000    0.5180    0.6825       278
   Occupied     0.0946    1.0000    0.1728        14

avg / total     0.9566    0.5411    0.6580       292





