<!-- TOC -->

- [051417_36_perwinsize_w_except_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05142017_2101](#051417_36_perwinsize_w_except_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05142017_2101)
    - [426-202, 6-302](#426-202-6-302)
        - [Time elapsed: 0:37:14.951660](#time-elapsed-03714951660)
- [051417_36_perwinsize_w_except_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05142017_2102](#051417_36_perwinsize_w_except_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05142017_2102)
    - [701-194, 0-140](#701-194-0-140)
        - [Time elapsed: 0:38:29.735158](#time-elapsed-03829735158)
- [051417_36_perwinsize_w_except_tests_3_results_eeei_5th_solar_040517_040717_morning_a05142017_2045](#051417_36_perwinsize_w_except_tests_3_results_eeei_5th_solar_040517_040717_morning_a05142017_2045)
        - [sTime elapsed: 0:20:42.742719](#stime-elapsed-02042742719)
- [051417_36_perwinsize_w_except_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05142017_2236](#051417_36_perwinsize_w_except_tests_4_results_eeei_5th_solar_040517_040717_morning_b05142017_2236)
    - [2524-1020, 0-0](#2524-1020-0-0)
        - [Time elapsed: 0:20:42.742719](#time-elapsed-02042742719)
- [051417_36_perwinsize_w_except_tests_5_results_eeei_5th_solar_040517_040717_morning_b05142017_2124](#051417_36_perwinsize_w_except_tests_5_results_eeei_5th_solar_040517_040717_morning_b05142017_2124)
    - [959-369, 0-0](#959-369-0-0)
        - [Time elapsed: 0:20:42.742719](#time-elapsed-02042742719-1)
- [051417_36_perwinsize_w_except_tests_6_results_eeei_CNL_031717_morning05142017_2045](#051417_36_perwinsize_w_except_tests_6_results_eeei_cnl_031717_morning05142017_2045)
    - [164-114, 0-14](#164-114-0-14)
        - [Time elapsed: 0:20:42.742719](#time-elapsed-02042742719-2)
- [051417_36_perwinsize_w_except_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2136](#051417_36_perwinsize_w_except_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2136)
    - [154-103, 51-1420](#154-103-51-1420)
        - [Time elapsed: 1:35:16.261877](#time-elapsed-13516261877)

<!-- /TOC -->
# 051417_36_perwinsize_w_except_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05142017_2101
## 426-202, 6-302
Parking lot images: 104
Segmented spaces: 936
### Time elapsed: 0:37:14.951660

Features in dataset (100%):
Count: 936

Average time in pred: 2.3764052335
Max time in pred: 10.7990498543
Min time in pred: 0.159250974655

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 259
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 3
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 242

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.678343949045
Occupied: 0.980519480519

Confusion Report:
[[426 202]
 [  6 302]]
True Negative: 426
False Negative: 6
False Positive: 202 
True Positive: 302 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0194805194805
False Positive Rate: 0.321656050955

             precision    recall  f1-score   support

      Empty     0.9861    0.6783    0.8038       628
   Occupied     0.5992    0.9805    0.7438       308

avg / total     0.8588    0.7778    0.7841       936

# 051417_36_perwinsize_w_except_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05142017_2102
## 701-194, 0-140
Parking lot images: 115
Segmented spaces: 1035
### Time elapsed: 0:38:29.735158
Features in dataset (100%):
Count: 1035

Average time in pred: 2.22035344511
Max time in pred: 12.7639849186
Min time in pred: 0.135259866714

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 184
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 149

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.783240223464
Occupied: 1.0

Confusion Report:
[[701 194]
 [  0 140]]
True Negative: 701
False Negative: 0
False Positive: 194 
True Positive: 140 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.216759776536

             precision    recall  f1-score   support

      Empty     1.0000    0.7832    0.8784       895
   Occupied     0.4192    1.0000    0.5907       140

avg / total     0.9214    0.8126    0.8395      1035

# 051417_36_perwinsize_w_except_tests_3_results_eeei_5th_solar_040517_040717_morning_a05142017_2045

Parking lot images: 62
Segmented spaces: 496
### sTime elapsed: 0:20:42.742719

Features in dataset (100%):
Count: 496

Average time in pred: 2.49392243018
Max time in pred: 12.7094731331
Min time in pred: 0.00258612632751

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 107
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 2
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 41

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.697580645161
Occupied: None

Confusion Report:
[[346 150]
 [  0   0]]
True Negative: 346
False Negative: 0
False Positive: 150 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.302419354839

             precision    recall  f1-score   support

      Empty     1.0000    0.6976    0.8219       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.6976    0.8219       496

# 051417_36_perwinsize_w_except_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05142017_2236
## 2524-1020, 0-0
Parking lot images: 62
Segmented spaces: 496
### Time elapsed: 0:20:42.742719

Features in dataset (100%):
Count: 3544

Average time in pred: 2.22882026528
Max time in pred: 12.464441061
Min time in pred: 0.00211691856384

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 800
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 3
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 217

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.712189616253
Occupied: None

Confusion Report:
[[2524 1020]
 [   0    0]]
True Negative: 2524
False Negative: 0
False Positive: 1020 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.287810383747

             precision    recall  f1-score   support

      Empty     1.0000    0.7122    0.8319      3544
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7122    0.8319      3544

# 051417_36_perwinsize_w_except_tests_5_results_eeei_5th_solar_040517_040717_morning_b05142017_2124
## 959-369, 0-0
Parking lot images: 62
Segmented spaces: 496
### Time elapsed: 0:20:42.742719

Features in dataset (100%):
Count: 1328

Average time in pred: 2.70285450227
Max time in pred: 12.5524098873
Min time in pred: 0.00214910507202

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 248
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 120

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.722138554217
Occupied: None

Confusion Report:
[[959 369]
 [  0   0]]
True Negative: 959
False Negative: 0
False Positive: 369 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.277861445783

             precision    recall  f1-score   support

      Empty     1.0000    0.7221    0.8387      1328
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7221    0.8387      1328

# 051417_36_perwinsize_w_except_tests_6_results_eeei_CNL_031717_morning05142017_2045
## 164-114, 0-14
Parking lot images: 62
Segmented spaces: 496
###Time elapsed: 0:20:42.742719

Features in dataset (100%):
Count: 292

Average time in pred: 4.25557986841
Max time in pred: 9.67039704323
Min time in pred: 0.156856060028

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 44
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 5
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 79

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.589928057554
Occupied: 1.0

Confusion Report:
[[164 114]
 [  0  14]]
True Negative: 164
False Negative: 0
False Positive: 114 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.410071942446

             precision    recall  f1-score   support

      Empty     1.0000    0.5899    0.7421       278
   Occupied     0.1094    1.0000    0.1972        14

avg / total     0.9573    0.6096    0.7160       292

# 051417_36_perwinsize_w_except_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2136
## 154-103, 51-1420
Parking lot images: 192
Segmented spaces: 1728
### Time elapsed: 1:35:16.261877
Features in dataset (100%):
Count: 1728

Average time in pred: 2.46011651018
Max time in pred: 10.5973100662
Min time in pred: 0.600157976151

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 872
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 22
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 629

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.599221789883
Occupied: 0.965329707682

Confusion Report:
[[ 154  103]
 [  51 1420]]
True Negative: 154
False Negative: 51
False Positive: 103 
True Positive: 1420 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0346702923182
False Positive Rate: 0.400778210117

             precision    recall  f1-score   support

      Empty     0.7512    0.5992    0.6667       257
   Occupied     0.9324    0.9653    0.9486      1471

avg / total     0.9054    0.9109    0.9066      1728

