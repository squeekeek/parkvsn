
<!-- TOC -->

- [Tue May 9 22:04:35 PHT 2017###### Tue May 9 22:04:36 PHT 2017](#tue-may-9-220435-pht-2017-tue-may-9-220436-pht-2017)
- [blk-3_cell-6](#blk-3_cell-6)
    - [w_flip C=0.1 orientations=0](#w_flip-c01-orientations0)
        - [186-71, 3-1468](#186-71-3-1468)
            - [Time elapsed: 1:11:26.265030](#time-elapsed-11126265030)
    - [w_flip C=1 orientations=9](#w_flip-c1-orientations9)
        - [174-83, 4-1467](#174-83-4-1467)
            - [Time elapsed: 1:11:26.265030](#time-elapsed-11126265030-1)

<!-- /TOC -->

# Tue May 9 22:04:35 PHT 2017###### Tue May 9 22:04:36 PHT 2017

# blk-3_cell-6
## w_flip C=0.1 orientations=0
### 186-71, 3-1468
Parking lot images: 192
Segmented spaces: 1728
#### Time elapsed: 1:11:26.265030

perwinsize_36_flip_01_results_eeei_5th_solar_041017_a_morning_parkinglinebased05092017_2104
Features in dataset (100%):
Count: 1728

Average time in pred: 2.46837525925
Max time in pred: 10.5763599873
Min time in pred: 0.591088056564

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 7
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 894
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 638

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.72373540856
Occupied: 0.99796057104

Confusion Report:
[[ 186   71]
 [   3 1468]]
True Negative: 186
False Negative: 3
False Positive: 71 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00203942895989
False Positive Rate: 0.27626459144

             precision    recall  f1-score   support

      Empty     0.9841    0.7237    0.8341       257
   Occupied     0.9539    0.9980    0.9754      1471

avg / total     0.9584    0.9572    0.9544      1728


## w_flip C=1 orientations=9
### 174-83, 4-1467
Parking lot images: 192
Segmented spaces: 1728
####Time elapsed: 1:11:26.265030


perwinsize_36_flip_1_results_eeei_5th_solar_041017_a_morning_parkinglinebased05092017_2105
Features in dataset (100%):
Count: 1728

Average time in pred: 2.47418932451
Max time in pred: 12.9515049458
Min time in pred: 0.596216917038

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 4
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 907
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 639

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.677042801556
Occupied: 0.997280761387

Confusion Report:
[[ 174   83]
 [   4 1467]]
True Negative: 174
False Negative: 4
False Positive: 83 
True Positive: 1467 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00271923861319
False Positive Rate: 0.322957198444

             precision    recall  f1-score   support

      Empty     0.9775    0.6770    0.8000       257
   Occupied     0.9465    0.9973    0.9712      1471

avg / total     0.9511    0.9497    0.9457      1728

