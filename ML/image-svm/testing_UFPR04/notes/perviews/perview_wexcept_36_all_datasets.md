<!-- TOC -->

- [051317_36_perview_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05132017_2216](#051317_36_perview_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05132017_2216)
    - [403-225, 17-291](#403-225-17-291)
        - [Time elapsed: 1:12:18.079667](#time-elapsed-11218079667)
- [051317_36_perview_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05132017_2236](#051317_36_perview_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05132017_2236)
    - [586-309, 0-140](#586-309-0-140)
        - [Time elapsed: 1:32:54.266292](#time-elapsed-13254266292)
- [051317_36_perview_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05132017_2148](#051317_36_perview_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05132017_2148)
    - [352-144, 0-0](#352-144-0-0)
        - [Time elapsed: 0:44:58.652964](#time-elapsed-04458652964)
- [051317_36_perview_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05142017_0113](#051317_36_perview_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b05142017_0113)
    - [2241-1303, 0-0](#2241-1303-0-0)
        - [Time elapsed: 4:10:15.162646](#time-elapsed-41015162646)
- [051317_36_perview_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05132017_2313](#051317_36_perview_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05132017_2313)
    - [975-353, 0-0](#975-353-0-0)
        - [Time elapsed: 2:09:45.562622](#time-elapsed-20945562622)
- [051317_36_perview_all_tests_6_results_eeei_CNL_031717_morning05132017_2158](#051317_36_perview_all_tests_6_results_eeei_cnl_031717_morning05132017_2158)
    - [156-122, 0-14](#156-122-0-14)
        - [Time elapsed: 0:55:29.219428](#time-elapsed-05529219428)

- [051317_36_perview_all_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2159](#051317_36_perview_all_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2159)
    - [149-109, 38-1433](#149-109-38-1433)
        - [Time elapsed: 1:11:12.329871](#time-elapsed-11112329871)

<!-- /TOC -->
# 051317_36_perview_all_tests_1_results_eeei_5th_solar_040217_to_040317_morning_a05132017_2216
## 403-225, 17-291
Parking lot images: 104
Segmented spaces: 936
### Time elapsed: 1:12:18.079667
Features in dataset (100%):
Count: 936

Average time in pred: 4.62378444809
Max time in pred: 39.3613381386
Min time in pred: 0.156887054443

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 199
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 6
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 52
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 225
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 34
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.641719745223
Occupied: 0.944805194805

Confusion Report:
[[403 225]
 [ 17 291]]
True Negative: 403
False Negative: 17
False Positive: 225 
True Positive: 291 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0551948051948
False Positive Rate: 0.358280254777

             precision    recall  f1-score   support

      Empty     0.9595    0.6417    0.7691       628
   Occupied     0.5640    0.9448    0.7063       308

avg / total     0.8294    0.7415    0.7484       936

# 051317_36_perview_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05132017_2236
## 586-309, 0-140
Parking lot images: 115
Segmented spaces: 1035
### Time elapsed: 1:32:54.266292
Features in dataset (100%):
Count: 1035

Average time in pred: 5.37235715009
Max time in pred: 33.3994650841
Min time in pred: 0.139168977737

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 134
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 9
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 76
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 157
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 71
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 2
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.654748603352
Occupied: 1.0

Confusion Report:
[[586 309]
 [  0 140]]
True Negative: 586
False Negative: 0
False Positive: 309 
True Positive: 140 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.345251396648

             precision    recall  f1-score   support

      Empty     1.0000    0.6547    0.7914       895
   Occupied     0.3118    1.0000    0.4754       140

avg / total     0.9069    0.7014    0.7486      1035

# 051317_36_perview_all_tests_3_results_eeei_5th_solar_040517_040717_morning_a05132017_2148
## 352-144, 0-0
Parking lot images: 62
Segmented spaces: 496
### Time elapsed: 0:44:58.652964
Features in dataset (100%):
Count: 496

Average time in pred: 5.42752596111
Max time in pred: 40.6865339279
Min time in pred: 0.0062210559845

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 28
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 15
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 98
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.709677419355
Occupied: None

Confusion Report:
[[352 144]
 [  0   0]]
True Negative: 352
False Negative: 0
False Positive: 144 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.290322580645

             precision    recall  f1-score   support

      Empty     1.0000    0.7097    0.8302       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7097    0.8302       496

# 051317_36_perview_all_tests_4_results_eeei_5th_solar_040517_040717_morning_b.05142017_0113
## 2241-1303, 0-0
Parking lot images: 443
Segmented spaces: 3544
### Time elapsed: 4:10:15.162646

Features in dataset (100%):
Count: 3544

Average time in pred: 4.22274202122
Max time in pred: 34.9921388626
Min time in pred: 0.00566291809082

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 177
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 2
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 10
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 110
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 816
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 185
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.632336343115
Occupied: None

Confusion Report:
[[2241 1303]
 [   0    0]]
True Negative: 2241
False Negative: 0
False Positive: 1303 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.367663656885

             precision    recall  f1-score   support

      Empty     1.0000    0.6323    0.7748      3544
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.6323    0.7748      3544

# 051317_36_perview_all_tests_5_results_eeei_5th_solar_040517_040717_morning_b05132017_2313
## 975-353, 0-0
Parking lot images: 166
Segmented spaces: 1328
### Time elapsed: 2:09:45.562622

Features in dataset (100%):
Count: 1328

Average time in pred: 5.84863752073
Max time in pred: 50.2849040031
Min time in pred: 0.00561594963074

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 34
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 6
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 101
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 196
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 15
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.734186746988
Occupied: None

Confusion Report:
[[975 353]
 [  0   0]]
True Negative: 975
False Negative: 0
False Positive: 353 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.265813253012

             precision    recall  f1-score   support

      Empty     1.0000    0.7342    0.8467      1328
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7342    0.8467      1328

# 051317_36_perview_all_tests_6_results_eeei_CNL_031717_morning05132017_2158
## 156-122, 0-14
Parking lot images: 73
Segmented spaces: 292
### Time elapsed: 0:55:29.219428

Features in dataset (100%):
Count: 292

Average time in pred: 11.3711572281
Max time in pred: 34.2282660007
Min time in pred: 0.158410072327

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 52
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 10
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 3
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 44
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 25
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 2
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.561151079137
Occupied: 1.0

Confusion Report:
[[156 122]
 [  0  14]]
True Negative: 156
False Negative: 0
False Positive: 122 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.438848920863

             precision    recall  f1-score   support

      Empty     1.0000    0.5612    0.7189       278
   Occupied     0.1029    1.0000    0.1867        14

avg / total     0.9570    0.5822    0.6934       292


# 051317_36_perview_all_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05142017_2159
## 149-109, 38-1433
Parking lot images: 192
Segmented spaces: 1728
### Time elapsed: 1:11:12.329871

Features in dataset (100%):
Count: 1728

Average time in pred: 3.29691582808
Max time in pred: 35.1934561729
Min time in pred: 0.63879609108

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 625
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 5
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 50
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 830
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 31
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.579766536965
Occupied: 0.974167233175

Confusion Report:
[[ 149  108]
 [  38 1433]]
True Negative: 149
False Negative: 38
False Positive: 108 
True Positive: 1433 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0258327668253
False Positive Rate: 0.420233463035

             precision    recall  f1-score   support

      Empty     0.7968    0.5798    0.6712       257
   Occupied     0.9299    0.9742    0.9515      1471

avg / total     0.9101    0.9155    0.9098      1728

