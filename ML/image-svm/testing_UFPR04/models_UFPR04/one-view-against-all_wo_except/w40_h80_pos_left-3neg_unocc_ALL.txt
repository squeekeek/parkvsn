Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except/w40_h80_pos_left-3neg_unocc_ALL.model
Features in dataset (100%):
Count: 61830
Features in training dataset (60%):
Count: 37098
Counter({0: 35783, 1: 1315})
Features in cross-validation dataset (20%):
Count: 12366
Counter({0: 11974, 1: 392})
Features in test dataset (20%):
Count: 12366
Counter({0: 11961, 1: 405})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11974
   Occupied       1.00      1.00      1.00       392

avg / total       1.00      1.00      1.00     12366
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11961
   Occupied       1.00      1.00      1.00       405

avg / total       1.00      1.00      1.00     12366
