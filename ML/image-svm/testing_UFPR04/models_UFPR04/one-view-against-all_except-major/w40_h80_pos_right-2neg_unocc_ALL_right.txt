Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80
/home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/data/models/one-view-against-all_except-major/w40_h80_pos_right-2neg_unocc_ALL_right.model
Features in dataset (100%):
Count: 97826
Features in training dataset (60%):
Count: 58695
Counter({0: 54177, 1: 4518})
Features in cross-validation dataset (20%):
Count: 19566
Counter({0: 18065, 1: 1501})
Features in test dataset (20%):
Count: 19565
Counter({0: 18111, 1: 1454})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     18065
   Occupied       1.00      1.00      1.00      1501

avg / total       1.00      1.00      1.00     19566
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     18111
   Occupied       1.00      0.99      1.00      1454

avg / total       1.00      1.00      1.00     19565
