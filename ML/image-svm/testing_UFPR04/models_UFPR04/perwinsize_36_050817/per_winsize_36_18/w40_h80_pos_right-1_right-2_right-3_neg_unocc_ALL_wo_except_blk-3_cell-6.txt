Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_winsize_36_18/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_blk-3_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_winsize_36_18/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-3_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_winsize_36_18/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-3_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_winsize_36_18/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-3_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_winsize_36_18/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9993    0.9991    0.9992     11902
   Occupied     0.9965    0.9975    0.9970      3140

avg / total     0.9987    0.9987    0.9987     15042
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9988    0.9990     11942
   Occupied     0.9955    0.9968    0.9961      3100

avg / total     0.9984    0.9984    0.9984     15042
