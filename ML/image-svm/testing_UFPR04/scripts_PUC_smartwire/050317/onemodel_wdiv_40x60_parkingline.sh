
# eeei_5th_solar_041017_a_morning: parkinglinebased
python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_first_triggered/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_041017_a_morning \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json \
-i y \
-n puc_onemodel_wdiv_40x60 \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_PUC/onemodel_w40_h60_w_div \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_PUC/onemodel_w40_h60_w_div/models.json \
-hj /home/agatha/Documents/parkvsn/ML/image-svm/config/puc_hog_list.json \
-ho blk-4_cell-4 

