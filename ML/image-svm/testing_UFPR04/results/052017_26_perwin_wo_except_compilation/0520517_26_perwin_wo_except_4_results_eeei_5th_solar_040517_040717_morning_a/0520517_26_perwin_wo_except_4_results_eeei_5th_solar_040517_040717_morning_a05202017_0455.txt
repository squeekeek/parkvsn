0520517_26_perwin_wo_except_4_results_eeei_5th_solar_040517_040717_morning_a05202017_0455
Features in dataset (100%):
Count: 496

Average time in pred: 2.521081363
Max time in pred: 12.3093559742
Min time in pred: 0.00265097618103

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 11
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 5
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 87

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.792338709677
Occupied: None

Confusion Report:
[[393 103]
 [  0   0]]
True Negative: 393
False Negative: 0
False Positive: 103 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.207661290323

             precision    recall  f1-score   support

      Empty     1.0000    0.7923    0.8841       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7923    0.8841       496

