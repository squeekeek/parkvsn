parakeet_bs_results_eeei_CNL_031717_morning05222017_0443
Features in dataset (100%):
Count: 292

Images in dataset(100%):Count: 73

Average time in pred: 0.0
Max time in pred: 0
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.0949178101265
Max time in pred: 0.173000097275
Min time in pred: 0.0859999656677

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.212230215827
Occupied: 1.0

Confusion Report:
[[ 59 219]
 [  0  14]]
True Negative: 59
False Negative: 0
False Positive: 219 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.787769784173

             precision    recall  f1-score   support

      Empty     1.0000    0.2122    0.3501       278
   Occupied     0.0601    1.0000    0.1134        14

avg / total     0.9549    0.2500    0.3388       292

