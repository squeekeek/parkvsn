parakeet_bs_results_eeei_CNL_031717_morning05222017_0433
Features in dataset (100%):
Count: 292

Images in dataset(100%):Count: 73

Average time in pred: 0.0
Max time in pred: 0
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.0926575334105
Max time in pred: 0.170000076294
Min time in pred: 0.0839998722076

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0611510791367
Occupied: 1.0

Confusion Report:
[[ 17 261]
 [  0  14]]
True Negative: 17
False Negative: 0
False Positive: 261 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.938848920863

             precision    recall  f1-score   support

      Empty     1.0000    0.0612    0.1153       278
   Occupied     0.0509    1.0000    0.0969        14

avg / total     0.9545    0.1062    0.1144       292

