052217_36_perwin_wo_except_4_results_eeei_5th_solar_040517_040717_morning_a05222017_2042
Features in dataset (100%):
Count: 496

Average time in pred: 2.508890139
Max time in pred: 12.4631261826
Min time in pred: 0.0024938583374

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 2
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 72
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 20

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.810483870968
Occupied: None

Confusion Report:
[[402  94]
 [  0   0]]
True Negative: 402
False Negative: 0
False Positive: 94 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.189516129032

             precision    recall  f1-score   support

      Empty     1.0000    0.8105    0.8953       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.8105    0.8953       496

