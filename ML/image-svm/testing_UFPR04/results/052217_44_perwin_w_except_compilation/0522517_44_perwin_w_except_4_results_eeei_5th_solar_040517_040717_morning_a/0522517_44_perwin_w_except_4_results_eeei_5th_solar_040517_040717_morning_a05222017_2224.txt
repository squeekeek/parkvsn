0522517_44_perwin_w_except_4_results_eeei_5th_solar_040517_040717_morning_a05222017_2224
Features in dataset (100%):
Count: 496

Average time in pred: 5.80753744321
Max time in pred: 30.6834490299
Min time in pred: 0.00272798538208

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-4_cell-4_wo_div.model: 38
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-4_cell-4_wo_div.model: 5
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-4_cell-4_wo_div.model: 111

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.689516129032
Occupied: None

Confusion Report:
[[342 154]
 [  0   0]]
True Negative: 342
False Negative: 0
False Positive: 154 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.310483870968

             precision    recall  f1-score   support

      Empty     1.0000    0.6895    0.8162       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.6895    0.8162       496

