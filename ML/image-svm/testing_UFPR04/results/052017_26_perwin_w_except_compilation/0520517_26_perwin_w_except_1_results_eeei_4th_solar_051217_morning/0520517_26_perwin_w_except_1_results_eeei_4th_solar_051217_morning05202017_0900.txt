0520517_26_perwin_w_except_1_results_eeei_4th_solar_051217_morning05202017_0900
Features in dataset (100%):
Count: 3528

Average time in pred: 2.27911044634
Max time in pred: 9.30552506447
Min time in pred: 0.0909481048584

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-2_cell-6_wo_div.model: 1558
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-2_cell-6_wo_div.model: 8
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-2_cell-6_wo_div.model: 1573

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.790178571429
Occupied: 0.93583535109

Confusion Report:
[[ 177   47]
 [ 212 3092]]
True Negative: 177
False Negative: 212
False Positive: 47 
True Positive: 3092 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0641646489104
False Positive Rate: 0.209821428571

             precision    recall  f1-score   support

      Empty     0.4550    0.7902    0.5775       224
   Occupied     0.9850    0.9358    0.9598      3304

avg / total     0.9514    0.9266    0.9355      3528

