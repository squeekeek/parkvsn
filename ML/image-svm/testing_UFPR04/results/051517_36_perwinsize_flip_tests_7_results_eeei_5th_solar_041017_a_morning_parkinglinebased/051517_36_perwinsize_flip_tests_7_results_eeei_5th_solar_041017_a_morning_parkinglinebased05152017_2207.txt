051517_36_perwinsize_flip_tests_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05152017_2207
Features in dataset (100%):
Count: 1728

Average time in pred: 2.54398841894
Max time in pred: 13.7451939583
Min time in pred: 0.610713005066

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 7
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 894
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 638

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.72373540856
Occupied: 0.99796057104

Confusion Report:
[[ 186   71]
 [   3 1468]]
True Negative: 186
False Negative: 3
False Positive: 71 
True Positive: 1468 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00203942895989
False Positive Rate: 0.27626459144

             precision    recall  f1-score   support

      Empty     0.9841    0.7237    0.8341       257
   Occupied     0.9539    0.9980    0.9754      1471

avg / total     0.9584    0.9572    0.9544      1728

