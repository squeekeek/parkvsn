perwinsize_blk-2_cell-6_first_triggered80_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1448
Features in dataset (100%):
Count: 1728

Average time in pred: 0.710761605314
Max time in pred: 3.47467398643
Min time in pred: 0.00895500183105

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 470
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 297
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-6_wo_div.model: 399

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.894941634241
Occupied: 0.774303195105

Confusion Report:
[[ 230   27]
 [ 332 1139]]
True Negative: 230
False Negative: 332
False Positive: 27 
True Positive: 1139 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.225696804895
False Positive Rate: 0.105058365759

             precision    recall  f1-score   support

      Empty     0.4093    0.8949    0.5617       257
   Occupied     0.9768    0.7743    0.8639      1471

avg / total     0.8924    0.7922    0.8189      1728

