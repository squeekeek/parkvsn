perwinsize_blk-4_cell-4_first_triggered70_results_eeei_5th_solar_041017_a_morning_parkinglinebased05062017_1614
Features in dataset (100%):
Count: 1728

Average time in pred: 3.69140350487
Max time in pred: 26.2257959843
Min time in pred: 0.400476932526

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 650
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 570
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 330

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.688715953307
Occupied: 0.999320190347

Confusion Report:
[[ 177   80]
 [   1 1470]]
True Negative: 177
False Negative: 1
False Positive: 80 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.311284046693

             precision    recall  f1-score   support

      Empty     0.9944    0.6887    0.8138       257
   Occupied     0.9484    0.9993    0.9732      1471

avg / total     0.9552    0.9531    0.9495      1728

