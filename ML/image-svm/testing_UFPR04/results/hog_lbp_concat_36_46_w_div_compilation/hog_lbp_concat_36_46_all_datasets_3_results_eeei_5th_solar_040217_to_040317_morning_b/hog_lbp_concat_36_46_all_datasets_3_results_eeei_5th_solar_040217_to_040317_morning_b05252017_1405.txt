hog_lbp_concat_36_46_all_datasets_3_results_eeei_5th_solar_040217_to_040317_morning_b05252017_1405
Features in dataset (100%):
Count: 1035

Average time in pred: 1.04503103777
Max time in pred: 5.21023988724
Min time in pred: 0.0612790584564

List of model detections:
w40_h80_hog_lbp_right.model: 0
w40_h50_hog_lbp_center.model: 445
w30_h80_hog_lbp_left.model: 157

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.483798882682
Occupied: 1.0

Confusion Report:
[[433 462]
 [  0 140]]
True Negative: 433
False Negative: 0
False Positive: 462 
True Positive: 140 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.516201117318

             precision    recall  f1-score   support

      Empty     1.0000    0.4838    0.6521       895
   Occupied     0.2326    1.0000    0.3774       140

avg / total     0.8962    0.5536    0.6149      1035

