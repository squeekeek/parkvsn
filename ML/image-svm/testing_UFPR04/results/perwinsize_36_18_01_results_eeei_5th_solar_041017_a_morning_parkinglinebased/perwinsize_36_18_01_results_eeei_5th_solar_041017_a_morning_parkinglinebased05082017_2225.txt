Parking lot images: 192
Segmented spaces: 1728
Time elapsed: 2:12:09.847031

perwinsize_36_18_01_results_eeei_5th_solar_041017_a_morning_parkinglinebased05082017_2225
Features in dataset (100%):
Count: 1728

Average time in pred: 4.57812244749
Max time in pred: 19.1759898663
Min time in pred: 1.09619116783

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 9
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 875
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-3_cell-6_wo_div.model: 640

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.785992217899
Occupied: 0.998640380693

Confusion Report:
[[ 202   55]
 [   2 1469]]
True Negative: 202
False Negative: 2
False Positive: 55 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00135961930659
False Positive Rate: 0.214007782101

             precision    recall  f1-score   support

      Empty     0.9902    0.7860    0.8764       257
   Occupied     0.9639    0.9986    0.9810      1471

avg / total     0.9678    0.9670    0.9654      1728

