0522517_44_perwin_wo_except_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05222017_2337
Features in dataset (100%):
Count: 1728

Average time in pred: 5.99522883831
Max time in pred: 26.8023238182
Min time in pred: 1.27658200264

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 9
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 883
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 638

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.766536964981
Occupied: 0.999320190347

Confusion Report:
[[ 197   60]
 [   1 1470]]
True Negative: 197
False Negative: 1
False Positive: 60 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.233463035019

             precision    recall  f1-score   support

      Empty     0.9949    0.7665    0.8659       257
   Occupied     0.9608    0.9993    0.9797      1471

avg / total     0.9659    0.9647    0.9628      1728

