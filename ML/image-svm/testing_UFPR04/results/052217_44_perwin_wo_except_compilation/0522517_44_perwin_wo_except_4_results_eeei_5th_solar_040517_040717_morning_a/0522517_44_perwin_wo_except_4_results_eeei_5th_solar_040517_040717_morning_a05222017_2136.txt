0522517_44_perwin_wo_except_4_results_eeei_5th_solar_040517_040717_morning_a05222017_2136
Features in dataset (100%):
Count: 496

Average time in pred: 6.22070891771
Max time in pred: 31.0310659409
Min time in pred: 0.00269985198975

List of model detections:
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 4
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 73
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4_wo_div.model: 18

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.808467741935
Occupied: None

Confusion Report:
[[401  95]
 [  0   0]]
True Negative: 401
False Negative: 0
False Positive: 95 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.191532258065

             precision    recall  f1-score   support

      Empty     1.0000    0.8085    0.8941       496
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.8085    0.8941       496

