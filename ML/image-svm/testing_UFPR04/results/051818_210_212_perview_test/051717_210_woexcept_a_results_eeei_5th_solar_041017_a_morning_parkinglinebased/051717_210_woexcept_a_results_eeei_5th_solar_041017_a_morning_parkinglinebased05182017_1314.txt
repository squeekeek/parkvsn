051717_210_woexcept_a_results_eeei_5th_solar_041017_a_morning_parkinglinebased05182017_1314
Features in dataset (100%):
Count: 1728

Average time in pred: 1.41566451211
Max time in pred: 15.0185399055
Min time in pred: 0.246682882309

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 692
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 12
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 874
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 15
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 33
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 1
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-10_wo_div.model: 25

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.291828793774
Occupied: 0.999320190347

Confusion Report:
[[  75  182]
 [   1 1470]]
True Negative: 75
False Negative: 1
False Positive: 182 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.708171206226

             precision    recall  f1-score   support

      Empty     0.9868    0.2918    0.4505       257
   Occupied     0.8898    0.9993    0.9414      1471

avg / total     0.9043    0.8941    0.8684      1728

