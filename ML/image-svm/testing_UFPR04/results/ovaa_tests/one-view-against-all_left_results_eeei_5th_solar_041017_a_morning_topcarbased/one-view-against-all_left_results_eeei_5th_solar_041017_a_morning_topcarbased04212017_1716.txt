Features in dataset (100%):
Count: 1728

Average time in pred: 0.148360634568
Max time in pred: 0.246634960175
Min time in pred: 0.0662457942963

             precision    recall  f1-score   support

      Empty       0.65      0.74      0.69       258
   Occupied       0.95      0.93      0.94      1470

avg / total       0.91      0.90      0.90      1728

