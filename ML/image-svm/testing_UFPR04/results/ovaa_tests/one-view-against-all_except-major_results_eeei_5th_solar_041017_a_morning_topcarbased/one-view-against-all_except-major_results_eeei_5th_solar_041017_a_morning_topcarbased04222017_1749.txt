Features in dataset (100%):
Count: 1728

Average time in pred: 0.488775969793
Max time in pred: 2.90299701691
Min time in pred: 0.0599460601807

             precision    recall  f1-score   support

      Empty       0.93      0.60      0.73       258
   Occupied       0.93      0.99      0.96      1470

avg / total       0.93      0.93      0.93      1728

