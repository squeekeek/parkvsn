lbp_pr-18_rc-46_all_datasets_8_results_eeei_CNL_031717_morning05222017_0334
Features in dataset (100%):
Count: 292

Average time in pred: 1.46109453129
Max time in pred: 2.96866583824
Min time in pred: 0.0530171394348

List of model detections:
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-46_wo_div.model: 92
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-46_wo_div.model: 1
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-46_wo_div.model: 199

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0
Occupied: 1.0

Confusion Report:
[[  0 278]
 [  0  14]]
True Negative: 0
False Negative: 0
False Positive: 278 
True Positive: 14 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 1.0

             precision    recall  f1-score   support

      Empty     0.0000    0.0000    0.0000       278
   Occupied     0.0479    1.0000    0.0915        14

avg / total     0.0023    0.0479    0.0044       292

