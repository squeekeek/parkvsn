052217_36_perwin_w_except_6_results_eeei_5th_solar_040517_040717_morning_c05222017_2230
Features in dataset (100%):
Count: 1328

Average time in pred: 2.80765426141
Max time in pred: 15.7211551666
Min time in pred: 0.00216102600098

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 248
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 1
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 97

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.739457831325
Occupied: None

Confusion Report:
[[982 346]
 [  0   0]]
True Negative: 982
False Negative: 0
False Positive: 346 
True Positive: 0 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: nan
False Positive Rate: 0.260542168675

             precision    recall  f1-score   support

      Empty     1.0000    0.7395    0.8502      1328
   Occupied     0.0000    0.0000    0.0000         0

avg / total     1.0000    0.7395    0.8502      1328

