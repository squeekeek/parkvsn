052217_36_perwin_w_except_7_results_eeei_5th_solar_041017_a_morning_parkinglinebased05222017_2249
Features in dataset (100%):
Count: 1728

Average time in pred: 2.51132745144
Max time in pred: 11.0203089714
Min time in pred: 0.613662958145

List of model detections:
w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 876
w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 19
w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 637

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.583657587549
Occupied: 0.968728755948

Confusion Report:
[[ 150  107]
 [  46 1425]]
True Negative: 150
False Negative: 46
False Positive: 107 
True Positive: 1425 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0312712440517
False Positive Rate: 0.416342412451

             precision    recall  f1-score   support

      Empty     0.7653    0.5837    0.6623       257
   Occupied     0.9302    0.9687    0.9491      1471

avg / total     0.9056    0.9115    0.9064      1728

