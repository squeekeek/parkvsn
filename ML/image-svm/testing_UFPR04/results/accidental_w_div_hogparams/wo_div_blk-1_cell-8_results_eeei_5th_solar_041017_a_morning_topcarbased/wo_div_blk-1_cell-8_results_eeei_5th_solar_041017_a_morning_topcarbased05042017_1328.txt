wo_div_blk-1_cell-8_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1328
Features in dataset (100%):
Count: 1728

Average time in pred: 0.519639641736
Max time in pred: 4.83903598785
Min time in pred: 0.119295120239

List of model detections:
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 1383
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 26
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 221
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 20
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 8
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 31
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 0
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-8.model: 4

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.131782945736
Occupied: 0.999319727891

Confusion Report:
[[  34  224]
 [   1 1469]]
True Negative: 34
False Negative: 1
False Positive: 224 
True Positive: 1469 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000680272108844
False Positive Rate: 0.868217054264

             precision    recall  f1-score   support

      Empty     0.9714    0.1318    0.2321       258
   Occupied     0.8677    0.9993    0.9289      1470

avg / total     0.8832    0.8698    0.8248      1728

