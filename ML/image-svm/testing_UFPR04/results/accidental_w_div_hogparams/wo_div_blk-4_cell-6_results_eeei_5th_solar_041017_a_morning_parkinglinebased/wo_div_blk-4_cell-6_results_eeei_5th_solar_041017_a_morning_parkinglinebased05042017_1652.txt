wo_div_blk-4_cell-6_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_1652
Features in dataset (100%):
Count: 1728

Average time in pred: 0.33367649452
Max time in pred: 3.57672715187
Min time in pred: 0.0599920749664

List of model detections:
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 777
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 400
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 431
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 3
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 26
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-4_cell-6.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.354085603113
Occupied: 1.0

Confusion Report:
[[  91  166]
 [   0 1471]]
True Negative: 91
False Negative: 0
False Positive: 166 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.645914396887

             precision    recall  f1-score   support

      Empty     1.0000    0.3541    0.5230       257
   Occupied     0.8986    1.0000    0.9466      1471

avg / total     0.9137    0.9039    0.8836      1728

