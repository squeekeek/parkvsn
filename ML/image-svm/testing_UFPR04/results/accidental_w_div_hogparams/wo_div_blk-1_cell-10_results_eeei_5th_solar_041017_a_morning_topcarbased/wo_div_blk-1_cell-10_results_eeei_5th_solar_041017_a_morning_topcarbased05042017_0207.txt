wo_div_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_0207
Features in dataset (100%):
Count: 1728

Average time in pred: 0.358958670525
Max time in pred: 4.13361597061
Min time in pred: 0.0358009338379

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 892
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 586
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 5
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 193
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 8
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 6

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.143410852713
Occupied: 1.0

Confusion Report:
[[  37  221]
 [   0 1470]]
True Negative: 37
False Negative: 0
False Positive: 221 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.856589147287

             precision    recall  f1-score   support

      Empty     1.0000    0.1434    0.2508       258
   Occupied     0.8693    1.0000    0.9301      1470

avg / total     0.8888    0.8721    0.8287      1728

