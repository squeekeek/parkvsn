wo_div_blk-2_cell-10_results_eeei_5th_solar_041017_a_morning_topcarbased05042017_1402
Features in dataset (100%):
Count: 1728

Average time in pred: 0.641223469128
Max time in pred: 3.68227887154
Min time in pred: 0.0907859802246

List of model detections:
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 1397
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 90
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 16
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 11
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 168
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 17
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 0
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-2_cell-10.model: 1

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.108527131783
Occupied: 1.0

Confusion Report:
[[  28  230]
 [   0 1470]]
True Negative: 28
False Negative: 0
False Positive: 230 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.891472868217

             precision    recall  f1-score   support

      Empty     1.0000    0.1085    0.1958       258
   Occupied     0.8647    1.0000    0.9274      1470

avg / total     0.8849    0.8669    0.8182      1728

