wo_div_blk-1_cell-10_results_eeei_5th_solar_041017_a_morning_parkinglinebased05042017_0217
Features in dataset (100%):
Count: 1728

Average time in pred: 0.333197902612
Max time in pred: 3.44075584412
Min time in pred: 0.024482011795

List of model detections:
w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 856
w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 402
w40_h50_pos_center-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h80_pos_left-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 0
w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 339
w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 4
w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 1
w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-10.model: 2

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.47859922179
Occupied: 1.0

Confusion Report:
[[ 123  134]
 [   0 1471]]
True Negative: 123
False Negative: 0
False Positive: 134 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.52140077821

             precision    recall  f1-score   support

      Empty     1.0000    0.4786    0.6474       257
   Occupied     0.9165    1.0000    0.9564      1471

avg / total     0.9289    0.9225    0.9105      1728

