parakeet_bs_results_eeei_5th_solar_041017_a_morning_parkinglinebased05222017_0344
Features in dataset (100%):
Count: 1728

Images in dataset(100%):Count: 192

Average time in pred: 0.0
Max time in pred: 0
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.280739583075
Max time in pred: 0.362999916077
Min time in pred: 0.266000032425

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.00389105058366
Occupied: 1.0

Confusion Report:
[[   1  256]
 [   0 1471]]
True Negative: 1
False Negative: 0
False Positive: 256 
True Positive: 1471 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.996108949416

             precision    recall  f1-score   support

      Empty     1.0000    0.0039    0.0078       257
   Occupied     0.8518    1.0000    0.9199      1471

avg / total     0.8738    0.8519    0.7843      1728

