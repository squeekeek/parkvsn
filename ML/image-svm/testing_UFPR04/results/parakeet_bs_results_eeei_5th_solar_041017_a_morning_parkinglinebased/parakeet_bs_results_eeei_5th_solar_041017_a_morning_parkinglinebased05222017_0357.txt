parakeet_bs_results_eeei_5th_solar_041017_a_morning_parkinglinebased05222017_0357
Features in dataset (100%):
Count: 1728

Images in dataset(100%):Count: 192

Average time in pred: 5.78799733409e-07
Max time in pred: 0.00100016593933
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.280843749642
Max time in pred: 0.447000026703
Min time in pred: 0.263999938965

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.00389105058366
Occupied: 0.999320190347

Confusion Report:
[[   1  256]
 [   1 1470]]
True Negative: 1
False Negative: 1
False Positive: 256 
True Positive: 1470 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000679809653297
False Positive Rate: 0.996108949416

             precision    recall  f1-score   support

      Empty     0.5000    0.0039    0.0077       257
   Occupied     0.8517    0.9993    0.9196      1471

avg / total     0.7994    0.8513    0.7840      1728

