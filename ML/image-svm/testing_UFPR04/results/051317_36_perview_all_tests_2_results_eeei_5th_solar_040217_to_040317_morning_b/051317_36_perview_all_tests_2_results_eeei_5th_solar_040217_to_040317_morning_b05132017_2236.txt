051317_36_perview_all_tests_2_results_eeei_5th_solar_040217_to_040317_morning_b05132017_2236
Features in dataset (100%):
Count: 1035

Average time in pred: 5.37235715009
Max time in pred: 33.3994650841
Min time in pred: 0.139168977737

List of model detections:
w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 134
w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 9
w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 76
w40_h50_pos_center-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 157
w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 71
w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 2
w40_h80_pos_right-2_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0
w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-3_cell-6_wo_div.model: 0

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.654748603352
Occupied: 1.0

Confusion Report:
[[586 309]
 [  0 140]]
True Negative: 586
False Negative: 0
False Positive: 309 
True Positive: 140 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 0.345251396648

             precision    recall  f1-score   support

      Empty     1.0000    0.6547    0.7914       895
   Occupied     0.3118    1.0000    0.4754       140

avg / total     0.9069    0.7014    0.7486      1035

