parakeet_bs_results_eeei_4th_bridge_050417_morning_parkinglinebased05222017_0512
Features in dataset (100%):
Count: 710

Images in dataset(100%):Count: 71

Average time in pred: 1.40834862078e-06
Max time in pred: 0.000999927520752
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.0861408441839
Max time in pred: 0.162999868393
Min time in pred: 0.0799999237061

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0
Occupied: 1.0

Confusion Report:
[[  0 115]
 [  0 595]]
True Negative: 0
False Negative: 0
False Positive: 115 
True Positive: 595 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0
False Positive Rate: 1.0

             precision    recall  f1-score   support

      Empty     0.0000    0.0000    0.0000       115
   Occupied     0.8380    1.0000    0.9119       595

avg / total     0.7023    0.8380    0.7642       710

