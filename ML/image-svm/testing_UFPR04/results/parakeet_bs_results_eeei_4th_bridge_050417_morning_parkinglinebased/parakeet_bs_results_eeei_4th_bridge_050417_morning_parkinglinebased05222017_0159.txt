parakeet_bs_results_eeei_4th_bridge_050417_morning_parkinglinebased05222017_0159
Features in dataset (100%):
Count: 710

Images in dataset(100%):Count: 71

Average time in pred: 1.40868442159e-06
Max time in pred: 0.00100016593933
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.0803098611429
Max time in pred: 0.191999912262
Min time in pred: 0.0699999332428

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.0
Occupied: 0.998319327731

Confusion Report:
[[  0 115]
 [  1 594]]
True Negative: 0
False Negative: 1
False Positive: 115 
True Positive: 594 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00168067226891
False Positive Rate: 1.0

             precision    recall  f1-score   support

      Empty     0.0000    0.0000    0.0000       115
   Occupied     0.8378    0.9983    0.9110       595

avg / total     0.7021    0.8366    0.7635       710

