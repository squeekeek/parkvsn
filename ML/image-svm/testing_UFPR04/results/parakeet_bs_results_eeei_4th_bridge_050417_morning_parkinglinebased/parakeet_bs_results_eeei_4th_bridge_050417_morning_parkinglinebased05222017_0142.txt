parakeet_bs_results_eeei_4th_bridge_050417_morning_parkinglinebased05222017_0142
Features in dataset (100%):
Count: 710

Images in dataset(100%):Count: 71

Average time in pred: 0.0
Max time in pred: 0
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.0823098605787
Max time in pred: 0.171000003815
Min time in pred: 0.0699999332428

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.147826086957
Occupied: 0.806722689076

Confusion Report:
[[ 17  98]
 [115 480]]
True Negative: 17
False Negative: 115
False Positive: 98 
True Positive: 480 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.193277310924
False Positive Rate: 0.852173913043

             precision    recall  f1-score   support

      Empty     0.1288    0.1478    0.1377       115
   Occupied     0.8304    0.8067    0.8184       595

avg / total     0.7168    0.7000    0.7081       710

