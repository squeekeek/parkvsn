parakeet_bs_results_eeei_4th_bridge_050417_morning_parkinglinebased05222017_0234
Features in dataset (100%):
Count: 710

Images in dataset(100%):Count: 71

Average time in pred: 0.0
Max time in pred: 0
Min time in pred: 0.0

For per entire image:
Average time in pred: 0.075028171002
Max time in pred: 0.141000032425
Min time in pred: 0.069000005722

Accuracies for each class (predicted_of_class/actual_of_class):
Empty: 0.00869565217391
Occupied: 0.900840336134

Confusion Report:
[[  1 114]
 [ 59 536]]
True Negative: 1
False Negative: 59
False Positive: 114 
True Positive: 536 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0991596638655
False Positive Rate: 0.991304347826

             precision    recall  f1-score   support

      Empty     0.0167    0.0087    0.0114       115
   Occupied     0.8246    0.9008    0.8610       595

avg / total     0.6938    0.7563    0.7234       710

