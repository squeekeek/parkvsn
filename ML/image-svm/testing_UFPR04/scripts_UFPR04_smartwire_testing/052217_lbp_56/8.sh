python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_lbp_no_lbpimage/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_CNL_031717_morning \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_CNL_031717_morning.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_CNL_031717_morning.json \
-i y \
-n lbp_pr-18_rc-56_all_datasets_8 \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/lbp_pr-18_rc-56 \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/lbp_pr-18_rc-56/models.json \
-lj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list_5.json \
-lo pr-18_rc-56 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json

