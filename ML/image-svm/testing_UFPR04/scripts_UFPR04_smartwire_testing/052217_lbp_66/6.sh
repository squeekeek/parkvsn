python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_lbp_no_lbpimage/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_040517_040717_morning_c \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_c.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040517_040717_morning_c.json \
-i y \
-n lbp_pr-18_rc-66_all_datasets_6   \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/lbp_pr-18_rc-66 \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/lbp_pr-18_rc-66/models.json \
-lj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list_6.json \
-lo pr-18_rc-66 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json

