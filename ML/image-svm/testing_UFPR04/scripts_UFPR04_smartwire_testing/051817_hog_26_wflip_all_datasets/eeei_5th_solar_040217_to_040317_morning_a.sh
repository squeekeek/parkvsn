# eeei_5th_solar_040217_to_040317_morning_a: parkinglinebased
python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_hog_first_triggered/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_040217_to_040317_morning_a \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040217_to_040317_morning_a.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040217_to_040317_morning_a.json \
-i y \
-n 0518517_26_perwin_wo_except_flip_tests_2  \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/results/perwinsize_26_flip_wo_except/blk-2_cell-6/wo_div/ \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/results/perwinsize_26_flip_wo_except/blk-2_cell-6/wo_div/models.json \
-hj /home/agatha/Documents/parkvsn/ML/image-svm/config/hog_list_26.json \
-ho blk-2_cell-6 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json