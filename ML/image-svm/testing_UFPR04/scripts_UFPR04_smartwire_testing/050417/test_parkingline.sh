
# eeei_5th_solar_041017_a_morning: parkinglinebased
python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_highest_conf/pipeline_eval_all_wdet.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning_partial \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json \
-m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json \
-i y \
-n test_highest_conf \
-mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-4_cell-4/w_div \
-mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-4_cell-4/w_div/models.json \
-hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list.json \
-ho blk-4_cell-4 

