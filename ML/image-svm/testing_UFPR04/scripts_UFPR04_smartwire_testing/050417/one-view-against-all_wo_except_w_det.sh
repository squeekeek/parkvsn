

# eeei_5th_solar_041017_a_morning: parkinglinebased
python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_first_triggered/pipeline_eval_all_wdet.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json \
-m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json \
-i y \
-n one-view-against-all_wo_except_wo_div_w_det \
-mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/one-view-against-all_except-major_wo_div \
-mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/one-view-against-all_except-major_wo_div/models.json \
-hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog.json \
-ho blk-4_cell-4 
