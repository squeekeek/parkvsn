

# eeei_5th_solar_040517_040717_morning_a: parkinglinebased
python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_hog_first_triggered/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_040517_040717_morning_b \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_b.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040517_040717_morning_b.json \
-i y \
-n 052217_36_perwin_wo_except_flip_tests_5  \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_36_flip_wo_except/ \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_36_flip_wo_except/models.json \
-hj /home/agatha/Documents/parkvsn/ML/image-svm/config/hog_list_36.json \
-ho blk-3_cell-6 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json
