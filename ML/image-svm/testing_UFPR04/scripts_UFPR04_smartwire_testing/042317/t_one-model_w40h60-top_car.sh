python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts/pipeline_eval_all.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_041017_a_morning \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_topcarbased.json  \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_topcarbased.json \
-i y \
-n one-model_w40h60-top_car \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models/one-model_w40h60 \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models/one-model_w40h60/models.json