# eeei_5th_solar_041017_a_morning: parkinglinebased
python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_hog_first_triggered/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_041017_a_morning \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json  \
-i y \
-n 051317_36_perview_all_tests_7 \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-3_cell-6/wo_div/ \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-3_cell-6/wo_div/models.json \
-hj /home/agatha/Documents/parkvsn/ML/image-svm/config/hog_list_3_9.json \
-ho blk-3_cell-6 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json