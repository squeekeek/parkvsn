# eeei_5th_solar_040217_to_040317_morning_a: parkinglinebased
python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_hog_first_triggered/pipeline_eval_all_wdet.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_040217_to_040317_morning_a \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040217_to_040317_morning_a.json \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040217_to_040317_morning_a.json \
-i y \
-n 051217_36_all_tests_1 \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_36_050817/per_winsize_36_9_01/ \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_36_050817/per_winsize_36_9_01//models.json \
-hj /home/agatha/Documents/parkvsn/ML/image-svm/config/hog_list_3_9.json \
-ho blk-3_cell-6 \
-dj /home/agatha/Documents/parkvsn/ML/image-svm/config/detection.json