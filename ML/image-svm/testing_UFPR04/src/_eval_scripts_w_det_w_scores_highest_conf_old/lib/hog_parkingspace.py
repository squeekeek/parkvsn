"""
    Expects an image of .mat format, and a loadedmodel

"""


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
from datetime import datetime


DEBUG = 0



def hog_parkingspace(image, model, win_width, win_height, visualize_det, ifVisualize, hog_option):
    """
    Evaluates a parking space
    Passing image path and model path
    """

    # Load model 
    clf = model

    # Load image
    orig_image = image
    orig_height, orig_width = orig_image.shape[:2]
    orig_ratio = orig_width/float(orig_height)

    # Resize to height of 100 px maintaining aspect ratio
    res_height = 100
    res_width = int(res_height*orig_ratio)
    resized_image = cv2.resize(orig_image, (res_width, res_height))

    # Grayscale image
    gray_resized = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    parent_c = os.path.abspath(parent_c_temp)
    config_path = os.path.join(parent_c, "config")

    # Load HoG setting
    hog_config_path = os.path.join(config_path, "hog.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)
    # Load detection settings
    detection_config_path = os.path.join(config_path, "detection.json")
    with open(detection_config_path) as data_file:
        detection_dict = json.load(data_file)


    # Settings for HoG
    window_size = (win_height, win_width)
    orientations = hog_dict["hog"][0]["orientations"]
    pixels_per_cell = hog_dict["hog"][0]["pixels_per_cell"]
    cells_per_block = hog_dict["hog"][0]["cells_per_block"]
    transform_sqrt = hog_dict["hog"][0]["transform_sqrt"]
    block_norm = hog_dict["hog"][0]["block_norm"]


    # Settings for image pyramid
    downscale = detection_dict["detection"]["pyramid"]["downscale"]
    scale = 0
    step_size = detection_dict["detection"]["sliding_window"]["step_size"]
    detections = []
    # Settings for nms
    threshold = detection_dict["detection"]["nms"]["threshold"]

    # Downscale the image and iterate
    for im_scaled in pyramid(gray_resized, scale=downscale, minSize=window_size):
    #for im_scaled in pyramid_gaussian(gray_resized, downscale=downscale):
        # This list contains detections at the current scale
        cd = []
        # If the width or height of the scaled image is less than the width
        # or height of the widnow, then end the iterations
        if im_scaled.shape[0] < window_size[0] or im_scaled.shape[1] < window_size[1]:
            """
            print "im_scaled.shape[0]: {}, im_scaled.shape[1]: {}".format(im_scaled.shape[0], im_scaled.shape[1])
            print "breaked"
            """
            break
        if window_size[1] <= int(im_scaled.shape[1]*0.6):
            continue

        #  print "im_scaled.shape[:]: {}".format(im_scaled.shape[:])
        for (x, y, im_window) in sliding_window(im_scaled, window_size, step_size):
            if im_window.shape[0] != window_size[0] or im_window.shape[1] != window_size[1]:
                continue

            
            """
            print "im_window.shape[:]: {}".format(im_window.shape[:])
            print "im_window.shape[0]: {}".format(im_window.shape[0])
            print "im_window.shape[1]: {}".format(im_window.shape[1])
            """
            # sys.exit()
            # Calculate the HoG features
            fd = feature.hog(
                im_window,
                orientations=orientations,
                pixels_per_cell=pixels_per_cell,
                cells_per_block=cells_per_block,
                transform_sqrt=transform_sqrt,
                visualise=False,
                block_norm=block_norm
            )

            pred = clf.predict(fd.reshape(1, -1))
            if pred[0] > 0:
                test_df = clf.decision_function(fd.reshape(1, -1))
                """
                print "Detection:: Location -> ({}, {})".format(x, y)
                print "Scale ->  {} | Confidence Score {} \n".format(
                    scale, clf.decision_function(fd.reshape(1, -1)))
                print "test_df.shape: {}".format(test_df.shape)
                """
                detections.append((
                    x, y, test_df,
                    int(window_size[1]*(downscale**scale)),
                    int(window_size[0]*(downscale**scale))
                    ))
                cd.append(detections[-1])
            if visualize_det:
                clone = im_scaled.copy()
                for x1, y1, _, _, _ in cd:
                    # Draw the detectionas at this scale
                    cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + 
                        im_window.shape[0]), (0, 0,0), thickness=2)
                cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]),
                    (255, 255, 255), thickness=2)
                sliding_title = "({}, {}) Sliding Window in Progress".format(clone.shape[0], clone.shape[1])
                cv2.imshow(sliding_title, clone)
                cv2.waitKey(1)
        # Move the next scale
        #downscale = downscale + 1
        scale = scale + 1
    # Display the results before performing NMS
    clone = resized_image.copy()
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(
            resized_image, (x_tl, y_tl), (x_tl + w, y_tl + h), (0, 0, 0), thickness=2
        )
    if ifVisualize:
        cv2.imshow("Raw Detections before NMS", resized_image)
        cv2.waitKey()

    # Perform Non Maxima Suppression
    # detections = non_max_suppression(detections, threshold)
    detections = nms(detections, threshold)


    # Display the results after performing NMS
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(clone, (x_tl, y_tl), (x_tl+w,y_tl+h), (0, 0, 0), thickness=2)
    if ifVisualize:
        cv2.imshow("Final Detections after applying NMS", clone)
        cv2.waitKey()

    return (detections, clone)

if __name__ == "__main__":

    image = cv2.imread("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/img_marked_eeei_5th_solar_041017_a_morning_parkinglinebased/Segmented/Occupied/20170330_1030#8.jpg")
    win_width = 50
    win_height = 80
    visualize_det = 0
    ifVisualize = 0
    model = joblib.load("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except_wo_div/w50_h80_pos_right-1neg_unocc_ALL_wo_div.model")
    hog_option = 0

    start_time = datetime.now()
    prediction = hog_parkingspace(
            image, model, win_width,
            win_height, visualize_det, ifVisualize, hog_option)
    end_time = datetime.now()
    print "Time elapsed: {}".format(end_time-start_time)
    print "Car detections: {}".format(prediction)
