"""
python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts/lib/test_pyramid.py \
-i /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/img_marked_eeei_5th_solar_041017_a_morning_parkinglinebased/Segmented/Occupied/20170330_1030#8.jpg \
-s 1.01
"""

# import the necessary packages
from helpers import pyramid
from skimage.transform import pyramid_gaussian
import argparse
import cv2
 
# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
ap.add_argument("-s", "--scale", type=float, default=1.5, help="scale factor size")
args = vars(ap.parse_args())

# load the image
image = cv2.imread(args["image"])

# METHOD #1: No smooth, just scaling.
# loop over the image pyramid
for (i, resized) in enumerate(pyramid(image, scale=args["scale"], minSize=(80, 30))):
	# show the resized image
	cv2.imshow("Layer {}".format(i + 1), resized)
	cv2.waitKey(0)
 