"""
    Evaluates json produced by eval-classifier.py
    Produces classification scores and other evaluation in text

    Saves results in text file.

    Example usage:
    python eval_results.py \
    -j /Users/agatha/Desktop/test-on-dataset-scripts/results/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased.json \
    -n test041717_halfwaymodels
"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib
from lib.p1_hog_svm import hog_svm

from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from datetime import datetime


def class_accuracy(Y_pred, Y_actual, class_data):
    """
        Evaluates the accuracy of a class.
        Percentage predicted correctly.
        Accuracy = (Predicted of Class)/(Actual of Class)
    """
    num_actual = 0
    num_correct = 0
    for i, val_actual in enumerate(Y_actual):
        if val_actual == class_data:
            num_actual = num_actual + 1
            if val_actual == Y_pred[i]:
                num_correct = num_correct + 1
    if num_actual > 0:
        accuracy = num_correct/float(num_actual)
        return accuracy
    return None



def eval_results(json_path, model_json, exp_name, target_path=None):
    """
    Evaluates json produced by eval-classifier.py
    Produces classification scores and other evaluation in text

    Saves results in text file.

    Example usage:
    python eval-results.py -j results_test.json
    """
    # Load json file
    with open(json_path) as data_file:
        results_dict = json.load(data_file)

    # Load model json
    with open(model_json) as data_file:
        models_dict = json.load(data_file)
    # Dictionary with actual results
    dataset_name = " "
    for i in results_dict:
        dataset_name = i

    # For generalization classification scores
    Y_actual = []
    Y_pred = []
    total_time = 0
    min_time = float("inf")
    max_time = 0

    num_detections_dict = {}


    # Get the list of models
    for i, category in enumerate(models_dict):
        for j, model in enumerate(models_dict[category]):
            num_detections_dict[model] = 0


    # Get info from json file
    for im in results_dict[dataset_name]:
        for id in results_dict[dataset_name][im]: 

            ifOccupied = results_dict[dataset_name][im][id]["ifOccupied"]
            ifRightPred = results_dict[dataset_name][im][id]["ifRightPred"]
            pred = results_dict[dataset_name][im][id]["pred"]
            timePred = results_dict[dataset_name][im][id]["timePred"]
            highest_model = results_dict[dataset_name][im][id]["highest_model"]
            if highest_model is not None:
                num_detections_dict[highest_model] = num_detections_dict[highest_model] + 1

            Y_actual.append(ifOccupied)
            Y_pred.append(pred)
            total_time = total_time + timePred

            if timePred < min_time:
                min_time = timePred
            if timePred > max_time:
                max_time = timePred

    # Do evaluation
    len_total = len(Y_actual)

    folder_name = exp_name + "_results_" + dataset_name

    timestamp = "{:%m%d%Y_%H%M}".format(datetime.now())
    filename = folder_name + timestamp + ".txt"

    if target_path is None:
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        folder_path = os.path.join(dir_main, "results", folder_name)
    else:
        folder_path = target_path

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    filepath = os.path.join(folder_path, filename)
    print filepath
    f = open(filepath, 'w')
    f.truncate()
    #sys.exit()

    print_num = (
        "Features in dataset (100%):\n"
        "Count: {}\n\n"
    ).format(len_total)

    avg_time = total_time/float(len_total)
    print_time = (
        "Average time in pred: {}\n"
        "Max time in pred: {}\n"
        "Min time in pred: {}\n\n"
    ).format(avg_time, max_time, min_time)

    print print_num
    print print_time

    


    target_names = [
        "Empty",
        "Occupied"
    ]

    accuracy_empty = class_accuracy(Y_pred, Y_actual, 0)
    accuracy_occupied = class_accuracy(Y_pred, Y_actual, 1)
    accuracy_list = (
        "Accuracies for each class (predicted_of_class/actual_of_class):\n"
        "Empty: {}\n"
        "Occupied: {}\n\n"
    ).format(accuracy_empty, accuracy_occupied)

    conf_report = confusion_matrix(Y_actual, Y_pred, labels=[0,1])
    true_negative = conf_report[0][0]
    false_negative = conf_report[1][0]
    false_positive = conf_report[0][1]
    true_positive = conf_report[1][1]

    total_actual_positive = false_negative + true_positive
    total_actual_negative = false_positive + true_negative

    false_negative_rate = false_negative/float(total_actual_positive)
    false_positive_rate = false_positive/float(total_actual_negative)


    conf_breakdown = (
        "Confusion Report:\n"
        "{}\n"
        "True Negative: {}\n"
        "False Negative: {}\n"
        "False Positive: {} \n"
        "True Positive: {} \n"
        "\n"
        "(aim to reduce FN rate - misclassify occupied as empty)\n"
        "False Negative Rate: {}\n"
        "False Positive Rate: {}\n\n"
    ).format(
        conf_report, 
        true_negative, false_negative,
        false_positive, true_positive,
        false_negative_rate, false_positive_rate
    )

    print conf_breakdown

    c_report = classification_report(Y_actual, Y_pred, target_names=target_names, digits=4)
    print c_report

    results_name = folder_name + timestamp + "\n"


    f.write(results_name)
    f.write(print_num)
    f.write(print_time)

    str_a = "List of model detections:\n"
    print str_a
    f.write(str_a)
    list_models = list(num_detections_dict)
    for i, model in enumerate(list_models):
        str_a = "{}: {}\n".format(model, num_detections_dict[model])
        print str_a
        f.write(str_a)

    str_a = "\n"
    print str_a
    f.write(str_a)


    f.write(accuracy_list)
    f.write(conf_breakdown)
    f.write(c_report)
    f.write("\n")
    f.close()

if __name__ == "__main__":

    """
    Example usage:
    python eval_results.py \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/results/puc_onemodel_wdiv_40x60_local_results_eeei_5th_solar_041017_a_morning_parkinglinebased/puc_onemodel_wdiv_40x60_local_results_eeei_5th_solar_041017_a_morning_parkinglinebased.json \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_PUC/onemodel_w40_h60_w_div/models.json \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/results/puc_onemodel_wdiv_40x60_local_results_eeei_5th_solar_041017_a_morning_parkinglinebased \
    -n puc_onemodel
    
    """

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of results",
        required=True)
    ap.add_argument(
        '-mj', "--model_json", help="Path to json of models",
        required=True)
    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    args = vars(ap.parse_args())

    # Load json path
    json_path = args["json_path"]
    model_json = args["model_json"]
    exp_name = args["exp_name"]
    folder_path = args["folder_path"]

    eval_results(json_path, model_json, exp_name, folder_path)
