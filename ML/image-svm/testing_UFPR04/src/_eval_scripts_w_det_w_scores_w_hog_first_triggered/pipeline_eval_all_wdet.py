"""
    Uses classifier, outputs results, and option to show images
    Uses eval_classifier.py, eval_results.py, and images_results.py

    Example usage:

    python eval_all.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json -i y

    python eval_all.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json  -m /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -i y

   python eval_all.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_parkinglinebased.json  -m /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json -i y

    python pipeline_eval_all.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json  -m /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -i y -n test041717_halfwaymodels

    python pipeline_eval_all.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -m /Users/agatha/Desktop/test-on-dataset-scripts/data/preprocess/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -i y -n test041717_halfwaymodels
"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib
from lib.p1_hog_svm import hog_svm

from eval_classifier_save_detections import eval_classifier
from eval_results import eval_results
from images_results import image_results
from datetime import datetime

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)
    ap.add_argument(
        '-m', "--marked_json_path", help="Path to json of marked images",
        required=True)
    ap.add_argument(
        '-i', "--ifImages", help="Yes or no to output images",
        required=True
    )
    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True
    )
    ap.add_argument(
        '-mf', "--model_folder", help="Folder containing models",
        required=True
    )
    ap.add_argument(
        '-mj', "--model_json", help="Folder containing models",
        required=True
    )
    ap.add_argument(
        "-hj", "--hog_json", help="HoG json file path", 
        required=True
    )
    ap.add_argument(
        "-ho", "--hog_option", help="HoG option id",
        required=True
    )
    ap.add_argument(
        "-dj", "--detection_json", help="Detection json file path",
        required=True
    )
    start_time = datetime.now()

    args = vars(ap.parse_args())

    dataset_path = args["dataset"]
    json_path = args["json_path"]
    marked_path = args["marked_json_path"]
    ifImages = args["ifImages"]
    exp_name = args["exp_name"]
    model_folder = args["model_folder"]
    model_json = args["model_json"]
    hog_json = args["hog_json"]
    hog_option = args["hog_option"]
    detection_json = args["detection_json"]

    with open(json_path) as data_file:
        to_mark_dict = json.load(data_file)

    # Use parking_id as dataset_name
    dataset_name = to_mark_dict["parking_id"]

    results_folder_name = exp_name + "_results_" + dataset_name 

    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    results_path = os.path.join(dir_main, "results", results_folder_name)

    if not os.path.isdir(results_path):
        os.makedirs(results_path)

    # Evaluate parking spaces
    results_json = eval_classifier(dataset_path, json_path, marked_path, exp_name, results_path, model_folder, model_json, hog_json, hog_option, detection_json)
    # Show results
    eval_results(results_json, model_json, exp_name, results_path)
    # Output images
    if (ifImages == "y") or (ifImages == "Y"):
        image_results(dataset_path, json_path, results_json, model_json, exp_name, results_path)

    end_time = datetime.now()
    total_time = end_time - start_time
    print "Time elapsed: {}".format(total_time)

