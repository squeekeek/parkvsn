"""
    Evaluates a given image with the different models from a json file

    Timing difference bet loading model list initially vs joblib
    0.161170005798 <= load models and send list of models
    0.162690877914 <= load model via joblib with json file

"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian

from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

from hog_parkingspace import hog_parkingspace
from timeit import default_timer as timer

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


def hog_svm_model_list(image, model_list, json_dict):
    """
    Just returns if a model was triggered as occupied
    Return 1 occupied
    Return 0 unoccupied
    """
    for i, category in enumerate(json_dict):
        num_occupied = 0
        num_models = 0
        for j, model in enumerate(json_dict[category]):
            #clf = joblib.load(model)
            win_rows = json_dict[category][model]["rows"]
            win_cols = json_dict[category][model]["cols"]
            prediction = hog_parkingspace(
                image, model_list[j], win_cols, win_rows, 0, 0
            )
            if prediction > 0:
                return 1
            if DEBUG:
                print "# Occupied: {}".format(prediction)
                print " "
                num_models = num_models + 1
    return 0


def hog_svm(image, json_dict, model_folder=None, hog_setting=None):
    """
    Just returns if a model was triggered as occupied
    Return 1 occupied
    Return 0 unoccupied

    {
        "model": ,
        "predictions:": ,
        "highest_confidence": 
    }

    """
    list_predictions = []
    # Entry for detections of a single model
    entry = {"model": "", "detections": "", "highest_confidence": ""}
    highest_value = 0
    highest_model = None
    clone = None
    if model_folder is None:
        model_folder = "."

    # Go through each model
    for i, category in enumerate(json_dict):
        num_occupied = 0
        num_models = 0
        for j, model in enumerate(json_dict[category]):
            model_path = os.path.join(model_folder, model["model"])
            clf = joblib.load(model_path)
            win_rows = json_dict[category][j]["rows"]
            win_cols = json_dict[category][j]["cols"]
            detections, image, temp_hog = hog_parkingspace(
                image, clf, win_cols, win_rows, 0, 0, hog_setting
            )
            clone = image

            if len(detections) > 0:
                entry["model"] = model
                entry["detections"] = [detections[0][0], detections[0][1], detections[0][2].tolist(), detections[0][3],detections[0][4]]
                entry["highest_confidence"] = detections[0][2].tolist()
                highest_value = detections[0][2].tolist()[0]
                highest_model = model["model"]
                list_predictions.append(entry)
                hog_image = detections[0][5].copy()
                break
            else:
                hog_image = temp_hog

            if DEBUG:
                print "# Occupied: {}".format(prediction)
                print " "
                num_models = num_models + 1
    return (list_predictions, highest_value, highest_model, clone, hog_image)


if __name__ == "__main__":

    # Load JSON file
    json_path = "p1_hog_svm_models.json"
    with open(json_path) as data_file:
        json_dict = json.load(data_file)
    model_list = []
    for i, category in enumerate(json_dict):
        for j, model in enumerate(json_dict[category]):
            clf = joblib.load(model)
            model_list.append(clf)

    print len(model_list)

    image = cv2.imread("2012-10-31_06_53_00#052.jpg", 1)
    start_timer = timer()
    #pred = hog_svm(image, model_list, json_dict)
    pred = hog_svm(image, json_dict)
    end_timer = timer()
    print(end_timer - start_timer) 
    print "Prediction: {}".format(pred)
