"""
    Expects an image of .mat format, and a loadedmodel

"""


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

from datetime import datetime

from localbinarypatterns import LocalBinaryPatterns

# import matplotlib.pyplot as plt

from skimage.transform import rotate
from skimage.feature import local_binary_pattern
from skimage import data
from skimage.color import label2rgb

DEBUG = 0


def hist(ax, lbp):
    n_bins = int(lbp.max() + 1)
    return ax.hist(lbp.ravel(), normed=True, bins=n_bins, range=(0, n_bins),
                   facecolor='0.5')

def overlay_labels(image, lbp, labels):
    mask = np.logical_or.reduce([lbp == each for each in labels])
    return label2rgb(mask, image=image, bg_label=0, alpha=0.5)


def highlight_bars(bars, indexes):
    for i in indexes:
        bars[i].set_facecolor('r')

def lbp_parkingspace(image, model, win_width, win_height, visualize_det, ifVisualize, lbp_setting, detection_dict):
    """
    Evaluates a parking space
    Passing image path and model path
    """

    # Load model 
    clf = model

    # Load image
    orig_image = image
    orig_height, orig_width = orig_image.shape[:2]
    orig_ratio = orig_width/float(orig_height)

    # Resize to height of 100 px maintaining aspect ratio
    res_height = 100
    res_width = int(res_height*orig_ratio)
    resized_image = cv2.resize(orig_image, (res_width, res_height))

    # Grayscale image
    gray_resized = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    parent_c = os.path.abspath(parent_c_temp)
    config_path = os.path.join(parent_c, "config")



    # Settings for lbp
    window_size = (win_height, win_width)

    lbp_P = lbp_setting["P"]
    lbp_R = lbp_setting["R"]
    lbp_rows = lbp_setting["rows"]
    lbp_cols = lbp_setting["cols"]

    lbp = LocalBinaryPatterns(numPoints=lbp_P, radius=lbp_R)

    lbpImage = None

    # Settings for image pyramid
    downscale = detection_dict["detection"]["pyramid"]["downscale"]
    width_threshold = detection_dict["detection"]["pyramid"]["width_threshold"]

    scale = 0
    step_size = detection_dict["detection"]["sliding_window"]["step_size"]
    detections = []
    # Settings for nms
    threshold = detection_dict["detection"]["nms"]["threshold"]

    clone = resized_image.copy()
    # Downscale the image and iterate
    for im_scaled in pyramid(gray_resized, scale=downscale, minSize=window_size):
    #for im_scaled in pyramid_gaussian(gray_resized, downscale=downscale):
        # This list contains detections at the current scale
        cd = []
        # If the width or height of the scaled image is less than the width
        # or height of the widnow, then end the iterations
        if im_scaled.shape[0] < window_size[0] or im_scaled.shape[1] < window_size[1]:
            """
            print "im_scaled.shape[0]: {}, im_scaled.shape[1]: {}".format(im_scaled.shape[0], im_scaled.shape[1])
            print "breaked"
            """
            break
        # Skip scale when the specified window size occupies less than 70% of scaled image
        if window_size[1] <= int(im_scaled.shape[1]*width_threshold):
            continue

        #  print "im_scaled.shape[:]: {}".format(im_scaled.shape[:])
        for (x, y, im_window) in sliding_window(im_scaled, window_size, step_size):
            if im_window.shape[0] != window_size[0] or im_window.shape[1] != window_size[1]:
                continue

            """
            print "im_window.shape[:]: {}".format(im_window.shape[:])
            print "im_window.shape[0]: {}".format(im_window.shape[0])
            print "im_window.shape[1]: {}".format(im_window.shape[1])
            """
            # sys.exit()
            # Calculate the lbp features
            image_rows = im_window.shape[0]
            image_cols = im_window.shape[1]

            pixel_rows = image_rows/lbp_rows
            pixel_cols = image_cols/lbp_cols

            curr_row = 0

            for m in range(0, lbp_rows, 1):
                curr_col = 0
                for n in range(0, lbp_cols, 1):
                    curr_block = im_window[curr_row:curr_row+pixel_rows, curr_col:curr_col+pixel_cols]
                    fd_partial = lbp.describe(curr_block)
                    if (m == 0) and (n == 0):
                        fd = fd_partial
                    else:
                        fd = np.concatenate((fd, fd_partial))

                    curr_col = curr_col + pixel_cols

                curr_row = curr_row + pixel_rows


            pred = clf.predict(fd.reshape(1, -1))
            if pred[0] > 0:
                test_df = clf.decision_function(fd.reshape(1, -1))

                detections.append((
                    x, y, test_df,
                    int(window_size[1]*(downscale**scale)),
                    int(window_size[0]*(downscale**scale))
                    ))
                cd.append(detections[-1])
            if visualize_det:
                clone = im_scaled.copy()
                for x1, y1, _, _, _ in cd:
                    # Draw the detectionas at this scale
                    cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + 
                        im_window.shape[0]), (0, 0,0), thickness=2)
                cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]),
                    (255, 255, 255), thickness=2)
                sliding_title = "({}, {}) Sliding Window in Progress".format(clone.shape[0], clone.shape[1])
                cv2.imshow(sliding_title, clone)
                cv2.waitKey(1)
        # Move the next scale
        #downscale = downscale + 1
        scale = scale + 1
    # Display the results before performing NMS
    clone = resized_image.copy()
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(
            resized_image, (x_tl, y_tl), (x_tl + w, y_tl + h), (0, 0, 0), thickness=2
        )
    if ifVisualize:
        cv2.imshow("Raw Detections before NMS", resized_image)
        cv2.waitKey()

    # Perform Non Maxima Suppression
    # detections = non_max_suppression(detections, threshold)
    detections = nms(detections, threshold)

    # Display the results after performing NMS
    for (x_tl, y_tl, _, w, h) in detections:
        print "in loop cropping nms"
        # Draw the detections
        cv2.rectangle(clone, (x_tl, y_tl), (x_tl+w,y_tl+h), (0, 0, 0), thickness=2)
       
    if ifVisualize:
        cv2.imshow("Final Detections after applying NMS", clone)
        cv2.waitKey()

    return (detections, clone)

if __name__ == "__main__":

    image = cv2.imread("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/img_marked_eeei_5th_solar_041017_a_morning_parkinglinebased/Segmented/Occupied/20170330_1030#8.jpg")
    win_width = 50
    win_height = 80
    visualize_det = 0
    ifVisualize = 0
    model = joblib.load("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/lbp_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except_wo_div/w50_h80_pos_right-1neg_unocc_ALL_wo_div.model")
    lbp_option = 0

    start_time = datetime.now()
    prediction = lbp_parkingspace(
            image, model, win_width,
            win_height, visualize_det, ifVisualize, lbp_option)
    end_time = datetime.now()
    print "Time elapsed: {}".format(end_time-start_time)
    print "Car detections: {}".format(prediction)
