from __future__ import division
import os
import cv2
import numpy as np
import xmltodict as xmld
#import matplotlib
import math
#from matplotlib import pyplot as plt


def adaptive_threshold(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)
    return img_thresh
