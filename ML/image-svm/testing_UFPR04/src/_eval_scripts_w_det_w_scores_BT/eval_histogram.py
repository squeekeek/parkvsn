"""
    Template for testing and evaluating a classifier/predictor.
    Outputs misclassified and rightly classified images in json file.
    Returns path of json file.

    Example usage:
    python eval-classifier.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json

    python eval_classifier.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -m /Users/agatha/Desktop/test-on-dataset-scripts/data/preprocess/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -n test041717_halfwaymodels
"""


import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib
from lib.adaptive_threshold import adaptive_threshold
from lib.p1_hog_svm import hog_svm
import time


def eval_histogram(dataset_path, json_path, marked_path, exp_name, target_path):
    """
    Template for testing and evaluating a classifier/predictor.
    Outputs evaluation scores as a text file. 
    Outputs misclassified and rightly classified images in json file.
    Returns path of json file

    Example usage:
    python eval-classifier.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json -n 041417_halfwaymodels
    """

    # START CLASSIFIERS TO BE LOADED 
    # Load json file of models
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)

    #model_folder = os.path.join(dir_main, "models", "one-view-against-all_left")
    #model_filename = "hog_svm_models.json"
    #model_file = os.path.join(model_folder, model_filename)



    # Load json file containing parking lot spaces
    with open(json_path) as data_file:
        spaces_dict = json.load(data_file)

    # Load json file containing marked spaces
    with open(marked_path) as data_file:
        marked_dict = json.load(data_file)

    # Dictionary with actual results
    #marked_dict[dataset_name][image_name][id]["list_predictions"] = list_predictions

    dataset_name = spaces_dict["parking_id"]

    results_dict = {dataset_name: {}}

    actual_dataset = os.path.join(dataset_path, "*")

    list_black = []
    list_white = []
    max_black = 0
    max_white = 0
    min_black = float('Inf')
    min_white = float('Inf')
    ave_black = 0
    ave_white = 0
    num_spaces = 0
    # Go thru every parking lot image in the dataset
    for im_path in glob.glob(actual_dataset):
        image_name = im_path.split("/")[-1]
        ifImage = image_name.split(".")
        if ifImage[-1] != "jpg":
            continue
        results_dict[dataset_name][image_name] = {}
        # Crop images given json path of coordintes

        # Insret thresholding
        test_img = cv2.imread(im_path, 1)
        thresh_img = adaptive_threshold(test_img)
        # assumption is that json file is ordered by descending
        pspaces, ppts = improc_lib.slice_img_json(thresh_img, spaces_dict)

        image_name = im_path.split("/")[-1]
        print "Reading: " + image_name
        # Go thru each parking space in parking lot image (by id)
        # Mark as occupied or not occupied
        # img_preds = []
        for i, eachspace in enumerate(pspaces):
            id = str(i + 1)
            results_dict[dataset_name][image_name][id] = {}
            
            hist = cv2.calcHist([eachspace], [0], None, [256], [0, 256])
            # print ("Prediction time: {}").format(predict_time)
            hist_black = hist[0][0]
            hist_white = hist[255][0]
            results_dict[dataset_name][image_name][id]["hist_black"] = int(hist_black)
            results_dict[dataset_name][image_name][id]["hist_white"] = int(hist_white)
            list_black.append(hist_black)
            list_white.append(hist_white)

            num_spaces = num_spaces + 1
            if hist_black > max_black:
                max_black = hist_black
            if hist_white > max_white:
                max_white = hist_white

            if hist_black < min_black:
                min_black = hist_black
            if hist_white < min_white:
                min_white = hist_white

            ave_black = ave_black + hist_black
            ave_white = ave_white + hist_white
            num_spaces = num_spaces + 1
    ave_black = ave_black/num_spaces
    ave_white = ave_white/num_spaces

    pstd_black = np.std(list_black)
    pstd_white = np.std(list_white)

    print "Done predicting!"
    folder_name = exp_name + "_results_" + dataset_name

    # parent_b = os.path.abspath(parent_b_temp)
    if target_path is None:
        folder_path = os.path.join(dir_main, "results", folder_name)
    else:
        folder_path = target_path

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    json_filename = folder_name + ".json"
    json_path = os.path.join(folder_path, json_filename)
    text_filename = folder_name + ".txt"
    text_path = os.path.join(folder_path, text_filename)
    write_file = open(text_path, 'w')
    write_file.truncate()
    str_a = "Num of spaces: {}\n".format(num_spaces)
    print str_a
    write_file.write(str_a)
    str_a = "Max black: {}\n".format(max_black)
    print str_a
    write_file.write(str_a)
    str_a = "Min black: {}\n".format(min_black)
    print str_a
    write_file.write(str_a)
    str_a = "Ave black: {}\n".format(ave_black)
    print str_a
    write_file.write(str_a)
    str_a = "pstd black: {}\n".format(pstd_black)
    print str_a
    write_file.write(str_a)


    str_a = "Max white: {}\n".format(max_white)
    print str_a
    write_file.write(str_a)
    str_a = "Min white: {}\n".format(min_white)
    print str_a
    write_file.write(str_a)
    str_a = "Ave white: {}\n".format(ave_white)
    print str_a
    write_file.write(str_a)
    str_a = "pstd white: {}\n".format(pstd_white)
    print str_a
    write_file.write(str_a)

    str_a = "white list: {}\n"
    print str_a
    write_file.write(str_a)
    str_a = str(list_white) + "\n"
    write_file.write(str_a)
    str_a = "black list: {}\n"
    print str_a
    write_file.write(str_a)
    str_a = str(list_black) + "\n"
    write_file.write(str_a)
    write_file.close()
    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str
    with io.open(json_path, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        results_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    return json_path

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)
    ap.add_argument(
        '-m', "--marked_json_path", help="Path to json of marked images",
        required=True)
    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")


    """
    python eval_histogram.py \
    -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040517_040717_morning_b \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_b.json \
    -m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040517_040717_morning_b.json \
    -n testhist_BT_b

    python eval_histogram.py \
    -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040517_040717_morning_c \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_c.json \
    -m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_040517_040717_morning_c.json \
    -n testhist_BT_c
    """
    

    args = vars(ap.parse_args())

    dataset_path = args["dataset"]
    json_path = args["json_path"]
    marked_path = args["marked_json_path"]
    exp_name = args["exp_name"]
    folder_path = args["folder_path"]
   
    eval_histogram(dataset_path, json_path, marked_path, exp_name, folder_path)
