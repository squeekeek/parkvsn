<!-- TOC -->

- [eeei_CNL_031717_morning](#eeei_cnl_031717_morning)
    - [Status: Done!](#status-done)
        - [Notes](#notes)
- [eeei_5th_solar_040217_to_040317_morning_a](#eeei_5th_solar_040217_to_040317_morning_a)
    - [Status: Done!](#status-done-1)
        - [Notes](#notes-1)
- [eeei_5th_solar_040217_to_040317_morning_b](#eeei_5th_solar_040217_to_040317_morning_b)
    - [Status: Done!](#status-done-2)
        - [Notes](#notes-2)
- [eeei_5th_solar_040517_040717_morning_a](#eeei_5th_solar_040517_040717_morning_a)
    - [Status: Done!](#status-done-3)
        - [Notes:](#notes)
- [eeei_5th_solar_040517_040717_morning_b](#eeei_5th_solar_040517_040717_morning_b)
    - [Status: Done!](#status-done-4)
        - [Notes:](#notes-1)
- [eeei_5th_solar_040517_040717_morning_c](#eeei_5th_solar_040517_040717_morning_c)
    - [Status: Done!](#status-done-5)
        - [Notes:](#notes-2)

<!-- /TOC -->
# eeei_CNL_031717_morning
## Status: Done!
### Notes
- obstruction with garbage truck
- obstruction with people beside garbage truck
python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_cnl_20170317_morning \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_CNL_031717_morning.json 


# eeei_5th_solar_040217_to_040317_morning_a
## Status: Done!
### Notes
- has a few frames with people as obstruction
python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040217_to_040317_morning_a \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040217_to_040317_morning_a.json


# eeei_5th_solar_040217_to_040317_morning_b
## Status: Done!
### Notes
- obstruction with the anomalous parking red truck with ladders on top
    - classified two occupied spaces as unoccupied
- there's a person Reading: 20170403_0842.jpg
    - classified as unoccupied
python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040217_to_040317_morning_b \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040217_to_040317_morning_b.json 

# eeei_5th_solar_040517_040717_morning_a
## Status: Done!
### Notes:
- has images with mostly shadows from trees
- all are empty
python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040517_040717_morning_a \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_a.json

# eeei_5th_solar_040517_040717_morning_b
## Status: Done!
### Notes:
- all are unoccupied
- some misalignment happened
- there is heavy background changes caused by shadows caused by trees

python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040517_040717_morning_b \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_b.json

# eeei_5th_solar_040517_040717_morning_c
## Status: Done!
### Notes: 
- all are empty
- shadows is a problem
- marked inside parking lot as  much as possible

python mark_dataset.py \
-d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_040517_040717_morning_c \
-j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_040517_040717_morning_c.json