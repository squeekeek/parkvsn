"""
    For generating template for test scripts given json of test sets in config

    // Need a testing script for each HoG Parameter

    Example Usage:
    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py -f /Users/agatha/Desktop -n test
    
    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_PUC_smartwire/050317 \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_first_triggered/pipeline_eval_all_wdet.py \
    -n puc_onemodel_wdiv_40x60_local \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_PUC/onemodel_w40_h60_w_div \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_PUC/onemodel_w40_h60_w_div/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/puc_hog_list.json \
    -ho "blk-4_cell-4" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050417_hogparams_solar_a \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_highest_conf/pipeline_eval_all_wdet.py \
    -n test_highest_conf \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-4_cell-4/w_div \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except/blk-4_cell-4/w_div/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list.json \
    -ho "blk-4_cell-4" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050417 \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_highest_conf/pipeline_eval_all_wdet.py \
    -n one-view-against-all_wo_except_w_det \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/one-view-against-all_except-major_wo_div \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/one-view-against-all_except-major_wo_div/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog.json \
    -ho "blk-4_cell-4" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json


    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050617 \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_highest_conf/pipeline_eval_all_wdet.py \
    -n perwinsize_blk-4_cell-4 \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-4_cell-4 \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-4_cell-4/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_2.json \
    -ho "blk-4_cell-4" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json


    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050617 \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_highest_conf/pipeline_eval_all_wdet.py \
    -n perwinsize_blk-2_cell-6 \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-2_cell-6 \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-2_cell-6/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_2.json \
    -ho "blk-2_cell-6" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_tool_scripts/script_test_generator.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050717_a \
    -s /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_hog_first_triggered70/pipeline_eval_all_wdet.py \
    -n perview_blk-4_cell-4_070_notbalanced \
    -i y \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perview_wo_except_notbalanced/blk-4_cell-4/wo_div \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perview_wo_except_notbalanced/blk-4_cell-4/wo_div/models.json \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_4.json \
    -ho "blk-4_cell-4" \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json
"""

import cv2
import numpy as np

import logging



import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld


def script_test_generator(folder_path, eval_script_path, exp_name, ifImages, model_folder, model_json, hog_json, hog_option, dataset_json):

    # Load dataset setting
    dataset_config_path = dataset_json
    with open(dataset_config_path) as data_file:
        dataset_dict = json.load(data_file)

    # Open file for writing
    script_name = exp_name + ".sh"
    template_path = os.path.join(folder_path, script_name)
    write_file = open(template_path, 'w')
    write_file.truncate()

    # Creates folder of same name as dataset_name
    for i, dataset in enumerate(dataset_dict["testing"]):
        dataset_name = dataset["name"]
        dataset_path = dataset["location"]
        for j, test in enumerate(dataset["marking_json"]):
            # Put in script for testset
            script_text = (
                "# {dataset_name}: {marking_type}\n"
                "python {eval_script} \\\n"
                "-d {dataset} \\\n"
                "-j {spaces_path} \\\n"
                "-m {marked_path} \\\n"
                "-i {ifImages} \\\n"
                "-n {exp_name} \\\n"
                "-mf {model_folder} \\\n"
                "-mj {model_json} \\\n"
                "-hj {hog_json} \\\n"
                "-ho {hog_option} \n\n"
            ).format(
                dataset_name=dataset_name,
                marking_type=test["name"],
                eval_script=eval_script_path,
                dataset=dataset["location"],
                spaces_path=dataset["marking_json"][j]["json"],
                marked_path=dataset["occupancy_json"][j]["json"],
                ifImages=ifImages,
                exp_name=exp_name,
                model_folder=model_folder,
                model_json=model_json,
                hog_json=hog_json,
                hog_option=hog_option
            )
            print script_text
            write_file.write(script_text)

    write_file.close()

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store script generated")
    ap.add_argument(
        '-s', "--eval_script_path", help="Path to eval script to use")
    ap.add_argument(
        '-n', "--exp_name", help="Name of experiment"
    )
    ap.add_argument(
        '-i', "--ifImages", help="Yes or no to output images",
        required=True
    )
    ap.add_argument(
        '-mf', "--model_folder", help="Folder containing models",
        required=True
    )
    ap.add_argument(
        '-mj', "--model_json", help="Json containing models",
        required=True
    )
    ap.add_argument(
        "-hj", "--hog_json", help="HoG json file path", 
        required=True
    )
    ap.add_argument(
        "-ho", "--hog_option", help="HoG option id",
        required=True
    )
    ap.add_argument(
        "-dj", "--dataset_json", help="Dataset json path",
        required=True
    )

    args = vars(ap.parse_args())
    folder_path = args["folder_path"]
    eval_script_path = args["eval_script_path"]
    exp_name = args["exp_name"]
    ifImages = args["ifImages"]
    model_folder = args["model_folder"]
    model_json = args["model_json"]
    hog_json = args["hog_json"]
    hog_option = args["hog_option"]
    dataset_json = args["dataset_json"]

    script_test_generator(folder_path, eval_script_path, exp_name, ifImages, model_folder, model_json, hog_json, hog_option, dataset_json)

