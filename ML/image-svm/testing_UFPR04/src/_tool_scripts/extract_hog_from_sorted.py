from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
from datetime import datetime

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-sf', "--sorted_folder", help="Folder of sorted images")
    ap.add_argument(
        '-tf', "--target_folder", help="Folder to store hog copy of sorted folder")
    args = vars(ap.parse_args())

    sorted_folder = args["sorted_folder"]
    target_folder = args["target_folder"]

    sorted_images = os.path.join(sorted_folder, "*", "*")

    for im_path in glob.glob(sorted_images):
        
