"""
    Script for grouping models, placing them into folders,
    and generating models.json for each.

    Example usage:
    python resolve_models_hogparam.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models/per_view_wo_except \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list.json

    python resolve_models_hogparam.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models/per_view_w_except \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_winsize_notbalanced \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_notbalanced \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_4.json


    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_36_flip \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models/perwinsize_36_flip \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_3_9.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_hogparams_w_except_28_210 \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perview_w_except_210_212 \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_210_212.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_hogparams_wo_except_28_210 \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perview_wo_except_210_212 \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_210_212.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_hog_w_except_26 \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_26_w_except \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_26.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_26_flip_wo_except \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_26_wo_except \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_26.json

    python resolve_models_hogparam_list.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_26_flip_wo_except \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/results/perwinsize_26_flip_wo_except\
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_26.json


"""

import os
import sys
import json
import argparse
import glob
import io
import shutil


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-mf", "--model_folder",
        help="folder containing models."
    )
    ap.add_argument(
        "-f", "--target_folder",
        help="Target folder"
    )
    ap.add_argument(
        "-j", "--hog_json",
        help="HoG json file to follow"
    )
    args = vars(ap.parse_args())
    model_folder = args["model_folder"]
    target_folder = args["target_folder"]
    hog_json = args["hog_json"]

    with open(hog_json) as data_file:
        hog_dict = json.load(data_file)

    model_paths = os.path.join(model_folder, "*")

    for j, option in enumerate(hog_dict["hog"]):
        curr_id = option["id"]
        w_div_dict = {"UFPR04": []}
        wo_div_dict = {"UFPR04": []}
        w_div_path = os.path.join(target_folder, curr_id, "w_div")
        wo_div_path = os.path.join(target_folder, curr_id,  "wo_div")
        # Create wo_div and w_div folders
        if not os.path.isdir(w_div_path):
            os.makedirs(w_div_path)
        if not os.path.isdir(wo_div_path):
            os.makedirs(wo_div_path)

        for model_path in glob.glob(model_paths):
            # Parse if it is wo_div or not
            model = model_path.split("/")[-1]
            # if list items > 1, has uses hog option
            if len(model.split(curr_id)) > 1 and len(model.split(".model")) > 1:
                if len(model.split("_wo_div.model")) > 1:
                    # Put in wo_div
                    model_split = model.split("_")
                    width = int(model_split[0][1:])
                    height = int(model_split[1][1:])
                    wo_div_dict["UFPR04"].append({"model": model, "rows": height, "cols": width})
                    copyFile(model_path, wo_div_path)
                else:
                    model_split = model.split("_")
                    width = int(model_split[0][1:])
                    height = int(model_split[1][1:])
                    w_div_dict["UFPR04"].append({"model": model, "rows": height, "cols": width})
                    copyFile(model_path, w_div_path)

        wo_div_model_json = os.path.join(wo_div_path, "models.json")
        w_div_model_json = os.path.join(w_div_path, "models.json")

        print "wo_div_model_json: {}".format(wo_div_model_json)
        print "w_div_model_json: {}".format(w_div_model_json)

        # store in json file
        try:
            to_unicode = unicode
        except NameError:
            to_unicode = str


        with io.open(wo_div_model_json, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            wo_div_dict,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))

        with io.open(w_div_model_json, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            w_div_dict,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))





