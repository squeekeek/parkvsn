"""
    For finding lbp params, this is specifically for the format for UFPR04 lbp parameter finding.

    Will generate test script, one for each test case listed in lbp_json file.

    python script_test_generator_lbpparam.py \
    -mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_wo_except \
    -s /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_first_triggered/pipeline_eval_all_wdet.py \
    -tf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050417/lbpparams_solar_a \
    -hj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list.json \
    -dj /home/agatha/Documents/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python script_test_generator_lbpparam.py \
    -mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_view_w_except \
    -s /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_first_triggered/pipeline_eval_all_wdet.py \
    -tf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050417_lbpparams_solar_a_w_except \
    -hj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list.json \
    -dj /home/agatha/Documents/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python script_test_generator_lbpparam.py \
    -mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-2_cell-6 \
    -s /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_lbp_first_triggered/pipeline_eval_all_wdet.py \
    -tf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050617_lbpparams_perwinsize \
    -hj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list_2.json \
    -dj /home/agatha/Documents/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python script_test_generator_lbpparam.py \
    -mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/per_winsize_blk-4_cell-4 \
    -s /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_lbp_first_triggered/pipeline_eval_all_wdet.py \
    -tf /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/050617_lbpparams_perwinsize \
    -hj /home/agatha/Documents/parkvsn/ML/image-svm/config/lbp_list_2.json \
    -dj /home/agatha/Documents/parkvsn/ML/image-svm/config/datasets_smartwire.json

    python script_test_generator_lbpparam.py \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/pw_36_01_lbp_18/r-1_p-8/wo_div \
    -s /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts_w_det_w_scores_w_lbp_first_triggered/pipeline_eval_all_wdet.py \
    -tf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/scripts_UFPR04_smartwire_testing/051017 \
    -lj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/lbp.json \
    -dj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/datasets_smartwire.json
"""

import cv2
import numpy as np

import logging



import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from script_test_generator import script_test_generator

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-mf', "--main_model_folder", help="Folder path containing models for lbp param")
    ap.add_argument(
        '-s', "--eval_script_path", help="Path to eval script to use")
    ap.add_argument(
        '-tf', "--target_folder", help="Folder where to store script generated"
    )
    ap.add_argument(
        '-lj', "--lbp_json", help="lbp json file path"
    )
    ap.add_argument(
        "-dj", "--dataset_json", help="Dataset json path",
        required=True
    )
    args = vars(ap.parse_args())
    main_model_folder = args["main_model_folder"]
    eval_script_path = args["eval_script_path"]
    target_folder = args["target_folder"]
    lbp_json = args["lbp_json"]
    dataset_json = args["dataset_json"]

    with open(lbp_json) as data_file:
        lbp_dict = json.load(data_file)

    ifImages = "y"


    for i, option in enumerate(lbp_dict["lbp"]):
        """
        # For w_div
        model_folder = os.path.join(main_model_folder, option["id"], "w_div")
        model_json = os.path.join(model_folder, "models.json")
        lbp_option = option["id"]
        exp_name = "w_div_" + "w_except_" +  lbp_option

        script_test_generator(
            target_folder, eval_script_path, exp_name, ifImages, model_folder, model_json, lbp_json, lbp_option, dataset_json
        )
        """
        # For w_div
        model_folder = os.path.join(main_model_folder, option["id"], "w_div")
        model_json = os.path.join(model_folder, "models.json")
        lbp_option = option["id"]
        exp_name = "wo_div_" +"w_except_" + lbp_option

        script_test_generator(
            target_folder, eval_script_path, exp_name, ifImages, model_folder, model_json, lbp_json, lbp_option, dataset_json
        )