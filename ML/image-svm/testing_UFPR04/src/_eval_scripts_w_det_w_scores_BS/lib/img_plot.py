from __future__ import division

import cv2
from matplotlib import pyplot as plt
import numpy as np


def plot_spaces(pspaces):
    # Plot each parking space
    plt.figure()
    plt.title('Segmented Parking Spaces')
    sqsize = np.ceil(np.sqrt(len(pspaces)))

    for k, eachimg in enumerate(pspaces):
        plt.subplot(sqsize, sqsize, k + 1)
        plt.title(str(k + 1), size=10, color='b', y=-0.01, weight='bold')
        plt.xticks([]), plt.yticks([])

        if len(eachimg.shape) == 3 and eachimg.shape[2] == 3:
            plt.imshow(eachimg[:, :, ::-1])
        else:
            plt.imshow(eachimg, 'gray')

    plt.subplots_adjust(wspace=0.05, hspace=0.05)


def plot_poly(polypts, strparams):
    if type(polypts).__module__ == np.__name__:
        polypts = polypts.tolist()

    for p in zip(polypts + [polypts[0]], \
                 polypts[1:] + [polypts[0]]):
        aline_xs, aline_ys = (p[0][0], p[1][0]), (p[0][1], p[1][1])
        #print aline_xs, aline_ys

        plt.plot(aline_xs, aline_ys, strparams, linewidth=1)


def plot_img_with_segments(img, ppts):
    # Show original image
    # Show with boxes
    plt.figure()
    plt.title('Parking Lot with ROIs', fontsize=10)

    try:
        if len(img[0][0]) == 3:
            plt.imshow(img[:, :, ::-1])
        elif len(img[0][0]) < 3:
            plt.imshow(img, 'gray')
    except:
        plt.imshow(img, 'gray')

    plt.xticks([]), plt.yticks([])

    for k, (isfull, plotpts) in enumerate(ppts):
        plt.text(plotpts[0][0], plotpts[0][1], str(k + 1), size=10, color='b', weight='bold')

        if isfull == -1:
            plot_poly(plotpts, 'b-')
        elif isfull == 0:
            plot_poly(plotpts, 'g-')
        else:
            plot_poly(plotpts, 'r-')


def slice_one_img(img, rectpts):
    # Get each parking space by masking
    smask = np.zeros(img.shape, dtype=np.uint8)
    nchannels = img.shape[2] if (len(img.shape) == 3) else 1
    smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
    pkspace = cv2.bitwise_and(smask, img)

    # Crop rectangles to masked size
    mincoords = map(min, zip(*rectpts))
    maxcoords = map(max, zip(*rectpts))
    normpts = map(tuple, np.subtract(rectpts, mincoords))

    # Then rotate and perspective transform
    #new_img = trans_space(pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]], normpts)

    return new_img


def write_seg_img(dirname, basename, pspaces):
    for k, eachimg in enumerate(pspaces):
        cv2.imwrite("{}/{}_seg{}.jpg".format(dirname, basename, k + 1), eachimg)