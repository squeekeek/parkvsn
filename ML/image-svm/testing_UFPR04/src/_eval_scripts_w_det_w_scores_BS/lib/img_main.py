from __future__ import division
import cv2
import os
import numpy as np
import sys

from matplotlib import pyplot as plt
import img_math
import img_predict
import img_slice
import img_rectify
import img_plot


def get_occupancy_list(img, spaces_dict, use_calibration=False):
    ref_img_path = 'parakeet_bs_ref.jpg'

    bsobj = img_predict.ParakeetBackgroundSubtractor()

    if os.path.isfile(ref_img_path):
        img_ref = cv2.imread(ref_img_path, cv2.IMREAD_GRAYSCALE)
        bsobj.set_ref_img(img_ref)

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_masked = bsobj.feed(img_gray)
    img_masked_imp = cv2.dilate(img_masked, cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10)))
    #nlabels, labels, stat, centroids = cv2.connectedComponentsWithStats(img_masked_imp)

    if 'calibration' in spaces_dict and use_calibration:
        detect_top_thresh = 0.70
        detect_bot_thresh = 0.50

        vert_pts = np.float32(spaces_dict['calibration']['vert_pts'])
        horiz_pts = np.float32(spaces_dict['calibration']['horiz_pts'])
        line_vec = np.float32(spaces_dict['calibration']['line_vec'])

        dr = [[0, 0], [0, img.shape[0]], img.shape[1::-1], [img.shape[1], 0]]
        rect_trans, chulls, tmat, (pspaces_bottom, pspaces_top) = img_rectify.rectify(img_masked_imp, spaces_dict, vert_pts, horiz_pts, line_vec, slice_spaces=True)
        ht, rdim = img_rectify.get_new_pdim(dr, tmat)
        img_new = cv2.warpPerspective(img, ht.dot(tmat), rdim)
        prect = [list(a) for a in zip(range(0, len(rect_trans[0])), rect_trans[0])]

        for rectidx, eachhull in enumerate(chulls):
            prect[rectidx][0] = 0

            _, contours_top, _ = cv2.findContours(pspaces_top[rectidx], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            _, contours_bottom, _ = cv2.findContours(pspaces_bottom[rectidx], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for eachcontour_top, eachcontour_bot in zip(contours_top, contours_bottom):
                c_area_top = cv2.contourArea(eachcontour_top)
                c_area_bot = cv2.contourArea(eachcontour_bot)

                if c_area_bot >= detect_top_thresh * img_math.polygon_area(rect_trans[0][rectidx]):
                    prect[rectidx][0] = 1
                if c_area_top >= detect_bot_thresh * img_math.polygon_area(rect_trans[1][rectidx]):
                    prect[rectidx][0] = 1

                if prect[rectidx][0] == 1:
                    break
    else:
        detect_thresh = 0.50

        img_sliced, prect = img_slice.slice_img_json(img_masked_imp, spaces_dict)

        for rectidx, (isfull, eachrect) in enumerate(prect):
            prect[rectidx][0] = 0

            _, contours, _ = cv2.findContours(img_sliced[rectidx], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for eachcontour in contours:
                c_area = cv2.contourArea(eachcontour)

                if c_area >= detect_thresh * img_math.polygon_area(eachrect):
                    prect[rectidx][0] = 1
                    break

    occlist, _ = zip(*prect)
    bsobj.save_ref_img()
    return zip(range(0, len(occlist)), occlist)


def main():
    test_path = '../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626'
    img = cv2.imread(test_path + '.jpg', 1)

    with open(test_path + '.xml') as fh:
        pspaces_dict = xmld.parse(fh.read())

        print get_occupancy_list(img, pspaces_dict)

if __name__ == "__main__":
    main()