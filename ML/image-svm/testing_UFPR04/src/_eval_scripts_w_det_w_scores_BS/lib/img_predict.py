from __future__ import division

import os
import cv2
import numpy as np
import math
import xmltodict as xmld

from matplotlib import pyplot as plt
from scipy.signal import argrelmin

import img_math
import img_slice
import img_plot


class ParakeetBackgroundSubtractor2:
    u"""
    A Low-Complexity Vision-Based System for Real-Time Traffic Monitoring
    by Engel et al (2017)
    """
    def __init__(self):
        pass

    def feed(self, img):
        pass


class ParakeetBackgroundSubtractor:
    u"""
    Robust Background Subtraction on Traffic Videos by Nunes et al (2011)
    """
    def __init__(self):
        pass

    def set_ref_img(self, img):
        self.refimg = img

    def get_ref_img(self):
        try:
            return self.refimg
        except AttributeError:
            return None

    def save_ref_img(self):
        cv2.imwrite('parakeet_bs_ref.jpg', self.refimg)

    def get_last_fg(self):
        try:
            return self.lastfg
        except AttributeError:
            return None

    def feed(self, img):
        self.ref_calibrate(img)

        return self.lastfg

    def ref_calibrate(self, img):
        try:
            greatmat = np.greater(img, self.refimg)
            lessmat = np.less(img, self.refimg)

            self.refimg += greatmat
            self.refimg -= lessmat

            diffgray = np.abs(self.refimg - img)
            diffhist = cv2.calcHist([diffgray], [0], None, [256], [0, 256])
            lmin = argrelmin(np.ravel(diffhist))[0][1]

            self.lastfg = np.uint8((diffgray < lmin) * 255)

        except AttributeError:
            self.refimg = img
            self.lastfg = np.uint8(np.zeros(img.shape[:2]))
        except Exception as e:
            print 'Exception at img_predict: {}'.format(e)


def main():
    dataset_path = "../datasets/EEEI/eeei_5th_solar_20170310/"

    fnames = [os.path.splitext(f)[0] for f in os.listdir(dataset_path) if f.endswith('.jpg')]
    bsobj = ParakeetBackgroundSubtractor()

    xmlf = ''
    with open(dataset_path + '20170311-1626' + '.xml') as fd:
        xmlf = xmld.parse(fd.read())

    #'''
    # Background calibration
    print "Feeding background subtraction..."

    for k, eachfile in enumerate(fnames):
        timeofday = int(eachfile.split('-')[1])

        if timeofday < 600 or timeofday > 1800:
            continue

        img = cv2.imread(dataset_path + eachfile + '.jpg', 1)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        bsobj.ref_calibrate(img_gray)
    #'''

    plt.figure()
    plt.imshow(bsobj.get_ref_img(), 'gray')

    # Background Subtraction Test
    print "Executing background subtraction..."
    for k, eachfile in enumerate(fnames):
        timeofday = int(eachfile.split('-')[1])

        if timeofday < 600 or timeofday > 1800:
            continue

        img = cv2.imread(dataset_path + eachfile + '.jpg', 1)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        img_masked = bsobj.feed(img_gray)
        #img_masked_imp = cv2.morphologyEx(img_masked, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10)))
        img_masked_imp = cv2.dilate(img_masked, cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10)))
        #nlabels, labels, stat, centroids = cv2.connectedComponentsWithStats(img_masked_imp)

        img_sliced, prect = img_slice.slice_img_pklot(img_masked_imp, xmlf)

        #cv2.imwrite('test/' + eachfile + '.jpg', img_masked)

        if timeofday == 1406:
            for rectidx, (isfull, eachrect) in enumerate(prect):
                prect[rectidx][0] = 0

                _, contours, _ = cv2.findContours(img_sliced[rectidx], cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

                for k in contours:
                    carea = cv2.contourArea(k)

                    if carea >= 0.40 * img_math.polygon_area(eachrect):
                        prect[rectidx][0] = 1
                        break

            img_plot.plot_spaces(img_sliced)
            #img_plot.plot_img_with_segments(img_masked_imp, prect)
            img_plot.plot_img_with_segments(img, prect)

    bsobj.save_ref_img()

    plt.show()

if __name__ == "__main__":
    main()