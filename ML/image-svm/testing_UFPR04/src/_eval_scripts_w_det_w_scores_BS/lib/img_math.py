from __future__ import division

import math
import numpy as np


def ccwtest(a, b, c):
    return np.cross(a, b) + np.cross(b, c) + np.cross(c, a)


def sortfourcw(rectpts):
    """Sort four points clockwise

    Sort a vector of four points contained in *rectpts* in clockwise starting
    from the upper-left (wrt image axis) and return it.
    """

    if len(rectpts) != 4:
        return None

    a, b, c, d = rectpts

    if ccwtest(a, b, c) < 0.0:
        if ccwtest(a, c, d) < 0.0:
            pass
        elif ccwtest(a, b, d) < 0.0:
            d, c = c, d
        else:
            d, a = a, d
        pass
    elif ccwtest(a, c, d) < 0.0:
        if ccwtest(a, b, d) < 0.0:
            c, b = b, c
        else:
            b, a = a, b
    else:
        c, a = a, c

    ccwpts = [a, b, c, d]
    ccwpts = np.roll(ccwpts, -np.argmin(np.sum(ccwpts, axis=1)), axis=0)

    return np.array(ccwpts)


def linepair_to_circle_90deg(linepair):
    """Convert a pair of lines into a circle

    Returns a pair of lines *linepair* converted to a circle with a certain
    radius and center. The algorithm is based on a paper by Liebowitz and
    Zisserman (1998), and is assumed that the pairs make a 90deg angle wrt
    each other.
    """

    # ((center_x, center_y), rad)
    if len(linepair) != 2:
        return None

    alines = map(linepts_to_abc, linepair)

    if np.isclose(alines[0][0], 0, atol=1e-6) or np.isclose(alines[1][0], 0, atol=1e-6):
        # In case of alines[0][0] or alines[1][0] be zero,
        # then the circle becomes the y-axis. :O
        return (None, float('inf'))

    lla = -alines[0][1] / alines[0][0]
    llb = -alines[1][1] / alines[1][0]

    calpha = (lla + llb) / 2
    r = abs((lla - llb) / 2)

    return ((calpha, 0), r)


def linepair_to_circle_samearg(linepair):

    if len(linepair) != 2:
        return None

    alines = map(linepts_to_abc, linepair)

    if np.isclose(alines[0][0], 0, atol=1e-6) or np.isclose(alines[1][0], 0, atol=1e-6):
        # In case of alines[0][0] or alines[1][0] be zero,
        # then the circle becomes the y-axis. :O
        return (None, float('inf'))

    lla = -alines[0][1] / alines[0][0]
    llb = -alines[1][1] / alines[1][0]

    eqdenom = lla[0] - llb[0] - lla[1] + llb[1]
    calpha = (lla[0] * llb[1] - llb[0] * lla[1]) / eqdenom
    r = calpha**2
    r += ((lla[0] - llb[0]) * (lla[0] * llb[0] - lla[1] * llb[1])) / eqdenom
    r -= lla[0] * llb[0]
    r = math.sqrt(r)

    return ((calpha, 0), r)


def circle_intersect(circpair):
    u"""

    """

    if len(circpair) != 2:
        return None
    elif circpair[0][1] == float('inf') and circpair[1][1] != None:
        # y = 0
        return (circpair[1][1] + circpair[1][0][0], 0)
    elif circpair[1][1] == float('inf') and circpair[0][1] != None:
        # y = 0
        return (circpair[0][1] + circpair[0][0][0], 0)

    cntr_a, rad_a = circpair[0]
    cntr_b, rad_b = circpair[1]

    d = math.sqrt((cntr_a[0] - cntr_b[0]) ** 2 + (cntr_a[1] - cntr_b[1]) ** 2)

    if d > rad_a + rad_b or d < abs(rad_a - rad_b):
        print "Warning: Separate circles in circle_intersect"
        # No solution: Circles are separate or one is inside of another
        return None
    elif d == 0 and rad_a == rad_b:
        # Circles are the same
        return None

    a = ((rad_a ** 2) - (rad_b ** 2) + (d ** 2)) / (2 * d)
    h = rad_a ** 2 - a ** 2
    p2 = np.array(cntr_a) + a * (np.array(cntr_b) - np.array(cntr_a)) / d

    pta = (p2[0] + h * (cntr_b[1] - cntr_a[1]) / d, p2[1] - h * (cntr_b[0] - cntr_a[0]) / d)
    ptb = (p2[0] - h * (cntr_b[1] - cntr_a[1]) / d, p2[1] + h * (cntr_b[0] - cntr_a[0]) / d)

    return pta if pta[1] > 0 else ptb


def rect_to_pts(rect):
    u"""Convert rectangle representation into coordinates

    This function converts a rectangle represented as a tuple of
    (xul, yul, w, h) with the first two the coordinates of the upper-left
    of the rectangle, and the last two the dimensions.

    :return: rectangle represented as a list of coordinates of its four corners
             sorted clockwise
    :rtype: numpy list of four elements
    """
    # Should be in xul, yul, w, h
    xul, yul, w, h = rect

    return sortfourcw([(xul, yul), (xul + w, yul), (xul + w, yul + h), (xul, yul + h)])


def linepts_to_abc(ptpair):
    u"""Convert a two-point representation of a line into a
    homogeneous standard form

    Returns an equation of the line in standard form ax + by + c = 0 made by the
    point pairs *ptpair*. The function will return the line with c = 1, which is
    also equivalent to a line in homogeneous coordinates.

    """
    if ptpair[0][0] == ptpair[1][0]:
        return np.array([1, 0, -ptpair[0][0]])

    sl = (ptpair[0][1] - ptpair[1][1]) / (ptpair[0][0] - ptpair[1][0])

    la = sl
    lb = -1
    lc = -sl * ptpair[0][0] + ptpair[0][1]

    la /= lc
    lb /= lc
    lc = 1

    return np.array([la, lb, lc])


def get_closest_to_line(pts, line_mxb):
    u"""Get points on a least-squares line closest to the given ones

    :param xpts: list of x-coordinates
    :param ypts: list of y-coordinates
    :param line_mxb: dictionary of a line with slope m and y-intercept b
    """
    # y = mx + b, 0 = mx - y + b
    den_r = line_mxb["m"]**2 + 1

    xpts_closest = map(lambda pt: (pt[0] + line_mxb["m"] * (pt[1] - line_mxb["b"])) / den_r, pts)
    ypts_closest = map(lambda pt: (line_mxb["b"] + line_mxb["m"] * (pt[0] + line_mxb["m"] * pt[1])) / den_r,  pts)

    return zip(xpts_closest, ypts_closest)


def is_point_in_rect(point, rect):
    rect = sortfourcw(rect)

    for idx in range(0, len(rect)):
        if np.cross(rect[idx] - point, rect[(idx + 1) % len(rect)] - point) > 0.0:
            return False

    return True


def polygon_area(poly):
    u""" Get the area of a polygon using the shoelace formula

    :param poly: polygon

    :type poly: list of points
    """
    poly = sortfourcw(poly)
    xs, ys = zip(*poly)

    return 0.5 * np.abs(np.dot(xs, np.roll(ys, 1)) - np.dot(ys, np.roll(xs, 1)))


def get_length(rectpts):
    totaldist = 0

    for i in range(0, 4):
        totaldist += np.linalg.norm(rectpts[i] - rectpts[(i + 1) % len(rectpts)])

    return totaldist / 8
