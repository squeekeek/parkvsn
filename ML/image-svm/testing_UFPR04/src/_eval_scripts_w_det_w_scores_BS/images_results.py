"""
    Saves images of the parking lots in a folder. 
    Green = occupied
    Red = unoccupied

    Example usage:
    python images-results.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -r results_test.json

    python images_results.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -r /Users/agatha/Desktop/test-on-dataset-scripts/results/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased.json -n test041717_halfwaymodels 

    python images_results.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -r /Users/agatha/Desktop/test-on-dataset-scripts/results/041417_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased/results_eeei_5th_solar_041017_a_morning_topcarbased.json -n 041717_halfwaymodels

     python images_results.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -r /Users/agatha/Desktop/test-on-dataset-scripts/results/041417_halfwaymodels_results_eeei_5th_solar_041017_a_morning_parkinglinebased/results_eeei_5th_solar_041017_a_morning_parkinglinebased.json -n 041417_halfwaymodels

"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib
from lib import img_slice

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib.p1_hog_svm import hog_svm

def image_results(dataset_path, json_path, results_json_path, exp_name, target_path=None):
    """
    Saves images of the parking lots in a folder. 
    Green = correct prediction
    Red = wrong prediction

    Example usage:
    python images-results.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -r results_test.json
    """

    # Load json file containing parking lot spaces
    with open(json_path) as data_file:
        spaces_dict = json.load(data_file)

    # Load json file containing marked spaces
    with open(results_json_path) as data_file:
        results_dict = json.load(data_file)


    # Dictionary with actual results
    dataset_name = " "
    for i in results_dict:
        dataset_name = i

    actual_dataset = os.path.join(dataset_path, "*")

    # Creates folder of same name as dataset_name
    folder_name = exp_name + "_results_" + dataset_name 
    img_folder_name = "img_" + exp_name + "_results_" + dataset_name 


    if target_path is None:
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        save_path = os.path.join(dir_main, "results", folder_name, img_folder_name)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
    else:
        folder_path = folder_name
        save_path = os.path.join(target_path, folder_name)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)

    marked_path = os.path.join(save_path, "Boxed")
    segmented_path = os.path.join(save_path, "Segmented")
    occupied_path = os.path.join(segmented_path, "Occupied")
    empty_path = os.path.join(segmented_path, "Empty")

    if not os.path.isdir(marked_path):
        os.makedirs(marked_path)

    if not os.path.isdir(segmented_path):
        os.makedirs(segmented_path)

    if not os.path.isdir(occupied_path):
        os.makedirs(occupied_path)

    if not os.path.isdir(empty_path):
        os.makedirs(empty_path)




    num_images = 0
    # print spaces_dict
    # print spaces_dict["spaces"]
    num_orig_spaces = len(spaces_dict["spaces"])

    # Go thru every parking lot image in the dataset
    for im_path in glob.glob(actual_dataset):
        image_name = im_path.split("\\")[-1]
        ifImage = image_name.split(".")
        if ifImage[-1] != "jpg":
            continue

        #seg_path = os.path.join(segmented_path, ifImage[0])
        # Crop images given json path of coordintes
        test_img = cv2.imread(im_path, 1)
        # assumption is that json file is ordered by descending
        pspaces, ppts = img_slice.slice_img_json(test_img, spaces_dict)

        # Mark each space for the image
        for i, id in enumerate(results_dict[dataset_name][image_name]):
            if id != "total_time":
                j = int(id) -1
                if results_dict[dataset_name][image_name][id]["pred"] == 0:
                    rectcolor = (0, 255, 0)
                    im_name = id + ".jpg"
                    #actual_seg_path = os.path.join(seg_path, im_name)


                    seg_path = os.path.join(occupied_path, ifImage[0])
                    actual_seg_path = seg_path + "#" + im_name

                    cv2.imwrite(actual_seg_path, pspaces[j])
                else:
                    im_name = id + ".jpg"
                    #actual_seg_path = os.path.join(seg_path, im_name)

                    seg_path = os.path.join(empty_path, ifImage[0])
                    actual_seg_path = seg_path + "#" + im_name
                    cv2.imwrite(actual_seg_path, pspaces[j])
                    rectcolor = (0, 0, 255)

                cv2.polylines(test_img, np.array([ppts[j][1]]), True, rectcolor, thickness=2)
                cv2.putText(test_img, str(j + 1), tuple(ppts[j][1][0]), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 0, 0), thickness=2)
        #cv2.imshow(image_name, test_img)
        #cv2.waitKey(0)
        #sys.exit()
        #actual_save_path = os.path.join(save_path, image_name)
        #print actual_save_path
        #cv2.imwrite(actual_save_path, test_img)
        actual_marked_path = os.path.join(marked_path, image_name)
        #print actual_marked_path
        cv2.imwrite(actual_marked_path, test_img)
        num_images = num_images + 1

    print "Parking lot images: {}".format(num_images)
    print "Segmented spaces: {}".format(num_images*num_orig_spaces)

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)

    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store images")
   
    args = vars(ap.parse_args())

    dataset_path = args["dataset"]
    json_path = args["json_path"]
    results_json_path = args["results_json_path"]
    exp_name = args["exp_name"]
    folder_path = args["folder_path"]

    image_results(dataset_path, json_path, results_json_path, exp_name, folder_path)
