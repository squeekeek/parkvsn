from __future__ import division
import os
import cv2
import numpy as np
import xmltodict as xmld
import matplotlib
import math
#from matplotlib import pyplot as plt
from operator import itemgetter

def sortfourcw(rectpts):
    crossprod = lambda a, b: a[0] * b[1] - a[1] * b[0]
    ccwtest = lambda a, b, c: crossprod(a, b) + crossprod(b, c) + crossprod(c, a)

    if len(rectpts) != 4:
        return None

    a, b, c, d = rectpts
    
    if ccwtest(a, b, c) < 0.0:
        if ccwtest(a, c, d) < 0.0:
            pass
        elif ccwtest(a, b, d) < 0.0:
            d, c = c, d
        else:
            d, a = a, d
        pass
    elif ccwtest(a, c, d) < 0.0:
        if ccwtest(a, b, d) < 0.0:
            c, b = b, c
        else:
            b, a = a, b
    else:
        c, a = a, c
    
    ccwpts = [a, b, c, d]
    ccwpts = np.roll(ccwpts, -np.argmin(np.sum(ccwpts, axis=1)), axis=0)
    
    return np.array(ccwpts)
    
def trans_space(myimg, rectpts):
    rectpts = np.array(rectpts)
    
    # Correct rotation
    rectctr, rectdim, rectarg = np.array(cv2.minAreaRect(rectpts))
    if rectdim[0] > rectdim[1]:
        rectarg += 90
    
    # Transform points
    rotmat = cv2.getRotationMatrix2D(rectctr, rectarg, 1)
    s = np.array(map(lambda t: tuple(t) + (1, ), rectpts))
    rectpts = np.transpose(np.matmul(rotmat, np.transpose(s)))
    
    destsize = np.array(cv2.minAreaRect(np.int32(rectpts)))[1]
    rectdim = map(int, (destsize[0], destsize[1]) if destsize[0] < destsize[1] else (destsize[1], destsize[0]))
    deltax, deltay = rectdim[0] / 2.0 - rectctr[0], rectdim[1] / 2.0 - rectctr[1]
    
    rotmat[0][2] += deltax
    rotmat[1][2] += deltay
    rectpts = np.float32(map(lambda pt: (pt[0] + deltax, pt[1] + deltay), rectpts))
    imgrotd = cv2.warpAffine(myimg, rotmat, tuple(rectdim))
    
    # Correct perspective
    ccwpts = sortfourcw(rectpts)
    newpts = np.float32([(0, 0), (0, rectdim[1]), rectdim, (rectdim[0], 0)])
    
    perspmat = cv2.getPerspectiveTransform(ccwpts, newpts)
    newimg = cv2.warpPerspective(imgrotd, perspmat, tuple(rectdim))
    
    # Returns corrected image
    return newimg

def slice_img(img, xmlboxes):
    print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']
    
    pspaces = []
    pspacepts = []
    for eachspace in xmlboxes['parking']['space']:
        rectpts = sortfourcw([(int(a['@x']), int(a['@y'])) for a in eachspace['contour']['point']])
        
        # Get each parking space by masking
        smask = np.zeros(img.shape, dtype=np.uint8)
        nchannels = img.shape[2] if (len(img.shape) == 3) else 1
        smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
        pkspace = cv2.bitwise_and(smask, img)
        
        # Crop rectangles to masked size
        mincoords = map(min, zip(*rectpts))
        maxcoords = map(max, zip(*rectpts))
        normpts = map(tuple, np.subtract(rectpts, mincoords))
        
        # Then rotate and perspective transform
        pspaces.append(trans_space(pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]], normpts))
        pspacepts.append((int(eachspace['@occupied']) if '@occupied' in eachspace else -1, rectpts.tolist()))
    
    print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return (pspaces, pspacepts)


def slice_img_json(img, json_dict):
    #print 'Reading dataset in parking lot ' + json_dict["parking_id"]
    
    pspaces = []
    ppts = []
    for eachspace in json_dict["spaces"]:
        #print "ID: " + str(eachspace["id"])
        rectpts = sortfourcw([(int(a['x']), int(a['y'])) for a in eachspace['pts']])
        
        # Get each parking space by masking
        smask = np.zeros(img.shape, dtype=np.uint8)
        nchannels = img.shape[2] if (len(img.shape) == 3) else 1
        smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
        pkspace = cv2.bitwise_and(smask, img)
        
        # Crop rectangles to masked size
        mincoords = map(min, zip(*rectpts))
        maxcoords = map(max, zip(*rectpts))
        normpts = map(tuple, np.subtract(rectpts, mincoords))
        
        # Then rotate and perspective transform
        pspaces.append(trans_space(pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]], normpts))
        ppts.append(rectpts.tolist())

    #print 'Read ' + str(len(json_dict['spaces'])) + ' parking spaces successfully!'
    return pspaces, ppts

def write_seg_img(dirname, basename, pspaces):
    for k, eachimg in enumerate(pspaces):
        cv2.imwrite("{}/{}_seg{}.jpg".format(dirname, basename, k + 1), eachimg)