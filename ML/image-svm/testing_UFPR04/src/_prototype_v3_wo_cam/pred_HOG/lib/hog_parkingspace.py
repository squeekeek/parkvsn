"""
    Expects an image of .mat format, and a loadedmodel

"""


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
from datetime import datetime


DEBUG = 0



def hog_parkingspace(image, model, win_width, win_height, visualize_det, ifVisualize, hog_setting, detection_dict):
    """
    Evaluates a parking space
    Passing image path and model path
    """

    # Load model 
    clf = model

    # Load image
    orig_image = image
    orig_height, orig_width = orig_image.shape[:2]
    orig_ratio = orig_width/float(orig_height)

    # Resize to height of 100 px maintaining aspect ratio
    res_height = 100
    res_width = int(res_height*orig_ratio)
    resized_image = cv2.resize(orig_image, (res_width, res_height))

    # Grayscale image
    gray_resized = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

    """
    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    parent_c = os.path.abspath(parent_c_temp)
    config_path = os.path.join(parent_c, "config")


    # Load detection settings
    detection_config_path = os.path.join(config_path, "detection.json")
    with open(detection_config_path) as data_file:
        detection_dict = json.load(data_file)

    """
    # Settings for HoG
    window_size = (win_height, win_width)
    orientations = hog_setting["orientations"]
    pixels_per_cell = hog_setting["pixels_per_cell"]
    cells_per_block = hog_setting["cells_per_block"]
    transform_sqrt = hog_setting["transform_sqrt"]
    block_norm = hog_setting["block_norm"]



    # Settings for image pyramid
    downscale = detection_dict["detection"]["pyramid"]["downscale"]
    scale = 0
    step_size = detection_dict["detection"]["sliding_window"]["step_size"]
    detections = []
    # Settings for nms
    threshold = detection_dict["detection"]["nms"]["threshold"]

    clone = resized_image.copy()
    # Downscale the image and iterate
    for im_scaled in pyramid(gray_resized, scale=downscale, minSize=window_size):
    #for im_scaled in pyramid_gaussian(gray_resized, downscale=downscale):
        # This list contains detections at the current scale
        cd = []
        # If the width or height of the scaled image is less than the width
        # or height of the widnow, then end the iterations
        if im_scaled.shape[0] < window_size[0] or im_scaled.shape[1] < window_size[1]:
            break
        # Skip scale when the specified window size occupies less than 70% of scaled image
        if window_size[1] <= int(im_scaled.shape[1]*0.6):
            continue

        #  print "im_scaled.shape[:]: {}".format(im_scaled.shape[:])
        for (x, y, im_window) in sliding_window(im_scaled, window_size, step_size):
            if im_window.shape[0] != window_size[0] or im_window.shape[1] != window_size[1]:
                continue
            # sys.exit()
            # Calculate the HoG features
            fd = feature.hog(
                im_window,
                orientations=orientations,
                pixels_per_cell=pixels_per_cell,
                cells_per_block=cells_per_block,
                transform_sqrt=transform_sqrt,
                visualise=False,
                block_norm=block_norm
            )

            pred = clf.predict(fd.reshape(1, -1))
            if pred[0] > 0:
                test_df = clf.decision_function(fd.reshape(1, -1))
                detections.append((
                    x, y, test_df,
                    int(window_size[1]*(downscale**scale)),
                    int(window_size[0]*(downscale**scale))
                    ))
                cd.append(detections[-1])
        # Move the next scale
        #downscale = downscale + 1
        scale = scale + 1

    return detections

if __name__ == "__main__":

    image = cv2.imread("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/img_marked_eeei_5th_solar_041017_a_morning_parkinglinebased/Segmented/Occupied/20170330_1030#8.jpg")
    win_width = 50
    win_height = 80
    visualize_det = 0
    ifVisualize = 0
    model = joblib.load("/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except_wo_div/w50_h80_pos_right-1neg_unocc_ALL_wo_div.model")
    hog_option = 0

    start_time = datetime.now()
    prediction = hog_parkingspace(
            image, model, win_width,
            win_height, visualize_det, ifVisualize, hog_option)
    end_time = datetime.now()
    print "Time elapsed: {}".format(end_time-start_time)
    print "Car detections: {}".format(prediction)
