"""
    Template for testing and evaluating a classifier/predictor.
    Outputs misclassified and rightly classified images in json file.
    Returns path of json file.

    Example usage:
    python eval-classifier.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json

    python eval_classifier.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_topcarbased.json -m /Users/agatha/Desktop/test-on-dataset-scripts/data/preprocess/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -n test041717_halfwaymodels
"""


import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib
from lib.p1_lbp_svm import lbp_svm
import time
# import matplotlib.pyplot as plt


def eval_classifier(dataset_path, json_path, marked_path, exp_name, target_path, model_folder, model_json, lbp_json, lbp_option, detection_json):
    """
    Template for testing and evaluating a classifier/predictor.
    Outputs evaluation scores as a text file. 
    Outputs misclassified and rightly classified images in json file.
    Returns path of json file

    Example usage:
    python eval-classifier.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json -n 041417_halfwaymodels
    """

    # START CLASSIFIERS TO BE LOADED 
    # Load json file of models
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)

    #model_folder = os.path.join(dir_main, "models", "one-view-against-all_left")
    #model_filename = "lbp_svm_models.json"
    #model_file = os.path.join(model_folder, model_filename)

    with open(model_json) as data_file:
        models_dict = json.load(data_file)
    # END CLASSIFIERS TO BE LOADED 

    # Load json file containing parking lot spaces
    with open(json_path) as data_file:
        spaces_dict = json.load(data_file)

    # Load json file containing marked spaces
    with open(marked_path) as data_file:
        marked_dict = json.load(data_file)

    # Load detection settings
    with open(detection_json) as data_file:
        detection_dict = json.load(data_file)

    # Load lbp setting
    with open(lbp_json) as data_file:
        lbp_dict = json.load(data_file)

    lbp_setting = None
    for option in lbp_dict["lbp"]:
        if lbp_option == option["id"]:
            lbp_setting = option
            break
    if lbp_setting is None:
        print "Invalid lbp option or lbp id"
        sys.exit()

    # Dictionary with actual results
    dataset_name = spaces_dict["parking_id"]
    results_dict = {dataset_name: {}}

    actual_dataset = os.path.join(dataset_path, "*")

    # for target folder
    if target_path is None:
        folder_path = os.path.join(dir_main, "results", folder_name)
        img_path = os.path.join(folder_path, "detections")
        empty_path = os.path.join(img_path, "Empty")
        occupied_path = os.path.join(img_path, "Occupied")
        lbp_path = os.path.join(folder_path, "lbp")
        lbp_empty_path = os.path.join(lbp_path, "Empty")
        lbp_occupied_path = os.path.join(lbp_path, "Occupied")
    else:
        folder_path = target_path
        img_path = os.path.join(folder_path, "detections")
        empty_path = os.path.join(img_path, "Empty")
        occupied_path = os.path.join(img_path, "Occupied")
        lbp_path = os.path.join(folder_path, "lbp")
        lbp_empty_path = os.path.join(lbp_path, "Empty")
        lbp_occupied_path = os.path.join(lbp_path, "Occupied")

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    if not os.path.isdir(empty_path):
        os.makedirs(empty_path)

    if not os.path.isdir(occupied_path):
        os.makedirs(occupied_path)

    if not os.path.isdir(lbp_path):
        os.makedirs(lbp_path)
    
    if not os.path.isdir(lbp_empty_path):
        os.makedirs(lbp_empty_path)

    if not os.path.isdir(lbp_occupied_path):
        os.makedirs(lbp_occupied_path)


    # Go thru every parking lot image in the dataset
    for im_path in glob.glob(actual_dataset):
        image_name = im_path.split("/")[-1]
        ifImage = image_name.split(".")
        if ifImage[-1] != "jpg":
            continue
        results_dict[dataset_name][image_name] = {}
        # Crop images given json path of coordintes
        test_img = cv2.imread(im_path, 1)
        # assumption is that json file is ordered by descending
        pspaces, ppts = improc_lib.slice_img_json(test_img, spaces_dict)

        image_name = im_path.split("/")[-1]
        print "Reading: " + image_name
        # Go thru each parking space in parking lot image (by id)
        # Mark as occupied or not occupied
        # img_preds = []
        highest_value = []
        for i, eachspace in enumerate(pspaces):
            id = str(i + 1)
            results_dict[dataset_name][image_name][id] = {}
            start_time = time.time()
            # START DO PREDICTION WITH CLASSIFIER
            # Do prediction
            list_predictions, highest_value, highest_model, image, lbpImage = lbp_svm(eachspace, models_dict, model_folder, lbp_setting, detection_dict)
            # predict, image_det = lbp_svm(eachspace, models_dict, model_folder)
            print "highest_value: {}".format(highest_value)
            if highest_value > 0:
                predict = 1
                results_dict[dataset_name][image_name][id]["pred"] = 1
                image_individual = image_name + "#" + id + ".jpg"
                sorted_folder = os.path.join(occupied_path, highest_model)
                if not os.path.isdir(sorted_folder):
                    os.makedirs(sorted_folder)
                write_path = os.path.join(sorted_folder, image_individual)
                cv2.imwrite(write_path, image)

                lbp_sorted_folder = os.path.join(lbp_occupied_path, highest_model)
                if not os.path.isdir(lbp_sorted_folder):
                    os.makedirs(lbp_sorted_folder)
                write_path = os.path.join(lbp_sorted_folder, image_individual)
                cv2.imwrite(write_path, lbpImage)
                #lbpImage.savefig(write_path, dpi=55)
            else:
                predict = 0
                results_dict[dataset_name][image_name][id]["pred"] = 0
                image_individual = image_name + "#" + id + ".jpg"
                write_path = os.path.join(empty_path, image_individual)
                cv2.imwrite(write_path, image)
                if highest_model is not None:
                    lbp_sorted_folder = os.path.join(lbp_empty_path, highest_model)
                else: 
                    lbp_sorted_folder = os.path.join(lbp_empty_path, "none")

                if not os.path.isdir(lbp_sorted_folder):
                    os.makedirs(lbp_sorted_folder)
                write_path = os.path.join(lbp_sorted_folder, image_individual)
                #lbpImage.savefig(write_path, dpi=55)
                cv2.imwrite(write_path, lbpImage)
            # END DO PREDICTION WITH CLASSIFIER
            end_time = time.time()
            predict_time = end_time - start_time
            # print ("Prediction time: {}").format(predict_time)
            results_dict[dataset_name][image_name][id]["list_predictions"] = list_predictions
            results_dict[dataset_name][image_name][id]["highest_value"] = highest_value
            results_dict[dataset_name][image_name][id]["highest_model"] = highest_model
            results_dict[dataset_name][image_name][id]["timePred"] = predict_time
            actual_result = marked_dict[dataset_name][image_name][id]["ifOccupied"]

            actual_result = marked_dict[dataset_name][image_name][id]["ifOccupied"]
            results_dict[dataset_name][image_name][id]["ifOccupied"] = marked_dict[dataset_name][image_name][id]["ifOccupied"]
            if actual_result == predict:
                results_dict[dataset_name][image_name][id]["ifRightPred"] = 1
            else:
                results_dict[dataset_name][image_name][id]["ifRightPred"] = 0

            print "################################"
            print "image_individual: {}".format(image_individual)
            print "highest_model: {}".format(highest_model)
            print "highest_value: {}".format(highest_value)
            print "Predict time: {}".format(predict_time)
            print "list_predictions:"
            print list_predictions
            print "actual_result: {}".format(actual_result)
            print "predict: {}".format(predict)
            print "################################"

    print "Done predicting!"
    folder_name = exp_name + "_results_" + dataset_name

    # parent_b = os.path.abspath(parent_b_temp)


    json_filename = folder_name + ".json"
    json_path = os.path.join(folder_path, json_filename)

    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str
    with io.open(json_path, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        results_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    return json_path

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)
    ap.add_argument(
        '-m', "--marked_json_path", help="Path to json of marked images",
        required=True)
    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-mf', "--model_folder", help="Folder containing models",
        required=True
    )
    ap.add_argument(
        '-mj', "--model_json", help="Folder containing models",
        required=True
    )




    args = vars(ap.parse_args())

    dataset_path = args["dataset"]
    json_path = args["json_path"]
    marked_path = args["marked_json_path"]
    exp_name = args["exp_name"]
    folder_path = args["folder_path"]
    model_folder = args["model_folder"]
    model_json = args["model_json"]

    eval_classifier(dataset_path, json_path, marked_path, exp_name, folder_path, model_folder, model_json)
