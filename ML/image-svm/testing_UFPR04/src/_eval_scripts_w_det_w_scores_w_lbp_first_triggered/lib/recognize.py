# USAGE
# python recognize.py --training images/training --testing images/testing
# python recognize.py --training images_a/training --testing images_a/testing

# import the necessary packages
from pyimagesearch.localbinarypatterns import LocalBinaryPatterns
from sklearn.svm import LinearSVC
from imutils import paths
import argparse
import cv2
import os
import sys
from sklearn.externals import joblib

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--training", required=True,
    help="path to the training images")
ap.add_argument("-e", "--testing", required=True, 
    help="path to the tesitng images")
args = vars(ap.parse_args())

# initialize the local binary patterns descriptor along with
# the data and label lists
desc = LocalBinaryPatterns(8, 1)
#desc = LocalBinaryPatterns(24, 8)
data = []
labels = []

feat_path = "lbpfeats"
if not os.path.isdir(feat_path):
    os.makedirs(feat_path)


# loop over the training images
for imagePath in paths.list_images(args["training"]):
    # load the image, convert it to grayscale, and describe it
    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hist = desc.describe(gray)
    hist_len = len(hist.tolist())
    print hist
    print "{}: {}".format(imagePath, hist_len)
    fd_name = imagePath.split("/")[-1].split(".")[0] + ".feat"
    fd_path = os.path.join(feat_path, fd_name)
    joblib.dump(hist, fd_path)

    # extract the label from the image path, then update the
    # label and data lists
    labels.append(imagePath.split("/")[-2])
    data.append(hist)

# train a Linear SVM on the data
model = LinearSVC(C=100.0, random_state=42)
model.fit(data, labels)

# loop over the testing images
for imagePath in paths.list_images(args["testing"]):
    # load the image, convert it to grayscale, describe it,
    # and classify it
    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hist = desc.describe(gray)
    prediction = model.predict(hist.reshape(1, -1))

    # display the image and the prediction
    cv2.putText(image, prediction[0], (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
        0.3, (0, 0, 255), 1)
    cv2.imshow("Image", image)
    cv2.waitKey(0)