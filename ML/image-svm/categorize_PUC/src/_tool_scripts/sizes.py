"""
    - Use this to have info on the minimum, maximum and
    average dimensions of each category, or all categories.
    - Use this with the JSON file containing positive/occupied lots.

    Exmaple Usage:
    python sizes.py \
    -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ \
    -c ALL \
    -s PUC \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_PUC/data/sorted_paths/PUC_pos_paths.json 

"""
import argparse 
import glob
import os
import json
import io
import cv2
import sys

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05, ALL",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL"
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to list of paths",
        required=True)
    ap.add_argument(
        "-f", "--target_folder",
        help="Target folder"
    )
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]
    target_folder = args["target_folder"]

    if target_folder is None:
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        target_folder = os.path.join(dir_main, "data", "winsize")

    if not os.path.isdir(target_folder):
        os.makedirs(target_folder)
    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []

    if category == "ALL":
        to_extract = json_paths.keys()
    else:
        to_extract.append(category)


    # instance variables
    num_images = 0
    min_dim = [float('Inf'), float('Inf')]
    max_dim = [0, 0]
    ave_dim = [0, 9]

    actual_pklot = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    print actual_pklot

    filename = set_option + "_" + category + "_sizes.txt"
    filepath = os.path.join(target_folder, filename)


    write_file = open(filepath, 'w')
    write_file.truncate()
    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        if len(json_paths[key]) == 0:
            continue
        for j, partial_path in enumerate(json_paths[key]):
            # do something
            im_path = actual_pklot + partial_path
            #im_path = os.path.join(actual_pklot, partial_path)
          
            image = cv2.imread(im_path)

            width = image.shape[1]
            height = image.shape[0]
            dim = width*height
            if dim < (min_dim[0]*min_dim[1]):
                min_dim[0] = height
                min_dim[1] = width 
                min_path = im_path
            if dim > (max_dim[0]*max_dim[1]):
                max_dim[0] = height
                max_dim[1] = width 
                max_path = im_path
            ave_dim[0] = ave_dim[0] + height
            ave_dim[1] = ave_dim[1] + width

            num_images = num_images + 1

        ave_dim[0] = ave_dim[0]/num_images
        ave_dim[1] = ave_dim[1]/num_images
        str_a = "########################"+ "\n"
        print str_a
        write_file.write(str_a)
        str_a = "Category: " + str(key)+ "\n"
        print str_a
        write_file.write(str_a)
        str_a = "Number of images: " + str(num_images)+ "\n"
        print str_a
        write_file.write(str_a)
        str_a = "Image Dimensions:"+ "\n"
        print str_a
        write_file.write(str_a)
        str_a = ("Min. Dim: [{}, {}]\n").format(min_dim[0], min_dim[1])
        print str_a
        write_file.write(str_a)
        str_a = ("Min Path: {}\n").format(min_path)
        print str_a
        write_file.write(str_a)
        str_a = ("Max. Dim: [{}, {}]\n").format(max_dim[0], max_dim[1])
        print str_a
        write_file.write(str_a)
        str_a = ("Max Path: {}\n").format(max_path)
        print str_a
        write_file.write(str_a)
        str_a = ("Ave. Dim: [{}, {}]\n").format(ave_dim[0], ave_dim[1])
        print str_a
        write_file.write(str_a)
        str_a = ""+ "\n"
        print str_a
        write_file.write(str_a)
        str_a = "########################"+ "\n"
        print str_a
        write_file.write(str_a)
        # Reset all variables
        ave_dim[0] = 0
        ave_dim[1] = 0
        num_images = 0
        min_dim = [float('Inf'), float('Inf')]
        max_dim = [0, 0]
        ave_dim = [0, 9]

    write_file.close()





