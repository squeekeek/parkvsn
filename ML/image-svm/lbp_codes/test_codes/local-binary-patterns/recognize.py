# USAGE
# python recognize.py --training images/training --testing images/testing
# python recognize.py --training images_a/training --testing images_a/testing

# import the necessary packages
from pyimagesearch.localbinarypatterns import LocalBinaryPatterns
from sklearn.svm import LinearSVC
from imutils import paths
import argparse
import cv2
import os
import sys
from sklearn.externals import joblib
import numpy as np

import glob
import sys
from sklearn.model_selection import train_test_split
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--training", required=True,
    help="path to the training images")
ap.add_argument("-e", "--testing", required=True, 
    help="path to the tesitng images")
args = vars(ap.parse_args())

# initialize the local binary patterns descriptor along with
# the data and label lists
#desc = LocalBinaryPatterns(8, 1)

desc = LocalBinaryPatterns(8, 1)
data = []
labels = []

feat_path = "lbpfeats"
if not os.path.isdir(feat_path):
    os.makedirs(feat_path)


lbp_rows = 1
lbp_cols = 2
# loop over the training images
for imagePath in paths.list_images(args["training"]):
    # load the image, convert it to grayscale, and describe it
    image = cv2.imread(imagePath)
    gray_resized = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_rows = gray_resized.shape[0]
    image_cols = gray_resized.shape[1]

    pixel_rows = image_rows/lbp_rows
    pixel_cols = image_cols/lbp_cols

    curr_row = 0

    for m in range(0, lbp_rows, 1):
        curr_col = 0
        for n in range(0, lbp_cols, 1):
            curr_block = gray_resized[curr_row:curr_row+pixel_rows, curr_col:curr_col+pixel_cols]
            fd_partial = desc.describe(curr_block)
            if (m == 0) and (n == 0):
                fd = fd_partial
            else:
                fd = np.concatenate((fd, fd_partial))

            curr_col = curr_col + pixel_cols

        curr_row = curr_row + pixel_rows

    
    #hist = desc.describe(gray)
    #print hist
    hist_len = len(fd.tolist())
    print fd
    print "{}: {}".format(imagePath, hist_len)

    #fd_name = imagePath.split("/")[-1].split(".")[0] + ".feat"
    #fd_path = os.path.join(feat_path, fd_name)
    #joblib.dump(hist, fd_path)

    # extract the label from the image path, then update the
    # label and data lists

    #labels.append(imagePath.split("/")[-2])
    #data.append(fd)
    fd_name = imagePath.split("/")[-1].split(".")[0] + ".feat"
    label = imagePath.split("/")[-2]
    label_path = os.path.join(feat_path, label)
    if not os.path.isdir(label_path):
        os.makedirs(label_path)
    fd_path = os.path.join(label_path, fd_name)
    joblib.dump(fd, fd_path)

glob_feat_path = os.path.join(feat_path,"*", "*")
for feat_path in glob.glob(glob_feat_path):
    fd = joblib.load(feat_path)
    #actual_fd = np.array(fd, dtype=np.float)
    #print fd
    #print "before sys exit"
    #sys.exit()
    data.append(fd)
    labels.append(feat_path.split("/")[-2])


# train a Linear SVM on the data
#actual_data = np.array(data, dtype=np.float)
model = LinearSVC(C=0.01, class_weight="balanced")
print len(data)
print len(labels)
print data

fds_train, fds_temp, labels_train, labels_temp = train_test_split(
    data, labels, test_size=0.40, random_state=5)
fds_test, fds_cv, labels_test, labels_cv = train_test_split(
    fds_temp, labels_temp, test_size=0.50, random_state=5)
model.fit(fds_train, labels_train)
#model.fit(data, labels)

# loop over the testing images
for imagePath in paths.list_images(args["testing"]):
    # load the image, convert it to grayscale, describe it,
    # and classify it
    image = cv2.imread(imagePath)
    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray_resized = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_rows = gray_resized.shape[0]
    image_cols = gray_resized.shape[1]

    pixel_rows = image_rows/lbp_rows
    pixel_cols = image_cols/lbp_cols


    curr_row = 0

    for m in range(0, lbp_rows, 1):
        curr_col = 0
        for n in range(0, lbp_cols, 1):
            curr_block = gray_resized[curr_row:curr_row+pixel_rows, curr_col:curr_col+pixel_cols]
            fd_partial = desc.describe(curr_block)
            if (m == 0) and (n == 0):
                fd = fd_partial
            else:
                fd = np.concatenate((fd, fd_partial))

            curr_col = curr_col + pixel_cols

        curr_row = curr_row + pixel_rows

    #hist = desc.describe(gray)
    prediction = model.predict(fd.reshape(1, -1))

    # display the image and the prediction
    cv2.putText(image, prediction[0], (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
        0.3, (0, 0, 255), 1)
    cv2.imshow("Image", image)
    cv2.waitKey(0)