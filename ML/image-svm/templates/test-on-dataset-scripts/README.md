Use for evaluating classifiers

Save json from /webgui/label_spaces.htm.
Have the parking_id the same as the dataset folder it would account for, for unique id. Also for adjustments such as having the plot aligned with parking spaces or with the top of cars

mark_dataset.py
- script for marking parking lots
- uses a modified slice_img function, as slice_img_json from improc_lib.py
- example usage:
    - python mark_dataset.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json

segmented_images.py
- script for generating parking lot images with boxes, and segmented images like in the Pklot dataset into a folder
- example usage: 
    -      python segmented_images.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json

eval_all.py
- script for evaluating classifier, producing evaluation results, and optionally outputting parking lot images with boxes, where green boxes signify correct classification, and red as wrong
- makes use of the ff. scripts: eval_classifier.py, eval_results.py, improc_lib.py (modified for json), image_results.py
- example usage:
    - python eval_all.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json  -m /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/marked_eeei_5th_solar_041017_a_morning_topcarbased.json -i y


eval_classifier.py
"""
    Template for testing and evaluating a classifier/predictor.
    Outputs misclassified and rightly classified images in json file.


    Example usage:
    python eval-classifier.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -m marked_test.json
"""

eval_results.py
"""
    Evaluates json produced by eval-classifier.py
    Produces classification scores and other evaluation in text

    Saves results in text file.

    Example usage:
    python eval-results.py -j results_test.json
"""

improc_lib.py
Modifications done:
- created modiifed slice_img to slice_img_json
    - removes tuple from ppts for occupied, ppts just remains as is

image_results.py
"""
    Saves images of the parking lots in a folder. 
    Green = correct prediction
    Red = wrong prediction

    Example usage:
    python images-results.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j eeei-5thsolar-parking-spaces.json -r results_test.json
"""