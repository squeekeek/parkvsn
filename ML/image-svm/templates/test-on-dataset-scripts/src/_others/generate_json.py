"""
    Outputs JSON file to be filled out with whether empty or not.

    Example usage:
    python generate-json.py -ds /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/eeei_5th_solar_dataset -j eeei-5thsolar-parking-spaces.json
"""

import argparse 
import glob
import os
import json
import io
import sys


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-ds', "--dataset_path", help="Path to dataset of images",
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json path of parking spaces",
        required=True)
    args = vars(ap.parse_args())

    dataset_path = args["dataset_path"]
    json_path = args["json_path"]

    # load json dict
    with open(json_path) as data_file:
        parking_dict = json.load(data_file)

    to_mark_dict = {}
    to_mark_dict["parking_id"] = parking_dict["parking_id"]
    to_mark_dict["spaces"] = []

    filename = "to-mark_" + json_path.split(".json")[0] + ".json"

    for i, lot in enumerate(parking_dict['spaces']):
        print lot['id']
        print lot['pts']
        temp = {}
        temp["id"] = lot["id"]
        temp["pts"] = lot["pts"]
        temp["ifOccupied"] = 0
        temp["ifRestricted"] = 0
        to_mark_dict["spaces"].append(temp)

    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        to_mark_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))
