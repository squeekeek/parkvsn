"""
    Tool for marking parking lot spaces.
    Outputs marked json.

    Usage: 
    Click on window with picture
    Enter appropriate keys
    u - unoccupied
    o - occupied

    Example usage:
    python mark_dataset.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/ -j to-mark_eeei-5thsolar-parking-spaces.json 

    

    python mark_dataset.py -d /Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/test_blanks -j to-mark_eeei-5thsolar-parking-spaces.json

    python mark_dataset.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json
"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)

    args = vars(ap.parse_args())

    # Load folder containing images of .jpg
    dataset_path = args["dataset"]
    json_path = args["json_path"]

    # Load json file containing parking lot spaces
    with open(json_path) as data_file:
        to_mark_dict = json.load(data_file)

    # Use parking_id as dataset_name
    dataset_name = to_mark_dict["parking_id"]

    # Dictionary of marked parking spaces
    marked_dict = {}
    marked_dict[dataset_name] = {}

    # Show each parsed image to mark as occupied or unoccupied
    actual_dataset = os.path.join(dataset_path, "*")


    # Go thru every parking lot image in the dataset
    prevMarks = []
    for im_path in glob.glob(actual_dataset):

        # Add image to dictionary
        image_name = im_path.split("/")[-1]
        ifImage = image_name.split(".")
        if ifImage[-1] != "jpg":
            continue
        print "Reading: " + image_name
        marked_dict[dataset_name][image_name] = {}

        # Crop images given json path of coordintes
        test_img = cv2.imread(im_path, 1)
        # assumption is that json file is ordered by descending
        pspaces, ppts = improc_lib.slice_img_json(test_img, to_mark_dict)

        # Add image to dictionary
        image_name = im_path.split("/")[-1]
        print "Reading: " + image_name
        marked_dict[dataset_name][image_name] = {}
        # Go thru each parking space in parking lot image (by id)
        # Mark as occupied or not occupied
        sameMarks = 0
        for i, eachspace in enumerate(pspaces):
            id = str(i + 1)
            marked_dict[dataset_name][image_name][id] = {}
            print "Enter o = occupied, u = unoccupied, s = same as before (if id == '1')"

            test_copy = test_img.copy()
            rectcolor = (0, 255, 0)
            cv2.polylines(test_copy, np.array([ppts[i]]), True, rectcolor, thickness=2)
            cv2.putText(test_copy, str(i + 1), tuple(ppts[i][0]), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 0, 0), thickness=2)

            while True:
                title = id + ":" + image_name
                cv2.imshow(image_name, test_copy)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("o"):
                    # means not same as prev marks
                    if (i == 0):
                        prevMarks = []
                    marked_dict[dataset_name][image_name][id]["ifOccupied"] = 1
                    print "#{}: Marked as occupied!".format(id)
                    prevMarks.append(1)
                    break
                elif key == ord("u"):
                    if (i == 0):
                        prevMarks = []
                    marked_dict[dataset_name][image_name][id]["ifOccupied"] = 0
                    print "#{}: Marked as unoccupied!".format(id)
                    prevMarks.append(0)
                    break
                elif (key == ord("s")) and (id == "1"):
                    print "Same as before"
                    sameMarks = 1
                    break

            cv2.destroyAllWindows()
            if sameMarks == 1:
                break
        if sameMarks == 1:
            sameMarks = 0
            # put same marks as before
            for i, eachspace in enumerate(pspaces):
                id = str(i + 1)
                marked_dict[dataset_name][image_name][id] = {}
                marked_dict[dataset_name][image_name][id]["ifOccupied"] = prevMarks[i]

    print "Done marking!"
    filename = "marked_" + dataset_name + ".json"
    # Creates folder of same name as dataset_name
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    parent_b = os.path.abspath(parent_b_temp)

    save_path = os.path.join(parent_b, "data", "preprocess", filename)
    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str
    with io.open(save_path, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        marked_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))
    print "Output: {}".format(filename)



