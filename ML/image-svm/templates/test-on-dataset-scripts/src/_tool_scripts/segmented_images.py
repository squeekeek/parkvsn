"""
    Outputs folder of images with segmented, and boxed images.

    Example usage:
    python segmented_images.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_parkinglinebased.json

    python segmented_images.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Documents/CoE198/parkvsn/ML/test-on-dataset-scripts/eeei_5th_solar_a_morning_topcarbased.json


    python segmented_images.py -d /Users/agatha/Documents/CoE198/datasets/EEEI/eeei_5th_solar_041017_a_morning -j /Users/agatha/Desktop/test-on-dataset-scripts/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json

"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-d', "--dataset", help="Path to dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of parking lot spaces coordinates",
        required=True)

    args = vars(ap.parse_args())

    dataset_path = args["dataset"]
    json_path = args["json_path"]

    # Load json file containing parking lot spaces
    with open(json_path) as data_file:
        to_mark_dict = json.load(data_file)

    # Use parking_id as dataset_name
    dataset_name = to_mark_dict["parking_id"]

    # Creates folder of same name as dataset_name
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    parent_b = os.path.abspath(parent_b_temp)

    save_path = os.path.join(parent_b, "data", "preprocess", dataset_name)
    marked_path = os.path.join(save_path, "Boxed")
    segmented_path = os.path.join(save_path, "Segmented")

    #    save_path = dataset_name
    #    marked_path = os.path.join(".", ".", save_path, "Boxed")
    #    segmented_path = os.path.join(save_path, "Segmented")
    if not os.path.isdir(save_path):
        os.makedirs(save_path)
        os.makedirs(marked_path)
        os.makedirs(segmented_path)

    # Show each parsed image to mark as occupied or unoccupied
    actual_dataset = os.path.join(dataset_path, "*")

    num_images = 0
    num_orig_spaces = len(to_mark_dict["spaces"])
    # Go thru every parking lot image in the dataset
    for im_path in glob.glob(actual_dataset):

        image_name = im_path.split("/")[-1]
        ifImage = image_name.split(".")
        if ifImage[-1] != "jpg":
            continue
        print "Reading: " + image_name

        seg_path = os.path.join(segmented_path, ifImage[0])
        #if not os.path.isdir(seg_path):
        #    os.makedirs(seg_path)
        # Crop images given json path of coordintes
        test_img = cv2.imread(im_path, 1)
        # assumption is that json file is ordered by descending
        pspaces, ppts = improc_lib.slice_img_json(test_img, to_mark_dict)



        # Go thru each parking space in parking lot image (by id)
        # Mark as occupied or not occupied
        for i, eachspace in enumerate(pspaces):
            id = str(i + 1)
            im_name = id + ".jpg"
            #actual_seg_path = os.path.join(seg_path, im_name)
            actual_seg_path = seg_path + "#" + im_name
            cv2.imwrite(actual_seg_path, eachspace)

            rectcolor = (0, 255, 0)
            cv2.polylines(test_img, np.array([ppts[i]]), True, rectcolor, thickness=2)
            cv2.putText(test_img, str(i + 1), tuple(ppts[i][0]), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 0, 0), thickness=2)

        actual_marked_path = os.path.join(marked_path, image_name)
        print actual_marked_path
        cv2.imwrite(actual_marked_path, test_img)
        num_images = num_images + 1
    
    print "Parking lot images: {}".format(num_images)
    print "Segmented spaces: {}".format(num_images*num_orig_spaces)