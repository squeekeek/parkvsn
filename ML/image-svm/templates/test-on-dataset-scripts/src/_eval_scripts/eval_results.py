"""
    Evaluates json produced by eval-classifier.py
    Produces classification scores and other evaluation in text

    Saves results in text file.

    Example usage:
    python eval_results.py -j /Users/agatha/Desktop/test-on-dataset-scripts/results/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased/test041717_halfwaymodels_results_eeei_5th_solar_041017_a_morning_topcarbased.json -n test041717_halfwaymodels
"""

import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from lib import improc_lib
from lib.p1_hog_svm import hog_svm

from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

from datetime import datetime

def eval_results(json_path, exp_name, target_path=None):
    """
    Evaluates json produced by eval-classifier.py
    Produces classification scores and other evaluation in text

    Saves results in text file.

    Example usage:
    python eval-results.py -j results_test.json
    """
    # Load json file
    with open(json_path) as data_file:
        results_dict = json.load(data_file)

    # Dictionary with actual results
    dataset_name = " "
    for i in results_dict:
        dataset_name = i

    # For generalization classification scores
    Y_actual = []
    Y_pred = []
    total_time = 0
    min_time = float("inf")
    max_time = 0

    # Get info from json file
    for im in results_dict[dataset_name]:
        for id in results_dict[dataset_name][im]:
            ifOccupied = results_dict[dataset_name][im][id]["ifOccupied"]
            ifRightPred = results_dict[dataset_name][im][id]["ifRightPred"]
            pred = results_dict[dataset_name][im][id]["pred"]
            timePred = results_dict[dataset_name][im][id]["timePred"]

            Y_actual.append(ifOccupied)
            Y_pred.append(pred)
            total_time = total_time + timePred

            if timePred < min_time:
                min_time = timePred
            if timePred > max_time:
                max_time = timePred

    # Do evaluation
    len_total = len(Y_actual)

    folder_name = exp_name + "_results_" + dataset_name

    timestamp = "{:%m%d%Y_%H%M}".format(datetime.now())
    filename = folder_name + timestamp + ".txt"

    if target_path is None:
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        folder_path = os.path.join(dir_main, "results", folder_name)
    else:
        folder_path = target_path

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    filepath = os.path.join(folder_path, filename)
    print filepath
    #sys.exit()

    print_num = (
        "Features in dataset (100%):\n"
        "Count: {}\n\n"
    ).format(len_total)

    avg_time = total_time/float(len_total)
    print_time = (
        "Average time in pred: {}\n"
        "Max time in pred: {}\n"
        "Min time in pred: {}\n\n"
    ).format(avg_time, max_time, min_time)

    print print_num
    print print_time

    target_names = [
        "Empty",
        "Occupied"
    ]
    c_report = classification_report(
        Y_actual, Y_pred, target_names=target_names)
    print c_report

    with open(filepath, 'w') as f:
        f.write(print_num)
        f.write(print_time)
        f.write(c_report)
        f.write("\n")


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-j', "--json_path", help="Path to json file of results",
        required=True)
    ap.add_argument(
        '-n', "--exp_name", help="Name of the experiment",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    args = vars(ap.parse_args())

    # Load json path
    json_path = args["json_path"]
    exp_name = args["exp_name"]
    folder_path = args["folder_path"]

    eval_results(json_path, exp_name, folder_path)
