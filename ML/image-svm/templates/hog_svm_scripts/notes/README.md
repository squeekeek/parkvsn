## generate-images.py

### Example usage
python generate-roi-images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j roi_UFPR04_complete-front-center.json

## generate-occupiedpaths.py

### Example usage
python generate-occupiedpaths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04

## generate-neg-roi-json.py

### Example usage
python generate-neg-roi-json.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j gen_UFPR04_occ_paths_59718.json -w 30 -ht 80


## filter-ratios.py
python filter-ratios.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j gennegroi_gen_UFPR04_occ_paths_59718_59718.json -r 1.25


## extract-hog.py
python extract-hog.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j roi_UFPR04_complete-front-center.json -w 40 -ht 50

## train-model.py
python train-model.py -p 

## generate-partial-roi-json.py
python generate-partial-roi-json.py -j gennegroi_gen_UFPR04_occ_paths_59718_48595.json -p 0.25

#### Cmd for occupied and unoccupied of right-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 -n feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80 -m pos_right-2neg_unocc_right-2

#### Cmd for occupied and unoccupied, and except right of right-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 -n feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-2neg_unocc_right-2_right



#### Cmd for occupied and unoccupied of right-3:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 -n feats_pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80 -m pos_right-3neg_unocc_right-3

#### Cmd for occupied and unoccupied, and except right of right-3:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 -n feats_pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-3neg_unocc_right-3_right


#### Cmd for occupied and unoccupied of center-1:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50 -n feats_pos_upper-left_genwin_center-1_UFPR04_neg_paths_14981_w-40h-50 -m pos_center-1neg_unocc_center-1

#### Cmd for occupied and unoccupied, and except center of center-1:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50 -n feats_pos_upper-left_genwin_center-1_UFPR04_neg_paths_14981_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-1neg_unocc_center-1_center

#### Cmd for occupied and unoccupied of center-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50 -n feats_pos_upper-left_genwin_center-2_UFPR04_neg_paths_11485_w-40h-50 -m pos_center-2neg_unocc_center-2

#### Cmd for occupied and unoccupied, and except center of center-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50 -n feats_pos_upper-left_genwin_center-2_UFPR04_neg_paths_11485_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-2neg_unocc_center-2_center

#### Cmd for occupied and unoccupied of center-3:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50 -n feats_pos_upper-left_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50 -m pos_center-3neg_unocc_center-3

#### Cmd for occupied and unoccupied, and except center of center-3:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50 -n feats_pos_upper-left_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 -m pos_center-3neg_unocc_center-3_center