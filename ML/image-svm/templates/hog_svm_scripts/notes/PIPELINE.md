#### UFPR04: left-2

python generate-occ-to-neg.py -j UFPR04_pos_paths.json -e left


python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c left-2 -w 30 -ht 80

python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-2_UFPR04_pos_paths_1469_w-30h-80.json -p upper-left

python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80.json -w 30 -ht 80

python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80  -m pos_left-2_neg_unocc

python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -n2  feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-2_neg_unocc_exceptleft


#### UFPR04: left-3

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c left-3 -w 40 -ht 80

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_neg_paths.json -c left-3 -w 40 -ht 80


python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -p upper-left


python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-3_UFPR04_neg_paths_1639_w-40h-80.json -p upper-left


python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80.json -w 40 -ht 80

python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j  pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -w 40 -ht 80

python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 -n feats_pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80 -m pos_left-3_neg_unocc-left-3

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths_except_left.json -c ALL -w 40 -ht 80

python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80.json -p upper-left

 pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80.json

 python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j   pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80.json -w 40 -ht 80

  

 python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 -n feats_pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80 -m pos_left-3_neg_unocc

 python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 -n feats_pos_upper-left_genwin_left-3_UFPR04_neg_paths_1639_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80 -m pos_left-3_neg_unocc_exceptleft

# UFPR04: right-1
**unocc*



python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c right-1 -w 40 -ht 80


python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_neg_paths.json -c right-1 -w 40 -ht 80

##### occupied right-1

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c right-1 -w 40 -ht 80

python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_right-1_UFPR04_pos_paths_2001_w-40h-80.json -p upper-left

python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80.json   -w 40 -ht 80

##### except right
python generate-occ-to-neg.py -j UFPR04_pos_paths.json -e right

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths_except_right.json -c ALL -w 40 -ht 80

python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80.json -p upper-left

python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j   pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80.json -w 40 -ht 80


##### unoccupied right-1

python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_neg_paths.json -c right-1 -w 40 -ht 80

python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_right-1_UFPR04_neg_paths_1789_w-40h-80.json -p upper-left

python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80.json  -w 40 -ht 80

##### training right-1
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 -n feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80  -m pos_right-1_neg_unocc_right-1


##### training right-1 with others

python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 -n feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 -m pos_right-1_neg_unocc_right-1_except-right



#### Cmd for occupied and unoccupied of left-1:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -m pos_left-1neg_unocc_left-1

#### Cmd for occupied and unoccupied, and except left of left-1:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1neg_unocc_left-1_left


#### Cmd for occupied and unoccupied of left-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_left-2_UFPR04_neg_paths_2295_w-30h-80 -m pos_left-2neg_unocc_left-2

#### Cmd for occupied and unoccupied, and except left of left-2:
python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 -n feats_pos_upper-left_genwin_left-2_UFPR04_neg_paths_2295_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-2neg_unocc_left-2_left