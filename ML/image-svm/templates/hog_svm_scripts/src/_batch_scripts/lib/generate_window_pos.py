"""
    Loads JSON file from generate-window.py 

    Creates JSON file similar to output of extract-roi.py
    - Can select if will crop from:
        - upper-left (for now ito muna, since it's just from 0,0)
        - upper-center
        - upper-right
        - lower-left
        - lower-center
        - lower-right

    Example Usage:
    python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json -p upper-left

     python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-1_UFPR04_neg_paths_8680_w-30h-80.json -p upper-left

     python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json -p upper-left

     python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_adjusted_dim/UFPR04_halfway/genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -p upper-left -n test
"""

import argparse
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0

def generate_window_pos(pklot_path, json_path, position, folder_path, project_name):

    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    with open(json_path) as data_file:
        json_dict = json.load(data_file)

    cropped_decoded = {}
    cropped_images = 0

    pos_dict = {
        "upper-right": "UR",
        "upper-left": "UL",
        "upper-center": "UC",
        "lower-left": "LL",
        "lower-center": "LC",
        "lower-right": "LR"
    }

    for i, key in enumerate(json_dict):
        cropped_decoded.update(
            {
                key: {}
            }
        )
        for j, im_path in enumerate(json_dict[key]):
            decoded_path = actual_pklot_path + im_path
            orig_image = cv2.imread(decoded_path)

            orig_dim_0 = json_dict[key][im_path]["orig_dim"][0]
            orig_dim_1 = json_dict[key][im_path]["orig_dim"][1]

            resized_dim_0 = json_dict[key][im_path]["resized_dim"][0]
            resized_dim_1 = json_dict[key][im_path]["resized_dim"][1]

            win_size_0 = json_dict[key][im_path]["win_size"][0]
            win_size_1 = json_dict[key][im_path]["win_size"][1]

            ifIncreasedWidth = json_dict[key][im_path]["ifIncreasedWidth"]
            ifIncreasedHeight = json_dict[key][im_path]["ifIncreasedHeight"]

            # TODO: Modify this to be position dependent
            pt1_0 = 0
            pt1_1 = 0

            pt2_0 = win_size_0
            pt2_1 = win_size_1

            cropped_decoded[key].update(
                {
                    im_path: {
                        "orig_dim": [orig_dim_0, orig_dim_1],
                        "resized_dim": [resized_dim_0, resized_dim_1],
                        "win_size": [win_size_0, win_size_1],
                        "ifIncreasedWidth": ifIncreasedWidth,
                        "ifIncreasedHeight": ifIncreasedHeight,
                        "pt1": [pt1_0, pt1_1],
                        "pt2": [pt2_0, pt2_1]
                    }
                }
            )

            cropped_images = cropped_images + 1
            if (cropped_images % 1000) == 0:
                print cropped_images

    print cropped_images


    cropped_filename = "pos_" + pos_dict[position] + "_" + json_path.split("/")[-1].split(".json")[0] + ".json"
    #output_filename = os.path.join(output_folder, cropped_filename)
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "json_cropped", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, cropped_filename)
    else:
        output_filename = os.path.join(folder_path, cropped_filename)
    """
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    """
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(output_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        cropped_decoded,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    print "Cropped details saved in {}".format(output_filename)

    return output_filename

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of paths to convert to window ROI json format",
        required=True)
    ap.add_argument(
        "-p", "--position",
        help="Enter position of cropping: upper-left, upper-center, upper-right, lower-left, lower-center, lower-right",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    position = args["position"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    generate_window_pos(pklot_path, json_path, position, folder_path, project_name)