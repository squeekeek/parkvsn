from sklearn.svm import LinearSVC
from sklearn.externals import joblib
from timeit import default_timer as timer

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

from skimage import exposure
from skimage import feature

from lib.generate_occ_to_neg import generate_occ_to_neg
from lib.generate_window import generate_window
from lib.generate_window_pos import generate_window_pos
from lib.extract_hog_paths import extract_hog_paths

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False

"""

python pipeline-per-category.py \
-pk "/Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/" \
-mp "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-1" \
-nc "right-1" \
-ec "right" \
-w 40 \
-ht 80 \
-pos "upper-left" \
-n test1

pklot_path = "/Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/"
main_pos_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json"
main_neg_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json"
dataset_category = "UFPR04"
category = "right-1"
neg_category = category
except_category = "right"
width = 40
height = 80
position = "upper-left"
folder_path = None
project_name = "test1"
"""


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-mp", "--main_pos_path",
        help="Main JSON file for paths for positive features",
        required=True)
    ap.add_argument(
        "-mn", "--main_neg_path",
        help="Main JSON file for paths for positive features",
        required=True)
    ap.add_argument(
        "-dc", "--dataset_category",
        help="Enter dataset category: UFPR04, UFPR05, PUC, or ALL",
        required=True
    )
    ap.add_argument(
        "-c", "--category",
        help="Enter category name: left-1, left-2, etc. or ALL",
        required=True
    )
    ap.add_argument(
        "-nc", "--neg_category",
        help="Enter category to get unoccupied data from: left-1, [...] ALL.",
        required=True
    )
    ap.add_argument(
        "-ec", "--except_category",
        help="Enter category to exclude: left, right, center, left-1, etc.",
        required=True
    )
    ap.add_argument("-w", "--width", required=True, help="Width of window size")
    ap.add_argument("-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        "-pos", "--position",
        help="Enter position to crop from: upper-left, etc.",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    main_pos_path = args["main_pos_path"]
    main_neg_path = args["main_neg_path"]
    dataset_category = args["dataset_category"]
    category = args["category"]
    neg_category = args["neg_category"]
    except_category = args["except_category"]
    width = int(args["width"])
    height = int(args["height"])
    position = args["position"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]




    # Generate initial window json

    window_path_pos = generate_window(
        pklot_path, main_pos_path, category, width, height, folder_path, project_name
        )
    window_path_neg = generate_window(
        pklot_path, main_neg_path, neg_category, width, height, folder_path, project_name
        )
    occ_neg_path = generate_occ_to_neg(main_pos_path, except_category, folder_path, project_name)
    window_path_neg_occ = generate_window(
        pklot_path, occ_neg_path, "ALL", width, height, folder_path, project_name
        )

    # Generate roi window json
    roi_path_pos = generate_window_pos(
        pklot_path, window_path_pos, position, folder_path, project_name
    )
    roi_path_neg = generate_window_pos(
        pklot_path, window_path_neg, position, folder_path, project_name
    )
    roi_path_neg_occ = generate_window_pos(
        pklot_path, window_path_neg_occ, position, folder_path, project_name
    )

    # Extract HoG
    hog_path_pos = extract_hog_paths(
        pklot_path, roi_path_pos, width, height, folder_path, project_name
    )
    hog_path_neg = extract_hog_paths(
        pklot_path, roi_path_neg, width, height, folder_path, project_name
    )
    hog_path_neg_occ = extract_hog_paths(
        pklot_path, roi_path_neg_occ, width, height, folder_path, project_name
    )

    # Generate list of python scripts to enter 
    model_name_1 = (
        "pos_{}"
        "neg_unocc_{}"
    ).format(
        category, neg_category
    )
    cmd_1 = (
        "python train_svm_mult_w_div.py \\ \n"
        "-m {} \\ \n"
        "-p1 {} \\ \n"
        "-n1 {} \\ \n"
        "-w {} \\ \n"
        "-ht {} \\ \n"
        "-n {} \n"
    ).format(
         model_name_1, hog_path_pos, hog_path_neg, width, height, project_name
    )

    model_name_2 = (
        "pos_{}"
        "neg_unocc_{}"
        "_{}"
    ).format(
        category, neg_category, except_category
    )
    cmd_2 = (
        "python train_svm_mult_w_div.py "
        "-m {} \\ \n"
        "-p1 {} \\ \n"
        "-n1 {} \\  \n"
        "-n2 {} \\ \n"
        "-w {} \\ \n"
        "-ht {} \\ \n"
        "-n {} \n\n"
    ).format(
        model_name_2, hog_path_pos, hog_path_neg, hog_path_neg_occ, width, height, project_name
    )

    results_filename = model_name_2 + ".txt"
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "cmd_train_svm_mult", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        results_path = os.path.join(data_folder, results_filename)
    else:
        results_path = os.path.join(folder_path, results_filename)

    write_file = open(results_path, 'w')
    write_file.truncate()
    str_a = "Negative folders: \n"
    print str_a
    write_file.write(str_a)

    str_a =  "\n"
    print str_a
    write_file.write(str_a)
    str_a =  "#### Cmd for occupied and unoccupied of {}:\n".format(category)
    print str_a
    write_file.write(str_a)
    str_a =  cmd_1
    print str_a
    write_file.write(str_a)
    str_a =  ""
    print str_a
    write_file.write(str_a)
    str_a =  "#### Cmd for occupied and unoccupied, and except {} of {}:\n".format(except_category, category)
    print str_a
    write_file.write(str_a)
    str_a =  cmd_2
    print str_a
    write_file.write(str_a)
    str_a =  ""
    print str_a
    write_file.write(str_a)





