from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-p', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    parser.add_argument(
        '-n', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    parser.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)

    args = vars(parser.parse_args())

    pos_feat_path = args["posfeat"]
    neg_feat_path = args["negfeat"]
    model_name = args["model_name"]



    fds = []
    labels = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(1)

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(0)

    # Perform test split
    # 60% - train, 20% - cross validation, 20% - test

    fds_train, fds_temp, labels_train, labels_temp = train_test_split(
        fds, labels, test_size=0.40, random_state=5)
    fds_test, fds_cv, labels_test, labels_cv = train_test_split(
        fds_temp, labels_temp, test_size=0.50, random_state=5)

    clf = LinearSVC(class_weight="balanced")

    print "Training a Linear SVM Classifier"
    clf.fit(fds_train, labels_train)
    # If feature directories don't exist, create them
    model_path =  model_name + '.model'
    joblib.dump(clf, model_path)
    print "Classifier saved to {}".format(model_path)

    cv_predict = clf.predict(fds_cv)
    test_predict = clf.predict(fds_test)

    len_train = len(labels_train)
    len_cv = len(labels_cv)
    len_test = len(labels_test)
    len_total = len_train + len_cv + len_test

    print_total = (
        "Features in dataset (100%):\n"
        "Count: {}\n"
    ).format(len_total)

    print_train = (
        "Features in training dataset (60%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_train, Counter(labels_train))

    print_cv = (
        "Features in cross-validation dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_cv, Counter(labels_cv))

    print_test = (
        "Features in test dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_test, Counter(labels_test))

    print print_total
    print print_train
    print print_cv
    print print_test

    target_names = [
        "Empty",
        "Occupied"
    ]

    print "For cross-validation set (20%):"
    print (classification_report(
        labels_cv, cv_predict, target_names=target_names))

    print "For test set (20%):"
    print (classification_report(
        labels_test, test_predict, target_names=target_names))