"""
    Generate a partial version of an ROI json file. 
    Need to enter percentage of paths needed.
    Note: distribution is not equal

    Example usage:
    python generate-partial-roi-json.py -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -p 0.50 -n test
"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

from sklearn.model_selection import train_test_split

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False


if __name__ == "__main__":
    # construct the argument parse and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-j', "--json_path", help="Path to json with ROI details",
        required=True)
    ap.add_argument(
        '-p', "--percentage",
        help="Percentage of paths to acquire in decimal form",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    json_path = args["json_path"]
    percentage = float(args["percentage"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    with open(json_path) as data_file:
        json_dict = json.load(data_file)

    # Put all paths in a list
    temp_list = []
    for i, key in enumerate(json_dict):
        for j, path in enumerate(json_dict[key]):
            temp_list.append(path)

    # Create dummy list for labels
    dummy_length = len(temp_list)
    temp_labels = []
    for i in range(0, dummy_length, 1):
        temp_labels.append(1)

    # Use train split to randomly select a percentage
    paths_dump, paths_actual, labels_dump, labels_dump1 = train_test_split(
        temp_list, temp_labels, test_size=percentage, random_state=5
    )

    # Using the extract list of keys, copy everything to a separate dict

    partial_count = len(paths_actual)
    count = 0
    partial_dict = {}
    for i, key in enumerate(paths_actual):
        partial_dict.update(
            {
                key: json_dict[key].copy()
            }
        )
        count = count + 1
        if (count % 1000 == 0) or (count == partial_count):
            print count

    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    whole_percent = int(percentage*100)

    # Put separate dict in a json file
    temp_filename = "partial_" + str(whole_percent) + "_" + str(partial_count) + "_" + json_path.split("/")[-1]
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "json_cropped_partitioned", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, temp_filename)
    else:
        output_filename = os.path.join(folder_path, temp_filename)

    with io.open(output_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        partial_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))
