"""
    Use this to generate list of unsorted paths.

    Same format as output of num-to-paths.py.
    Can be UFPR04, UFPR05, or PUC.

    -c, class: empty, occupied
    Uses the "Empty" or "Occupied" folder of PKLot.

    Example usage:
    python generate-unsorted.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c empty
"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05, ALL",
        required=True)
    ap.add_argument(
        '-c', "--class_type", help="Empty or Occupied",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())


    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    class_type = args["class_type"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    actual_pklot = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    print actual_pklot

    if set_option == "ALL":
        actual_set_option = "*"
    else:
        actual_set_option = set_option

    if class_type == "empty":
        neg_path = os.path.join(str(pklot_path), actual_set_option, "*", "*", "Empty", "*")
    elif class_type == "occupied":
        neg_path = os.path.join(str(pklot_path), actual_set_option, "*", "*", "Occupied", "*")
    else:
        print "Invalid class type, 'empty' or 'unoccupied' only."
        sys.exit()

    neg_dict = {
        actual_set_option: []
    }

    count = 0
    for im_path in glob.glob(neg_path):
        decoded_path = im_path.split("PKLotSegmented")[1]
        neg_dict[actual_set_option].append(decoded_path)
        count = count + 1
        if (count % 1000 == 0):
            print "Found " + str(count) 

    num_paths = len(neg_dict[actual_set_option])
    if class_type == "empty":
        temp_filename_neg = "gen_" + set_option + "_unocc_paths_" + str(num_paths) +  ".json"
    elif class_type == "occupied":
        temp_filename_neg = "gen_" + set_option + "_occ_paths_" + str(num_paths) +  ".json"

    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "paths_unsorted", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        filename_neg = os.path.join(data_folder, temp_filename_neg)
    else:
        filename_neg = os.path.join(folder_path, temp_filename_neg)

    # store in json file
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str


    print "Done with generating " + filename_neg
    print "Generated {} paths".format(len(neg_dict[actual_set_option]))
    with io.open(filename_neg, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        neg_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))