"""
    Generate json of same format as output of extract-roi.py.
    Use for the negative images.
    Ideally, for unoccupied images.
    Needs json of same format as nums-to-paths.py. 
    Needs window size and target ratio.

    Example usage:
    python generate-neg-roi-json.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j gen_UFPR04_unocc_paths_59718.json -w 30 -ht 80 -n test
    
    python generate-neg-roi-json.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_adjusted_dim/UFPR04_halfway/genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -w 30 -ht 80 -n test

"""

import argparse
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of paths to convert to negative ROI json format",
        required=True)
    ap.add_argument("-w", "--width", required=True, help="Width of window size")
    ap.add_argument("-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    #ap.add_argument("-o", "--output_folder", required=True, help="Output folder")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    width = int(args["width"])
    height = int(args["height"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    #output_folder = args["output_folder"]

    ratio = height/float(width)

    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    cropped_decoded = {}


    cropped_images = 0
    for i, key in enumerate(json_paths):
        for j, im_path in enumerate(json_paths[key]):
            # crop picture to an appropriate ratio
            decoded_path = actual_pklot_path + im_path
            orig_image = cv2.imread(decoded_path)
            rows, cols = orig_image.shape[:2]
            # adjust cropping depending on the width of orig_image
            curr_width = cols
            curr_height = rows
            if curr_width < width:
                curr_width = width 
                curr_height = int(curr_width*ratio)
            while True:
                actual_ratio = curr_height/float(curr_width)
                if ratio != curr_height/float(curr_width):
                    curr_width = int(curr_width - 1)
                    curr_height = int(curr_width*ratio)
                    if DEBUG:
                        print "not equal: w: {}, h: {}, ar: {}".format(curr_width, curr_height, actual_ratio)
                    if curr_height >= rows:
                        roi = cv2.resize(orig_image, (height, width))
                        seg_path = im_path
                        pt1 = [0, 0]
                        pt2 = [height, width]
                        dim = [roi.shape[0], roi.shape[1]]
                        window = [height, width]

                        cropped_decoded.update(
                            {
                                seg_path: {
                                    "pt1": pt1,
                                    "pt2": pt2,
                                    "dim": dim,
                                    "window": window,
                                    "ratio": actual_ratio,
                                    "ifFront": 0

                                }
                            }
                        )
                        cropped_images = cropped_images + 1
                        break
                    
                else:
                    if DEBUG:
                        print "equal: w: {}, h: {}, ar: {}".format(curr_width, curr_height, actual_ratio)
                    roi = orig_image[0:curr_height, 0:curr_width]
                    seg_path = im_path
                    pt1 = [0, 0]
                    pt2 = [curr_height, curr_width]
                    dim = [roi.shape[0], roi.shape[1]]
                    window = [height, width]
                    cropped_decoded.update(
                        {
                            seg_path: {
                                "pt1": pt1,
                                "pt2": pt2,
                                "dim": dim,
                                "window": window,
                                "ratio": actual_ratio,
                                "ifFront": 0

                            }
                        }
                    )

                    cropped_images = cropped_images + 1
                    if (cropped_images % 1000 == 0):
                        print cropped_images
                    break
            # crop image
            


        print cropped_images

        cropped_filename = "gennegroi_" + json_path.split("/")[-1].split(".json")[0] + "_" + str(cropped_images) + "_w-" + str(width) + "h-" + str(height) + ".json"
        #output_filename = os.path.join(output_folder, cropped_filename)
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "json_cropped", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, cropped_filename)
    else:
        output_filename = os.path.join(folder_path, cropped_filename)
        """
        if not os.path.isdir(output_folder):
            os.makedirs(output_folder)
        """
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(output_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        cropped_decoded,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    print "Cropped details saved in {}".format(output_filename)

