"""
    Modified from extract-roi.py 
    Use this to test window sizes for categories

    Example usage:

    python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c left-1 -j /Users/agatha/Desktop/categorize_UFPR04/data/sorted_paths/UFPR04_pos_paths.json -w 30 -ht 80 -crop n
"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, got_pt1, im_bounds, ratio, a_refPt, scaleFactor

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if len(refPt) < 2:
        if event == cv2.EVENT_LBUTTONDOWN:
            # for getting 2nd pt
            if got_pt1 == 1:
                pt1_x = int(refPt[0][0]/float(scaleFactor))
                pt1_y = int(refPt[0][1]/float(scaleFactor))
                curr_width = int(x/float(scaleFactor)) - pt1_x
                curr_height = int(y/float(scaleFactor)) - pt1_y
                new_y = int(curr_width*ratio) + pt1_y
                temp_x = int(x/float(scaleFactor))
                temp_y = new_y
                while (1):
                    test_width = temp_x - pt1_x
                    test_height = temp_y - pt1_y
                    test_ratio = test_height/float(test_width)
                    if test_ratio != ratio:
                        temp_x = temp_x - 1
                        test_width = temp_x - pt1_x
                        temp_y = int(test_width*ratio) + pt1_y
                    else:
                        break
                del refPt[:]
                refPt.append((pt1_x, pt1_y))
                refPt.append((temp_x, temp_y))
            else:
                refPt.append((x, y))
                got_pt1 = 1

        elif event == cv2.EVENT_MOUSEMOVE:
            if got_pt1 == 1:
                if (y <= im_bounds[0]) and (x <= im_bounds[1]):
                    curr_width = x - refPt[0][0]
                    curr_height = y - refPt[0][1]
                    curr_height = int(curr_width*ratio)
                    new_y = refPt[0][1] + curr_height
                    if (new_y < im_bounds[0]):
                        cv2.rectangle(image, refPt[0], (x, new_y), (0, 255, 0), 1)



if __name__ == "__main__":
    global refPt, cropping, got_pt1, im_bounds, ratio, a_refPt, scaleFactor

    refPt = []
    cropping = False
    got_pt1 = 0
    im_bounds = [0, 0]
    ratio = 0
    scaleFactor = 5


    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL",
        required=True
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to the json with paths",
        required=True)
    ap.add_argument("-w", "--width", required=True, help="Width of ref. image size")
    ap.add_argument("-ht", "--height", required=True, help="Height of ref. image size")
    ap.add_argument("-crop", "--crop", required=True, help="If you want to save cropped images-- 'y', If you won't -- 'n'")
    ap.add_argument(
        "-f", "--target_folder",
        help="Target folder"
    )
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]
    width = int(args["width"])
    height = int(args["height"])
    crop_img = args["crop"]
    continue_crop = "n"
    target_folder = args["target_folder"]

    if target_folder is None:
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        target_folder = os.path.join(dir_main, "data", "winsize")

    if not os.path.isdir(target_folder):
        os.makedirs(target_folder)


    ratio = height/float(width) # rOI will attach depending on width

    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []
    to_extract.append(category)

    if (crop_img == "y") or (crop_img == "Y"):
        # If crop directories don't exist, create them
        temp_feat_path = "winsize_" + json_path.split(".")[-2].split("/")[-1] + "_" + category
        feat_path = os.path.join(target_folder, temp_feat_path)

        if not os.path.isdir(feat_path):
            os.makedirs(feat_path)



    num_images = 0
    actual_pklot_path = pklot_path.split("PKLotSegmented")[0]  + "PKLotSegmented"

    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        num_paths_category = len(json_paths[key])
        curr_key = key
        for j, im_path in enumerate(json_paths[key]):
            # print current status
            print"{}: {}/{} ({}%)".format(curr_key, j+1, num_paths_category, ((j+1)/float(num_paths_category))*100 )
            print"keys: r = reset, c = crop, a = crop all, s = skip, q = quit"
            fd_name = os.path.split(im_path)[1].split(".")[0] + ".jpg"

            if (crop_img == "y") or (crop_img == "Y"):
                fd_path = os.path.join(feat_path, fd_name)
            if continue_crop == 'y' or continue_crop == 'Y':
                seg_path = im_path
                #seg_path = im_path.split("PKLotSegmented")[1]
                if (seg_path in cropped_decoded) or (seg_path in skipped_decoded):
                    print "Skipped " + str(seg_path)
                    continue
            decoded_path = actual_pklot_path + im_path
            #decoded_path = pklot_path + im_path
            orig_image = cv2.imread(decoded_path)

            rows, cols = orig_image.shape[:2]
            image = cv2.resize(orig_image, (scaleFactor*cols, scaleFactor*rows), interpolation=cv2.INTER_LINEAR)
            im_bounds[0] = image.shape[0]
            im_bounds[1] = image.shape[1]

            clone = image.copy()
            cv2.namedWindow(fd_name)
            # display the image and wait for a keypress
            cv2.setMouseCallback(fd_name, click_and_crop)

            # keep looping until the 'q' key is pressed
            crop = 0
            print im_path
            while True:

                cv2.imshow(fd_name, image)
                # draw a rectangle around the region of interest
                if len(refPt) == 2:
                    image = orig_image.copy()
                    cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 1)
                    cv2.imshow(fd_name, image)

                key = cv2.waitKey(1) & 0xFF

                # if the 'r' key is pressed, reset the cropping region
                if key == ord("r"):
                    print "Reset"
                    got_pt1 = 0
                    crop = 0
                    del refPt[:]
                    image = clone.copy()

                # if the 'c' key is pressed, break from the loop
                elif key == ord("c"):
                    crop = 1
                    break
                # if the "q'" key is pressed, 
                elif key == ord("q"):
                    if (crop_img == "y") or (crop_img == "Y"):
                        print "Cropped images saved in {}".format(feat_path)
                    print "Window size used:\n width: {}, height: {}".format(width, height)
                    sys.exit()
                # select all
                elif key == ord("a"):
                    del refPt[:]
                    refPt.append((0, 0))
                    pt1_x = 0
                    pt1_y = 0
                    temp_x = orig_image.shape[1] - 1
                    temp_y = orig_image.shape[0] - 1
                    while (1):
                        test_width = temp_x - pt1_x
                        test_height = temp_y - pt1_y
                        test_ratio = test_height/float(test_width)
                        if test_ratio != ratio:
                            temp_x = temp_x - 1
                            test_width = temp_x - pt1_x
                            temp_y = int(test_width*ratio) + pt1_y
                        else:
                            break
                    refPt.append((temp_x, temp_y))

                    #refPt.append((orig_image.shape[1] - 1, orig_image.shape[0] - 1))
                    got_pt1 = 1
                # skip the sample
                elif key == ord("s"):
                    print "Skip"
                    got_pt1 = 0
                    del refPt[:]
                    break
              

            # if there are two reference points, then crop the region of interest
            # from the image and display it
            if (len(refPt) == 2) and (crop == 1):
                roi = orig_image[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
                if (crop_img == "y") or (crop_img == "Y"):
                    cv2.imwrite(fd_path, roi)
                print "window width: " + str(width)
                print "window height" + str(height)
                print "roi width: "  + str(roi.shape[1])
                print "roi height: " + str(roi.shape[0])
                actual_ratio = roi.shape[0]/float(roi.shape[1])
                print "roi ratio: " + str(actual_ratio)
                print "req. ratio: " + str(ratio)
                if (actual_ratio != ratio):
                    print "--WRONG"
                else:
                    print "++RIGHT"
                # cv2.imshow("ROI", roi)
                # cv2.waitKey(0)
                pt1 = [refPt[0][0], refPt[0][1]]
                pt2 = [refPt[1][0], refPt[1][1]]
                # dim = [rows, cols]
                dim = [roi.shape[0], roi.shape[1]]
                window = [height, width]
                #seg_path = im_path.split("PKLotSegmented")[1]
                seg_path = im_path

            else:
                print "skipped image"
                # skip image
                seg_path = im_path
                #seg_path = im_path.split("PKLotSegmented")[1]
              
            # close all open windows
            cv2.destroyAllWindows()

            num_images = num_images + 1

            if num_images % 1000 == 0:
                print num_images

    if (crop_img == "y") or (crop_img == "Y"):
        print "Cropped images saved in {}".format(feat_path)
    print "Window size used:\n width: {}, height: {}".format(width, height)


