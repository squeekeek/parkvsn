# Categorizing and extracting ROI from PKlot dataset

[USAGE]: Copy scripts to test/categorization/experiment folder.

## Steps:
1. Sort parking lot spaces into categories of similar views with `sort-nums.py`
    - Use `view-sorted.py` to review your sorted images. The images will be stored in
    their own folders within the directory of the python script.
2. Use `nums-to-paths.py` to have JSON files of the paths for each category.
3. Use `sizes.py` to view the image dimensions of each category, and then make a decision on what window size to use for each category or all categories.
    - Window size should be a clean number like 50x40, 60x45 etc. 
    - Multiple of 5 or 10. 10 is more preferable. 
    - A similar window size for all categories is preferrable, but if not possible, then that's ok.
    - Store window size decided on for each category in a text file for future reference, along with other info sizes (or just copy-paste from output of sizes.py) if preferred.
        - Ex: windowsize_UFPR04.txt
4. You can use `winsize.py` (a modified version of `extract-roi.py`), to test your window sizes.
4. Use `extract-roi.py` to extract the ROI from each category.



*Additional (#TODO haven't coded these yet):*
- [ ] Use view-roi.py to view extracted ROIs
- [X] Use view-sorted.py to view sorted parking lots

-------------------------------------------------------------------------------

## sort-nums.py
### Description:
Use this to sort the parking lot spaces into similar views

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05
```
### Example usage:
```
python sort-nums.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04
```
### How-to-use:
1. Upon initialization of windows, arrange category windows for visibility.
2. To start categorizing, make sure that the window containing a new image is selected.
3. Then you may click the buttons 1-9 to categorize, 0 to skip categorizing if unsure, 
and q to exit the program. 
4. If you skip categorizing, the categorized values will be stored in this format:
"skipped_" +  set_option + "_sort_nums.json"
5. You must categorize even the skipped values to proceed.
6. After you categoried everything, the skipped JSON file will be deleted, and replaced by:
set_option + "_sort_nums.json"

-------------------------------------------------------------------------------

## view-sorted.py
### Description
Save all sorted images with sort-nums.py into distinct folders for viewing.

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05
  -j JSON_PATH, --json_path JSON_PATH
                        JSON file of sorted numbers to load
```

### Example usage:
```
python view-sorted.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -j UFPR04_sort_nums.json
```

-------------------------------------------------------------------------------

## nums-to-paths.py
### Description:
Outputs JSON files to positive and negative paths, categorized according 
to the JSON file of sorted numbers with sort-nums.py

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05, ALL
  -j JSON_PATH, --json_path JSON_PATH
                        Path to list of numbers to parse
```
### Example usage:
```
python nums-to-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -j UFPR04_sort_nums.py
```

-------------------------------------------------------------------------------

## sizes.py
### Description:
- Use this to have info on the minimum, maximum and average dimensions of each category, or all categories.
- Use this with the JSON file containing positive/occupied lots.

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05
  -c CATEGORY, --category CATEGORY
                        Enter category name or ALL
  -j JSON_PATH, --json_path JSON_PATH
                        Path to list of paths
```
### Example usage:
```
python sizes.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c complete-front-center -j UFPR04_pos_paths.json
```
### Suggested usage
To store results every time you run the script, pipe results to a text file:
```
python sizes.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c complete-front-center -j UFPR04_pos_paths.json > UFPR04_complete-front-center.txt
```

-------------------------------------------------------------------------------

## winsize.py
### Description:
Modified from extract-roi.py 
Use this to test window sizes for categories.

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05
  -c CATEGORY, --category CATEGORY
                        Enter category name or ALL
  -j JSON_PATH, --json_path JSON_PATH
                        Path to the json with paths
  -w WIDTH, --width WIDTH
                        Width of ref. image size
  -ht HEIGHT, --height HEIGHT
                        Height of ref. image size
  -crop CROP, --crop CROP
                        If you want to save cropped images-- 'y', If you won't
                        -- 'n'
```
### Example Usage:
```
python winsize.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c complete-front-center -j UFPR04_pos_paths.json -w 40 -ht 50 -crop n
```

### How-to-use:
1. A window will appear containg a 5x magnified image of the image to be cropped.
2. Click the top-left point of the ROI, then click the bottom-right point of the ROI.
3. You could also press `a` to select the entire image.
4. The rectangle would automatically adjust for the needed ratio of the window size
as you move your mouse or for your marked region.
5. Upon clicking the bottom-right corner of the ROI image will then resize to its original size,
showing the final rectangle that would mark its ROI.
6. You could press `c` to "crop" the image as is or `r` to reset and redo the ROI.
7. If you think the image does not contain a good test set, you can 
press `s` to skip the image entirely for marking.
8. To quit the program, press `q`. 


-------------------------------------------------------------------------------


## extract-roi.py
### Description:
Use this to extract ROI from each sorted category.
Images are cropped automatically for viewing in a folder.
Data for cropping is stored in a JSON file.

### Arguments:
```
  -h, --help            show this help message and exit
  -pk PKLOT_PATH, --pklot_path PKLOT_PATH
                        Path to PkLot dataset
  -s SET_OPTION, --set_option SET_OPTION
                        Data set to use: PUC, UFPR04, UFPR05
  -c CATEGORY, --category CATEGORY
                        Enter category name or ALL
  -j JSON_PATH, --json_path JSON_PATH
                        Path to the json with paths
  -w WIDTH, --width WIDTH
                        Width of ref. image size
  -ht HEIGHT, --height HEIGHT
                        Height of ref. image size
  -cont CONTINUE_CROP, --continue_crop CONTINUE_CROP
                        If you will continue extracting ROI, and you have a
                        saved JSON file from before -- 'y', If you won't --
                        'n'

```
### Example usage:
```
python extract-roi.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c complete-front-center -j UFPR04_pos_paths.json -w 40 -ht 50 -cont n
```

### How-to-use:
1. A window will appear containg a 5x magnified image of the image to be cropped.
2. Click the top-left point of the ROI, then click the bottom-right point of the ROI.
3. You could also press `a` to select the entire image.
4. The rectangle would automatically adjust for the needed ratio of the window size
as you move your mouse or for your marked region.
5. Upon clicking the bottom-right corner of the ROI image will then resize to its original size,
showing the final rectangle that would mark its ROI.
6. Press `r` if you want to reset and redo the ROI.
7. If you want to confirm your extraction, press "f", if it features the front of the car, and "b" if it features the back of the car.
  - for sideview, press "f" if the front of the car is pointing downards, and "b" if the car is pointing upwards.
8. If you think the image does not contain a good test set, you can 
press `s` to skip the image entirely for marking.
9. To quit the program, press `q`. 
10. Your current marked ROIs are saved automatically everytime in
JSON files within the folder of your program regardless if you press `q` or not. 
11. If you chose to store cropped images, with `-crop y` a folder containg the cropped images would be in the same folder.
12. If you choose to not finish cropping everything upon quitting, a json file with "skipped" appended at the start of the file would be created. This would contain your past crop data. The program would find this json file if you choose to continue with the argument `-cont y` upon starting the program. If you choose to not continue, or to restart with `-cont n`, this file would be overwritten.

