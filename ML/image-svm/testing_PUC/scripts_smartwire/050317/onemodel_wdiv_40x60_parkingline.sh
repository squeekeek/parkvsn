python /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/src/_eval_scripts/pipeline_eval_all.py \
-d /home/agatha/Documents/datasets/EEEI/eeei_5th_solar_041017_a_morning \
-j /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/raw/eeei_5th_solar_a_morning_parkinglinebased.json  \
-m /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/marked_eeei_5th_solar_041017_a_morning_parkinglinebased.json \
-i y \
-n om_wdiv_w40_h60_parkingline \
-mf /home/agatha/Documents/parkvsn/ML/image-svm/testing_PUC/models/one-view-against-all_except-major \
-mj /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models/one-view-against-all_except-major/models.json