"""
Click the upper left and the lower right of the region of interest to
get the coordinates of the image. 

Crop images and store in a subfolder called "cropped"
"""

# USAGE
# python click_and_crop.py --image jurassic_park_kitchen.jpg

# import the necessary packages
import argparse
import cv2

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
got_pt1 = 0
got_pt2 = 0

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping, got_pt1, got_pt2

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if len(refPt) < 2:
        if event == cv2.EVENT_LBUTTONDOWN:
            cropping = True

        # check to see if the left mouse button was released
        elif event == cv2.EVENT_LBUTTONUP:
            # record the ending (x, y) coordinates and indicate that
            # the cropping operation is finished
            refPt.append((x, y))
            cropping = False



# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

# load the image, clone it, and setup the mouse callback function
image = cv2.imread(args["image"])
clone = image.copy()
cv2.namedWindow("image")
# display the image and wait for a keypress
cv2.setMouseCallback("image", click_and_crop)

# keep looping until the 'q' key is pressed
while True:

    cv2.imshow("image", image)
        # draw a rectangle around the region of interest
    if len(refPt) == 2:
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 1)
        cv2.imshow("image", image)

    key = cv2.waitKey(1) & 0xFF

    # if the 'r' key is pressed, reset the cropping region
    if key == ord("r"):
        print "Reset"
        got_pt1 = 0
        got_pt2 = 0
        refPt = []
        image = clone.copy()

    # if the 'c' key is pressed, break from the loop
    elif key == ord("c"):
        break

# if there are two reference points, then crop the region of interest
# from the image and display it
if len(refPt) == 2:
    roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
    cv2.imshow("ROI", roi)
    cv2.waitKey(0)

# close all open windows
cv2.destroyAllWindows()