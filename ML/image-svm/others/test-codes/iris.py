from sklearn import datasets 
iris = datasets.load_iris()
X_i, y_i = iris.data, iris.target

from sklearn.svm import SVC 
from sklearn.cross_validation import cross_val_score
import numpy as np
h_class = SVC(kernel='rbf', C=1.0, gamma=0.7, random_state=101)
scores = cross_val_score(h_class, X_i, y_i, cv=20, scoring='accuracy')
print 'Accuracy: %0.3f' % np.mean(scores)

h_class.fit(X_i, y_i)
print h_class.support_

