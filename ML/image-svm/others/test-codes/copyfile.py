import shutil
import os


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)

if __name__ == "__main__":

    file_path = "test"
    if not os.path.isdir(file_path):
        os.makedirs(file_path)
        print "made file path"

    copyFile("/Users/agatha/Documents/figure_1.png", "test/test.png")