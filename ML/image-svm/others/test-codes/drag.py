"""
Click the upper left and the lower right of the region of interest to
get the coordinates of the image. 

Crop images and store in a subfolder called "cropped"
"""

# USAGE
# python click_and_crop.py --image jurassic_park_kitchen.jpg

# import the necessary packages
import argparse
import cv2
import sys

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not



def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, got_pt1, im_bounds, ratio

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if len(refPt) < 2:
        if event == cv2.EVENT_LBUTTONDOWN:
            print "in event_button down"
            if got_pt1 == 1:
                print "in got_pt1 == 1"
                curr_width = x - refPt[0][0]
                curr_height = y - refPt[0][1]
                new_y = int(curr_width*ratio) + refPt[0][1]
                temp_x = x
                temp_y = new_y
                while (1):
                    print "in loop"
                    test_width = temp_x - refPt[0][0]
                    test_height = temp_y - refPt[0][1]
                    test_ratio = test_height/float(test_width)
                    if test_ratio != ratio:
                        print "test_ratio: {}, ratio: {}".format(test_ratio, ratio)
                        temp_x = temp_x - 1
                        test_width = temp_x - refPt[0][0]
                        temp_y = int(test_width*ratio) + refPt[0][1]
                    else:
                        print "break out of loop"
                        break

                refPt.append((temp_x, temp_y))
            else:
                print "in got_pt1 = 0"
                refPt.append((x, y))
                print refPt
                print refPt[0]
                print refPt[0][0]
                print refPt[0][1]
                got_pt1 = 1
                # sys.exit()
          

        elif event == cv2.EVENT_MOUSEMOVE:
           
            if got_pt1 == 1:
                print "in  mouse move"
                if (y <= im_bounds[0]) and (x <= im_bounds[1]):
                    print "within bounds"
                    print "len(refPt): {}".format(len(refPt))
                    print "x: {}, y: {}".format(x, y)
                    print "refPt[0][0]: {}".format(refPt[0][0])
                    print "refPt[0][1]: {}".format(refPt[0][1])
                    curr_width = x - refPt[0][0]
                    
                    curr_height = y - refPt[0][1]
                    curr_height= int(curr_width*ratio)
                    new_y = refPt[0][1] + curr_height
                    if (new_y < im_bounds[0]):
                        print "curr_height < im_bounds[0]"
                        print "new_y: {}".format(new_y)
                        print "NEW RECT"
                        cv2.rectangle(image, refPt[0], (x, new_y), (0, 255, 0), 1)

        elif event == cv2.EVENT_LBUTTONUP:
            # check to see if the left mouse button was released
            # record the ending (x, y) coordinates and indicate that
            # the cropping operation is finished
            print "in mouse up"


if __name__ == "__main__":
    # construct the argument parser and parse the arguments
    global refPt, got_pt1, im_bounds, ratio
    refPt = []
    got_pt1 = 0
    im_bounds = [0, 0]
    ratio = 0

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True, help="Path to the image")
    ap.add_argument("-w", "--width", required=True, help="Width of ref. image size")
    ap.add_argument("-ht", "--height", required=True, help="Height of ref. image size")
    args = vars(ap.parse_args())

    # load the image, clone it, and setup the mouse callback function
    image = cv2.imread(args["image"])
    width = int(args["width"])
    height = int(args["height"])
    ratio = height/float(width) # rOI will attach depending on width

    im_bounds[0] = image.shape[0]
    im_bounds[1] = image.shape[1]
    clone = image.copy()
    cv2.namedWindow("image")
    # display the image and wait for a keypress
    cv2.setMouseCallback("image", click_and_crop)

    # keep looping until the 'q' key is pressed
    while True:

        cv2.imshow("image", image)
        # draw a rectangle around the region of interest
        if len(refPt) == 2:
            image = clone.copy()
            cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 1)
            cv2.imshow("image", image)

        key = cv2.waitKey(1) & 0xFF

        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            print "Reset"
            got_pt1 = 0
            got_pt2 = 0
            refPt = []
            image = clone.copy()

        # if the 'c' key is pressed, break from the loop
        elif key == ord("c"):
            break

    # if there are two reference points, then crop the region of interest
    # from the image and display it
    if len(refPt) == 2:
        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        print "roi width: "  + str(roi.shape[1])
        print "roi height: " + str(roi.shape[0])
        actual_ratio = roi.shape[0]/float(roi.shape[1])
        print "roi ratio: " + str(actual_ratio)
        print "req. ratio: " + str(ratio)
        if (actual_ratio != ratio):
            print "--WRONG"
        else:
            print "++RIGHT"
        #cv2.imshow("ROI", roi)
        #cv2.waitKey(0)

    # close all open windows
    cv2.destroyAllWindows()