import json 
import os
import io

# load json dict
json_path = "test_update.json"
if os.path.exists(json_path):
    with open(json_path) as data_file:
        json_decoded = json.load(data_file)
else:
    json_decoded = {}

test= "testing_string-2"
dim = [0, 1]
json_decoded.update({test: {"pt1": dim }})
print "json_decoded: " + str(json_decoded)

# store in json file
try:
    to_unicode = unicode
except NameError:
    to_unicode = str

with io.open(json_path, 'w', encoding='utf8') as outfile:
    str_ = json.dumps(
                    json_decoded,
                    indent=4, sort_keys=True,
                    separators=(',', ':'), ensure_ascii=False)
    outfile.write(to_unicode(str_))