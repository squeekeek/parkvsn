# Find the number of pos and neg images for Pklot dataset

import argparse 
import glob
import os
import json
import io
import cv2
import sys

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05, ALL",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL"
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to list of numbers to parse",
        required=True)
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]

    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []

    if category == "ALL":
        to_extract = json_paths.keys()
    else:
        to_extract.append(category)

    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        for j, im_path in enumerate(json_paths[key]):
            # do something
            try:
                print im_path
                image = cv2.imread(im_path)
                cv2.imshow(im_path, image)
                key = cv2.waitKey(0)
                cv2.destroyWindow(im_path)
                if ord('0') == key:
                    sys.exit()
            except KeyboardInterrupt, SystemExit:
                sys.exit()
                raise
"""
python load-paths-template.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented -c complete-front-center -s UFPR04 -j UFPR04_pos_paths.json
"""