
/* JSON for storing cropped sizes of image paths */
{
    "image-path": {
        "pt1": [x, y],
        "pt2": [x, y],
        "dim": [cols, rows],
        "window": [cols, rows],
        "ratio": 1.5
    },
    "image-path-2": {
        "pt1": [x, y],
        "pt2": [x, y],
        "dim": [cols, rows],
         "window": [cols, rows],
        "ratio": 1.5
    }
}

/* JSON for storing skipped images */
{
    "image-path": {
        "set": "PUC",
        "category": "category"
    }
}


{
    "set-name": [
        "image-path",
        "image-path"
    ]
}


