# Analyze the sizes of pos and neg images for Pklot dataset
# Get minimum, maximum, and average size

import argparse 
import glob
import os
import cv2


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument('-pk', "--pklot_path", help="Path to PkLot dataset", required=True)
    ap.add_argument("-s", "--set_option", help="Data set to use: PUC, UFPR04, UFPR05, ALL", default="ALL")
    args = vars(ap.parse_args())

    # Parsing pathnames to datasets
    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    if set_option == "ALL":
        neg_path = str(pklot_path) + "/*/*/*/Empty/*"
        pos_path = str(pklot_path) + "/*/*/*/Occupied/*"
        #neg_path = os.path.join(pklot_path, "/*/*/*/Empty/*")
        #pos_path = os.path.join(pklot_path, "/*/*/*/Occupied/*")
    else:
        neg_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Empty/*"
        pos_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Occupied/*"

    print neg_path
    print pos_path
    neg_num = 0
    pos_num = 0
    min_neg_dim = [float('Inf'), float('Inf')]
    min_neg_path = ""

    min_pos_dim = [float('Inf'), float('Inf')]
    min_pos_path = ""

    max_neg_dim = [0,0]
    max_neg_path = ""

    max_pos_dim = [0,0]
    max_pos_path = ""

    ave_neg_dim = [0,0]
    ave_pos_dim = [0, 0]

    # Parsing
    for im_path in glob.glob(neg_path):
        image = cv2.imread(im_path)
        width = image.shape[1]
        height = image.shape[0]
        dim = width*height
        if dim < (min_neg_dim[0]*min_neg_dim[1]):
            min_neg_dim[0] = height
            min_neg_dim[1] = width
            min_neg_path = im_path
        if dim > (max_neg_dim[0]*max_neg_dim[1]):
            max_neg_dim[0] = height
            max_neg_dim[1] = width
            max_neg_path = im_path
        ave_neg_dim[0] = ave_neg_dim[0] + height
        ave_neg_dim[1] = ave_neg_dim[1] + width

        neg_num = neg_num + 1
        print "Neg: " + str(neg_num)

    ave_neg_dim[0] = ave_neg_dim[0]/neg_num
    ave_neg_dim[1] = ave_neg_dim[1]/neg_num

   
    for im_path in glob.glob(pos_path):
        image = cv2.imread(im_path)
        width = image.shape[1]
        height = image.shape[0]
        dim = width*height
        if dim < (min_pos_dim[0]*min_pos_dim[1]):
            min_pos_dim[0] = height
            min_pos_dim[1] = width
            min_pos_path = im_path
        if dim > (max_pos_dim[0]*max_pos_dim[1]):
            max_pos_dim[0] = height
            max_pos_dim[1] = width
            max_pos_path = im_path
        ave_pos_dim[0] = ave_pos_dim[0] + height
        ave_pos_dim[1] = ave_pos_dim[1] + width

        pos_num = pos_num + 1
        print "Pos: " + str(pos_num)

    ave_pos_dim[0] = ave_pos_dim[0]/pos_num
    ave_pos_dim[1] = ave_pos_dim[1]/pos_num

    print "Option chosen: " + str(set_option)
    print "Number of Neg images: " + str(neg_num)
    print "Number of Pos images: " + str(pos_num)
    print ""
    print "Neg Images Dimensions:"
    print ("Min. Dim: [{}, {}]").format(min_neg_dim[0], min_neg_dim[1])
    print ("Min Path: {}").format(min_neg_path)
    print ("Max. Dim: [{}, {}]").format(max_neg_dim[0], max_neg_dim[1])
    print ("Max Path: {}").format(max_neg_path)
    print ("Ave. Dim: [{}, {}]").format(ave_neg_dim[0], ave_neg_dim[1])
    print ""
    print "Pos Images Dimensions:"
    print ("Min. Dim: [{}, {}]").format(min_pos_dim[0], min_pos_dim[1])
    print ("Min Path: {}").format(min_pos_path)
    print ("Max. Dim: [{}, {}]").format(max_pos_dim[0], max_pos_dim[1])
    print ("Max Path: {}").format(max_pos_path)
    print ("Ave. Dim: [{}, {}]").format(ave_pos_dim[0], ave_pos_dim[1])
    print ""
