from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL",
        required=True
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to list of numbers to parse",
        required=True)
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]

    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []

    if category == "ALL":
        to_extract = json_paths.keys()
    else:
        to_extract.append(category)

    # If feature directories don't exist, create them
    feat_path = "./feats_" + json_path.split(".")[-2].split("/")[-1] + "_" + category
    #feat_path = "./feats" + "/" + 

    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)

    # instance variables
    num_images = 0
    # window_size = (80, 39)
    # UFPR04: 
    # window_size = (50, 40)
    # PUC
    window_size = (55, 36)
    orientations = 9
    pixels_per_cell = [4,4]
    cells_per_block = [4,4]
    transform_sqrt = True

    # Check if window size is odd or even for cropping
    if window_size[0] % 2 == 1:
        add_y2 = window_size[0]/2
        add_y1 = window_size[0]/2 
    else:
        add_y2 = window_size[0]/2
        add_y1 = window_size[0]/2 - 1

    if window_size[1] % 2 == 1:
        add_x2 = window_size[1]/2
        add_x1 = window_size[1]/2 
    else:
        add_x2 = window_size[1]/2
        add_x1 = window_size[1]/2 - 1

    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        for j, im_path in enumerate(json_paths[key]):
            # do something
            image = cv2.imread(im_path)
            if DEBUG:
                cv2.imshow("Original", image)

            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            if DEBUG:
                cv2.imshow("Grayed", gray)
            # cv2.imshow(im_path, image)
            # key = cv2.waitKey(0)
            # cv2.destroyWindow(im_path)
            # if ord('0') == key:
            #    sys.exit()

            # Have a threshold for the size of the image compared to window size
            # Resize image if it exceeds the threshold
            # assume 20% threshold for now
            width = gray.shape[1]
            height = gray.shape[0]
            if height > (window_size[0] + (0.2)*window_size[0]):
                new_height = window_size[0]
                ratio = width/float(height)
                new_width = int(ratio*new_height)
                dim = (new_height, new_width)
                gray = cv2.resize(gray, dim, interpolation=cv2.INTER_AREA)

            # If the image is smaller than the bounding rectangle,
            # resize the image to a larger scale,
            # and then crop appropriately

            # print "gray size: y: {} x:{}".format(gray.shape[0], gray.shape[1])
            counter_resize = 0
            while (1):
                width = gray.shape[1]
                height = gray.shape[0]
                if height < window_size[0]:
                    new_height = window_size[0] + counter_resize
                    ratio = width/float(height)
                    new_width = int(ratio*new_height)
                    dim = (new_height, new_width)
                    gray = cv2.resize(gray, dim, interpolation=cv2.INTER_AREA)
                elif width < window_size[1]:
                    new_width = window_size[1] + counter_resize
                    ratio = height/width
                    new_height = ratio*new_width
                    dim = (new_height, new_width)
                    gray = cv2.resize(gray, dim, interpolation=cv2.INTER_AREA)
                else:
                    break
                counter_resize = counter_resize + 1


            # find bounding rectangle
            # see which region it is, divide the image into 4 quarters
            # then crop the image from the corner of the quarter it is in
            blurred = cv2.GaussianBlur(gray, (5, 5), 0)
            thresh = cv2.adaptiveThreshold(
                blurred, 255,
                cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 3)
            if DEBUG:
                cv2.imshow("Gaussian Thresh", thresh)

            # find contours in the edge map, keeping only the largest one
            # is presumed to be the car
            edged = imutils.auto_canny(thresh)
            #edged1 = imutils.auto_canny(blurred)

            (_, cnts, _) = cv2.findContours(
                edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            """
            (_, cnts1, _) = cv2.findContours(
                edged1.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            """

            c = max(cnts, key=cv2.contourArea)

            # c1 = max(cnts1, key=cv2.contourArea)

            if DEBUG:
                img1 = gray.copy()
                img2 = gray.copy()
                img3 = gray.copy()
                img4 = gray.copy()
                cv2.drawContours(img1, cnts, -1, (255,0,0), 3)
                cv2.imshow("Contours from thresh", img1)
                #cv2.drawContours(img2, cnts1, -1, (255,0,0), 3)
                cv2.imshow("Contours from original", img2)
                cv2.drawContours(img3, c, -1, (255,0,0), 3)
                cv2.imshow("Max contour from thresh", img3)
                #cv2.drawContours(img4, c1, -1, (255,0,0), 3)
                cv2.imshow("Max contour from original", img4)
            # Create bounding rectangle
            (x, y, w, h) = cv2.boundingRect(c)
            mid_x = (w/2) + x
            mid_y = (h/2) + y

            new_y2 = mid_y + add_y2 
            new_x2 = mid_x + add_x2 
            new_y1 = mid_y - add_y1
            new_x1 = mid_x - add_x1
            if DEBUG:
                print "The first values before while loop"
                print "new_x1: {}, new_y1: {}, new_x2: {}, new_y2: {}".format(
                    new_x1, new_y1, new_x2, new_y2)
            # Get midpoint
            while (1):
                #check if any boundary exceeded limits
                # too far to the left, so move to the right
                if DEBUG:
                    print "In while loop"
                    print "new_x1: {}, new_y1: {}, new_x2: {}, new_y2: {}".format(
                        new_x1, new_y1, new_x2, new_y2)    
                if new_x1 < 0:
                    extra = 0 - new_x1
                    new_x2 = new_x2 + extra
                    new_x1 = 0
                    continue

                # too far upwards, so move downards
                elif new_y1 < 0:
                    extra = 0 - new_y1
                    new_y2 = new_y2 + extra
                    new_y1 = 0
                    continue

                # to far to the right, so move to the left
                elif new_x2 >= gray.shape[1]:
                    extra = 1 + new_x2 - gray.shape[1]
                    new_x1 = new_x1 - extra
                    new_x2 = gray.shape[1] - 1
                    continue

                # too far downwards, so move upwrds
                elif new_y2 >= gray.shape[0]:
                    extra = 1 + new_y2 - gray.shape[0]
                    new_y1 = new_y1 - extra
                    new_y2 = gray.shape[0] - 1
                    continue

                else:
                    break

                """
                print "In while loop"
                if new_x1 < 0:
                    new_x1 = new_x1 + 1
                    new_x2 = new_x2 + 1
                    continue

                # too far upwards, so move downards
                elif new_y1 < 0:
                    new_y1 = new_y1 + 1
                    new_y2 = new_y2 + 1
                    continue
                # to far to the right, so move to the left
                elif new_y2 > window_size[0]:
                    new_y2 = new_y2 - 1
                    new_y1 = new_y1 - 1
                    continue

                # too far downwards, so move upwrds
                elif new_x2 > window_size[1]:
                    new_x1 = new_x1 - 1
                    new_x2 = new_x2 - 1
                    continue
                else:
                    break
                """
                if DEBUG:
                    print "in while loop"

            if DEBUG:
                print "add_x1: {}, add_x2: {}, add_y1: {}, add_y2: {}".format(
                    add_x1, add_x2, add_y1, add_y2)
                print "mid_x: {}, mid_y: {}".format(mid_x, mid_y)
                print "x: {}, y: {}, w: {}, h: {}".format(x, y, w, h)
                print "new_x1: {}, new_y1: {}, new_x2: {}, new_y2: {}".format(
                    new_x1, new_y1, new_x2, new_y2)

            # Crop image
            cropped = gray[new_y1:(new_y2+1), new_x1:(new_x2+1)]

            if DEBUG:
                print "Num_images " + str(num_images)
                cv2.imshow("Cropped", cropped)

            # print "cropped size: y:{} x:{}".format(cropped.shape[0], cropped.shape[1])

            fd = feature.hog(
                cropped, 
                orientations=orientations,
                pixels_per_cell=pixels_per_cell,
                cells_per_block=cells_per_block,
                transform_sqrt=transform_sqrt,
                visualise=False)
            """
            hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
            hogImage = hogImage.astype("uint8")
            cv2.imshow("HOG Image", hogImage)
            key = cv2.waitKey(0)
            if key == ord('0'):
                sys.exit()
            else:
                cv2.destroyAllWindows
            """

            fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
            fd_path = os.path.join(feat_path, fd_name)
            joblib.dump(fd, fd_path)

            if (cropped.shape[0] != window_size[0] or cropped.shape[1] != window_size[1]):
                print "error in shape size"
                sys.exit()
            num_images = num_images + 1
            if (num_images%1000 == 0):
                print num_images
    print "Features saved in {}".format(feat_path)


