Todos
* Test Exemplar SVMs
* Create script for generating list of all unoccupied paths
* Create script for generating list of occupied paths
    * copy paste results of findpaths.py instead and manually do so.
* Get one sample of a category as input

Script for testing exemplar SVM
Inputs
- pklot dataset path
- json of list of all unoccupied paths
- json list of occupied paths that are negative
- json of image to input (just put one image, and then try to see what'll happen if others are put in)


