from imutils import paths
import argparse
import imutils
import cv2
import os
import glob

from config import *

# Load images
if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('-pk', "--pklot_path", help="Path to PkLot dataset", required=True)
    ap.add_argument("-s", "--set_option", help="Data set to use: PUC, UFPR04, UFPR05, ALL", default="ALL")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    if set_option == "ALL":
        neg_path = str(pklot_path) + "/*/*/*/Empty/*"
        pos_path = str(pklot_path) + "/*/*/*/Occupied/*"
        #neg_path = os.path.join(pklot_path, "/*/*/*/Empty/*")
        #pos_path = os.path.join(pklot_path, "/*/*/*/Occupied/*")
    else:
        neg_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Empty/*"
        pos_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Occupied/*"

    print neg_path
    print pos_path

    pos_img_path ="../images/cropped_pos"

    # If feature directories don't exist, create them
    if not os.path.isdir(pos_img_path):
        os.makedirs(pos_img_path)

    neg_num = 0
    pos_num = 0

    for im_path in glob.glob(pos_path):
        pos_num = pos_num + 1

        # load the image, convert it to grayscale, and detect edges
        image = cv2.imread(im_path)
        cv2.imshow("Image", image)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        cv2.imshow("Grayed", gray)
       

        # Get threshold of image
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        thresh = cv2.adaptiveThreshold(blurred, 255, 
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 3)
        cv2.imshow("Gaussian Thresh", thresh)
        """
        thresh = cv2.adaptiveThreshold(blurred, 255, 
            cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)
        cv2.imshow("Mean Thresh", thresh)
        """
        # find contours in the edge map, keeping only the largest one
        # is presumed to be the car logo
        edged = imutils.auto_canny(thresh)
        (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        c = max(cnts, key=cv2.contourArea)

        # Create bounding rectangle
        (x, y, w, h) = cv2.boundingRect(c)
        obj = gray[y:y + h, x:x + w]

        # Save copies of cropped rectangle images
        cv2.imshow("Cropped", obj)
        cv2.waitKey(0)

        fd_name = os.path.split(im_path)[1]
        print "fd_name: " + str(fd_name)
        fd_path = os.path.join(pos_img_path, fd_name)
        print "fd_path: " + str(fd_path)
        cv2.imwrite(fd_path, obj)
        if pos_num == 3:
            break

    print "num of positive images: " + str(pos_num)