# Find the number of pos and neg images for Pklot dataset

import argparse 
import glob
import os
#from config import *




if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument('-pk', "--pklot_path", help="Path to PkLot dataset", required=True)

    args = vars(ap.parse_args())

    
    pklot_path = args["pklot_path"]
    set_option = "PUC"
    if set_option == "ALL":
        neg_path = str(pklot_path) + "/*/*/*/Empty/*"
        pos_path = str(pklot_path) + "/*/*/*/Occupied/*"
        #neg_path = os.path.join(pklot_path, "/*/*/*/Empty/*")
        #pos_path = os.path.join(pklot_path, "/*/*/*/Occupied/*")
    else:
        neg_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Empty/*"
        pos_path = str(pklot_path) + "/" + str(set_option) + "/*/*/Occupied/*"
        

    print neg_path
    print pos_path
    neg_num = 0
    pos_num = 0

    for im_path in glob.glob(pos_path):
        pos_num = pos_num + 1
        
    print "Option chosen: " + str(set_option)
    print "Number of Pos images: " + str(pos_num)