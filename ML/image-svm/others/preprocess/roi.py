"""
Extract ROI. 
Will crop images and store into a folder.
*expound details later*
"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, got_pt1, im_bounds, ratio, a_refPt, scaleFactor

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if len(refPt) < 2:
        if event == cv2.EVENT_LBUTTONDOWN:
            print "in event_button down"
            # for getting 2nd pt
            if got_pt1 == 1:
                print "in got_pt1 == 1"
                pt1_x = int(refPt[0][0]/float(scaleFactor))
                pt1_y = int(refPt[0][1]/float(scaleFactor))
                curr_width = int(x/float(scaleFactor)) - pt1_x
                curr_height = int(y/float(scaleFactor)) - pt1_y
                new_y = int(curr_width*ratio) + pt1_y
                temp_x = int(x/float(scaleFactor))
                temp_y = new_y
                while (1):
                    print "in loop"
                    test_width = temp_x - pt1_x
                    test_height = temp_y - pt1_y
                    test_ratio = test_height/float(test_width)
                    if test_ratio != ratio:
                        print "test_ratio: {}, ratio: {}".format(test_ratio, ratio)
                        temp_x = temp_x - 1
                        test_width = temp_x - pt1_x
                        temp_y = int(test_width*ratio) + pt1_y
                    else:
                        print "break out of loop"
                        break

                refPt = []
                refPt.append((pt1_x, pt1_y))
                refPt.append((temp_x, temp_y))
            else:
                print "in got_pt1 = 0"
                refPt.append((x, y))
                print refPt
                print refPt[0]
                print refPt[0][0]
                print refPt[0][1]
                got_pt1 = 1
                # sys.exit()

        elif event == cv2.EVENT_MOUSEMOVE:
            print "x: {}, y: {}".format(x, y)
            print "im_bounds[0]: {}, im_bounds[1]: {}".format(im_bounds[0], im_bounds[1])
            if got_pt1 == 1:
                print "in  mouse move"
                if (y <= im_bounds[0]) and (x <= im_bounds[1]):
                    print "within bounds"
                    print "len(refPt): {}".format(len(refPt))
                    print "x: {}, y: {}".format(x, y)
                    print "refPt[0][0]: {}".format(refPt[0][0])
                    print "refPt[0][1]: {}".format(refPt[0][1])
                    curr_width = x - refPt[0][0]
                    curr_height = y - refPt[0][1]
                    curr_height = int(curr_width*ratio)
                    new_y = refPt[0][1] + curr_height
                    if (new_y < im_bounds[0]):
                        print "curr_height < im_bounds[0]"
                        print "new_y: {}".format(new_y)
                        print "NEW RECT"
                        cv2.rectangle(image, refPt[0], (x, new_y), (0, 255, 0), 1)

        elif event == cv2.EVENT_LBUTTONUP:
            # check to see if the left mouse button was released
            # record the ending (x, y) coordinates and indicate that
            # the cropping operation is finished
            print "in mouse up"




if __name__ == "__main__":
    global refPt, cropping, got_pt1, im_bounds, ratio, a_refPt, scaleFactor

    refPt = []
    cropping = False
    got_pt1 = 0
    im_bounds = [0, 0]
    ratio = 0

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name or ALL",
        required=True
    )
    ap.add_argument(
        '-j', "--json_path", help="Path to the json with paths",
        required=True)
    ap.add_argument("-w", "--width", required=True, help="Width of ref. image size")
    ap.add_argument("-ht", "--height", required=True, help="Height of ref. image size")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    json_path = args["json_path"]
    category = args["category"]
    width = int(args["width"])
    height = int(args["height"])

    ratio = height/float(width) # rOI will attach depending on width

    # load json dict
    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    to_extract = []
    to_extract.append(category)

    # If crop directories don't exist, create them
    feat_path = "./cropped_" + json_path.split(".")[-2].split("/")[-1] + "_" + category

    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)

    num_images = 0

    # go thru every category
    for i, key in enumerate(to_extract):
        # go thru every path in the category
        for j, im_path in enumerate(json_paths[key]):
            orig_image = cv2.imread(im_path)
         

            scaleFactor = 5
            rows, cols = orig_image.shape[:2]
            image = cv2.resize(orig_image, (scaleFactor*cols, scaleFactor*rows), interpolation=cv2.INTER_LINEAR)
            im_bounds[0] = image.shape[0]
            im_bounds[1] = image.shape[1]

            clone = image.copy()
            cv2.namedWindow("image")
            # display the image and wait for a keypress
            cv2.setMouseCallback("image", click_and_crop)

            # keep looping until the 'q' key is pressed
            while True:

                cv2.imshow("image", image)
                # draw a rectangle around the region of interest
                if len(refPt) == 2:
                    image = orig_image.copy()
                    cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 1)
                    cv2.imshow("image", image)

                key = cv2.waitKey(1) & 0xFF

                # if the 'r' key is pressed, reset the cropping region
                if key == ord("r"):
                    print "Reset"
                    got_pt1 = 0
                    refPt = []
                    image = clone.copy()

                # if the 'c' key is pressed, break from the loop
                elif key == ord("c"):
                    break


            # if there are two reference points, then crop the region of interest
            # from the image and display it
            if len(refPt) == 2:
                roi = orig_image[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
                fd_name = os.path.split(im_path)[1].split(".")[0] + ".jpg"
                fd_path = os.path.join(feat_path, fd_name)
                cv2.imwrite(fd_path, roi)
                print "roi width: "  + str(roi.shape[1])
                print "roi height: " + str(roi.shape[0])
                actual_ratio = roi.shape[0]/float(roi.shape[1])
                print "roi ratio: " + str(actual_ratio)
                print "req. ratio: " + str(ratio)
                if (actual_ratio != ratio):
                    print "--WRONG"
                else:
                    print "++RIGHT"
                cv2.imshow("ROI", roi)
                cv2.waitKey(0)

            # close all open windows
            cv2.destroyAllWindows()

            num_images = num_images + 1

            if num_images % 1000 == 0:
                print num_images

    print "Features saved in {}".format(feat_path)


