# Import the modules
from sklearn.externals import joblib
from skimage.feature import hog
from skimage import exposure
from sklearn.svm import LinearSVC
from sklearn.datasets import fetch_mldata
import numpy as np
from collections import Counter
import cv2

mnist = fetch_mldata('MNIST original')
print mnist.data.shape
print mnist.target.shape
print np.unique(mnist.target)
 