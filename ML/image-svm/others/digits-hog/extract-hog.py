# import the necessary packages
from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

from imutils import paths
import argparse
import imutils
import cv2
import os
import glob

from config import *


if __name__ == "__main__":
   
    # initialize the data matrix and labels
    print "[INFO] extracting features..."
    data = []
    labels = []

    pos_im_path = args["pospath"]
    neg_im_path = args["negpath"]

    des_type = args["descriptor"]

    # If feature directories don't exist, create them
    if not os.path.isdir(pos_feat_ph):
        os.makedirs(pos_feat_ph)

    # If feature directories don't exist, create them
    if not os.path.isdir(neg_feat_ph):
        os.makedirs(neg_feat_ph)

    print "Calculating the descriptors for the positive samples and saving them"
    for im_path in glob.glob(os.path.join(pos_im_path, "*")):
        image = cv2.imread(im_path)
        if des_type == "HOG":
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            sample = cv2.resize(gray, (30, 50))
            cv2.imshow("Before HOG", sample)
            fd = feature.hog(sample, orientations=orientations, pixels_per_cell=pixels_per_cell, 
                cells_per_block=cells_per_block, transform_sqrt=transform_sqrt, visualise = visualize)
        
        #hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
        #hogImage = hogImage.astype("uint8")
        #cv2.imshow("HOG Image", hogImage)
        #cv2.waitKey(0)
        fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
        fd_path = os.path.join(pos_feat_ph, fd_name)
        joblib.dump(fd, fd_path)
        #print "Stored a pos desc"
    print "Positive features saved in {}".format(pos_feat_ph)

    print "Calculating the descriptors for the negative samples and saving them"
    for im_path in glob.glob(os.path.join(neg_im_path, "*")):
        image = cv2.imread(im_path)
        if des_type == "HOG":
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            sample = cv2.resize(gray, (30, 50))
            fd = feature.hog(sample, orientations=orientations, pixels_per_cell=pixels_per_cell, 
            cells_per_block=cells_per_block, transform_sqrt=transform_sqrt)
        
        # visualizing the HOG image
        #hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
        #hogImage = hogImage.astype("uint8")
        #cv2.imshow("HOG Image", hogImage)
        fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
        fd_path = os.path.join(neg_feat_ph, fd_name)
        joblib.dump(fd, fd_path)
    print "Negative features saved in {}".format(neg_feat_ph)

    print "Completed calculating features from training images"

