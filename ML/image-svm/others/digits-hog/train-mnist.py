# Import the modules
from sklearn.externals import joblib
from skimage.feature import hog
from skimage import exposure
from sklearn.svm import LinearSVC
from sklearn.datasets import fetch_mldata
import numpy as np
from collections import Counter
import cv2

mnist = fetch_mldata('MNIST original')
print mnist.data.shape
print mnist.target.shape
print np.unique(mnist.target)

# Extract the features and labels
features = np.array(mnist.data, 'int16')
labels = np.array(mnist.target, 'int')

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=5)


# Extract the hog features
list_hog_fd = []
for i, feature in enumerate(features):
    fd= hog(feature.reshape((28, 28)), orientations=9, 
        pixels_per_cell=(14, 14), cells_per_block=(1, 1), 
        visualise=False)
    #hog_im = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    #hog_im = hog_im.astype("uint8")
    #cv2.imshow("HOG Image", hog_im)
    #cv2.waitKey(0)
    if (i%100== 0):
        print i
    list_hog_fd.append(fd)
hog_features = np.array(list_hog_fd, 'float64')

print "Count of digits in dataset ", Counter(labels)

# Create a linear SVM object
clf = LinearSVC()

# Perform the training
clf.fit(hog_features, labels)

# Save the classifier
joblib.dump(clf, "digits_cls.pkl", compress=3)