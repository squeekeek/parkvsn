import imutils

def pyramid(image, scale, minSize):
    # yield the original image
    yield image

    # keep looping over the pyramid
    while True:
        # compute the new dimensions of the image and resize it 
        w = int(image.shape[1] / scale)
        image = imutils.resize(image, width=w)

        # if the resized image does not meet the supplied minimum
        # size, then stop constructing the pyramid
        if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
            break

        # yield the next image in the pyramid
        yield image

def sliding_window(image, windowSize, stepSize):
    # slide a window across the image
    for y in xrange(0, image.shape[0], stepSize[1]):
        for x in xrange(0, image.shape[1], stepSize[0]):
            # yield the current window 
            yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])