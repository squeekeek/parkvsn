# Import the modules
from sklearn.externals import joblib
from skimage.feature import hog
from skimage import exposure
from sklearn.svm import LinearSVC
from sklearn.datasets import fetch_mldata
import numpy as np
from collections import Counter
import cv2
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

mnist = fetch_mldata('MNIST original')
print mnist.data.shape
print mnist.target.shape
print np.unique(mnist.target)

# Extract the features and labels
X = np.array(mnist.data, 'int16')
y = np.array(mnist.target, 'int')

# Perform test split
# 60% - train, 20% - cross validation, 20% - test
X_train, X_temp, y_train, y_temp = train_test_split(
    X, y, test_size=0.40, random_state=5)
X_test, X_cv, y_test, y_cv = train_test_split(
   X_temp, y_temp, test_size=0.50, random_state=5)


# Perform training

# Extract the hog features
list_hog_fd = []
for i, feature in enumerate(X_train):
    fd = hog(
        feature.reshape((28, 28)), orientations=9,
        pixels_per_cell=(4, 4), cells_per_block=(4, 4),
        visualise=False)
    # hog_im = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    # hog_im = hog_im.astype("uint8")
    # cv2.imshow("HOG Image", hog_im)
    # cv2.waitKey(0)
    if i % 100 == 0:
        print i
    list_hog_fd.append(fd)
hog_features = np.array(list_hog_fd, 'float64')

# Create a linear SVM object
clf = LinearSVC()

# Perform the training
clf.fit(hog_features, y_train)

# Save the classifier
joblib.dump(clf, "digits_cls.pkl", compress=3)

print "After dump"
# Perform evaluation on cross-valiation set
# compute F1 score of the classifier
"""
num_validations = 5
f1 = cross_val_score(
    clf, X, y, scoring='f1_weighted', cv=num_validations)
print "F1 score: " + str(round(100*f1.mean(), 2)) + "%"

presicion = cross_val_score(
    clf, X, y, scoring='precision_weighted', cv=num_validations)
print "Precision: " + str(round(100*precision.mean(), 2)) + "%"

recall = cross_val_score(
    clf, X, y, scoring="recall_weighted", cv=num_validations)
print "Recall: " + str(round(100*recall.mean(), 2)) + "%"
"""
# Generate classification report for cv and test set
target_names = [
    "Class-0",
    "Class-1",
    "Class-2",
    "Class-3",
    "Class-4",
    "Class-5",
    "Class-6",
    "Class-7",
    "Class-8",
    "Class-9",
    ]

list_hog_fd_cv = []
for i, feature in enumerate(X_cv):
    fd = hog(
        feature.reshape((28, 28)), orientations=9,
        pixels_per_cell=(14, 14), cells_per_block=(1, 1),
        visualise=False)
    # hog_im = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    # hog_im = hog_im.astype("uint8")
    # cv2.imshow("HOG Image", hog_im)
    # cv2.waitKey(0)
    if i % 100 == 0:
        print i
    list_hog_fd_cv.append(fd)
hog_features_cv = np.array(list_hog_fd_cv, 'float64')

cv_predict = clf.predict(hog_features_cv)


list_hog_fd_test = []
for i, feature in enumerate(X_test):
    fd = hog(
        feature.reshape((28, 28)), orientations=9,
        pixels_per_cell=(14, 14), cells_per_block=(1, 1),
        visualise=False)
    # hog_im = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    # hog_im = hog_im.astype("uint8")
    # cv2.imshow("HOG Image", hog_im)
    # cv2.waitKey(0)
    if i % 100 == 0:
        print i
    list_hog_fd_test.append(fd)
hog_features_test = np.array(list_hog_fd_test, 'float64')

test_predict = clf.predict(hog_features_test)

len_train = len(y_train)
len_cv = len(y_cv)
len_test = len(y_test)
len_total = len_train + len_cv + len_test

print_total = (
    "Digits in dataset (100%):\n"
    "Count: {}\n"
).format(len_total)

print_train = (
    "Digits in training dataset (60%):\n"
    "Count: {}\n"
    "{}\n"
).format(len_train, Counter(y_train))

print_cv = (
    "Digits in cross-validation dataset (20%):\n"
    "Count: {}\n"
    "{}\n"
).format(len_cv, Counter(y_cv))

print_test = (
    "Digits in test dataset (20%):\n"
    "Count: {}\n"
    "{}\n"
).format(len_test, Counter(y_test))

print print_total
print print_train
print print_cv
print print_test

print "For cross-validation set (20%):"
print (classification_report(
    y_cv, cv_predict, target_names=target_names))

print "For test set (20%):"
print (classification_report(
    y_test, test_predict, target_names=target_names))