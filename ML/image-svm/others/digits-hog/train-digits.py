import numpy as np
import matplotlib.pyplot as plt

from scipy import stats

from sklearn import datasets
from sklearn.semi_supervised import label_propagation

from sklearn.metrics import confusion_matrix, classification_report

from skimage import exposure
from skimage import feature

from config import *
import cv2

digits = datasets.load_digits()
rng = np.random.RandomState(0)
indices = np.arange(len(digits.data))
rng.shuffle(indices)

X = digits.data[indices[:330]]
y = digits.target[indices[:330]]
images = digits.images[indices[:330]]

# Display the first digit
plt.figure(1, figsize=(2,2))
plt.imshow(images[0], cmap=plt.cm.gray_r, interpolation='nearest')
plt.show()

fd, hogImage = feature.hog(images[0], orientations=orientations, pixels_per_cell=pixels_per_cell, 
                cells_per_block=cells_per_block, transform_sqrt=transform_sqrt, visualise = visualize)

hog_im = exposure.rescale_intensity(hogImage, out_range=(0, 255))
hog_im = hog_im.astype("uint8")
cv2.imshow("HOG Image", hog_im)
cv2.waitKey(0)