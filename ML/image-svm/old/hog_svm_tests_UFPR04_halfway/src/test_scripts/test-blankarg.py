"""
    python test-blankarg.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -m pos_left-1_neg_left-1
"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-p', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    parser.add_argument(
        '-n', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    parser.add_argument(
        "-n1", "--negfeat2", 
        help="Path to 2nd negative features directory",
        default=""
    )
    parser.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)

    args = vars(parser.parse_args())

    pos_feat_path = args["posfeat"]
    neg_feat_path = args["negfeat"]
    neg_feat_path_2 = args["negfeat2"]
    model_name = args["model_name"]


    if len(neg_feat_path_2) == 0:
        print "in null"