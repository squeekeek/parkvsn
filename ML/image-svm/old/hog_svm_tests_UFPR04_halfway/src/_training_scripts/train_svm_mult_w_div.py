"""
    Trains and tests model
    -p1, -p2, -p3
    -n1, n2, -n3

    Creates own division of 60% training, 20% cv, 20% test

    Example usage:

    python train_svm_trainingset_mult.py \
    -m right-1-2-3-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -p2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
    -p3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

    python train_svm_trainingset_mult.py \
    -m right-1-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 



"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse 
import glob
import os
import sys
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-p1', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    ap.add_argument(
        "-p2", "--posfeat2",
        help="Path to 2nd positive features directory"
    )
    ap.add_argument(
        "-p3", "--posfeat3",
        help="Path to 3rd positive features directory"
    )
    ap.add_argument(
        '-n1', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    ap.add_argument(
        "-n2", "--negfeat2",
        help="Path to 2nd negative features directory"
    )
    ap.add_argument(
        "-n3", "--negfeat3",
        help="Path to 3rd negative features directory"
    )
    ap.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)
    ap.add_argument(
        '-w', "--width",
        help="width of the window size",
        required=True
    )
    ap.add_argument(
        '-ht', "--height",
        help="Height of the window size",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())

    pos_feat_path = args["posfeat"]
    pos_feat_path_2 = args["posfeat2"]
    pos_feat_path_3 = args["posfeat3"]
    neg_feat_path = args["negfeat"]
    neg_feat_path_2 = args["negfeat2"]
    neg_feat_path_3 = args["negfeat3"]
    model_name = args["model_name"]
    width = args["width"]
    height = args["height"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    


    # If feature directories don't exist, create them
    temp_model_path =  "w" + str(width) + "_h" + str(height) + "_" + model_name + '.model'
    results_filename = temp_model_path.split(".model")[0] + ".txt"
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "models", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        model_path = os.path.join(data_folder, temp_model_path)
        results_path = os.path.join(data_folder, results_filename)
    else:
        model_path = os.path.join(folder_path, temp_model_path)
        results_path = os.path.join(folder_path, results_filename)

    write_file = open(results_path, 'w')
    write_file.truncate()

    str_a = "Positive folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = pos_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    fds = []
    labels = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(1)

    if pos_feat_path_2 is not None:
        print "Has 2nd pospaths folder"
        str_a = pos_feat_path_2  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_2, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 2nd pos paths folder"

    if pos_feat_path_3 is not None:
        print "Has 3rd pospaths folder"
        str_a = pos_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_3, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 3rd pos paths folder"

    str_a = "\n"
    print str_a
    write_file.write(str_a)
    
    str_a = "Negative folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = neg_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(0)

    if neg_feat_path_2 is not None:
        print "Has 2nd negpaths folder"
        str_a = neg_feat_path_2  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_2, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 2nd neg paths folder"

    if neg_feat_path_3 is not None:
        print "Has 3rd negpaths folder"
        str_a = neg_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_3, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 3rd neg paths folder"

    str_a = "\n"
    print str_a
    write_file.write(str_a)

    # Perform test split
    # 60% - train, 20% - cross validation, 20% - test

    fds_train, fds_temp, labels_train, labels_temp = train_test_split(
        fds, labels, test_size=0.40, random_state=5)
    fds_test, fds_cv, labels_test, labels_cv = train_test_split(
        fds_temp, labels_temp, test_size=0.50, random_state=5)

    clf = LinearSVC(class_weight="balanced")

    print "Training a Linear SVM Classifier"
    clf.fit(fds_train, labels_train)




    joblib.dump(clf, model_path)
    str_a =  "Classifier saved to {}\n".format(model_path)
    print str_a
    write_file.write(str_a)

    cv_predict = clf.predict(fds_cv)
    test_predict = clf.predict(fds_test)

    len_train = len(labels_train)
    len_cv = len(labels_cv)
    len_test = len(labels_test)
    len_total = len_train + len_cv + len_test

    


    print_total = (
        "Features in dataset (100%):\n"
        "Count: {}\n"
    ).format(len_total)

    print_train = (
        "Features in training dataset (60%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_train, Counter(labels_train))

    print_cv = (
        "Features in cross-validation dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_cv, Counter(labels_cv))

    print_test = (
        "Features in test dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_test, Counter(labels_test))

    write_file.write(print_total)
    print print_total
    write_file.write(print_train)
    print print_train
    write_file.write(print_cv)
    print print_cv
    write_file.write(print_test)
    print print_test

    target_names = [
        "Empty",
        "Occupied"
    ]
    
    str_a = "For cross-validation set (20%):\n"
    print str_a
    write_file.write(str_a)
    str_a =  (classification_report(
        labels_cv, cv_predict, target_names=target_names))
    print str_a
    write_file.write(str_a)

    str_a =  "For test set (20%):\n"
    print str_a
    write_file.write(str_a)
    str_a =  (classification_report(
        labels_test, test_predict, target_names=target_names))
    print str_a
    write_file.write(str_a)
    write_file.close()