"""
    For filtering roi json files from those with incorrect ratios
"""

import argparse
import glob
import os
import cv2
import json
import io
import sys
import shutil


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk',
        "--pklot_path", help="Path to PkLot dataset",
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of sorted numbers to load",
        required=True)
    ap.add_argument(
        "-r", "--ratio",
        help="The required ratio",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    test_pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    ratio = float(args["ratio"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    if os.path.exists(json_path):
        print "Viewing images from: {}".format(json_path)
        with open(json_path) as data_file:
            roi_dict = json.load(data_file)
    else:
        print "Json path does not exist"

    pklot_path = test_pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    
    folder_name = json_path.split("/")[-1].split(".json")[0]
    dest_path = "im_" + folder_name

    if os.path.isdir(dest_path):
        shutil.rmtree(dest_path)

    os.makedirs(dest_path)

    total_num = len(roi_dict)
    num_copied = 0
    filtered_dict = {}
    actual_num = 0

    for i, im_path in enumerate(roi_dict):
        actual_path = pklot_path + im_path
        if roi_dict[im_path]["ratio"] == ratio:
            filtered_dict.update(
                {
                    im_path: {
                        "pt1": roi_dict[im_path]["pt1"],
                        "pt2": roi_dict[im_path]["pt2"],
                        "dim": roi_dict[im_path]["dim"],
                        "window": roi_dict[im_path]["window"],
                        "ratio": roi_dict[im_path]["ratio"],
                        "ifFront": roi_dict[im_path]["ifFront"]
                    }
                }
            )
            actual_num = actual_num + 1

        num_copied = num_copied + 1
        if (num_copied % 1000 == 0 or num_copied == total_num):
            print "[{}%] {}/{} filtered".format(
                (num_copied/float(total_num))*100, num_copied, total_num)

    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    filtered_filename = "fiiltered_" + str(ratio) + "_" + str(actual_num) + "_" + json_path
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "json_cropped_partitioned", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, filtered_filename)
    else:
        output_filename = os.path.join(folder_path, filtered_filename)


    with io.open(filtered_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        filtered_dict,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

