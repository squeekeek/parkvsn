from sklearn.svm import LinearSVC
from sklearn.externals import joblib
from timeit import default_timer as timer

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

from skimage import exposure
from skimage import feature

from lib.generate_occ_to_neg import generate_occ_to_neg
from lib.generate_window import generate_window
from lib.generate_window_pos import generate_window_pos
from lib.extract_hog_paths import extract_hog_paths

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False


if __name__ == "__main__":

    pklot_path = "/Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/"
    main_pos_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json"
    main_neg_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04_halfway/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json"
    dataset_category = "UFPR04"
    category = "right-1"
    neg_category = category
    except_category = "right"
    width = 40
    height = 80
    position = "upper-left"
    project_name = "test"
    folder_path = None

    # Generate initial window json

    window_path_pos = generate_window(
        pklot_path, main_pos_path, category, width, height, folder_path, project_name
        )
    window_path_neg = generate_window(
        pklot_path, main_neg_path, neg_category, width, height, folder_path, project_name
        )
    occ_neg_path = generate_occ_to_neg(main_pos_path, except_category, folder_path, project_name)
    window_path_neg_occ = generate_window(
        pklot_path, occ_neg_path, "ALL", width, height, folder_path, project_name
        )

    # Generate roi window json
    roi_path_pos = generate_window_pos(
        pklot_path, window_path_pos, position, folder_path, project_name
    )
    roi_path_neg = generate_window_pos(
        pklot_path, window_path_neg, position, folder_path, project_name
    )
    roi_path_neg_occ = generate_window_pos(
        pklot_path, window_path_neg_occ, position, folder_path, project_name
    )

    # Extract HoG
    hog_path_pos = extract_hog_paths(
        pklot_path, roi_path_pos, width, height, folder_path, project_name
    )
    hog_path_neg = extract_hog_paths(
        pklot_path, roi_path_neg, width, height, folder_path, project_name
    )
    hog_path_neg_occ = extract_hog_paths(
        pklot_path, roi_path_neg_occ, width, height, folder_path, project_name
    )

    # Generate list of python scripts to enter 
    model_name_1 = (
        "pos_{}"
        "neg_unocc_{}"
    ).format(
        category, neg_category
    )
    cmd_1 = (
        "python train_svm_mult_w_div.py \\ \n"
        "-m {} \\ \n"
        "-p1 {} \\ \n"
        "-n1 {} \\ \n"
        "-w {} \\ \n"
        "-ht {} \\ \n"
        "-n {} \n"
    ).format(
         model_name_1, hog_path_pos, hog_path_neg, width, height, project_name
    )

    model_name_2 = (
        "pos_{}"
        "neg_unocc_{}"
        "_{}"
    ).format(
        category, neg_category, except_category
    )
    cmd_2 = (
        "python train_svm_mult_w_div.py "
        "-m {} \\ \n"
        "-p1 {} \\ \n"
        "-n1 {} \\  \n"
        "-n2 {} \\ \n"
        "-w {} \\ \n"
        "-ht {} \\ \n"
        "-n {} \n"
    ).format(
        model_name_2, hog_path_pos, hog_path_neg, hog_path_neg_occ, width, height, project_name
    )
    print ""
    print "#### Cmd for occupied and unoccupied of {}:".format(category)
    print cmd_1
    print ""
    print "#### Cmd for occupied and unoccupied, and except {} of {}:".format(except_category, category)
    print cmd_2
    print ""





