from skimage import exposure
from skimage.feature import hog
from sklearn.externals import joblib

import argparse 
import glob
import os
import json
import io
import cv2
import sys
import imutils
from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from skimage.io import imread
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window


import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-i', "--image", help="Path to the image", 
        required=True)
    ap.add_argument(
        "-m", "--model",
        help="Path to the model",
        required=True)
    args = vars(ap.parse_args())

    image_path = args["image"]
    # Read the input image
    im = cv2.imread(image_path)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    model_path = args["model"]

    clf = joblib.load(model_path)

    # settings for hog
    #min_wdw_sz = (55, 36)
    #window_size = (55, 36)
    min_wdw_sz = (80, 30)
    window_size = (80, 30)
    orientations = 9
    step_size = (4, 4)

    pixels_per_cell = [4, 4]
    cells_per_block = [4, 4]
    downscale = 1.001
    transform_sqrt = True
    visualize_det = False
    threshold = 0.5

    width = im.shape[1]
    height = im.shape[0]

    # If the image is smaller than the bounding rectangle,
    # resize the image to a larger scale,
    # and then crop appropriately

    # print "gray size: y: {} x:{}".format(gray.shape[0], gray.shape[1])
    counter_resize = 0
    while (1):
        width = im.shape[1]
        height = im.shape[0]
        if height < window_size[0]:
            new_height = window_size[0] + counter_resize
            ratio = width/float(height)
            new_width = int(ratio*new_height)
            dim = (new_height, new_width)
            im = cv2.resize(im, dim, interpolation=cv2.INTER_AREA)
        elif width < window_size[1]:
            new_width = window_size[1] + counter_resize
            ratio = height/width
            new_height = ratio*new_width
            dim = (new_height, new_width)
            im = cv2.resize(im, dim, interpolation=cv2.INTER_AREA)
        else:
            break
        counter_resize = counter_resize + 1

    # List to store the detections
    detections = []
    # The current scale of the image
    scale = 0
    # Downscale the image and iterate
    for im_scaled in pyramid_gaussian(im, downscale=downscale):
        # This list contains detections at the current scale 
        cd = []
        # If the width or height of the scaled image is less than
        # the width or height of the window, then end the iterations
        if im_scaled.shape[0] < min_wdw_sz[0] or im_scaled.shape[1] < min_wdw_sz[1]:
            print "im_scaled.shape[0]: {}, im_scaled.shape[1]: {}".format(im_scaled.shape[0], im_scaled.shape[1])
            print "min_wdw_sz[0]: {}, min_wdw_sz.shape[1]: {}".format(min_wdw_sz[0], min_wdw_sz[1])
            break

        for (x, y, im_window) in sliding_window(im_scaled, min_wdw_sz, step_size):
            if im_window.shape[0] != min_wdw_sz[0] or im_window.shape[1] != min_wdw_sz[1]:
                # print "in continue"
                # print "height: {}, width: {}".format(im_window.shape[0], im_window.shape[1])
                continue
            # Calculate the HOG features
            fd = hog(
                im_window, 
                orientations=orientations, pixels_per_cell=pixels_per_cell,
                cells_per_block=cells_per_block, visualise=False)

            pred = clf.predict(fd.reshape(1, -1))
            print "Pred: {}".format(pred)
            if len(pred) > 0:
                print "Detection:: Location -> ({}, {})".format(x, y)
                print "Scale ->  {} | Confidence Score {} \n".format(
                    scale, clf.decision_function(fd.reshape(1, -1)))
                test_df = clf.decision_function(fd.reshape(1, -1))
                print "test_df.shape: {}".format(test_df.shape)
                detections.append((
                    x, y, test_df,
                    int(min_wdw_sz[1]*(downscale**scale)),
                    int(min_wdw_sz[0]*(downscale**scale))
                    ))
                cd.append(detections[-1])
            if visualize_det:
                clone = im_scaled.copy()
                for x1, y1, _, _, _ in cd:
                    # Draw the detectionas at this scale
                    cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + 
                        im_window.shape[0]), (0, 0,0), thickness=2)
                cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]),
                    (255, 255, 255), thickness=2)
                cv2.imshow("Sliding Window in Progress", clone)
                cv2.waitKey(30)
        # Move the next scale
        scale +=1

    # Display the results before performing NMS
    clone = im.copy()
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(im, (x_tl, y_tl), (x_tl + w, y_tl + h), (0, 0, 0), thickness=2)
    cv2.imshow("Raw Detections before NMS", im)
    cv2.waitKey()

    # Perform Non Maxima Suppression
    # detections = non_max_suppression(detections, threshold)
    detections = nms(detections, threshold)

    # Display the results after performing NMS
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(clone, (x_tl, y_tl), (x_tl+w,y_tl+h), (0, 0, 0), thickness=2)
    cv2.imshow("Final Detections after applying NMS", clone)
    cv2.waitKey()
