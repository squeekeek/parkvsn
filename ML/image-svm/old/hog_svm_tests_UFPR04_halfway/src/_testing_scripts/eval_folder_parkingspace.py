"""
    Imports eval_vis_single_parkingspace.py


    Evaluates a folder of parking space images (per car). ".jpg"
    Outputs a folder of organized parking spaces, "Empty" or "Occupied", as well as marked parts that are recognized as a car.

    Folder is optional

    Example usage:
    python eval_folder_parkingspace.py \
    -i /Users/agatha/Documents/CoE198/parkvsn/prototype/v1/eeei_5th_solar \
    -m /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/models/UFPR04_halfway/w30_h80_pos_left-1neg_unocc_left-1_left.model \
    -w 30 -ht 80\
    -vd n -v n -wr y \
    -n test



"""
from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from lib.nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from skimage.io import imread
from imutils.object_detection import non_max_suppression
from lib.helpers import pyramid, sliding_window

from eval_vis_single_parkingspace import eval_parkingspace

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


DEBUG = 0

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-i', "--image_folder",
        help="Path to the image folder",
        required=True
    )
    ap.add_argument(
        '-m', "--model_path",
        help="Path to the model to use",
        required=True
    )
    ap.add_argument(
        '-w', "--width",
        help="Width of model",
        required=True
    )
    ap.add_argument(
        '-ht', "--height",
        help="Height of model",
        required=True
    )
    ap.add_argument(
        '-vd', "--visualize_detections",
        help="y to visualize detections",
        required=True
    )
    ap.add_argument(
        "-v", "--ifVisualize",
        help="y to visualize final detections",
        required=True
    )
    ap.add_argument(
        "-wr", "--ifWriteImage",
        help="y to write image",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    args = vars(ap.parse_args())

    image_folder = args["image_folder"]
    model_path = args["model_path"]
    win_width = int(args["width"])
    win_height = int(args["height"])
    visualize_det = args["visualize_detections"]
    ifVisualize = args["ifVisualize"]
    ifWriteImage = args["ifWriteImage"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    dataset_name = image_folder.split("/")[-1]
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "img_eval_folder_parkingspace", project_name, dataset_name)
        
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
    else:
        data_folder = os.path.join(folder_path, dataset_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)

    
    empty_folder = os.path.join(data_folder, "Empty")
    occupied_folder = os.path.join(data_folder, "Occupied")
    if not os.path.isdir(empty_folder):
        os.makedirs(empty_folder)
    if not os.path.isdir(occupied_folder):
        os.makedirs(occupied_folder)

    # Evaluate the images
    for im_path in glob.glob(os.path.join(image_folder, "*.jpg")):
        prediction = eval_parkingspace(
            im_path, model_path, win_width,
            win_height, visualize_det,
            ifVisualize, ifWriteImage, data_folder
        )
        print im_path
        print "# detections: {}\n".format(prediction)
