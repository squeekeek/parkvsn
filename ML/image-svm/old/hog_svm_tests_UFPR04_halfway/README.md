# How to train and test on dataset
(order of documentation according to purpose)


# src
## _path_scripts
- data/paths_unsorted

### generate-unoccupiedpaths.py
```
    Use this to generate list of unsorted paths.

    Same format as output of num-to-paths.py.
    Can be UFPR04, UFPR05, or PUC.

    -c, class: empty, occupied
    Uses the "Empty" or "Occupied" folder of PKLot.

    Example usage:
    python generate-unsorted.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -s UFPR04 -c occupied -n UFPR04_halfway
```

-------------------------------------------------------------------------------


## _partialpaths_scripts
- data/paths_sorted_partial

### generate-occ-to-neg.py
```
    Creates partial copy of paths file from pos paths. 

    Copies all except: left, center, right, left-1, left-2, [...]

    Example usage:
    python generate-occ-to-neg.py -j UFPR04_pos_paths.json -e left -n UFPR04_halfway
```

## _generate_adjusted_dim
- data/json_adjusted_dim

### generate-window.py
```

    Generates category paths JSON adjusted for window sizes.

    Needs a given window size to be used for all images. 
    Crops an image from upper left to lower right given window size. 
        If the image has greater width than specified, the image is first resized to window width maintaining aspect ratio. 
        If the image has smaller width than specified, the image is first reszied to window width maintaining aspect ratio. 

        If image has smaller height than window size, image is resized with increasing window width, maintaining aspect ratio. Upon reaching an appropriate width, cropping is done. 

    Logs
        Store image paths still by cateogry
        each image path has details regarding resizing and cropping
        new_dim should be window size
        {
            "left-1":
                {
                    image-path: {
                        orig_dim: [cols, rows],
                        resized_dim: [cols, rows],
                        new_dim: [cols, row],
                        ifIncreasedWidth: 1,
                        ifIncreasedHeight: 0
                    },
                    image-path: {
                        orig_dim: [cols, rows],
                        resized_dim: [cols, rows]
                        new_dim: [cols, row],
                        ifIncreasedWidth: 1,
                        ifIncreasedHeight: 0
                    },
                }
        }
    Example usage: 
    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c left-1 -w 30 -ht 80

    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_neg_paths.json -c ALL -w 30 -ht 80

    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths_except_left.json -c ALL -w 30 -ht 80

```
-------------------------------------------------------------------------------


## _generate_cropped_scripts
- data/json_cropped

### generate-window-pos.py
```
    Loads JSON file from generate-window.py 

    Creates JSON file similar to output of extract-roi.py
    - Can select if will crop from:
        - upper-left (for now ito muna, since it's just from 0,0)
        - upper-center
        - upper-right
        - lower-left
        - lower-center
        - lower-right

    Example Usage:
    python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json -p upper-left

     python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_left-1_UFPR04_neg_paths_8680_w-30h-80.json -p upper-left

     python generate-window-pos.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json -p upper-left

```

### generate-neg-roi-json.py
```
    Generate json of same format as output of extract-roi.py.
    Use for the negative images.
    Ideally, for unoccupied images.
    Needs json of same format as nums-to-paths.py. 
    Needs window size and target ratio.

    Example usage:
    python generate-neg-roi-json.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j gen_UFPR04_unocc_paths_59718.json -w 30 -ht 80 -n test
    
    python generate-neg-roi-json.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_adjusted_dim/UFPR04_halfway/genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -w 30 -ht 80 -n test

```
-------------------------------------------------------------------------------


## _modify_roi_cropped_json_scripts

### filter-ratios.py
```
    For filtering roi json files from those with incorrect ratios
```

### generate-partial-roi-json.py
```
    Generate a partial version of an ROI json file. 
    Need to enter percentage of paths needed.
    Note: distribution is not equal

    Example usage:
    python generate-partial-roi-json.py -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -p 0.50 -n test
```

### sep-pos-json.py
```
    A script that would randomly separate a given category into train, cv and/or test paths. Use the output JSON of generate-window-pos.py
    Assumption is that there's just one view and one category.

    Example usage:
    python sep-pos-json.py -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -wcv 1 -n test

```

## _img_roi_cropped_scripts

### generate_uncropped_roi_images.py
```
    Loads non-cropped images to a folder given an ROI json file.

    Example usage:
    python generate-uncropped-roi-images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j roi_UFPR04_left-1.json

    python generate_uncropped_roi_images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -n test
```

### generate_cropped_roi_images.py
```
    Generate folder of cropped roi images from the json generated with extract-roi.
    
    Example usage:

    python generate_cropped_roi_images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -n test
```
-------------------------------------------------------------------------------

## _extract_feat_scripts

### extract_hog_paths.py
```
    Extracts HoG given paths file from generate-window.py

    Loads like hog-roi, but will resize image first before cropping.

    Example usage:
    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json -w 30 -ht 80

    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80.json -w 30 -ht 80

    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json -w 30 -ht 80

    python extract_hog_paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -w 30 -ht 80 -n test

```

### extract_hog_roi.py
[fix in the future]

-------------------------------------------------------------------------------

## _training_scripts

### train_svm_mult_w_div.py
```
    Trains and tests model
    -p1, -p2, -p3
    -n1, n2, -n3

    Example usage:
    python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1_neg_left-1_except-left

    python train_svm_trainingset_mult.py \
    -m right-1-2-3-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -p2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
    -p3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

    python train_svm_trainingset_mult.py \
    -m right-1-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

```

### train_svm_mult_wo_div.py
```
    Trains model
    -p1, -p2, -p3
    -n1, n2, -n3

    Uses folder paths as training input as is.

    Example usage:
    python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1_neg_left-1_except-left

    python train_svm_mult_wo_div.py\
    -m right-1-2-3-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -p2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
    -p3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

    python train_svm_mult_wo_div.py \
    -m right-1-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

```

### train-model-hog.py
[future adjustments]

### train-model.py
[future adjustments]

### train-svm-trainingset.py
[future adjustments]

-------------------------------------------------------------------------------

## _testing_scripts

### eval_vis_single_parkingspace.py
```
    For testing a single image and a model.

    Expects an input of an appropriately cropped parking space and a model.
    Progress:
    - [X] Would work for left-1
        - [ ] Need to negative hardmine for other stuff
        - [ ] Need to create master script to test all models

    Outputs detections.

    Example usage:
    python eval_single_parkingspace.py -i /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/sorted_UFPR04_images/left-1/2012-12-12_10_00_05#011.jpg -m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests/pos_left-1_neg_left-1_except-left.model -w 30 -ht 80 -vd 1
```

### eval_folder_hog.py
```
    Tests a model given a folder of hog, expected label, and a model file

    Derived from train-svm-trainingset-mult.py

    Example usage:
    python eval_folder_hog.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 \
    -m /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/models/UFPR04_halfway/w30_h80_pos_left-1neg_unocc_left-1_left.model  \
    -l 1

```

### eval_posneg_hog.py
```
    Tests model

    Example usage:
    python test-svm-trainingset.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80  -m pos_left-1neg_unocc_left-1.model
```

### test_hardcode.py
```
    Test image or folder of images.
    Must hard code.

    Evaluates models in dict.
    Must hard code.
```

### eval_folder_parkingspace.py
```
    Imports eval_vis_single_parkingspace.py


    Evaluates a folder of parking space images (per car). ".jpg"
    Outputs a folder of organized parking spaces, "Empty" or "Occupied", as well as marked parts that are recognized as a car.

    Folder is optional

    Example usage:
    python eval_folder_parkingspace.py \
    -i /Users/agatha/Documents/CoE198/parkvsn/prototype/v1/eeei_5th_solar \
    -m /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/models/UFPR04_halfway/w30_h80_pos_left-1neg_unocc_left-1_left.model \
    -w 30 -ht 80\
    -vd n -v n -wr y \
    -n test

```