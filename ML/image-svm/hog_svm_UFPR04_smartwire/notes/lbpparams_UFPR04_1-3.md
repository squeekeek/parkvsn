<!-- TOC -->

- [pr-18 rc-11](#pr-18-rc-11)
    - [left](#left)
        - [Empty     0.9826    0.8706    0.9232     11965](#empty-----09826----08706----09232-----11965)
        - [Occupied     0.5360    0.9062    0.6736      1973](#occupied-----05360----09062----06736------1973)
    - [center](#center)
        - [Empty     0.9398    0.8218    0.8768     11980](#empty-----09398----08218----08768-----11980)
        - [Occupied     0.6188    0.8459    0.7147      4096](#occupied-----06188----08459----07147------4096)
    - [right](#right)
        - [Empty     0.9719    0.8891    0.9287     11942](#empty-----09719----08891----09287-----11942)
        - [Occupied     0.6784    0.9010    0.7740      3100](#occupied-----06784----09010----07740------3100)
- [pr-18 rc-12](#pr-18-rc-12)
    - [left](#left-1)
        - [Empty     0.9864    0.8939    0.9378     11965](#empty-----09864----08939----09378-----11965)
        - [Occupied     0.5897    0.9250    0.7202      1973](#occupied-----05897----09250----07202------1973)
    - [center](#center-1)
        - [Empty     0.9540    0.8712    0.9107     11980](#empty-----09540----08712----09107-----11980)
        - [Occupied     0.6996    0.8772    0.7784      4096](#occupied-----06996----08772----07784------4096)
    - [right](#right-1)
        - [Empty     0.9771    0.9013    0.9377     11942](#empty-----09771----09013----09377-----11942)
        - [Occupied     0.7072    0.9187    0.7992      3100](#occupied-----07072----09187----07992------3100)
- [pr-18 rc-13](#pr-18-rc-13)
    - [left](#left-2)
        - [Empty     0.9898    0.9084    0.9474     11965](#empty-----09898----09084----09474-----11965)
        - [Occupied     0.6294    0.9432    0.7550      1973](#occupied-----06294----09432----07550------1973)
    - [center](#center-2)
        - [Empty     0.9602    0.8936    0.9257     11980](#empty-----09602----08936----09257-----11980)
        - [Occupied     0.7412    0.8916    0.8095      4096](#occupied-----07412----08916----08095------4096)
    - [right](#right-2)
        - [Empty     0.9786    0.9158    0.9461     11942](#empty-----09786----09158----09461-----11942)
        - [Occupied     0.7399    0.9229    0.8213      3100](#occupied-----07399----09229----08213------3100)
- [pr-18 rc-14](#pr-18-rc-14)
    - [left](#left-3)
        - [Empty     0.9898    0.9097    0.9480     11965](#empty-----09898----09097----09480-----11965)
        - [Occupied     0.6326    0.9432    0.7573      1973](#occupied-----06326----09432----07573------1973)
    - [center](#center-3)
        - [Empty     0.9229    0.8209    0.8689     11980](#empty-----09229----08209----08689-----11980)
        - [Occupied     0.6041    0.7993    0.6881      4096](#occupied-----06041----07993----06881------4096)
    - [right](#right-3)
        - [Empty     0.9806    0.9112    0.9446     11942](#empty-----09806----09112----09446-----11942)
        - [Occupied     0.7311    0.9306    0.8189      3100](#occupied-----07311----09306----08189------3100)
- [pr-18 rc-15](#pr-18-rc-15)
    - [left](#left-4)
        - [Empty     0.9926    0.9161    0.9528     11965](#empty-----09926----09161----09528-----11965)
        - [Occupied     0.6532    0.9584    0.7769      1973](#occupied-----06532----09584----07769------1973)
    - [center](#center-4)
        - [Empty     0.9588    0.8878    0.9219     11980](#empty-----09588----08878----09219-----11980)
        - [Occupied     0.7303    0.8884    0.8016      4096](#occupied-----07303----08884----08016------4096)
    - [right](#right-4)
        - [Empty     0.9806    0.9124    0.9453     11942](#empty-----09806----09124----09453-----11942)
        - [Occupied     0.7338    0.9303    0.8205      3100](#occupied-----07338----09303----08205------3100)
- [pr-18 rc-16](#pr-18-rc-16)
    - [left](#left-5)
        - [Empty     0.9917    0.9168    0.9527     11965](#empty-----09917----09168----09527-----11965)
        - [Occupied     0.6538    0.9534    0.7757      1973](#occupied-----06538----09534----07757------1973)
    - [center](#center-5)
        - [Empty     0.9637    0.8962    0.9287     11980](#empty-----09637----08962----09287-----11980)
        - [Occupied     0.7481    0.9011    0.8175      4096](#occupied-----07481----09011----08175------4096)
    - [right](#right-5)
        - [Empty     0.9810    0.9179    0.9484     11942](#empty-----09810----09179----09484-----11942)
        - [Occupied     0.7464    0.9316    0.8288      3100](#occupied-----07464----09316----08288------3100)
- [pr-18 rc-21](#pr-18-rc-21)
    - [left](#left-6)
        - [Empty     0.9853    0.8656    0.9216     11965](#empty-----09853----08656----09216-----11965)
        - [Occupied     0.5306    0.9214    0.6735      1973](#occupied-----05306----09214----06735------1973)
    - [center](#center-6)
        - [Empty     0.9555    0.9003    0.9271     11980](#empty-----09555----09003----09271-----11980)
        - [Occupied     0.7505    0.8774    0.8090      4096](#occupied-----07505----08774----08090------4096)
    - [right](#right-6)
        - [Empty     0.9783    0.8968    0.9357     11942](#empty-----09783----08968----09357-----11942)
        - [Occupied     0.6989    0.9232    0.7956      3100](#occupied-----06989----09232----07956------3100)
- [pr-18 rc-22](#pr-18-rc-22)
    - [left](#left-7)
        - [Empty     0.9892    0.8974    0.9411     11965](#empty-----09892----08974----09411-----11965)
        - [Occupied     0.6018    0.9407    0.7340      1973](#occupied-----06018----09407----07340------1973)
    - [center](#center-7)
        - [Empty     0.9710    0.9184    0.9440     11980](#empty-----09710----09184----09440-----11980)
        - [Occupied     0.7941    0.9197    0.8523      4096](#occupied-----07941----09197----08523------4096)
    - [right](#right-7)
        - [Empty     0.9811    0.9109    0.9447     11942](#empty-----09811----09109----09447-----11942)
        - [Occupied     0.7310    0.9326    0.8196      3100](#occupied-----07310----09326----08196------3100)
- [pr-18 rc-23](#pr-18-rc-23)
    - [left](#left-8)
        - [Empty     0.9916    0.9255    0.9574     11965](#empty-----09916----09255----09574-----11965)
        - [Occupied     0.6783    0.9524    0.7923      1973](#occupied-----06783----09524----07923------1973)
    - [center](#center-8)
        - [Empty     0.9773    0.9316    0.9539     11980](#empty-----09773----09316----09539-----11980)
        - [Occupied     0.8239    0.9368    0.8767      4096](#occupied-----08239----09368----08767------4096)
    - [right](#right-8)
        - [Empty     0.9854    0.9355    0.9598     11942](#empty-----09854----09355----09598-----11942)
        - [Occupied     0.7921    0.9465    0.8624      3100](#occupied-----07921----09465----08624------3100)
- [pr-18 rc-24](#pr-18-rc-24)
    - [left](#left-9)
        - [Empty     0.9924    0.9265    0.9583     11965](#empty-----09924----09265----09583-----11965)
        - [Occupied     0.6823    0.9569    0.7966      1973](#occupied-----06823----09569----07966------1973)
    - [center](#center-9)
        - [Empty     0.9827    0.9517    0.9669     11980](#empty-----09827----09517----09669-----11980)
        - [Occupied     0.8706    0.9509    0.9090      4096](#occupied-----08706----09509----09090------4096)
    - [right](#right-9)
        - [Empty     0.9861    0.9390    0.9620     11942](#empty-----09861----09390----09620-----11942)
        - [Occupied     0.8016    0.9490    0.8691      3100](#occupied-----08016----09490----08691------3100)
- [pr-18 rc-25](#pr-18-rc-25)
    - [left](#left-10)
        - [Empty     0.9940    0.9345    0.9633     11965](#empty-----09940----09345----09633-----11965)
        - [Occupied     0.7086    0.9660    0.8175      1973](#occupied-----07086----09660----08175------1973)
    - [center](#center-10)
        - [Empty     0.9940    0.9345    0.9633     11965](#empty-----09940----09345----09633-----11965-1)
        - [Occupied     0.7086    0.9660    0.8175      1973](#occupied-----07086----09660----08175------1973-1)
    - [right](#right-10)
        - [Empty     0.9862    0.9479    0.9667     11942](#empty-----09862----09479----09667-----11942)
        - [Occupied     0.8255    0.9490    0.8830      3100](#occupied-----08255----09490----08830------3100)
- [pr-18 rc-26](#pr-18-rc-26)
    - [left](#left-11)
        - [Empty     0.9935    0.9389    0.9654     11965](#empty-----09935----09389----09654-----11965)
        - [Occupied     0.7221    0.9625    0.8251      1973](#occupied-----07221----09625----08251------1973)
    - [center](#center-11)
        - [Empty     0.9824    0.9477    0.9647     11980](#empty-----09824----09477----09647-----11980)
        - [Occupied     0.8613    0.9504    0.9037      4096](#occupied-----08613----09504----09037------4096)
    - [right](#right-11)
        - [Empty     0.9861    0.9479    0.9667     11942](#empty-----09861----09479----09667-----11942)
        - [Occupied     0.8254    0.9487    0.8828      3100](#occupied-----08254----09487----08828------3100)
- [pr-18 rc-31](#pr-18-rc-31)
    - [left](#left-12)
        - [Empty     0.9897    0.9065    0.9463     11965](#empty-----09897----09065----09463-----11965)
        - [Occupied     0.6244    0.9427    0.7512      1973](#occupied-----06244----09427----07512------1973)
    - [center](#center-12)
        - [Empty     0.9570    0.9001    0.9276     11980](#empty-----09570----09001----09276-----11980)
        - [Occupied     0.7510    0.8816    0.8111      4096](#occupied-----07510----08816----08111------4096)
    - [right](#right-12)
        - [Empty     0.9805    0.9181    0.9483     11942](#empty-----09805----09181----09483-----11942)
        - [Occupied     0.7466    0.9297    0.8282      3100](#occupied-----07466----09297----08282------3100)
- [pr-18 rc-32](#pr-18-rc-32)
    - [left](#left-13)
        - [Empty     0.9920    0.9519    0.9716     11965](#empty-----09920----09519----09716-----11965)
        - [Occupied     0.7659    0.9534    0.8494      1973](#occupied-----07659----09534----08494------1973)
    - [center](#center-13)
        - [Empty     0.9724    0.9275    0.9494     11980](#empty-----09724----09275----09494-----11980)
        - [Occupied     0.8131    0.9231    0.8646      4096](#occupied-----08131----09231----08646------4096)
    - [right](#right-13)
        - [Empty     0.9841    0.9406    0.9619     11942](#empty-----09841----09406----09619-----11942)
        - [Occupied     0.8045    0.9413    0.8675      3100](#occupied-----08045----09413----08675------3100)
- [pr-18 rc-33](#pr-18-rc-33)
    - [left](#left-14)
        - [Empty     0.9948    0.9697    0.9821     11965](#empty-----09948----09697----09821-----11965)
        - [Occupied     0.8404    0.9691    0.9002      1973](#occupied-----08404----09691----09002------1973)
    - [center](#center-14)
        - [Empty     0.9767    0.9422    0.9592     11980](#empty-----09767----09422----09592-----11980)
        - [Occupied     0.8469    0.9343    0.8885      4096](#occupied-----08469----09343----08885------4096)
    - [right](#right-14)
        - [Empty     0.9864    0.9536    0.9697     11942](#empty-----09864----09536----09697-----11942)
        - [Occupied     0.8416    0.9494    0.8922      3100](#occupied-----08416----09494----08922------3100)
- [pr-18 rc-34](#pr-18-rc-34)
    - [left](#left-15)
        - [Empty     0.9955    0.9682    0.9817     11965](#empty-----09955----09682----09817-----11965)
        - [Occupied     0.8345    0.9736    0.8987      1973](#occupied-----08345----09736----08987------1973)
    - [center](#center-15)
        - [Empty     0.9833    0.9541    0.9685     11980](#empty-----09833----09541----09685-----11980)
        - [Occupied     0.8765    0.9526    0.9130      4096](#occupied-----08765----09526----09130------4096)
    - [right](#right-15)
        - [Empty     0.9887    0.9606    0.9744     11942](#empty-----09887----09606----09744-----11942)
        - [Occupied     0.8631    0.9577    0.9080      3100](#occupied-----08631----09577----09080------3100)
- [pr-18 rc-35](#pr-18-rc-35)
    - [left](#left-16)
        - [Empty     0.9968    0.9774    0.9870     11965](#empty-----09968----09774----09870-----11965)
        - [Occupied     0.8776    0.9807    0.9263      1973](#occupied-----08776----09807----09263------1973)
    - [center](#center-16)
        - [Empty     0.9826    0.9510    0.9665     11980](#empty-----09826----09510----09665-----11980)
        - [Occupied     0.8690    0.9507    0.9080      4096](#occupied-----08690----09507----09080------4096)
    - [right](#right-16)
        - [Empty     0.9897    0.9638    0.9766     11942](#empty-----09897----09638----09766-----11942)
        - [Occupied     0.8734    0.9613    0.9152      3100](#occupied-----08734----09613----09152------3100)
- [pr-18 rc-36](#pr-18-rc-36)
    - [left](#left-17)
        - [Empty     0.9963    0.9778    0.9869     11965](#empty-----09963----09778----09869-----11965)
        - [Occupied     0.8788    0.9777    0.9256      1973](#occupied-----08788----09777----09256------1973)
    - [center](#center-17)
        - [Empty     0.9841    0.9563    0.9700     11980](#empty-----09841----09563----09700-----11980)
        - [Occupied     0.8820    0.9548    0.9170      4096](#occupied-----08820----09548----09170------4096)
    - [right](#right-17)
        - [Empty     0.9907    0.9666    0.9785     11942](#empty-----09907----09666----09785-----11942)
        - [Occupied     0.8823    0.9652    0.9219      3100](#occupied-----08823----09652----09219------3100)

<!-- /TOC -->
# pr-18 rc-11
## left

Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-11

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-11

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-11.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9837    0.8769    0.9273     11865
   Occupied     0.5657    0.9171    0.6998      2074

avg / total     0.9215    0.8829    0.8934     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9826    0.8706    0.9232     11965
###   Occupied     0.5360    0.9062    0.6736      1973

avg / total     0.9193    0.8757    0.8879     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-12.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9370    0.8293    0.8799     11900
   Occupied     0.6336    0.8410    0.7227      4176

avg / total     0.8582    0.8324    0.8390     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9398    0.8218    0.8768     11980
###   Occupied     0.6188    0.8459    0.7147      4096

avg / total     0.8580    0.8279    0.8355     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-11

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-11

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-11.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9724    0.8862    0.9273     11902
   Occupied     0.6772    0.9048    0.7746      3140

avg / total     0.9108    0.8901    0.8955     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9719    0.8891    0.9287     11942
###   Occupied     0.6784    0.9010    0.7740      3100

avg / total     0.9114    0.8916    0.8968     15042

# pr-18 rc-12
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-12.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9881    0.8966    0.9401     11865
   Occupied     0.6133    0.9383    0.7418      2074

avg / total     0.9323    0.9028    0.9106     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9864    0.8939    0.9378     11965
###   Occupied     0.5897    0.9250    0.7202      1973

avg / total     0.9302    0.8983    0.9070     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-13

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-13

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-13.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9562    0.8792    0.9161     11900
   Occupied     0.7200    0.8853    0.7941      4176

avg / total     0.8948    0.8808    0.8844     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9540    0.8712    0.9107     11980
###   Occupied     0.6996    0.8772    0.7784      4096

avg / total     0.8892    0.8727    0.8770     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-12.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9763    0.8973    0.9352     11902
   Occupied     0.7022    0.9175    0.7955      3140

avg / total     0.9191    0.9015    0.9060     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9771    0.9013    0.9377     11942
###   Occupied     0.7072    0.9187    0.7992      3100

avg / total     0.9215    0.9049    0.9091     15042

# pr-18 rc-13
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-13

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-13

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-13.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9906    0.9127    0.9500     11865
   Occupied     0.6555    0.9503    0.7758      2074

avg / total     0.9407    0.9183    0.9241     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9898    0.9084    0.9474     11965
###   Occupied     0.6294    0.9432    0.7550      1973

avg / total     0.9388    0.9133    0.9201     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-14

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-14

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-14.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9611    0.8959    0.9274     11900
   Occupied     0.7514    0.8968    0.8177      4176

avg / total     0.9067    0.8961    0.8989     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9602    0.8936    0.9257     11980
###   Occupied     0.7412    0.8916    0.8095      4096

avg / total     0.9044    0.8931    0.8961     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-13
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-13

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-13

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-13.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9775    0.9136    0.9445     11902
   Occupied     0.7376    0.9204    0.8189      3140

avg / total     0.9274    0.9150    0.9183     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9786    0.9158    0.9461     11942
###   Occupied     0.7399    0.9229    0.8213      3100

avg / total     0.9294    0.9172    0.9204     15042

# pr-18 rc-14
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-14

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-14

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-14.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9912    0.9158    0.9520     11865
   Occupied     0.6643    0.9532    0.7830      2074

avg / total     0.9425    0.9214    0.9268     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9898    0.9097    0.9480     11965
###   Occupied     0.6326    0.9432    0.7573      1973

avg / total     0.9392    0.9144    0.9210     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-11
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-11

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-11

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-11.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9223    0.8284    0.8728     11900
   Occupied     0.6209    0.8010    0.6996      4176

avg / total     0.8440    0.8213    0.8278     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9229    0.8209    0.8689     11980
###   Occupied     0.6041    0.7993    0.6881      4096

avg / total     0.8416    0.8154    0.8228     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-14
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-14

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-14

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-14.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9799    0.9067    0.9419     11902
   Occupied     0.7245    0.9296    0.8143      3140

avg / total     0.9266    0.9115    0.9153     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9806    0.9112    0.9446     11942
###   Occupied     0.7311    0.9306    0.8189      3100

avg / total     0.9292    0.9152    0.9187     15042


# pr-18 rc-15
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-15

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-15

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-15.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9921    0.9191    0.9542     11865
   Occupied     0.6742    0.9581    0.7915      2074

avg / total     0.9448    0.9249    0.9300     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9926    0.9161    0.9528     11965
###   Occupied     0.6532    0.9584    0.7769      1973

avg / total     0.9445    0.9221    0.9279     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-15

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-15

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-15.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9598    0.8907    0.9239     11900
   Occupied     0.7415    0.8937    0.8105      4176

avg / total     0.9031    0.8915    0.8945     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9588    0.8878    0.9219     11980
###   Occupied     0.7303    0.8884    0.8016      4096

avg / total     0.9006    0.8880    0.8913     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-15
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-15

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-15

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-15.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9811    0.9059    0.9420     11902
   Occupied     0.7236    0.9338    0.8154      3140

avg / total     0.9273    0.9117    0.9156     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9806    0.9124    0.9453     11942
###   Occupied     0.7338    0.9303    0.8205      3100

avg / total     0.9297    0.9161    0.9195     15042


# pr-18 rc-16
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-16

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-16

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-16.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9926    0.9208    0.9554     11865
   Occupied     0.6795    0.9609    0.7961      2074

avg / total     0.9460    0.9268    0.9317     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9917    0.9168    0.9527     11965
###   Occupied     0.6538    0.9534    0.7757      1973

avg / total     0.9439    0.9219    0.9277     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-16

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-16

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-16.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9631    0.9003    0.9307     11900
   Occupied     0.7605    0.9018    0.8252      4176

avg / total     0.9105    0.9007    0.9033     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9637    0.8962    0.9287     11980
###   Occupied     0.7481    0.9011    0.8175      4096

avg / total     0.9087    0.8975    0.9004     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-16
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-16

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-16

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-16.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9804    0.9128    0.9454     11902
   Occupied     0.7379    0.9309    0.8233      3140

avg / total     0.9298    0.9166    0.9199     15042
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9810    0.9179    0.9484     11942
###   Occupied     0.7464    0.9316    0.8288      3100

avg / total     0.9327    0.9207    0.9237     15042


# pr-18 rc-21

## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-21

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-21

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-21.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9868    0.8673    0.9232     11865
   Occupied     0.5516    0.9335    0.6934      2074

avg / total     0.9220    0.8772    0.8890     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9853    0.8656    0.9216     11965
###   Occupied     0.5306    0.9214    0.6735      1973

avg / total     0.9209    0.8735    0.8864     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-21

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-21

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-21.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9546    0.9024    0.9278     11900
   Occupied     0.7594    0.8776    0.8143      4176

avg / total     0.9039    0.8960    0.8983     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9555    0.9003    0.9271     11980
###   Occupied     0.7505    0.8774    0.8090      4096

avg / total     0.9033    0.8944    0.8970     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-21
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-21

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-21

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-21.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9769    0.8949    0.9341     11902
   Occupied     0.6978    0.9197    0.7935      3140

avg / total     0.9186    0.9001    0.9047     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9783    0.8968    0.9357     11942
###   Occupied     0.6989    0.9232    0.7956      3100

avg / total     0.9207    0.9022    0.9068     15042

# pr-18 rc-22
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-22

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-22

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-22.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9904    0.8989    0.9424     11865
   Occupied     0.6216    0.9503    0.7516      2074

avg / total     0.9356    0.9065    0.9140     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9892    0.8974    0.9411     11965
###   Occupied     0.6018    0.9407    0.7340      1973

avg / total     0.9344    0.9035    0.9118     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-22

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-22

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-22.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9711    0.9224    0.9461     11900
   Occupied     0.8064    0.9217    0.8602      4176

avg / total     0.9283    0.9222    0.9238     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9710    0.9184    0.9440     11980
###   Occupied     0.7941    0.9197    0.8523      4096

avg / total     0.9259    0.9188    0.9206     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-22
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-22

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-22

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-22.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9823    0.9099    0.9447     11902
   Occupied     0.7331    0.9379    0.8230      3140

avg / total     0.9303    0.9158    0.9193     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9811    0.9109    0.9447     11942
###   Occupied     0.7310    0.9326    0.8196      3100

avg / total     0.9296    0.9154    0.9189     15042

# pr-18 rc-23
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-23

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-23

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-23.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9929    0.9246    0.9575     11865
   Occupied     0.6903    0.9619    0.8038      2074

avg / total     0.9478    0.9301    0.9346     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9916    0.9255    0.9574     11965
###   Occupied     0.6783    0.9524    0.7923      1973

avg / total     0.9472    0.9293    0.9341     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-23

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-23

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-23.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9760    0.9345    0.9548     11900
   Occupied     0.8334    0.9346    0.8811      4176

avg / total     0.9390    0.9345    0.9357     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9773    0.9316    0.9539     11980
###   Occupied     0.8239    0.9368    0.8767      4096

avg / total     0.9382    0.9329    0.9342     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-23
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-23

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-23

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-23.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9848    0.9347    0.9591     11902
   Occupied     0.7925    0.9452    0.8622      3140

avg / total     0.9446    0.9369    0.9389     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9854    0.9355    0.9598     11942
###   Occupied     0.7921    0.9465    0.8624      3100

avg / total     0.9455    0.9378    0.9397     15042

# pr-18 rc-24
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-24

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-24

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-24.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9928    0.9279    0.9592     11865
   Occupied     0.6996    0.9614    0.8099      2074

avg / total     0.9492    0.9329    0.9370     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9924    0.9265    0.9583     11965
###   Occupied     0.6823    0.9569    0.7966      1973

avg / total     0.9485    0.9308    0.9354     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-24

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-24

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-24.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9821    0.9566    0.9692     11900
   Occupied     0.8848    0.9504    0.9164      4176

avg / total     0.9568    0.9550    0.9555     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9827    0.9517    0.9669     11980
###   Occupied     0.8706    0.9509    0.9090      4096

avg / total     0.9541    0.9515    0.9522     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-24
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-24

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-24

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-24.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9854    0.9419    0.9631     11902
   Occupied     0.8112    0.9471    0.8739      3140

avg / total     0.9491    0.9430    0.9445     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9861    0.9390    0.9620     11942
###   Occupied     0.8016    0.9490    0.8691      3100

avg / total     0.9481    0.9411    0.9429     15042


# pr-18 rc-25
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-25

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-25

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-25.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9932    0.9383    0.9650     11865
   Occupied     0.7319    0.9634    0.8318      2074

avg / total     0.9543    0.9420    0.9452     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9940    0.9345    0.9633     11965
###   Occupied     0.7086    0.9660    0.8175      1973

avg / total     0.9536    0.9389    0.9427     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-25

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-25

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-25.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9932    0.9383    0.9650     11865
   Occupied     0.7319    0.9634    0.8318      2074

avg / total     0.9543    0.9420    0.9452     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9940    0.9345    0.9633     11965
###  Occupied     0.7086    0.9660    0.8175      1973

avg / total     0.9536    0.9389    0.9427     13938

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-25
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-25

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-25

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-25.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9880    0.9482    0.9677     11902
   Occupied     0.8296    0.9564    0.8885      3140

avg / total     0.9549    0.9499    0.9511     15042
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9862    0.9479    0.9667     11942
###  Occupied     0.8255    0.9490    0.8830      3100

avg / total     0.9531    0.9481    0.9494     15042




# pr-18 rc-26
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-26

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-26

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-26.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9941    0.9402    0.9664     11865
   Occupied     0.7391    0.9682    0.8382      2074

avg / total     0.9562    0.9444    0.9474     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9935    0.9389    0.9654     11965
###  Occupied     0.7221    0.9625    0.8251      1973

avg / total     0.9550    0.9422    0.9456     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-26

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-26

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-26.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9816    0.9526    0.9669     11900
   Occupied     0.8754    0.9492    0.9108      4176

avg / total     0.9541    0.9517    0.9523     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9824    0.9477    0.9647     11980
###   Occupied     0.8613    0.9504    0.9037      4096

avg / total     0.9516    0.9484    0.9492     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-26
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-26

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-26

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-26.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9863    0.9484    0.9670     11902
   Occupied     0.8293    0.9500    0.8856      3140

avg / total     0.9535    0.9487    0.9500     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9861    0.9479    0.9667     11942
###   Occupied     0.8254    0.9487    0.8828      3100

avg / total     0.9530    0.9481    0.9494     15042


# pr-18 rc-31

## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-31

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-31

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-31.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9905    0.9098    0.9484     11865
   Occupied     0.6480    0.9499    0.7704      2074

avg / total     0.9395    0.9158    0.9219     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9897    0.9065    0.9463     11965
###   Occupied     0.6244    0.9427    0.7512      1973

avg / total     0.9380    0.9116    0.9186     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-31

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-31

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-31.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9556    0.9041    0.9291     11900
   Occupied     0.7631    0.8803    0.8175      4176

avg / total     0.9056    0.8979    0.9001     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9570    0.9001    0.9276     11980
###   Occupied     0.7510    0.8816    0.8111      4096

avg / total     0.9045    0.8954    0.8980     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-31
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-31

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-31

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-31.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9814    0.9131    0.9460     11902
   Occupied     0.7394    0.9344    0.8255      3140

avg / total     0.9309    0.9176    0.9209     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9805    0.9181    0.9483     11942
###   Occupied     0.7466    0.9297    0.8282      3100

avg / total     0.9323    0.9205    0.9235     15042

# pr-18 rc-32
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-32

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-32

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-32.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9937    0.9512    0.9720     11865
   Occupied     0.7758    0.9658    0.8604      2074

avg / total     0.9613    0.9534    0.9554     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9920    0.9519    0.9716     11965
###   Occupied     0.7659    0.9534    0.8494      1973

avg / total     0.9600    0.9521    0.9543     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-32

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-32

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-32.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9730    0.9257    0.9488     11900
   Occupied     0.8141    0.9267    0.8667      4176

avg / total     0.9317    0.9260    0.9275     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9724    0.9275    0.9494     11980
###   Occupied     0.8131    0.9231    0.8646      4096

avg / total     0.9318    0.9263    0.9278     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-32
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-32

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-32

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-32.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9859    0.9387    0.9617     11902
   Occupied     0.8035    0.9490    0.8702      3140

avg / total     0.9478    0.9409    0.9426     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9841    0.9406    0.9619     11942
###   Occupied     0.8045    0.9413    0.8675      3100

avg / total     0.9471    0.9408    0.9424     15042

# pr-18 rc-33
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-33

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-33

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-33.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9953    0.9697    0.9824     11865
   Occupied     0.8491    0.9740    0.9073      2074

avg / total     0.9736    0.9704    0.9712     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9948    0.9697    0.9821     11965
###   Occupied     0.8404    0.9691    0.9002      1973

avg / total     0.9729    0.9696    0.9705     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-33

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-33

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-33.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9773    0.9391    0.9578     11900
   Occupied     0.8438    0.9377    0.8883      4176

avg / total     0.9426    0.9387    0.9397     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9767    0.9422    0.9592     11980
###   Occupied     0.8469    0.9343    0.8885      4096

avg / total     0.9436    0.9402    0.9412     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-33
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-33

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-33

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-33.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9868    0.9529    0.9696     11902
   Occupied     0.8422    0.9516    0.8935      3140

avg / total     0.9566    0.9527    0.9537     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9864    0.9536    0.9697     11942
###   Occupied     0.8416    0.9494    0.8922      3100

avg / total     0.9566    0.9527    0.9538     15042

# pr-18 rc-34
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-34

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-34

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-34.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9960    0.9679    0.9817     11865
   Occupied     0.8418    0.9778    0.9048      2074

avg / total     0.9731    0.9694    0.9703     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9955    0.9682    0.9817     11965
###   Occupied     0.8345    0.9736    0.8987      1973

avg / total     0.9727    0.9689    0.9699     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-34

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-34

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-34.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9827    0.9558    0.9691     11900
   Occupied     0.8832    0.9521    0.9163      4176

avg / total     0.9569    0.9548    0.9554     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9833    0.9541    0.9685     11980
###   Occupied     0.8765    0.9526    0.9130      4096

avg / total     0.9561    0.9537    0.9543     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-34

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-34

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-34.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9894    0.9566    0.9727     11902
   Occupied     0.8537    0.9611    0.9043      3140

avg / total     0.9611    0.9575    0.9584     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9887    0.9606    0.9744     11942
###   Occupied     0.8631    0.9577    0.9080      3100

avg / total     0.9628    0.9600    0.9607     15042

# pr-18 rc-35
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-35

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-35

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-35.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9967    0.9780    0.9872     11865
   Occupied     0.8863    0.9812    0.9314      2074

avg / total     0.9802    0.9785    0.9789     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9968    0.9774    0.9870     11965
###   Occupied     0.8776    0.9807    0.9263      1973

avg / total     0.9799    0.9779    0.9784     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-35

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-35

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-35.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9813    0.9555    0.9682     11900
   Occupied     0.8819    0.9480    0.9138      4176

avg / total     0.9555    0.9535    0.9541     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9826    0.9510    0.9665     11980
###   Occupied     0.8690    0.9507    0.9080      4096

avg / total     0.9536    0.9509    0.9516     16076


## right

Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-35
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-35

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-35

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-35.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9919    0.9595    0.9754     11902
   Occupied     0.8634    0.9704    0.9138      3140

avg / total     0.9651    0.9618    0.9626     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9897    0.9638    0.9766     11942
###   Occupied     0.8734    0.9613    0.9152      3100

avg / total     0.9657    0.9633    0.9639     15042


# pr-18 rc-36
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-36

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-36

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-36.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9966    0.9769    0.9866     11865
   Occupied     0.8813    0.9807    0.9283      2074

avg / total     0.9794    0.9775    0.9780     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9963    0.9778    0.9869     11965
###   Occupied     0.8788    0.9777    0.9256      1973

avg / total     0.9796    0.9778    0.9782     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-36

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-36

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-36.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9817    0.9554    0.9684     11900
   Occupied     0.8819    0.9492    0.9143      4176

avg / total     0.9558    0.9538    0.9543     16076
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9841    0.9563    0.9700     11980
###   Occupied     0.8820    0.9548    0.9170      4096

avg / total     0.9581    0.9560    0.9565     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-36
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-36

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-36

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-36.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9929    0.9682    0.9804     11902
   Occupied     0.8897    0.9739    0.9299      3140

avg / total     0.9714    0.9694    0.9699     15042
For test set (20%):
             precision    recall  f1-score   support
###     Empty     0.9907    0.9666    0.9785     11942
###  Occupied     0.8823    0.9652    0.9219      3100

avg / total     0.9684    0.9663    0.9668     15042


