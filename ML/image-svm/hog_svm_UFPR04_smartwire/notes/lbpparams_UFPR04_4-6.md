<!-- TOC -->

- [pr-18 rc-41](#pr-18-rc-41)
    - [left](#left)
        - [Empty     0.9883    0.9227    0.9544     11965](#empty-----09883----09227----09544-----11965)
        - [Occupied     0.6657    0.9336    0.7772      1973](#occupied-----06657----09336----07772------1973)
    - [center](#center)
        - [Empty     0.9599    0.9117    0.9352     11980](#empty-----09599----09117----09352-----11980)
        - [Occupied     0.7748    0.8887    0.8278      4096](#occupied-----07748----08887----08278------4096)
    - [right](#right)
        - [Empty     0.9834    0.9297    0.9558     11942](#empty-----09834----09297----09558-----11942)
        - [Occupied     0.7762    0.9397    0.8501      3100](#occupied-----07762----09397----08501------3100)
- [pr-18 rc-42](#pr-18-rc-42)
    - [left](#left-1)
        - [Empty     0.9917    0.9539    0.9724     11965](#empty-----09917----09539----09724-----11965)
        - [Occupied     0.7727    0.9513    0.8528      1973](#occupied-----07727----09513----08528------1973)
    - [center](#center-1)
        - [Empty     0.9770    0.9418    0.9591     11980](#empty-----09770----09418----09591-----11980)
        - [Occupied     0.8460    0.9351    0.8883      4096](#occupied-----08460----09351----08883------4096)
    - [right](#right-1)
        - [Empty     0.9875    0.9529    0.9699     11942](#empty-----09875----09529----09699-----11942)
        - [Occupied     0.8400    0.9535    0.8932      3100](#occupied-----08400----09535----08932------3100)
- [pr-18 rc-43](#pr-18-rc-43)
    - [left](#left-2)
        - [Empty     0.9950    0.9706    0.9827     11965](#empty-----09950----09706----09827-----11965)
        - [Occupied     0.8447    0.9706    0.9033      1973](#occupied-----08447----09706----09033------1973)
    - [center](#center-2)
        - [Empty     0.9804    0.9456    0.9627     11980](#empty-----09804----09456----09627-----11980)
        - [Occupied     0.8558    0.9446    0.8980      4096](#occupied-----08558----09446----08980------4096)
    - [right](#right-2)
        - [Empty     0.9897    0.9611    0.9752     11942](#empty-----09897----09611----09752-----11942)
        - [Occupied     0.8651    0.9616    0.9108      3100](#occupied-----08651----09616----09108------3100)
- [pr-18 rc-44](#pr-18-rc-44)
    - [left](#left-3)
        - [Empty     0.9952    0.9742    0.9846     11965](#empty-----09952----09742----09846-----11965)
        - [Occupied     0.8612    0.9716    0.9131      1973](#occupied-----08612----09716----09131------1973)
    - [center](#center-3)
        - [Empty     0.9853    0.9614    0.9732     11980](#empty-----09853----09614----09732-----11980)
        - [Occupied     0.8947    0.9580    0.9253      4096](#occupied-----08947----09580----09253------4096)
    - [right](#right-3)
        - [Empty     0.9926    0.9655    0.9789     11942](#empty-----09926----09655----09789-----11942)
        - [Occupied     0.8797    0.9723    0.9237      3100](#occupied-----08797----09723----09237------3100)
- [pr-18 rc-45](#pr-18-rc-45)
    - [left](#left-4)
        - [Empty     0.9965    0.9768    0.9866     11965](#empty-----09965----09768----09866-----11965)
        - [Occupied     0.8746    0.9792    0.9240      1973](#occupied-----08746----09792----09240------1973)
    - [center](#center-4)
        - [Empty     0.9843    0.9567    0.9703     11980](#empty-----09843----09567----09703-----11980)
        - [Occupied     0.8829    0.9553    0.9177      4096](#occupied-----08829----09553----09177------4096)
    - [right](#right-4)
        - [Empty     0.9930    0.9724    0.9826     11942](#empty-----09930----09724----09826-----11942)
        - [Occupied     0.9014    0.9735    0.9361      3100](#occupied-----09014----09735----09361------3100)
- [pr-18 rc-46](#pr-18-rc-46)
    - [left](#left-5)
        - [Empty     0.9966    0.9786    0.9875     11965](#empty-----09966----09786----09875-----11965)
        - [Occupied     0.8831    0.9797    0.9289      1973](#occupied-----08831----09797----09289------1973)
    - [center](#center-5)
        - [Empty     0.9845    0.9591    0.9716     11980](#empty-----09845----09591----09716-----11980)
        - [Occupied     0.8888    0.9558    0.9211      4096](#occupied-----08888----09558----09211------4096)
    - [right](#right-5)
        - [Empty     0.9951    0.9776    0.9863     11942](#empty-----09951----09776----09863-----11942)
        - [Occupied     0.9193    0.9813    0.9493      3100](#occupied-----09193----09813----09493------3100)
- [pr-18 rc-51](#pr-18-rc-51)
    - [left](#left-6)
        - [Empty     0.9895    0.9110    0.9486     11965](#empty-----09895----09110----09486-----11965)
        - [Occupied     0.6355    0.9412    0.7587      1973](#occupied-----06355----09412----07587------1973)
    - [center](#center-6)
        - [Empty     0.9664    0.9342    0.9500     11980](#empty-----09664----09342----09500-----11980)
        - [Occupied     0.8247    0.9050    0.8630      4096](#occupied-----08247----09050----08630------4096)
    - [right](#right-6)
        - [Empty     0.9832    0.9267    0.9541     11942](#empty-----09832----09267----09541-----11942)
        - [Occupied     0.7689    0.9390    0.8455      3100](#occupied-----07689----09390----08455------3100)
- [pr-18 rc-52](#pr-18-rc-52)
    - [left](#left-7)
        - [Empty     0.9933    0.9509    0.9716     11965](#empty-----09933----09509----09716-----11965)
        - [Occupied     0.7636    0.9610    0.8510      1973](#occupied-----07636----09610----08510------1973)
    - [center](#center-7)
        - [Empty     0.9817    0.9563    0.9688     11980](#empty-----09817----09563----09688-----11980)
        - [Occupied     0.8811    0.9478    0.9132      4096](#occupied-----08811----09478----09132------4096)
    - [right](#right-7)
        - [Empty     0.9864    0.9503    0.9680     11942](#empty-----09864----09503----09680-----11942)
        - [Occupied     0.8323    0.9494    0.8870      3100](#occupied-----08323----09494----08870------3100)
- [pr-18 rc-53](#pr-18-rc-53)
    - [left](#left-8)
        - [Empty     0.9949    0.9683    0.9814     11965](#empty-----09949----09683----09814-----11965)
        - [Occupied     0.8347    0.9701    0.8973      1973](#occupied-----08347----09701----08973------1973)
    - [center](#center-8)
        - [Empty     0.9820    0.9541    0.9678     11980](#empty-----09820----09541----09678-----11980)
        - [Occupied     0.8760    0.9487    0.9109      4096](#occupied-----08760----09487----09109------4096)
    - [right](#right-8)
        - [Empty     0.9878    0.9565    0.9719     11942](#empty-----09878----09565----09719-----11942)
        - [Occupied     0.8505    0.9545    0.8995      3100](#occupied-----08505----09545----08995------3100)
- [pr-18 rc-54](#pr-18-rc-54)
    - [left](#left-9)
        - [Empty     0.9948    0.9696    0.9820     11965](#empty-----09948----09696----09820-----11965)
        - [Occupied     0.8401    0.9691    0.9000      1973](#occupied-----08401----09691----09000------1973)
    - [center](#center-9)
        - [Empty     0.9845    0.9632    0.9737     11980](#empty-----09845----09632----09737-----11980)
        - [Occupied     0.8987    0.9556    0.9263      4096](#occupied-----08987----09556----09263------4096)
    - [right](#right-9)
        - [Empty     0.9901    0.9658    0.9778     11942](#empty-----09901----09658----09778-----11942)
        - [Occupied     0.8798    0.9629    0.9195      3100](#occupied-----08798----09629----09195------3100)
- [pr-18 rc-55](#pr-18-rc-55)
    - [left](#left-10)
        - [Empty     0.9968    0.9762    0.9864     11965](#empty-----09968----09762----09864-----11965)
        - [Occupied     0.8717    0.9812    0.9232      1973](#occupied-----08717----09812----09232------1973)
    - [center](#center-10)
        - [Empty     0.9853    0.9644    0.9747     11980](#empty-----09853----09644----09747-----11980)
        - [Occupied     0.9019    0.9580    0.9291      4096](#occupied-----09019----09580----09291------4096)
    - [right](#right-10)
        - [Empty     0.9918    0.9703    0.9809     11942](#empty-----09918----09703----09809-----11942)
        - [Occupied     0.8943    0.9690    0.9302      3100](#occupied-----08943----09690----09302------3100)
- [pr-18 rc-56](#pr-18-rc-56)
    - [left](#left-11)
        - [Empty     0.9963    0.9793    0.9877     11965](#empty-----09963----09793----09877-----11965)
        - [Occupied     0.8861    0.9782    0.9299      1973](#occupied-----08861----09782----09299------1973)
    - [center](#center-11)
        - [Empty     0.9871    0.9618    0.9743     11980](#empty-----09871----09618----09743-----11980)
        - [Occupied     0.8960    0.9631    0.9283      4096](#occupied-----08960----09631----09283------4096)
    - [right](#right-11)
        - [Empty     0.9935    0.9758    0.9846     11942](#empty-----09935----09758----09846-----11942)
        - [Occupied     0.9128    0.9755    0.9431      3100](#occupied-----09128----09755----09431------3100)
- [pr-18 rc-61](#pr-18-rc-61)
    - [left](#left-12)
        - [Empty     0.9897    0.9214    0.9543     11965](#empty-----09897----09214----09543-----11965)
        - [Occupied     0.6638    0.9417    0.7787      1973](#occupied-----06638----09417----07787------1973)
    - [center](#center-12)
        - [Empty     0.9677    0.9316    0.9493     11980](#empty-----09677----09316----09493-----11980)
        - [Occupied     0.8195    0.9092    0.8620      4096](#occupied-----08195----09092----08620------4096)
    - [right](#right-12)
        - [Empty     0.9835    0.9272    0.9545     11942](#empty-----09835----09272----09545-----11942)
        - [Occupied     0.7703    0.9400    0.8467      3100](#occupied-----07703----09400----08467------3100)
- [pr-18 rc-62](#pr-18-rc-62)
    - [left](#left-13)
        - [Empty     0.9929    0.9570    0.9746     11965](#empty-----09929----09570----09746-----11965)
        - [Occupied     0.7860    0.9584    0.8637      1973](#occupied-----07860----09584----08637------1973)
    - [center](#center-13)
        - [Empty     0.9811    0.9583    0.9696     11980](#empty-----09811----09583----09696-----11980)
        - [Occupied     0.8859    0.9460    0.9150      4096](#occupied-----08859----09460----09150------4096)
    - [right](#right-13)
        - [Empty     0.9871    0.9490    0.9677     11942](#empty-----09871----09490----09677-----11942)
        - [Occupied     0.8290    0.9523    0.8864      3100](#occupied-----08290----09523----08864------3100)
- [pr-18 rc-63](#pr-18-rc-63)
    - [left](#left-14)
        - [Empty     0.9949    0.9729    0.9838     11965](#empty-----09949----09729----09838-----11965)
        - [Occupied     0.8552    0.9696    0.9088      1973](#occupied-----08552----09696----09088------1973)
    - [center](#center-14)
        - [Empty     0.9826    0.9594    0.9709     11980](#empty-----09826----09594----09709-----11980)
        - [Occupied     0.8890    0.9502    0.9186      4096](#occupied-----08890----09502----09186------4096)
    - [right](#right-14)
        - [Empty     0.9881    0.9588    0.9732     11942](#empty-----09881----09588----09732-----11942)
        - [Occupied     0.8576    0.9555    0.9039      3100](#occupied-----08576----09555----09039------3100)
- [pr-18 rc-64](#pr-18-rc-64)
    - [left](#left-15)
        - [Empty     0.9955    0.9748    0.9850     11965](#empty-----09955----09748----09850-----11965)
        - [Occupied     0.8641    0.9731    0.9154      1973](#occupied-----08641----09731----09154------1973)
    - [center](#center-15)
        - [Empty     0.9854    0.9661    0.9756     11980](#empty-----09854----09661----09756-----11980)
        - [Occupied     0.9062    0.9580    0.9314      4096](#occupied-----09062----09580----09314------4096)
    - [right](#right-15)
        - [Empty     0.9904    0.9634    0.9767     11942](#empty-----09904----09634----09767-----11942)
        - [Occupied     0.8724    0.9639    0.9159      3100](#occupied-----08724----09639----09159------3100)
- [pr-18 rc-65](#pr-18-rc-65)
    - [left](#left-16)
        - [Empty     0.9971    0.9790    0.9880     11965](#empty-----09971----09790----09880-----11965)
        - [Occupied     0.8854    0.9828    0.9315      1973](#occupied-----08854----09828----09315------1973)
    - [center](#center-16)
        - [Empty     0.9841    0.9652    0.9745     11980](#empty-----09841----09652----09745-----11980)
        - [Occupied     0.9036    0.9543    0.9283      4096](#occupied-----09036----09543----09283------4096)
    - [right](#right-16)
        - [Empty     0.9914    0.9683    0.9797     11942](#empty-----09914----09683----09797-----11942)
        - [Occupied     0.8878    0.9677    0.9261      3100](#occupied-----08878----09677----09261------3100)
- [pr-18 rc-66](#pr-18-rc-66)
    - [left](#left-17)
        - [Empty     0.9966    0.9803    0.9884     11965](#empty-----09966----09803----09884-----11965)
        - [Occupied     0.8912    0.9797    0.9334      1973](#occupied-----08912----09797----09334------1973)
    - [center](#center-17)
        - [Empty     0.9856    0.9622    0.9738     11980](#empty-----09856----09622----09738-----11980)
        - [Occupied     0.8966    0.9590    0.9267      4096](#occupied-----08966----09590----09267------4096)
    - [right](#right-17)
        - [Empty     0.9926    0.9729    0.9826     11942](#empty-----09926----09729----09826-----11942)
        - [Occupied     0.9029    0.9719    0.9362      3100](#occupied-----09029----09719----09362------3100)

<!-- /TOC -->

# pr-18 rc-41

## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-41

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-41

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-41.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9879    0.9229    0.9543     11865
   Occupied     0.6795    0.9354    0.7872      2074

avg / total     0.9420    0.9247    0.9294     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9883    0.9227    0.9544     11965
###   Occupied     0.6657    0.9336    0.7772      1973

avg / total     0.9426    0.9242    0.9293     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-41

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-41

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-41.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9615    0.9169    0.9387     11900
   Occupied     0.7908    0.8954    0.8398      4176

avg / total     0.9172    0.9113    0.9130     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9599    0.9117    0.9352     11980
###   Occupied     0.7748    0.8887    0.8278      4096

avg / total     0.9128    0.9058    0.9078     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-41
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-41

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-41

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-41.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9848    0.9248    0.9539     11902
   Occupied     0.7684    0.9459    0.8480      3140

avg / total     0.9396    0.9292    0.9318     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9834    0.9297    0.9558     11942
###   Occupied     0.7762    0.9397    0.8501      3100

avg / total     0.9407    0.9317    0.9340     15042

# pr-18 rc-42
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-42

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-42

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-42.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9914    0.9569    0.9738     11865
   Occupied     0.7944    0.9523    0.8662      2074

avg / total     0.9621    0.9562    0.9578     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9917    0.9539    0.9724     11965
###   Occupied     0.7727    0.9513    0.8528      1973

avg / total     0.9607    0.9535    0.9555     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-42

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-42

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-42.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9775    0.9408    0.9588     11900
   Occupied     0.8477    0.9382    0.8907      4176

avg / total     0.9438    0.9402    0.9411     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9770    0.9418    0.9591     11980
###   Occupied     0.8460    0.9351    0.8883      4096

avg / total     0.9436    0.9401    0.9410     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-42
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-42

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-42

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-42.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9880    0.9529    0.9701     11902
   Occupied     0.8428    0.9561    0.8959      3140

avg / total     0.9577    0.9536    0.9546     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9875    0.9529    0.9699     11942
###   Occupied     0.8400    0.9535    0.8932      3100

avg / total     0.9571    0.9530    0.9541     15042

# pr-18 rc-43
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-43

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-43

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-43.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9953    0.9733    0.9841     11865
   Occupied     0.8643    0.9735    0.9156      2074

avg / total     0.9758    0.9733    0.9740     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9950    0.9706    0.9827     11965
###   Occupied     0.8447    0.9706    0.9033      1973

avg / total     0.9738    0.9706    0.9714     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-43

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-43

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-43.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9783    0.9455    0.9616     11900
   Occupied     0.8583    0.9401    0.8974      4176

avg / total     0.9471    0.9441    0.9449     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9804    0.9456    0.9627     11980
###   Occupied     0.8558    0.9446    0.8980      4096

avg / total     0.9486    0.9453    0.9462     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-43
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-43

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-43

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-43.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9899    0.9593    0.9744     11902
   Occupied     0.8620    0.9627    0.9096      3140

avg / total     0.9632    0.9600    0.9608     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9897    0.9611    0.9752     11942
###   Occupied     0.8651    0.9616    0.9108      3100

avg / total     0.9640    0.9612    0.9619     15042

# pr-18 rc-44
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-44

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-44

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-44.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9951    0.9768    0.9859     11865
   Occupied     0.8800    0.9725    0.9240      2074

avg / total     0.9780    0.9762    0.9767     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9952    0.9742    0.9846     11965
###   Occupied     0.8612    0.9716    0.9131      1973

avg / total     0.9762    0.9738    0.9745     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-44

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-44

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-44.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9845    0.9596    0.9719     11900
   Occupied     0.8926    0.9569    0.9236      4176

avg / total     0.9606    0.9589    0.9593     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9853    0.9614    0.9732     11980
###   Occupied     0.8947    0.9580    0.9253      4096

avg / total     0.9622    0.9606    0.9610     16076

## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-44
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-44

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-44

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_4/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-44.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9914    0.9663    0.9787     11902
   Occupied     0.8835    0.9682    0.9239      3140

avg / total     0.9689    0.9667    0.9672     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9926    0.9655    0.9789     11942
###   Occupied     0.8797    0.9723    0.9237      3100

avg / total     0.9693    0.9669    0.9675     15042

# pr-18 rc-45
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-45

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-45

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-45.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9967    0.9797    0.9881     11865
   Occupied     0.8942    0.9817    0.9359      2074

avg / total     0.9815    0.9800    0.9804     13939
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9965    0.9768    0.9866     11965
###   Occupied     0.8746    0.9792    0.9240      1973

avg / total     0.9792    0.9772    0.9777     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-45

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-45

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-45.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9822    0.9569    0.9694     11900
   Occupied     0.8856    0.9507    0.9170      4176

avg / total     0.9571    0.9553    0.9558     16076
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9843    0.9567    0.9703     11980
###   Occupied     0.8829    0.9553    0.9177      4096

avg / total     0.9585    0.9563    0.9569     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-45
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-45

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-45

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-45.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9936    0.9722    0.9828     11902
   Occupied     0.9026    0.9764    0.9380      3140

avg / total     0.9746    0.9731    0.9735     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9930    0.9724    0.9826     11942
###   Occupied     0.9014    0.9735    0.9361      3100

avg / total     0.9741    0.9726    0.9730     15042



# pr-18 rc-46
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-46

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-46

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-46.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9963    0.9812    0.9887     11865
   Occupied     0.9011    0.9793    0.9385      2074

avg / total     0.9821    0.9809    0.9812     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9966    0.9786    0.9875     11965
###  Occupied     0.8831    0.9797    0.9289      1973

avg / total     0.9805    0.9788    0.9792     13938

## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-46

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-46

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-46.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9847    0.9621    0.9733     11900
   Occupied     0.8986    0.9574    0.9271      4176

avg / total     0.9623    0.9609    0.9613     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9845    0.9591    0.9716     11980
###  Occupied     0.8888    0.9558    0.9211      4096

avg / total     0.9601    0.9583    0.9587     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-46

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-46

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-46.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9933    0.9777    0.9854     11902
   Occupied     0.9200    0.9748    0.9467      3140

avg / total     0.9780    0.9771    0.9773     15042
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9951    0.9776    0.9863     11942
###   Occupied     0.9193    0.9813    0.9493      3100

avg / total     0.9794    0.9784    0.9787     15042


# pr-18 rc-51
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-51

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-51

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-51.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9902    0.9152    0.9512     11865
   Occupied     0.6615    0.9479    0.7792      2074

avg / total     0.9413    0.9201    0.9256     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9895    0.9110    0.9486     11965
###   Occupied     0.6355    0.9412    0.7587      1973

avg / total     0.9394    0.9153    0.9217     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-51

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-51

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-51.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9695    0.9366    0.9528     11900
   Occupied     0.8353    0.9159    0.8738      4176

avg / total     0.9346    0.9313    0.9323     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9664    0.9342    0.9500     11980
###  Occupied     0.8247    0.9050    0.8630      4096

avg / total     0.9303    0.9268    0.9279     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-51
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-51

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-51

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-51.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9819    0.9219    0.9509     11902
   Occupied     0.7596    0.9357    0.8385      3140

avg / total     0.9355    0.9247    0.9275     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9832    0.9267    0.9541     11942
###   Occupied     0.7689    0.9390    0.8455      3100

avg / total     0.9390    0.9293    0.9317     15042



# pr-18 rc-52
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-52

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-52

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-52.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9942    0.9501    0.9716     11865
   Occupied     0.7723    0.9682    0.8592      2074

avg / total     0.9612    0.9528    0.9549     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9933    0.9509    0.9716     11965
###   Occupied     0.7636    0.9610    0.8510      1973

avg / total     0.9608    0.9524    0.9546     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-52

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-52

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-52.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9834    0.9591    0.9711     11900
   Occupied     0.8911    0.9538    0.9214      4176

avg / total     0.9594    0.9577    0.9582     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9817    0.9563    0.9688     11980
###   Occupied     0.8811    0.9478    0.9132      4096

avg / total     0.9560    0.9541    0.9546     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-52
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-52

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-52

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-52.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9857    0.9472    0.9661     11902
   Occupied     0.8256    0.9481    0.8826      3140

avg / total     0.9523    0.9473    0.9486     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9864    0.9503    0.9680     11942
###   Occupied     0.8323    0.9494    0.8870      3100

avg / total     0.9546    0.9501    0.9513     15042


# pr-18 rc-53
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-53

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-53

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-53.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9950    0.9716    0.9832     11865
   Occupied     0.8568    0.9720    0.9108      2074

avg / total     0.9744    0.9717    0.9724     13939
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9949    0.9683    0.9814     11965
###    Occupied     0.8347    0.9701    0.8973      1973

avg / total     0.9723    0.9686    0.9695     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-53

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-53

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-53.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9822    0.9557    0.9688     11900
   Occupied     0.8828    0.9507    0.9155      4176

avg / total     0.9564    0.9544    0.9549     16076
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9820    0.9541    0.9678     11980
###   Occupied     0.8760    0.9487    0.9109      4096

avg / total     0.9550    0.9527    0.9533     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-53

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-53

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-53.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9877    0.9538    0.9705     11902
   Occupied     0.8450    0.9551    0.8967      3140

avg / total     0.9579    0.9541    0.9551     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9878    0.9565    0.9719     11942
###   Occupied     0.8505    0.9545    0.8995      3100

avg / total     0.9595    0.9561    0.9570     15042


# pr-18 rc-54
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-54

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-54

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-54.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9961    0.9724    0.9841     11865
   Occupied     0.8608    0.9783    0.9158      2074

avg / total     0.9760    0.9732    0.9739     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9948    0.9696    0.9820     11965
###   Occupied     0.8401    0.9691    0.9000      1973

avg / total     0.9729    0.9695    0.9704     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-54

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-54

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-54.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9854    0.9658    0.9755     11900
   Occupied     0.9078    0.9593    0.9328      4176

avg / total     0.9653    0.9641    0.9644     16076
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9845    0.9632    0.9737     11980
###   Occupied     0.8987    0.9556    0.9263      4096

avg / total     0.9626    0.9612    0.9616     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-54
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-54

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-54

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-54.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9905    0.9666    0.9784     11902
   Occupied     0.8839    0.9650    0.9227      3140

avg / total     0.9683    0.9662    0.9668     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9901    0.9658    0.9778     11942
###   Occupied     0.8798    0.9629    0.9195      3100

avg / total     0.9674    0.9652    0.9658     15042


# pr-18 rc-55
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-55

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-55

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-55.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9964    0.9796    0.9879     11865
   Occupied     0.8936    0.9797    0.9347      2074

avg / total     0.9811    0.9796    0.9800     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9968    0.9762    0.9864     11965
###   Occupied     0.8717    0.9812    0.9232      1973

avg / total     0.9791    0.9769    0.9775     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-55

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-55

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-55.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9860    0.9676    0.9767     11900
   Occupied     0.9122    0.9607    0.9359      4176

avg / total     0.9668    0.9658    0.9661     16076
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9853    0.9644    0.9747     11980
###   Occupied     0.9019    0.9580    0.9291      4096

avg / total     0.9641    0.9627    0.9631     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-55
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-55

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-55

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-55.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9923    0.9700    0.9810     11902
   Occupied     0.8952    0.9713    0.9317      3140

avg / total     0.9720    0.9703    0.9707     15042
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9918    0.9703    0.9809     11942
###    Occupied     0.8943    0.9690    0.9302      3100

avg / total     0.9717    0.9700    0.9705     15042


# pr-18 rc-56
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-56

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-56

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-56.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9961    0.9793    0.9876     11865
   Occupied     0.8919    0.9783    0.9331      2074

avg / total     0.9806    0.9791    0.9795     13939
For test set (20%):
             precision    recall  f1-score   support

###       Empty     0.9963    0.9793    0.9877     11965
###    Occupied     0.8861    0.9782    0.9299      1973

avg / total     0.9807    0.9791    0.9795     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-56

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-56

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-56.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9866    0.9672    0.9768     11900
   Occupied     0.9116    0.9626    0.9364      4176

avg / total     0.9671    0.9660    0.9663     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9871    0.9618    0.9743     11980
###   Occupied     0.8960    0.9631    0.9283      4096

avg / total     0.9639    0.9621    0.9626     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-56
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-56

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-56

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-56.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9923    0.9765    0.9843     11902
   Occupied     0.9159    0.9713    0.9428      3140

avg / total     0.9764    0.9754    0.9757     15042
For test set (20%):
             precision    recall  f1-score   support
###     Empty     0.9935    0.9758    0.9846     11942
###   Occupied     0.9128    0.9755    0.9431      3100

avg / total     0.9769    0.9757    0.9760     15042



# pr-18 rc-61
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-61

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-61

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-61.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9900    0.9251    0.9564     11865
   Occupied     0.6883    0.9465    0.7970      2074

avg / total     0.9451    0.9283    0.9327     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9897    0.9214    0.9543     11965
###   Occupied     0.6638    0.9417    0.7787      1973

avg / total     0.9435    0.9242    0.9294     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-61

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-61

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-61.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9690    0.9376    0.9531     11900
   Occupied     0.8373    0.9145    0.8742      4176

avg / total     0.9348    0.9316    0.9326     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9677    0.9316    0.9493     11980
###   Occupied     0.8195    0.9092    0.8620      4096

avg / total     0.9300    0.9259    0.9271     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-61

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-61

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-61.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9821    0.9208    0.9504     11902
   Occupied     0.7571    0.9363    0.8372      3140

avg / total     0.9351    0.9240    0.9268     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9835    0.9272    0.9545     11942
###   Occupied     0.7703    0.9400    0.8467      3100

avg / total     0.9395    0.9299    0.9323     15042



# pr-18 rc-62
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-62

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-62

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-62.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9935    0.9571    0.9750     11865
   Occupied     0.7971    0.9643    0.8728      2074

avg / total     0.9643    0.9582    0.9598     13939
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9929    0.9570    0.9746     11965
###   Occupied     0.7860    0.9584    0.8637      1973

avg / total     0.9636    0.9572    0.9589     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-62

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-62

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-62.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9829    0.9597    0.9711     11900
   Occupied     0.8923    0.9523    0.9213      4176

avg / total     0.9593    0.9578    0.9582     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9811    0.9583    0.9696     11980
###   Occupied     0.8859    0.9460    0.9150      4096

avg / total     0.9569    0.9552    0.9557     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-62
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-62

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-62

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-62.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9867    0.9443    0.9651     11902
   Occupied     0.8185    0.9519    0.8802      3140

avg / total     0.9516    0.9459    0.9473     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9871    0.9490    0.9677     11942
###   Occupied     0.8290    0.9523    0.8864      3100

avg / total     0.9545    0.9497    0.9509     15042


# pr-18 rc-63
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-63

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-63

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-63.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9947    0.9738    0.9841     11865
   Occupied     0.8661    0.9701    0.9152      2074

avg / total     0.9755    0.9732    0.9739     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9949    0.9729    0.9838     11965
###   Occupied     0.8552    0.9696    0.9088      1973

avg / total     0.9751    0.9724    0.9732     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-63

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-63

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-63.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9825    0.9601    0.9712     11900
   Occupied     0.8932    0.9514    0.9214      4176

avg / total     0.9593    0.9578    0.9582     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9826    0.9594    0.9709     11980
###  Occupied     0.8890    0.9502    0.9186      4096

avg / total     0.9587    0.9571    0.9575     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-63
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-63

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-63

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-63.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9882    0.9578    0.9728     11902
   Occupied     0.8568    0.9567    0.9040      3140

avg / total     0.9608    0.9576    0.9584     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9881    0.9588    0.9732     11942
###   Occupied     0.8576    0.9555    0.9039      3100

avg / total     0.9612    0.9581    0.9589     15042


# pr-18 rc-64
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-64

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-64

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-64.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9959    0.9762    0.9860     11865
   Occupied     0.8778    0.9769    0.9247      2074

avg / total     0.9783    0.9763    0.9768     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9955    0.9748    0.9850     11965
###   Occupied     0.8641    0.9731    0.9154      1973

avg / total     0.9769    0.9745    0.9752     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-64

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-64

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-64.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9862    0.9676    0.9768     11900
   Occupied     0.9125    0.9614    0.9363      4176

avg / total     0.9671    0.9660    0.9663     16076
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9854    0.9661    0.9756     11980
###   Occupied     0.9062    0.9580    0.9314      4096

avg / total     0.9652    0.9640    0.9644     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-64
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-64

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-64

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-64.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9903    0.9640    0.9770     11902
   Occupied     0.8762    0.9643    0.9181      3140

avg / total     0.9665    0.9641    0.9647     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9904    0.9634    0.9767     11942
###   Occupied     0.8724    0.9639    0.9159      3100

avg / total     0.9661    0.9635    0.9642     15042


# pr-18 rc-65
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-65

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-65

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-65.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9968    0.9810    0.9888     11865
   Occupied     0.9001    0.9822    0.9394      2074

avg / total     0.9824    0.9811    0.9815     13939
For test set (20%):
             precision    recall  f1-score   support

###  Empty     0.9971    0.9790    0.9880     11965
###  Occupied     0.8854    0.9828    0.9315      1973

avg / total     0.9813    0.9796    0.9800     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-65

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-65

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-65.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9861    0.9689    0.9774     11900
   Occupied     0.9156    0.9612    0.9379      4176

avg / total     0.9678    0.9669    0.9672     16076
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9841    0.9652    0.9745     11980
###  Occupied     0.9036    0.9543    0.9283      4096

avg / total     0.9636    0.9624    0.9628     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-65
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-65

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-65

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-65.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9922    0.9674    0.9796     11902
   Occupied     0.8871    0.9710    0.9272      3140

avg / total     0.9702    0.9682    0.9687     15042
For test set (20%):
             precision    recall  f1-score   support

###     Empty     0.9914    0.9683    0.9797     11942
###  Occupied     0.8878    0.9677    0.9261      3100

avg / total     0.9701    0.9682    0.9687     15042


# pr-18 rc-66
## left
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-66

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-66

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-66.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9955    0.9817    0.9885     11865
   Occupied     0.9030    0.9744    0.9374      2074

avg / total     0.9817    0.9806    0.9809     13939
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9966    0.9803    0.9884     11965
###   Occupied     0.8912    0.9797    0.9334      1973

avg / total     0.9817    0.9802    0.9806     13938


## center
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-66

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-66

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-66.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9850    0.9641    0.9744     11900
   Occupied     0.9036    0.9581    0.9300      4176

avg / total     0.9638    0.9626    0.9629     16076
For test set (20%):
             precision    recall  f1-score   support

###   Empty     0.9856    0.9622    0.9738     11980
###  Occupied     0.8966    0.9590    0.9267      4096

avg / total     0.9629    0.9614    0.9618     16076


## right
Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-66
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-66

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-66

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-66.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9934    0.9729    0.9830     11902
   Occupied     0.9046    0.9755    0.9387      3140

avg / total     0.9749    0.9734    0.9738     15042
For test set (20%):
             precision    recall  f1-score   support

###      Empty     0.9926    0.9729    0.9826     11942
###   Occupied     0.9029    0.9719    0.9362      3100

avg / total     0.9741    0.9727    0.9730     15042
