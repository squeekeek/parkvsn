Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-4_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-4_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_wo_except_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
Confusion Report:
[[11853    12]
 [    1  2073]]
True Negative: 11853
False Negative: 1
False Positive: 12 
True Positive: 2073 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.000482160077146
False Positive Rate: 0.00101137800253

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9999    0.9990    0.9995     11865
   Occupied     0.9942    0.9995    0.9969      2074

avg / total     0.9991    0.9991    0.9991     13939
Confusion Report:
[[11949    16]
 [    2  1971]]
True Negative: 11949
False Negative: 2
False Positive: 16 
True Positive: 1971 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00101368474404
False Positive Rate: 0.00133723359799

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9998    0.9987    0.9992     11965
   Occupied     0.9919    0.9990    0.9955      1973

avg / total     0.9987    0.9987    0.9987     13938
