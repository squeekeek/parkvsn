Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_4/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_4/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_4/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-4_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_4/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_4/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-4_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_4/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-4_cell-4.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 57547, 1: 5958})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 19087, 1: 2082})
Features in test dataset (20%):
Count: 21169
Counter({0: 19235, 1: 1934})
Confusion Report:
[[19034    53]
 [    7  2075]]
True Negative: 19034
False Negative: 7
False Positive: 53 
True Positive: 2075 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00336215177714
False Positive Rate: 0.00277675905066

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9996    0.9972    0.9984     19087
   Occupied     0.9751    0.9966    0.9857      2082

avg / total     0.9972    0.9972    0.9972     21169
Confusion Report:
[[19176    59]
 [    4  1930]]
True Negative: 19176
False Negative: 4
False Positive: 59 
True Positive: 1930 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00206825232678
False Positive Rate: 0.00306732518846

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9998    0.9969    0.9984     19235
   Occupied     0.9703    0.9979    0.9839      1934

avg / total     0.9971    0.9970    0.9970     21169
