Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-34
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-34

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-34

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-34.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9827    0.9558    0.9691     11900
   Occupied     0.8832    0.9521    0.9163      4176

avg / total     0.9569    0.9548    0.9554     16076
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9833    0.9541    0.9685     11980
   Occupied     0.8765    0.9526    0.9130      4096

avg / total     0.9561    0.9537    0.9543     16076
