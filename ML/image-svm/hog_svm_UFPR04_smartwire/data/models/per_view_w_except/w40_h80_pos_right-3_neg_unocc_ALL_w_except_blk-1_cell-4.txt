Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-1_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-1_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80_blk-1_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w40_h80_pos_right-3_neg_unocc_ALL_w_except_blk-1_cell-4.model
Features in dataset (100%):
Count: 96369
Features in training dataset (60%):
Count: 57821
Counter({0: 54210, 1: 3611})
Features in cross-validation dataset (20%):
Count: 19274
Counter({0: 18083, 1: 1191})
Features in test dataset (20%):
Count: 19274
Counter({0: 18060, 1: 1214})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       0.99      0.99      0.99     18083
   Occupied       0.80      0.85      0.82      1191

avg / total       0.98      0.98      0.98     19274
For test set (20%):
             precision    recall  f1-score   support

      Empty       0.99      0.99      0.99     18060
   Occupied       0.81      0.87      0.84      1214

avg / total       0.98      0.98      0.98     19274
