Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-2_cell-10

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-2_cell-10

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w30_h80_pos_left-2_neg_unocc_ALL_w_except_blk-2_cell-10.model
Features in dataset (100%):
Count: 97338
Features in training dataset (60%):
Count: 58402
Counter({0: 57492, 1: 910})
Features in cross-validation dataset (20%):
Count: 19468
Counter({0: 19184, 1: 284})
Features in test dataset (20%):
Count: 19468
Counter({0: 19193, 1: 275})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19184
   Occupied       0.99      0.99      0.99       284

avg / total       1.00      1.00      1.00     19468
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19193
   Occupied       0.99      0.99      0.99       275

avg / total       1.00      1.00      1.00     19468
