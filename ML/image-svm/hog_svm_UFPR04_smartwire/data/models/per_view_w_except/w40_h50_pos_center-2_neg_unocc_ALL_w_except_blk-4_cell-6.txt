Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-4_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-4_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-4_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-4_cell-6.model
Features in dataset (100%):
Count: 92616
Features in training dataset (60%):
Count: 55569
Counter({0: 51103, 1: 4466})
Features in cross-validation dataset (20%):
Count: 18524
Counter({0: 17026, 1: 1498})
Features in test dataset (20%):
Count: 18523
Counter({0: 17053, 1: 1470})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     17026
   Occupied       0.98      1.00      0.99      1498

avg / total       1.00      1.00      1.00     18524
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     17053
   Occupied       0.99      0.99      0.99      1470

avg / total       1.00      1.00      1.00     18523
