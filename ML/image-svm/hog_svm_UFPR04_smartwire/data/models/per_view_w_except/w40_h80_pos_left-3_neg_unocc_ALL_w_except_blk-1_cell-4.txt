Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80_blk-1_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-1_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80_blk-1_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w40_h80_pos_left-3_neg_unocc_ALL_w_except_blk-1_cell-4.model
Features in dataset (100%):
Count: 97981
Features in training dataset (60%):
Count: 58788
Counter({0: 57492, 1: 1296})
Features in cross-validation dataset (20%):
Count: 19597
Counter({0: 19188, 1: 409})
Features in test dataset (20%):
Count: 19596
Counter({0: 19189, 1: 407})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19188
   Occupied       0.96      0.99      0.97       409

avg / total       1.00      1.00      1.00     19597
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19189
   Occupied       0.97      0.99      0.98       407

avg / total       1.00      1.00      1.00     19596
