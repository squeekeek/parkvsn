Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-2_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-2_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w40_h50_pos_center-3_neg_unocc_ALL_w_except_blk-2_cell-4.model
Features in dataset (100%):
Count: 86881
Features in training dataset (60%):
Count: 52128
Counter({0: 51094, 1: 1034})
Features in cross-validation dataset (20%):
Count: 17377
Counter({0: 17028, 1: 349})
Features in test dataset (20%):
Count: 17376
Counter({0: 17060, 1: 316})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     17028
   Occupied       0.97      0.99      0.98       349

avg / total       1.00      1.00      1.00     17377
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     17060
   Occupied       0.97      0.98      0.98       316

avg / total       1.00      1.00      1.00     17376
