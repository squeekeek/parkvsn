Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-50h-80_blk-3_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-50h-80_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-50h-80_blk-3_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w50_h80_pos_right-1_neg_unocc_ALL_w_except_blk-3_cell-8.model
Features in dataset (100%):
Count: 92354
Features in training dataset (60%):
Count: 55412
Counter({0: 54197, 1: 1215})
Features in cross-validation dataset (20%):
Count: 18471
Counter({0: 18042, 1: 429})
Features in test dataset (20%):
Count: 18471
Counter({0: 18114, 1: 357})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     18042
   Occupied       0.99      1.00      1.00       429

avg / total       1.00      1.00      1.00     18471
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     18114
   Occupied       1.00      1.00      1.00       357

avg / total       1.00      1.00      1.00     18471
