Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-4_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_w_except/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-4_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_w_except/w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-4_cell-4.model
Features in dataset (100%):
Count: 102262
Features in training dataset (60%):
Count: 61357
Counter({0: 57493, 1: 3864})
Features in cross-validation dataset (20%):
Count: 20453
Counter({0: 19205, 1: 1248})
Features in test dataset (20%):
Count: 20452
Counter({0: 19171, 1: 1281})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19205
   Occupied       0.99      1.00      0.99      1248

avg / total       1.00      1.00      1.00     20453
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     19171
   Occupied       0.99      1.00      0.99      1281

avg / total       1.00      1.00      1.00     20452
