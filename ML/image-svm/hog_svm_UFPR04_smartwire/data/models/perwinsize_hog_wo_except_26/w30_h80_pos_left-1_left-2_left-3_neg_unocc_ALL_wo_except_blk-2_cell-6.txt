Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_hog_wo_except_26/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_hog_wo_except_26/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_hog_wo_except_26/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-2_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_hog_wo_except_26/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_hog_wo_except_26/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_blk-2_cell-6.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9994    0.9985    0.9989     11865
   Occupied     0.9914    0.9966    0.9940      2074

avg / total     0.9982    0.9982    0.9982     13939
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9990    0.9984    0.9987     11965
   Occupied     0.9904    0.9939    0.9922      1973

avg / total     0.9978    0.9978    0.9978     13938
