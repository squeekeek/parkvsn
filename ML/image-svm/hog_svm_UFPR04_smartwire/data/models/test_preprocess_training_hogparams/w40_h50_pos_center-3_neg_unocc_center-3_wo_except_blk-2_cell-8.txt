Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/test_preprocess_training_hogparams/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-2_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/test_preprocess_training_hogparams/feats_pos_UL_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50_blk-2_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/test_preprocess_training_hogparams/w40_h50_pos_center-3_neg_unocc_center-3_wo_except_blk-2_cell-8.model
Features in dataset (100%):
Count: 3788
Features in training dataset (60%):
Count: 2272
Counter({0: 1240, 1: 1032})
Features in cross-validation dataset (20%):
Count: 758
Counter({0: 418, 1: 340})
Features in test dataset (20%):
Count: 758
Counter({0: 431, 1: 327})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       418
   Occupied       1.00      1.00      1.00       340

avg / total       1.00      1.00      1.00       758
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00       431
   Occupied       1.00      1.00      1.00       327

avg / total       1.00      1.00      1.00       758
