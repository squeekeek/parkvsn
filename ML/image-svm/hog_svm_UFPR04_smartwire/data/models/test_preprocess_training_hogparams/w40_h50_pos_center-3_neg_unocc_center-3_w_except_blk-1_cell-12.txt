Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/test_preprocess_training_hogparams/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-1_cell-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/test_preprocess_training_hogparams/feats_pos_UL_genwin_center-3_UFPR04_neg_paths_2089_w-40h-50_blk-1_cell-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/test_preprocess_training_hogparams/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-1_cell-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/test_preprocess_training_hogparams/w40_h50_pos_center-3_neg_unocc_center-3_w_except_blk-1_cell-12.model
Features in dataset (100%):
Count: 29252
Features in training dataset (60%):
Count: 17551
Counter({0: 16521, 1: 1030})
Features in cross-validation dataset (20%):
Count: 5851
Counter({0: 5482, 1: 369})
Features in test dataset (20%):
Count: 5850
Counter({0: 5550, 1: 300})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5482
   Occupied       0.94      0.99      0.97       369

avg / total       1.00      1.00      1.00      5851
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00      5550
   Occupied       0.96      0.99      0.97       300

avg / total       1.00      1.00      1.00      5850
