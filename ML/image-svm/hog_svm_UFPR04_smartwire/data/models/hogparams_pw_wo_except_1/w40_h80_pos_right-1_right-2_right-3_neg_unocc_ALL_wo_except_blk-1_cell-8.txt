Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_1/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_blk-1_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_1/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-1_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_1/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-1_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_1/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-1_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_wo_except_1/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-1_cell-8.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
Confusion Report:
[[11768   134]
 [   45  3095]]
True Negative: 11768
False Negative: 45
False Positive: 134 
True Positive: 3095 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0143312101911
False Positive Rate: 0.011258611998

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9962    0.9887    0.9925     11902
   Occupied     0.9585    0.9857    0.9719      3140

avg / total     0.9883    0.9881    0.9882     15042
Confusion Report:
[[11807   135]
 [   58  3042]]
True Negative: 11807
False Negative: 58
False Positive: 135 
True Positive: 3042 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0187096774194
False Positive Rate: 0.0113046390889

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9951    0.9887    0.9919     11942
   Occupied     0.9575    0.9813    0.9693      3100

avg / total     0.9874    0.9872    0.9872     15042
