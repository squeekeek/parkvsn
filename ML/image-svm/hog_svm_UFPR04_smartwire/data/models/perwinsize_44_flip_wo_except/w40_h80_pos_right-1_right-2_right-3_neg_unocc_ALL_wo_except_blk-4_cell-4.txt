Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_44_flip_wo_except/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_44_flip_wo_except/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-4_cell-4
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_44_flip_wo_except/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-4_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_44_flip_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-4_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_44_flip_wo_except/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_blk-4_cell-4.model
Features in dataset (100%):
Count: 150416
Features in training dataset (60%):
Count: 90249
Counter({0: 71627, 1: 18622})
Features in cross-validation dataset (20%):
Count: 30084
Counter({0: 23861, 1: 6223})
Features in test dataset (20%):
Count: 30083
Counter({0: 23948, 1: 6135})
Confusion Report:
[[23825    36]
 [   22  6201]]
True Negative: 23825
False Negative: 22
False Positive: 36 
True Positive: 6201 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00353527237667
False Positive Rate: 0.00150873810821

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9991    0.9985    0.9988     23861
   Occupied     0.9942    0.9965    0.9953      6223

avg / total     0.9981    0.9981    0.9981     30084
Confusion Report:
[[23898    50]
 [   33  6102]]
True Negative: 23898
False Negative: 33
False Positive: 50 
True Positive: 6102 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00537897310513
False Positive Rate: 0.00208785702355

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9986    0.9979    0.9983     23948
   Occupied     0.9919    0.9946    0.9932      6135

avg / total     0.9972    0.9972    0.9972     30083
