Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-2_cell-10

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-2_cell-10

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_hogparams_w_except_28_210/w30_h80_pos_left-1_neg_unocc_ALL_w_except_blk-2_cell-10.model
Features in dataset (100%):
Count: 102262
Features in training dataset (60%):
Count: 61357
Counter({0: 57493, 1: 3864})
Features in cross-validation dataset (20%):
Count: 20453
Counter({0: 19205, 1: 1248})
Features in test dataset (20%):
Count: 20452
Counter({0: 19171, 1: 1281})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9997    0.9974    0.9985     19205
   Occupied     0.9613    0.9952    0.9780      1248

avg / total     0.9973    0.9973    0.9973     20453
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9998    0.9967    0.9983     19171
   Occupied     0.9530    0.9977    0.9748      1281

avg / total     0.9969    0.9968    0.9968     20452
