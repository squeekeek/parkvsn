Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-2_cell-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_hogparams_w_except_28_210/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-2_cell-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_hogparams_w_except_28_210/w40_h50_pos_center-2_neg_unocc_ALL_w_except_blk-2_cell-12.model
Features in dataset (100%):
Count: 92616
Features in training dataset (60%):
Count: 55569
Counter({0: 51103, 1: 4466})
Features in cross-validation dataset (20%):
Count: 18524
Counter({0: 17026, 1: 1498})
Features in test dataset (20%):
Count: 18523
Counter({0: 17053, 1: 1470})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9993    0.9888    0.9940     17026
   Occupied     0.8862    0.9927    0.9364      1498

avg / total     0.9902    0.9891    0.9894     18524
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9991    0.9892    0.9941     17053
   Occupied     0.8877    0.9898    0.9360      1470

avg / total     0.9903    0.9893    0.9895     18523
