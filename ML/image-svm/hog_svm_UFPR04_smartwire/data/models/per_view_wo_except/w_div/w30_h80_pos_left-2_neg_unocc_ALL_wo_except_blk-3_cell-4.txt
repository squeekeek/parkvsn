Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-3_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-3_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except/w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-3_cell-4.model
Features in dataset (100%):
Count: 61187
Features in training dataset (60%):
Count: 36712
Counter({0: 35809, 1: 903})
Features in cross-validation dataset (20%):
Count: 12238
Counter({0: 11938, 1: 300})
Features in test dataset (20%):
Count: 12237
Counter({0: 11971, 1: 266})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11938
   Occupied       0.98      0.98      0.98       300

avg / total       1.00      1.00      1.00     12238
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11971
   Occupied       0.98      0.98      0.98       266

avg / total       1.00      1.00      1.00     12237
