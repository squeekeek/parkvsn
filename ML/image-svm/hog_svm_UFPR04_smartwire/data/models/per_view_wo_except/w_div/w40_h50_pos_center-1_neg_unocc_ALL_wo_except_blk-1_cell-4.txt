Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-1_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-1_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except/w40_h50_pos_center-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model
Features in dataset (100%):
Count: 71246
Features in training dataset (60%):
Count: 42747
Counter({0: 35826, 1: 6921})
Features in cross-validation dataset (20%):
Count: 14250
Counter({0: 12000, 1: 2250})
Features in test dataset (20%):
Count: 14249
Counter({0: 11892, 1: 2357})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       0.99      0.99      0.99     12000
   Occupied       0.96      0.97      0.97      2250

avg / total       0.99      0.99      0.99     14250
For test set (20%):
             precision    recall  f1-score   support

      Empty       0.99      0.99      0.99     11892
   Occupied       0.96      0.97      0.97      2357

avg / total       0.99      0.99      0.99     14249
