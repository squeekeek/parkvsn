Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-3_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-3_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except/w30_h80_pos_left-1_neg_unocc_ALL_wo_except_blk-3_cell-8.model
Features in dataset (100%):
Count: 66111
Features in training dataset (60%):
Count: 39666
Counter({0: 35854, 1: 3812})
Features in cross-validation dataset (20%):
Count: 13223
Counter({0: 11926, 1: 1297})
Features in test dataset (20%):
Count: 13222
Counter({0: 11938, 1: 1284})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11926
   Occupied       0.99      1.00      1.00      1297

avg / total       1.00      1.00      1.00     13223
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11938
   Occupied       0.99      1.00      1.00      1284

avg / total       1.00      1.00      1.00     13222
