Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-3_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-3_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except/w40_h80_pos_right-2_neg_unocc_ALL_wo_except_blk-3_cell-6.model
Features in dataset (100%):
Count: 67191
Features in training dataset (60%):
Count: 40314
Counter({0: 35871, 1: 4443})
Features in cross-validation dataset (20%):
Count: 13439
Counter({0: 11888, 1: 1551})
Features in test dataset (20%):
Count: 13438
Counter({0: 11959, 1: 1479})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11888
   Occupied       0.99      1.00      1.00      1551

avg / total       1.00      1.00      1.00     13439
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11959
   Occupied       1.00      1.00      1.00      1479

avg / total       1.00      1.00      1.00     13438
