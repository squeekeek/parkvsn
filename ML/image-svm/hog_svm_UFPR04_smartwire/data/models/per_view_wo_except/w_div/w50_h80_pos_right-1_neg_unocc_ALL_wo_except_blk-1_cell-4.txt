Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-50h-80_blk-1_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_view_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-50h-80_blk-1_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/per_view_wo_except/w50_h80_pos_right-1_neg_unocc_ALL_wo_except_blk-1_cell-4.model
Features in dataset (100%):
Count: 61719
Features in training dataset (60%):
Count: 37031
Counter({0: 35789, 1: 1242})
Features in cross-validation dataset (20%):
Count: 12344
Counter({0: 11977, 1: 367})
Features in test dataset (20%):
Count: 12344
Counter({0: 11952, 1: 392})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11977
   Occupied       0.99      0.99      0.99       367

avg / total       1.00      1.00      1.00     12344
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11952
   Occupied       0.99      1.00      0.99       392

avg / total       1.00      1.00      1.00     12344
