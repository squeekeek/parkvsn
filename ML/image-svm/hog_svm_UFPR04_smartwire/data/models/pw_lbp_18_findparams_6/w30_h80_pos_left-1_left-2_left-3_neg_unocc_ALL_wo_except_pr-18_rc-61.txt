Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_pr-18_rc-61
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_pr-18_rc-61

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_pr-18_rc-61

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_6/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_wo_except_pr-18_rc-61.model
Features in dataset (100%):
Count: 69692
Features in training dataset (60%):
Count: 41815
Counter({0: 35888, 1: 5927})
Features in cross-validation dataset (20%):
Count: 13939
Counter({0: 11865, 1: 2074})
Features in test dataset (20%):
Count: 13938
Counter({0: 11965, 1: 1973})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9900    0.9251    0.9564     11865
   Occupied     0.6883    0.9465    0.7970      2074

avg / total     0.9451    0.9283    0.9327     13939
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9897    0.9214    0.9543     11965
   Occupied     0.6638    0.9417    0.7787      1973

avg / total     0.9435    0.9242    0.9294     13938
