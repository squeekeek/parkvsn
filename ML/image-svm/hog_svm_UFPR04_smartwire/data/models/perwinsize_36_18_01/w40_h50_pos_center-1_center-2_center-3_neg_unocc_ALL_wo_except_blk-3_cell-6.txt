Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_18_01/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-3_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_18_01/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-3_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_18_01/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-3_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_18_01/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-3_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perwinsize_36_18_01/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9989    0.9991     11900
   Occupied     0.9969    0.9978    0.9974      4176

avg / total     0.9986    0.9986    0.9986     16076
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9990    0.9983    0.9987     11980
   Occupied     0.9951    0.9971    0.9961      4096

avg / total     0.9980    0.9980    0.9980     16076
