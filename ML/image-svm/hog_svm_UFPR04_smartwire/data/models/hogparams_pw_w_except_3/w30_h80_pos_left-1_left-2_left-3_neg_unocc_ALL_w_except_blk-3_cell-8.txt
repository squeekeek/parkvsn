Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-3_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-3_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_3/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-3_cell-8.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 57547, 1: 5958})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 19087, 1: 2082})
Features in test dataset (20%):
Count: 21169
Counter({0: 19235, 1: 1934})
Confusion Report:
[[18831   256]
 [   21  2061]]
True Negative: 18831
False Negative: 21
False Positive: 256 
True Positive: 2061 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0100864553314
False Positive Rate: 0.0134122701315

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9989    0.9866    0.9927     19087
   Occupied     0.8895    0.9899    0.9370      2082

avg / total     0.9881    0.9869    0.9872     21169
Confusion Report:
[[18946   289]
 [   14  1920]]
True Negative: 18946
False Negative: 14
False Positive: 289 
True Positive: 1920 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00723888314374
False Positive Rate: 0.0150246945672

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9993    0.9850    0.9921     19235
   Occupied     0.8692    0.9928    0.9269      1934

avg / total     0.9874    0.9857    0.9861     21169
