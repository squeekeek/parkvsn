Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-3_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-3_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_3/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-3_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_3/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-3_cell-8.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 51104, 1: 12401})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 16947, 1: 4222})
Features in test dataset (20%):
Count: 21169
Counter({0: 17131, 1: 4038})
Confusion Report:
[[16794   153]
 [   17  4205]]
True Negative: 16794
False Negative: 17
False Positive: 153 
True Positive: 4205 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00402652771198
False Positive Rate: 0.00902814657461

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9990    0.9910    0.9950     16947
   Occupied     0.9649    0.9960    0.9802      4222

avg / total     0.9922    0.9920    0.9920     21169
Confusion Report:
[[16983   148]
 [   23  4015]]
True Negative: 16983
False Negative: 23
False Positive: 148 
True Positive: 4015 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00569588905399
False Positive Rate: 0.00863930885529

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9986    0.9914    0.9950     17131
   Occupied     0.9644    0.9943    0.9791      4038

avg / total     0.9921    0.9919    0.9920     21169
