Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-4_cell-4

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-4_cell-4

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_wo_except_notbalanced/w30_h80_pos_left-2_neg_unocc_ALL_wo_except_blk-4_cell-4.model
Features in dataset (100%):
Count: 61187
Features in training dataset (60%):
Count: 36712
Counter({0: 35809, 1: 903})
Features in cross-validation dataset (20%):
Count: 12238
Counter({0: 11938, 1: 300})
Features in test dataset (20%):
Count: 12237
Counter({0: 11971, 1: 266})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9997    0.9999    0.9998     11938
   Occupied     0.9966    0.9900    0.9933       300

avg / total     0.9997    0.9997    0.9997     12238
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9999    0.9998    0.9999     11971
   Occupied     0.9925    0.9962    0.9944       266

avg / total     0.9998    0.9998    0.9998     12237
