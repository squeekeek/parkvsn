Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-2_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_wo_except_notbalanced/w40_h50_pos_center-2_neg_unocc_ALL_wo_except_blk-2_cell-6.model
Features in dataset (100%):
Count: 67152
Features in training dataset (60%):
Count: 40291
Counter({0: 35866, 1: 4425})
Features in cross-validation dataset (20%):
Count: 13431
Counter({0: 11909, 1: 1522})
Features in test dataset (20%):
Count: 13430
Counter({0: 11943, 1: 1487})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9994    0.9990    0.9992     11909
   Occupied     0.9921    0.9954    0.9938      1522

avg / total     0.9986    0.9986    0.9986     13431
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9983    0.9988     11943
   Occupied     0.9866    0.9939    0.9903      1487

avg / total     0.9979    0.9978    0.9978     13430
