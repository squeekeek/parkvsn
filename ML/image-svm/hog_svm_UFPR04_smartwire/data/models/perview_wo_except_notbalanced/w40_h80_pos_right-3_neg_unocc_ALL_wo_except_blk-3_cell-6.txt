Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-3_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perview_wo_except_notbalanced/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-3_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/perview_wo_except_notbalanced/w40_h80_pos_right-3_neg_unocc_ALL_wo_except_blk-3_cell-6.model
Features in dataset (100%):
Count: 65734
Features in training dataset (60%):
Count: 39440
Counter({0: 35844, 1: 3596})
Features in cross-validation dataset (20%):
Count: 13147
Counter({0: 11937, 1: 1210})
Features in test dataset (20%):
Count: 13147
Counter({0: 11937, 1: 1210})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9990    0.9991     11937
   Occupied     0.9901    0.9926    0.9913      1210

avg / total     0.9984    0.9984    0.9984     13147
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9996    0.9993    0.9995     11937
   Occupied     0.9934    0.9959    0.9946      1210

avg / total     0.9990    0.9990    0.9990     13147
