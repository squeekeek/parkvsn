Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_36_01_lbp_18/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_r-1_p-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_36_01_lbp_18/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_r-1_p-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_36_01_lbp_18/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_r-1_p-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_36_01_lbp_18/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_r-1_p-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_36_01_lbp_18/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_r-1_p-8.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9724    0.8862    0.9273     11902
   Occupied     0.6772    0.9048    0.7746      3140

avg / total     0.9108    0.8901    0.8955     15042
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9719    0.8891    0.9287     11942
   Occupied     0.6784    0.9010    0.7740      3100

avg / total     0.9114    0.8916    0.8968     15042
