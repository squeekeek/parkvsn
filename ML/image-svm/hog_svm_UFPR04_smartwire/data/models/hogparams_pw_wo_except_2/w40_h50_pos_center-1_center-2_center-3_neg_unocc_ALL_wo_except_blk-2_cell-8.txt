Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-2_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-2_cell-8
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-2_cell-8

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-8

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_wo_except_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-8.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
Confusion Report:
[[11860    40]
 [    7  4169]]
True Negative: 11860
False Negative: 7
False Positive: 40 
True Positive: 4169 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00167624521073
False Positive Rate: 0.00336134453782

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9994    0.9966    0.9980     11900
   Occupied     0.9905    0.9983    0.9944      4176

avg / total     0.9971    0.9971    0.9971     16076
Confusion Report:
[[11940    40]
 [   10  4086]]
True Negative: 11940
False Negative: 10
False Positive: 40 
True Positive: 4086 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00244140625
False Positive Rate: 0.00333889816361

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9967    0.9979     11980
   Occupied     0.9903    0.9976    0.9939      4096

avg / total     0.9969    0.9969    0.9969     16076
