Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-2_cell-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-2_cell-12
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-2_cell-12

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_wo_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-12

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_wo_except_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_blk-2_cell-12.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
Confusion Report:
[[11830    70]
 [   23  4153]]
True Negative: 11830
False Negative: 23
False Positive: 70 
True Positive: 4153 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00550766283525
False Positive Rate: 0.00588235294118

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9981    0.9941    0.9961     11900
   Occupied     0.9834    0.9945    0.9889      4176

avg / total     0.9943    0.9942    0.9942     16076
Confusion Report:
[[11900    80]
 [   13  4083]]
True Negative: 11900
False Negative: 13
False Positive: 80 
True Positive: 4083 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.003173828125
False Positive Rate: 0.00667779632721

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9989    0.9933    0.9961     11980
   Occupied     0.9808    0.9968    0.9887      4096

avg / total     0.9943    0.9942    0.9942     16076
