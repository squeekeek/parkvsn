Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-53
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-53

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_5/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-53

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_5/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_wo_except_pr-18_rc-53.model
Features in dataset (100%):
Count: 80379
Features in training dataset (60%):
Count: 48227
Counter({0: 35838, 1: 12389})
Features in cross-validation dataset (20%):
Count: 16076
Counter({0: 11900, 1: 4176})
Features in test dataset (20%):
Count: 16076
Counter({0: 11980, 1: 4096})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9822    0.9557    0.9688     11900
   Occupied     0.8828    0.9507    0.9155      4176

avg / total     0.9564    0.9544    0.9549     16076
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9820    0.9541    0.9678     11980
   Occupied     0.8760    0.9487    0.9109      4096

avg / total     0.9550    0.9527    0.9533     16076
