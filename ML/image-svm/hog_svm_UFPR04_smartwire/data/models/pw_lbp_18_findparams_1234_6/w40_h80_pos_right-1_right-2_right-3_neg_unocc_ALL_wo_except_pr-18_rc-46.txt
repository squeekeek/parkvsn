Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_pr-18_rc-46
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_pr-18_rc-46

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_pr-18_rc-46

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/pw_lbp_18_findparams_1234_6/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_wo_except_pr-18_rc-46.model
Features in dataset (100%):
Count: 75208
Features in training dataset (60%):
Count: 45124
Counter({0: 35874, 1: 9250})
Features in cross-validation dataset (20%):
Count: 15042
Counter({0: 11902, 1: 3140})
Features in test dataset (20%):
Count: 15042
Counter({0: 11942, 1: 3100})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9933    0.9777    0.9854     11902
   Occupied     0.9200    0.9748    0.9467      3140

avg / total     0.9780    0.9771    0.9773     15042
For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9951    0.9776    0.9863     11942
   Occupied     0.9193    0.9813    0.9493      3100

avg / total     0.9794    0.9784    0.9787     15042
