Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except/w40_h50_pos_center-3neg_unocc_ALL.model
Features in dataset (100%):
Count: 61417
Features in training dataset (60%):
Count: 36850
Counter({0: 35798, 1: 1052})
Features in cross-validation dataset (20%):
Count: 12284
Counter({0: 11937, 1: 347})
Features in test dataset (20%):
Count: 12283
Counter({0: 11983, 1: 300})
For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11937
   Occupied       1.00      1.00      1.00       347

avg / total       1.00      1.00      1.00     12284
For test set (20%):
             precision    recall  f1-score   support

      Empty       1.00      1.00      1.00     11983
   Occupied       1.00      1.00      1.00       300

avg / total       1.00      1.00      1.00     12283
