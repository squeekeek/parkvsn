Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-2_cell-10

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80_blk-2_cell-10

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-2_cell-10.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 54227, 1: 9278})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 17974, 1: 3195})
Features in test dataset (20%):
Count: 21169
Counter({0: 18152, 1: 3017})
Confusion Report:
[[17748   226]
 [   31  3164]]
True Negative: 17748
False Negative: 31
False Positive: 226 
True Positive: 3164 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00970266040689
False Positive Rate: 0.0125737175921

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9983    0.9874    0.9928     17974
   Occupied     0.9333    0.9903    0.9610      3195

avg / total     0.9885    0.9879    0.9880     21169
Confusion Report:
[[17904   248]
 [   30  2987]]
True Negative: 17904
False Negative: 30
False Positive: 248 
True Positive: 2987 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00994365263507
False Positive Rate: 0.0136624063464

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9983    0.9863    0.9923     18152
   Occupied     0.9233    0.9901    0.9555      3017

avg / total     0.9876    0.9869    0.9871     21169
