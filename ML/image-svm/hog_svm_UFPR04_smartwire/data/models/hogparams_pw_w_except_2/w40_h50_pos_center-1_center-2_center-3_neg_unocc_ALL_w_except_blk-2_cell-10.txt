Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-2_cell-10

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50_blk-2_cell-10

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_2/w40_h50_pos_center-1_center-2_center-3_neg_unocc_ALL_w_except_blk-2_cell-10.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 51104, 1: 12401})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 16947, 1: 4222})
Features in test dataset (20%):
Count: 21169
Counter({0: 17131, 1: 4038})
Confusion Report:
[[16790   157]
 [   33  4189]]
True Negative: 16790
False Negative: 33
False Positive: 157 
True Positive: 4189 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00781620085268
False Positive Rate: 0.00926417655042

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9980    0.9907    0.9944     16947
   Occupied     0.9639    0.9922    0.9778      4222

avg / total     0.9912    0.9910    0.9911     21169
Confusion Report:
[[16977   154]
 [   25  4013]]
True Negative: 16977
False Negative: 25
False Positive: 154 
True Positive: 4013 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00619118375433
False Positive Rate: 0.00898955110618

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9985    0.9910    0.9948     17131
   Occupied     0.9630    0.9938    0.9782      4038

avg / total     0.9918    0.9915    0.9916     21169
