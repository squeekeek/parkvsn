Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80_blk-2_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80_blk-2_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_2/w40_h80_pos_right-1_right-2_right-3_neg_unocc_ALL_w_except_blk-2_cell-6.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 54227, 1: 9278})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 17974, 1: 3195})
Features in test dataset (20%):
Count: 21169
Counter({0: 18152, 1: 3017})
Confusion Report:
[[17806   168]
 [   67  3128]]
True Negative: 17806
False Negative: 67
False Positive: 168 
True Positive: 3128 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0209702660407
False Positive Rate: 0.00934683431623

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9963    0.9907    0.9934     17974
   Occupied     0.9490    0.9790    0.9638      3195

avg / total     0.9891    0.9889    0.9890     21169
Confusion Report:
[[17965   187]
 [   51  2966]]
True Negative: 17965
False Negative: 51
False Positive: 187 
True Positive: 2966 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.0169042094796
False Positive Rate: 0.010301895108

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9972    0.9897    0.9934     18152
   Occupied     0.9407    0.9831    0.9614      3017

avg / total     0.9891    0.9888    0.9889     21169
