Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-2_cell-10

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-10
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-2_cell-10

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-2_cell-10.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 57547, 1: 5958})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 19087, 1: 2082})
Features in test dataset (20%):
Count: 21169
Counter({0: 19235, 1: 1934})
Confusion Report:
[[18912   175]
 [   15  2067]]
True Negative: 18912
False Negative: 15
False Positive: 175 
True Positive: 2067 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00720461095101
False Positive Rate: 0.00916854403521

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9992    0.9908    0.9950     19087
   Occupied     0.9219    0.9928    0.9561      2082

avg / total     0.9916    0.9910    0.9912     21169
Confusion Report:
[[19060   175]
 [    8  1926]]
True Negative: 19060
False Negative: 8
False Positive: 175 
True Positive: 1926 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00413650465357
False Positive Rate: 0.00909799844034

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9996    0.9909    0.9952     19235
   Occupied     0.9167    0.9959    0.9546      1934

avg / total     0.9920    0.9914    0.9915     21169
