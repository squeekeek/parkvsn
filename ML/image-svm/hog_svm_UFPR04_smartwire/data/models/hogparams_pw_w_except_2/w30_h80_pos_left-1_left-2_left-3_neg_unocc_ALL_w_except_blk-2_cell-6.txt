Positive folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-2_cell-6

Negative folders: 
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-6
/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/hogparams_pw_w_except_2/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80_blk-2_cell-6

Classifier saved to /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/hogparams_pw_w_except_2/w30_h80_pos_left-1_left-2_left-3_neg_unocc_ALL_w_except_blk-2_cell-6.model
Features in dataset (100%):
Count: 105843
Features in training dataset (60%):
Count: 63505
Counter({0: 57547, 1: 5958})
Features in cross-validation dataset (20%):
Count: 21169
Counter({0: 19087, 1: 2082})
Features in test dataset (20%):
Count: 21169
Counter({0: 19235, 1: 1934})
Confusion Report:
[[19005    82]
 [   13  2069]]
True Negative: 19005
False Negative: 13
False Positive: 82 
True Positive: 2069 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00624399615754
False Positive Rate: 0.0042961177765

For cross-validation set (20%):
             precision    recall  f1-score   support

      Empty     0.9993    0.9957    0.9975     19087
   Occupied     0.9619    0.9938    0.9776      2082

avg / total     0.9956    0.9955    0.9955     21169
Confusion Report:
[[19140    95]
 [   14  1920]]
True Negative: 19140
False Negative: 14
False Positive: 95 
True Positive: 1920 

(aim to reduce FN rate - misclassify occupied as empty)
False Negative Rate: 0.00723888314374
False Positive Rate: 0.00493891343904

For test set (20%):
             precision    recall  f1-score   support

      Empty     0.9993    0.9951    0.9972     19235
   Occupied     0.9529    0.9928    0.9724      1934

avg / total     0.9950    0.9949    0.9949     21169
