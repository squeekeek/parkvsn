Negative folders: 

#### Cmd for occupied and unoccupied of left-2:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-m pos_left-2neg_unocc_ALL \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80 \
-w 30 \
-ht 80 \
-n one-view-against-all_except-major 

#### Cmd for occupied and unoccupied, and except left of left-2:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-m pos_left-2neg_unocc_ALL_left \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 \
-w 30 \
-ht 80 \
-n one-view-against-all_except-major 

