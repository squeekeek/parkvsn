
import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from collections import Counter

from datetime import datetime

from timeit import default_timer as timer


from preprocess_training_hogparams import preprocess_training_hogparams



if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-mp", "--main_pos_path",
        help="Main JSON file for paths for positive features",
        required=True)
    ap.add_argument(
        "-dc", "--dataset_category",
        help="Enter dataset category: UFPR04, UFPR05, PUC, or ALL",
        required=True
    )
    args = vars(ap.parse_args())
    pklot_path = args["pklot_path"]
    main_pos_path = args["main_pos_path"]
    dataset_category = args["dataset_category"]

    start_time = datetime.now()
    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    config_path = os.path.join(top_dir, "config")

    # Load HoG setting
    hog_config_path = os.path.join(config_path, "hog_list.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    # Test #1
    # Doing for each individual view without except
    project_name = "per_view_wo_except"

    