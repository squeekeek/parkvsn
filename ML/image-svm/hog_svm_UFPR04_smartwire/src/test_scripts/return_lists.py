def returnList():
    a = [1,2,3]
    b = ["a", "b", "c"]
    c = ["lala", "boom", "test" ]

    return {"a": a, "b": b, "c": c}

if __name__ == "__main__":
    test_dict = returnList()
    print test_dict["a"]
    print test_dict["b"]
    print test_dict["c"][0]