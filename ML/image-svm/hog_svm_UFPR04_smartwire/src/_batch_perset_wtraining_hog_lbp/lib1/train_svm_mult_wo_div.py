"""
    Trains model
    -p1, -p2, -p3
    -n1, n2, -n3

    Uses folder paths as training input as is.

    Example usage:
    python train-svm-trainingset-mult.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -n2 feats_pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 -m pos_left-1_neg_left-1_except-left

    python train_svm_mult_wo_div.py\
    -m right-1-2-3-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -p2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
    -p3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

    python train_svm_mult_wo_div.py \
    -m right-1-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 



"""
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse 
import glob
import os
import sys
import json
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report


def train_svm_mult_wo_div(hog_pos_feat_path, hog_pos_feat_path_2, hog_pos_feat_path_3, hog_neg_feat_path, lbp_pos_feat_path, lbp_pos_feat_path_2, lbp_pos_feat_path_3, lbp_neg_feat_path, model_name, width, height, folder_path, project_name, svm_dict):
    # If feature directories don't exist, create them
    temp_model_path =  "w" + str(width) + "_h" + str(height) + "_" + model_name + '_wo_div.model'
    results_filename = temp_model_path.split(".model")[0] + ".txt"
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "models", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        model_path = os.path.join(data_folder, temp_model_path)
        results_path = os.path.join(data_folder, results_filename)
    else:
        model_path = os.path.join(folder_path, temp_model_path)
        results_path = os.path.join(folder_path, results_filename)

    write_file = open(results_path, 'w')
    write_file.truncate()

    str_a = "Positive folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = hog_pos_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    hog_fds = []
    fds = []
    labels = []
    # Load the positive features
    for feat_path in sorted(glob.glob(os.path.join(hog_pos_feat_path, "*.feat"))):
        fd = joblib.load(feat_path)
        hog_fds.append(fd)
    for i, feat_path in enumerate(sorted(glob.glob(os.path.join(lbp_pos_feat_path, "*.feat")))):
        fd = joblib.load(feat_path)
        fd = np.concatenate((hog_fds[i], fd))
        fds.append(fd)
        labels.append(1)

    hog_fds = []
    if hog_pos_feat_path_2 is not None:
        print "Has 2nd pospaths folder"
        str_a = hog_pos_feat_path_2  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in sorted(glob.glob(os.path.join(hog_pos_feat_path_2, "*.feat"))):
            fd = joblib.load(feat_path)
            hog_fds.append(fd)
        for i, feat_path in enumerate(sorted(glob.glob(os.path.join(lbp_pos_feat_path_2, "*.feat")))):
            fd = joblib.load(feat_path)
            fd = np.concatenate((hog_fds[i], fd))
            fds.append(fd)
            labels.append(1)

    else:
        print "No 2nd pos paths folder"
    hog_fds = []
    if hog_pos_feat_path_3 is not None:
        print "Has 3rd pospaths folder"
        str_a = hog_pos_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in sorted(glob.glob(os.path.join(hog_pos_feat_path_3, "*.feat"))):
            fd = joblib.load(feat_path)
            hog_fds.append(fd)
        for i, feat_path in enumerate(sorted(glob.glob(os.path.join(lbp_pos_feat_path_3, "*.feat")))):
            fd = joblib.load(feat_path)
            fd = np.concatenate((hog_fds[i], fd))
            fds.append(fd)
            labels.append(1)
    else:
        print "No 3rd pos paths folder"


    str_a = "\n"
    print str_a
    write_file.write(str_a)
    
    str_a = "Negative folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = hog_neg_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    hog_fds = []
    # Load the negative features
    for feat_path in sorted(glob.glob(os.path.join(hog_neg_feat_path, "*.feat"))):
        fd = joblib.load(feat_path)
        hog_fds.append(fd)
    for i, feat_path in enumerate(sorted(glob.glob(os.path.join(lbp_neg_feat_path, "*.feat")))):
        fd = joblib.load(feat_path)
        fd = np.concatenate((hog_fds[i], fd))
        fds.append(fd)
        labels.append(0)



    str_a = "\n"
    print str_a
    write_file.write(str_a)

    # No test split
    clf = LinearSVC(class_weight=svm_dict["svm"]["class_weight"], C=svm_dict["svm"]["C"])

    print "Training a Linear SVM Classifier"
    clf.fit(fds, labels)


    joblib.dump(clf, model_path)
    str_a =  "Classifier saved to {}\n".format(model_path)
    print str_a
    write_file.write(str_a)


    len_labels = len(labels)


    print_train = (
        "Features in training dataset (100%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_labels, Counter(labels))


    write_file.write(print_train)
    print print_train

    target_names = [
        "Empty",
        "Occupied"
    ]

    write_file.close()

    return model_path

if __name__ == "__main__":
    # Parse the command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-hp1', "--hog_pos_feat_path",
        help="Path to the positive features directory",
        required=True)
    ap.add_argument(
        "-hp2", "--hog_pos_feat_path_2",
        help="Path to 2nd positive features directory"
    )
    ap.add_argument(
        "-hp3", "--hog_pos_feat_path_3",
        help="Path to 3rd positive features directory"
    )
    ap.add_argument(
        '-lp1', "--lbp_pos_feat_path",
        help="Path to the positive features directory",
        required=True)
    ap.add_argument(
        "-lp2", "--lbp_pos_feat_path_2",
        help="Path to 2nd positive features directory"
    )
    ap.add_argument(
        "-lp3", "--lbp_pos_feat_path_3",
        help="Path to 3rd positive features directory"
    )

    ap.add_argument(
        '-hn1', "--hog_neg_feat_path",
        help="Path to the negative features directory",
        required=True)
    ap.add_argument(
        "-ln1", "--lbp_neg_feat_path",
        help="Path to 2nd negative features directory"
    )


    ap.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)
    ap.add_argument(
        '-w', "--width",
        help="width of the window size",
        required=True
    )
    ap.add_argument(
        '-ht', "--height",
        help="Height of the window size",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    ap.add_argument(
        "-sj", "--svm_json",
        help="Enter path to svm json file to use",
        required=True
    )

    args = vars(ap.parse_args())

    hog_pos_feat_path = args["hog_pos_feat_path"]
    hog_pos_feat_path_2 = args["hog_pos_feat_path_2"]
    hog_pos_feat_path_3 = args["hog_pos_feat_path_3"]
    hog_neg_feat_path = args["hog_neg_feat_path"]

    lbp_pos_feat_path = args["lbp_pos_feat_path"]
    lbp_pos_feat_path_2 = args["lbp_pos_feat_path_2"]
    lbp_pos_feat_path_3 = args["lbp_pos_feat_path_3"]
    lbp_neg_feat_path = args["lbp_neg_feat_path"]

    model_name = args["model_name"]
    width = args["width"]
    height = args["height"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    svm_json = args["svm_json"]

    # Load SVM setting
    with open(svm_json) as data_file:
        svm_dict = json.load(data_file)

    model_path = train_svm_mult_wo_div(hog_pos_feat_path, hog_pos_feat_path_2, hog_pos_feat_path_3, hog_neg_feat_path, lbp_pos_feat_path, lbp_pos_feat_path_2, lbp_pos_feat_path_3, lbp_neg_feat_path, model_name, width, height, folder_path, project_name, svm_dict)

    print "Model path:"
    print model_path
