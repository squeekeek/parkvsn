"""
    A script that would randomly separate a given category into train, cv and/or test paths. Use the output JSON of generate-window-pos.py
    Assumption is that there's just one view and one category.

    Example usage:
    python sep-pos-json.py -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_right-3_UFPR04_neg_paths_9083_w-40h-80.json -wcv 1 -n test

"""

from sklearn.model_selection import train_test_split

import argparse 
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0

if __name__ == "__main__":
    # Parse the command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-j', "--json_path",
        help="Path to the pos json file",
        required=True
    )
    ap.add_argument(
        "-wcv", "--with_cv",
        help="1= with cv, 0= w/o cv",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())

    json_path = args["json_path"]
    with_cv = int(args["with_cv"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    with open(json_path) as data_file:
        json_dict = json.load(data_file)

    # Get category (assumption is that there's just one category)
    category = ""
    for i, cat in enumerate(json_dict):
        category = cat

    # Puth all paths in a list
    temp_list = []
    for i, path in enumerate(json_dict[category]):
        temp_list.append(path)

    # Create dummy list for labels
    dummy_length = len(temp_list)
    temp_labels = []
    for i in range(0, dummy_length, 1):
        temp_labels.append(1)

    dict_train = {}
    dict_test = {}
    dict_cv = {}
    dict_train.update(
        {
            category: {
            }
        }
    )

    dict_test.update(
        {
            category: {
            }
        }
    )
    dict_cv.update(
        {
            category: {
            }
        }
    )
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    # Copying dictionaries
    if with_cv == 0:
        # 60% training set, 40% test set split
        paths_train, paths_test, labels_train, labels_test = train_test_split(
            temp_list, temp_labels, test_size=0.4, random_state=5
        )
        for i, key in enumerate(paths_train):
            dict_train[category].update(
                {
                    key: json_dict[category][key].copy()
                }
            )
        for i, key in enumerate(paths_test):
            dict_test[category].update(
                {
                    key: json_dict[category][key].copy()
                }
            )

        train_num = len(paths_train)
        test_num = len(paths_test)
        temp_train_filename = "wocv-train_" + str(train_num) + json_path.split("/")[-1]
        temp_test_filename = "wocv-test_" + str(test_num) + json_path.split("/")[-1]

        if folder_path is None:
            if project_name is None:
                print "You did not put a project name"
                sys.exit()
            parent_a = os.path.abspath("..")
            parent_b_temp = os.path.join(parent_a, "..")
            dir_main = os.path.abspath(parent_b_temp)
            data_folder = os.path.join(dir_main, "data", "json_cropped_partitioned", project_name)
            if not os.path.isdir(data_folder):
                os.makedirs(data_folder)
            train_filename = os.path.join(data_folder, temp_train_filename)
            test_filename = os.path.join(data_folder, temp_test_filename)
        else:
            train_filename = os.path.join(folder_path, temp_train_filename)
            test_filename = os.path.join(folder_path, temp_test_filename)

        with io.open(train_filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            dict_train,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        with io.open(test_filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            dict_test,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        print "Json files saved to:\n{}\n{}\n".format(train_filename, test_filename)
    else:
        # 60% training set, 20% cv set, 20% test set
        paths_train, paths_temp, labels_train, labels_temp = train_test_split(
            temp_list, temp_labels, test_size=0.4, random_state=5
        )
        paths_test, paths_cv, labels_test, labels_cv = train_test_split(
            paths_temp, labels_temp, test_size=0.5, random_state=5
        )
        for i, key in enumerate(paths_train):
            dict_train[category].update(
            {
                key: json_dict[category][key].copy()
            }
        )
        for i, key in enumerate(paths_cv):
            dict_cv[category].update(
                {
                    key: json_dict[category][key].copy()
                }
        )
        for i, key in enumerate(paths_test):
            dict_test[category].update(
            {
                key: json_dict[category][key].copy()
            }
        )
        train_num = len(paths_train)
        cv_num = len(paths_cv)
        test_num = len(paths_test)
        temp_train_filename = "wcv-train_" + str(train_num) + "_" + json_path.split("/")[-1]
        temp_test_filename = "wcv-test_" + str(test_num) + "_" + json_path.split("/")[-1]
        temp_cv_filename = "wcv-cv-" + str(cv_num) + "_" + json_path.split("/")[-1]

        if folder_path is None:
            if project_name is None:
                print "You did not put a project name"
                sys.exit()
            parent_a = os.path.abspath("..")
            parent_b_temp = os.path.join(parent_a, "..")
            dir_main = os.path.abspath(parent_b_temp)
            data_folder = os.path.join(dir_main, "data", "json_cropped_partitioned", project_name)
            if not os.path.isdir(data_folder):
                os.makedirs(data_folder)
            train_filename = os.path.join(data_folder, temp_train_filename)
            test_filename = os.path.join(data_folder, temp_test_filename)
            cv_filename = os.path.join(data_folder, temp_cv_filename)
        else:
            train_filename = os.path.join(folder_path, temp_train_filename)
            test_filename = os.path.join(folder_path, temp_test_filename)
            cv_filename = os.path.join(folder_path, temp_cv_filename)

        with io.open(train_filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            dict_train,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        with io.open(test_filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            dict_test,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        with io.open(cv_filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(
                            dict_cv,
                            indent=4, sort_keys=True,
                            separators=(',', ':'), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        print "Json files saved to:\n{}\n{}\n{}\n".format(train_filename, cv_filename, test_filename)



