"""
    Bypasses numstopaths, generate_window, and generate_window_pos
    
    Hard mine negative images from the "Empty" portion of PKLot for more unoccupied data.

    Extracts HoG into a folder.

    Has option for flipping vertically, and flipping horizontally.

    Example usage:
    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_perset_wtraining/lib/extract_hog_neg.py \
    -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ \
    -s UFPR04 \
    -w 30 \
    -ht 80 \
    -f /Users/agatha/Desktop/test \
    -fh y \
    -fv y\

"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

from lib.helpers import pyramid, sliding_window
from lib.nms import nms

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False

def extract_hog_neg(pklot_path, set_option, width, height, folder_path, project_name, hog_option, ifFlipH, ifFlipV):

    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    config_path = os.path.join(top_dir, "config")

    # Load HoG Setting
    hog_config_path = os.path.join(config_path, "hog.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    # Load detection settings
    detection_config_path = os.path.join(config_path, "detection.json")
    with open(detection_config_path) as data_file:
        detection_dict = json.load(data_file)

    # HoG folder name
    temp_feat_path = "feats_neg_" + set_option + "_fh-" + ifFlipH + "_fv-" + ifFlipV

    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        feat_path = os.path.join(dir_main, "data", "feats_hog", project_name, temp_feat_path)
    else:
        feat_path = os.path.join(folder_path, temp_feat_path)

    # If feature directories don't exist, create them
    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)

    # PKLot paths
    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    if set_option == "ALL":
        neg_path = os.path.join(str(pklot_path), "*", "*", "*", "Empty", "*")
    else:
        neg_path = os.path.join(str(pklot_path), str(set_option), "*", "*", "Empty", "*")

    # HoG parameters
    window_size = (width, height)
    orientations = hog_dict["hog"][hog_option]["orientations"]
    pixels_per_cell = hog_dict["hog"][hog_option]["pixels_per_cell"]
    cells_per_block = hog_dict["hog"][hog_option]["cells_per_block"]
    transform_sqrt = hog_dict["hog"][hog_option]["transform_sqrt"]
    block_norm = hog_dict["hog"][hog_option]["block_norm"]

    # Settings for image pyramid
    downscale = detection_dict["detection"]["pyramid"]["downscale"]
    scale = 0
    step_size = detection_dict["detection"]["sliding_window"]["step_size"]
    detections = []
    # Settings for nms
    threshold = detection_dict["detection"]["nms"]["threshold"]


    # Go thru every image in specified "Empty" set option
    counter = 0
    for im_path in glob.glob(neg_path):
        image_name = os.path.split("/")[-1].split(".")[0]

        # Load image
        orig_image = cv2.imread(im_path)
        orig_height, orig_width = orig_image.shape[:2]
        orig_ratio = orig_width/float(orig_height)

        # Resize to height of 100 px maintaining aspect ratio
        res_height = 100
        res_width = int(res_height*orig_ratio)
        resized_image = cv2.resize(orig_image, (res_width, res_height))

        # Grayscale image
        gray_resized = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

        # Downscale the image and iterate
        # for im_scaled in pyramid(gray_resized, scale, window_size):
        # for im_scaled in pyramid_gaussian(gray_resized, downscale=downscale):
        for im_scaled in pyramid(gray_resized, scale=downscale, minSize=window_size):
            # This list contains detections at the current scale
            cd = []
            # If the width or height of the scaled image is less than the width
            # or height of the widnow, then end the iterations
            if im_scaled.shape[0] < window_size[0] or im_scaled.shape[1] < window_size[1]:
                """
                print "im_scaled.shape[0]: {}, im_scaled.shape[1]: {}".format(im_scaled.shape[0], im_scaled.shape[1])
                print "breaked"
                """
                break

            #  print "im_scaled.shape[:]: {}".format(im_scaled.shape[:])
            for (x, y, im_window) in sliding_window(im_scaled, window_size, step_size):
                if im_window.shape[0] != window_size[0] or im_window.shape[1] != window_size[1]:
                    continue

                """
                print "im_window.shape[:]: {}".format(im_window.shape[:])
                print "im_window.shape[0]: {}".format(im_window.shape[0])
                print "im_window.shape[1]: {}".format(im_window.shape[1])
                """
                # sys.exit()
                # Calculate the HoG features
                # for normal view
                fd = feature.hog(
                    im_window,
                    orientations=orientations,
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block,
                    transform_sqrt=transform_sqrt,
                    visualise=False,
                    block_norm=block_norm
                )
                # insert stuff to do to detection
                fd_name = image_name + "s" + str(scale) + ".feat"
                fd_path = os.path.join(feat_path, fd_name)
                joblib.dump(fd, fd_path)

                # flip original horizontally
                im_flipH = cv2.flip(im_window, 0)
                fd = feature.hog(
                    im_flipH,
                    orientations=orientations,
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block,
                    transform_sqrt=transform_sqrt,
                    visualise=False,
                    block_norm=block_norm
                )
                # insert stuff to do to detection
                fd_name = image_name + "s" + str(scale) + "fH.feat"
                fd_path = os.path.join(feat_path, fd_name)
                joblib.dump(fd, fd_path)

                # flip original vertically
                im_flipV = cv2.flip(im_window, 1)
                fd = feature.hog(
                    im_flipV,
                    orientations=orientations,
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block,
                    transform_sqrt=transform_sqrt,
                    visualise=False,
                    block_norm=block_norm
                )
                # insert stuff to do to detection
                fd_name = image_name + "s" + str(scale) + "fV.feat"
                fd_path = os.path.join(feat_path, fd_name)
                joblib.dump(fd, fd_path)

                # flip horizontally, then vertically
                im_flipHV = cv2.flip(im_flipH, 1)
                fd = feature.hog(
                    im_flipHV,
                    orientations=orientations,
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block,
                    transform_sqrt=transform_sqrt,
                    visualise=False,
                    block_norm=block_norm
                )
                # insert stuff to do to detection
                fd_name = image_name + "s" + str(scale) + "fHV.feat"
                fd_path = os.path.join(feat_path, fd_name)
                joblib.dump(fd, fd_path)


            # Move the next scale
            scale = scale + 1

            # Incrememnt counter for extracted features
            counter = counter + 1
            if (counter % 1000 == 0):
                print "Extracted: {}".format(counter)

    print "HoG features saved in {}".format(feat_path)
    return feat_path

if __name__ == "__main__":
    # construct the argument parse and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        '-s', "--set_option", help="Set option to extract from, UFPR04, UFPR05, PUC, ALL", 
        required=True)
    ap.add_argument(
        "-w", "--width", required=True, help="Width of window size")
    ap.add_argument(
        "-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    ap.add_argument(
        '-ho', "--hog_option", help="Index of hog option to use", default=0
    )
    ap.add_argument(
        '-fh', "--flip_horizontally", help="Flip samples horizontally", default="n"
    )
    ap.add_argument(
        '-fv', "--flip_vertically", help="Flip samples vertically", default="n"
    )

    args = vars(ap.parse_args())

    # initialize the data matrix and labels
    print "[INFO] extracting features..."
    data = []
    labels = []

    pklot_path = args["pklot_path"]
    set_option = args["set_option"]
    width = int(args["width"])
    height = int(args["height"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    hog_option = int(args["hog_option"])
    ifFlipH = args["flip_horizontally"]
    ifFlipV = args["flip_vertically"]


    extract_hog_neg(pklot_path, set_option, width, height, folder_path, project_name, hog_option, ifFlipH, ifFlipV)