"""
    Extracts HoG ton an output folder given an ROI json file and width and height.

    Basically, does not resize.
"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False

if __name__ == "__main__":
    # construct the argument parse and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json of images to extract hog",
        required=True)
    ap.add_argument(
        "-w", "--width", required=True, help="Width of window size")
    ap.add_argument(
        "-ht", "--height", required=True, help="Height of window size")


    args = vars(ap.parse_args())

    # initialize the data matrix and labels
    print "[INFO] extracting features..."
    data = []
    labels = []

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    width = int(args["width"])
    height = int(args["height"])

    # Load json_path
    with open(json_path) as data_file:
        roi_dict = json.load(data_file)

    feat_path =  "feats_" + json_path.split("/")[-1].split(".json")[0]


    # If feature directories don't exist, create them
    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)

    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    # HoG parameters
    window_size = (width, height)
    orientations = 9
    pixels_per_cell = [4,4]
    cells_per_block = [4,4]
    transform_sqrt = True

    # Go thru every image
    # resize to window size
    # resize to window size 
    # extract HoG

    num_total = len(roi_dict)
    counter = 0

    for i, im_path in enumerate(roi_dict):
        actual_path = actual_pklot_path + im_path
        orig_image = cv2.imread(actual_path)
        row_0 = roi_dict[im_path]["pt1"][0]
        row_1 = roi_dict[im_path]["pt2"][0]
        col_0 = roi_dict[im_path]["pt1"][1]
        col_1 = roi_dict[im_path]["pt2"][1]
        roi_image = orig_image[col_0:col_1, row_0:row_1]
        resized_roi = cv2.resize(roi_image, (width, height))
        gray_resized = cv2.cvtColor(resized_roi, cv2.COLOR_BGR2GRAY)
        equalized_hist = cv2.equalizeHist(gray_resized)
        
        if not ifVisualize:
            fd = feature.hog(
                gray_resized,
                orientations=orientations, 
                pixels_per_cell=pixels_per_cell, 
                cells_per_block=cells_per_block, 
                transform_sqrt=transform_sqrt, 
                visualise=False
            )
        else:
            cv2.imshow("Gray Image with equalize", equalized_hist)
            cv2.imshow("Gray image", gray_resized)
            fd, hogImage = feature.hog(
                gray_resized, 
                orientations=orientations, 
                pixels_per_cell=pixels_per_cell, 
                cells_per_block=cells_per_block, 
                transform_sqrt=transform_sqrt, 
                visualise=True
            )
            hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
            hogImage = hogImage.astype("uint8")
            cv2.imshow("HOG Image", hogImage)

            fd, eq_hogImage = feature.hog(
                equalized_hist, 
                orientations=orientations, 
                pixels_per_cell=pixels_per_cell, 
                cells_per_block=cells_per_block, 
                transform_sqrt=transform_sqrt, 
                visualise=True
            )
            eq_hogImage = exposure.rescale_intensity(eq_hogImage, out_range=(0, 255))
            eq_hogImage = eq_hogImage.astype("uint8")
            cv2.imshow("HOG Image w Equalize", eq_hogImage)
            cv2.waitKey(0)
        counter = counter + 1

        if (counter % 1000 == 0) or (counter == num_total):
            print "Extracted: {}".format(counter)

        fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
        fd_path = os.path.join(feat_path, fd_name)
        joblib.dump(fd, fd_path)
    
    print "HoG features saved in {}".format(feat_path)