"""
    script for extracting a sample of HoG given a setting every 50th sample from a certain view

    python extract_hog_sorted.py \
    -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ \
    -s UFPR04 \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/data/sorted_spaces/UFPR04_sort_nums.json \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog_sorted/sorted_UFPR04_blk-2_cell-6 \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_4.json \
    -ho blk-2_cell-6


    python extract_hog_sorted.py \
    -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ \
    -s UFPR04 \
    -j /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/data/sorted_spaces/UFPR04_sort_nums.json \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog_sorted/sorted_UFPR04_blk-3_cell-6 \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_4.json \
    -ho blk-3_cell-6

"""

import argparse
import glob
import os
import cv2
import json
import io
import sys
import shutil

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk',
        "--pklot_path", help="Path to PkLot dataset",
        required=True)
    ap.add_argument(
        "-s", "--set_option",
        help="Data set to use: PUC, UFPR04, UFPR05",
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of sorted numbers to load",
        required=True)
    ap.add_argument(
        "-f", "--target_folder",
        help="Target folder"
    )
    ap.add_argument(
        "-hj", "--hog_json",
        help="JSON file of hog settings"
    )
    ap.add_argument(
        "-ho", "--hog_option",
        help="ID of hog setting to use"
    )
    args = vars(ap.parse_args())

    test_pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    set_option = args["set_option"]
    target_folder = args["target_folder"]
    hog_json = args["hog_json"]
    hog_option = args["hog_option"]

    with open(hog_json) as datafile:
        hog_dict = json.load(datafile)

    hog_setting = None
    for option in hog_dict["hog"]:
        if hog_option == option["id"]:
            hog_setting = option
            break
    if hog_setting is None:
        print "Invalid hog option or hog id"
        sys.exit()

    if not os.path.isdir(target_folder):
        os.makedirs(target_folder)

    if os.path.exists(json_path):
        print "Viewing images from: {}".format(json_path)
        with open(json_path) as data_file:
            sorted_dict = json.load(data_file)
    else:
        print "Json path does not exist"
        sys.exit()

    pklot_path = test_pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    pos_path = os.path.join(str(pklot_path), set_option, "*", "*", "Occupied", "*")
    save_path = "sorted_nums_" + set_option

    set_length = 0
    if set_option == "UFPR04":
        set_length = 28
    elif set_option == "UFPR05":
        # set_length = 45
        set_length = 40
    elif set_option == "PUC":
        set_length = 100
    else:
        print "Invalid -s argument."
        print ""
        sys.exit()

    num_sorted = []
    #temp_sorted_directory = "sorted_" + set_option + "_images"
    sorted_directory = target_folder
    if os.path.isdir(sorted_directory):
        shutil.rmtree(sorted_directory)

    os.makedirs(sorted_directory)
    for i, category in enumerate(sorted_dict):
        if len(sorted_dict[category]) > 0:
            new_dir = os.path.join(sorted_directory, category)
            os.makedirs(new_dir)

    orientations = hog_setting["orientations"]
    pixels_per_cell = hog_setting["pixels_per_cell"]
    cells_per_block = hog_setting["cells_per_block"]
    transform_sqrt = hog_setting["transform_sqrt"]
    block_norm = hog_setting["block_norm"]


    for im_path in glob.glob(pos_path):
        if len(num_sorted) == set_length:
            break

        # parse path for image number
        check_num = im_path.split('#')[1].split('.')[0]
        check_num_int = int(check_num)  

        if check_num_int in num_sorted:
            continue
        else:
            num_sorted.append(check_num_int)
            for i, category in enumerate(sorted_dict):
                if check_num_int in sorted_dict[category]:
                    if set_option == "UFPR04":
                        if category == "left-1":
                            win_height = 80
                            win_width = 30

                        elif category == "left-2":
                            win_height = 80
                            win_width = 30

                        elif category == "left-3":
                            win_height = 80
                            win_width = 40
                        
                        elif category == "center-1":
                            win_height = 50
                            win_width = 40

                        elif category == "center-2":
                            win_height = 50
                            win_width = 40

                        elif category == "center-3":
                            win_height = 50
                            win_width = 40

                        elif category == "right-1":
                            win_height = 80
                            win_width = 50

                        elif category == "right-2":
                            win_height = 80
                            win_width = 40

                        elif category == "right-3":
                            win_height = 80
                            win_width = 40
                    window_size = (win_height, win_width)
                    im_window = cv2.imread(im_path)
                    im_window = cv2.cvtColor(im_window, cv2.COLOR_BGR2GRAY)
                    fd, hogImage = feature.hog(
                        im_window,
                        orientations=orientations,
                        pixels_per_cell=pixels_per_cell,
                        cells_per_block=cells_per_block,
                        transform_sqrt=transform_sqrt,
                        visualise=True,
                        block_norm=block_norm
                    )
                    hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
                    hogImage = hogImage.astype("uint8")
                    print("{}: {}".format(category, check_num_int))
                    print("{}/{} copied".format(len(num_sorted), set_length))
                    dest_path = os.path.join(sorted_directory, category)
                    image_name = im_path.split("/")[-1].split(".jpg")[0]
                    hog_image_name = image_name + "_hog.jpg"
                    hog_image_path = os.path.join(dest_path, hog_image_name)
                    cv2.imwrite(hog_image_path, hogImage)
                    copyFile(im_path, dest_path)

