"""
    Creates partial copy of paths file from pos paths. 

    Copies all except: left, center, right, left-1, left-2, [...]

    Example usage:
    python generate-occ-to-neg.py -j UFPR04_pos_paths.json -e left
"""

import argparse
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0

def generate_occ_to_neg(json_path, except_category, folder_path, project_name):

    with open(json_path) as data_file:
        json_paths = json.load(data_file)

    if except_category == "left":
        json_paths.pop("left-1")
        json_paths.pop("left-2")
        json_paths.pop("left-3")
    elif except_category == "center":
        json_paths.pop("center-1")
        json_paths.pop("center-2")
        json_paths.pop("center-3")
    elif except_category == "right":
        json_paths.pop("right-1")
        json_paths.pop("right-2")
        json_paths.pop("right-3")
    elif except_category == "left-1":
        json_paths.pop("left-1")
    elif except_category == "left-2":
        json_paths.pop("left-2")
    elif except_category == "left-3":
        json_paths.pop("left-3")
    elif except_category == "center-1":
        json_paths.pop("center-1")
    elif except_category == "center-2":
        json_paths.pop("center-2")
    elif except_category == "center-3":
        json_paths.pop("center-3")
    elif except_category == "right-1":
        json_paths.pop("right-1")
    elif except_category == "right-2":
        json_paths.pop("right-2")
    elif except_category == "right-3":
        json_paths.pop("right-3")
    else:
        print "You entered an invalid category"
        sys.exit()

    partial_filename = json_path.split("/")[-1].split(".json")[0] + "_except_" + except_category + ".json"
    print partial_filename 

    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "paths_sorted_partial", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, partial_filename)
    else:
        output_filename = os.path.join(folder_path, partial_filename)


    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(output_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        json_paths,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    print "New JSON saved in {}".format(output_filename)

    return output_filename

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of paths to convert to window paths json format",
        required=True)
    ap.add_argument(
        "-e", "--except_category",
        help="Enter category to except: left, right, center, and variants e.g. left-1",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())

    json_path = args["json_path"]
    except_category = args["except_category"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    generate_occ_to_neg(json_path, except_category, folder_path, project_name)
