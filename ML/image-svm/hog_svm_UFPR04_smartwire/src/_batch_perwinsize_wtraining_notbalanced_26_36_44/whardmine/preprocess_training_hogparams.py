"""
    Batch script for preprocess and training.
    Will output models, results for testing test.

    Also relevant model paths grouped according to hog parameter testing for.

    Return model paths list for each HoG parameter
    Return result paths list for each HoG parameter for w div.

    Example usage:
    python preprocess_training_hogparams.py \
    -pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
    -mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
    -dc UFPR04 \
    -nc "UFPR04" \
    -c1 "center-3" \
    -w 40 \
    -ht 50 \
    -pos "upper-left" \
    -n "test_preprocess_training_hogparams"

    python preprocess_training_hogparams.py \
    -pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
    -mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
    -dc UFPR04 \
    -nc "UFPR04" \
    -c1 "center-3" \
    -ec "center" \
    -w 40 \
    -ht 50 \
    -pos "upper-left" \
    -n "test_preprocess_training_hogparams"


"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib
from timeit import default_timer as timer

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

from skimage import exposure
from skimage import feature

from lib.generate_occ_to_neg import generate_occ_to_neg
from lib.generate_window import generate_window
from lib.generate_window_pos import generate_window_pos
from lib.extract_hog_paths import extract_hog_paths
from lib.extract_hog_neg import extract_hog_neg
from lib.train_svm_mult_w_div import train_svm_mult_w_div
from lib.train_svm_mult_wo_div import train_svm_mult_wo_div

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False


def preprocess_training_hogparams(
        pklot_path,
        main_pos_path,
        dataset_category,
        negative_category,
        category1,
        category2,
        category3,
        category4,
        category5,
        except_category,
        width,
        height,
        position,
        folder_path,
        project_name
        ):

    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    config_path = os.path.join(top_dir, "config")

    # Load HoG setting
    hog_config_path = os.path.join(config_path, "hog_list.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    # Generating for positive features

    # Generate adjusted dim
    window_path_pos_1 = generate_window(
        pklot_path, main_pos_path, category1, width, height, folder_path, project_name
        )
    if category2 is not None:
        window_path_pos_2 = generate_window(
            pklot_path, main_pos_path, category2,
            width, height, folder_path, project_name
        )
    if category3 is not None:
        window_path_pos_3 = generate_window(
            pklot_path, main_pos_path, category3,
            width, height, folder_path, project_name
        )
    if category4 is not None:
        window_path_pos_4 = generate_window(
            pklot_path, main_pos_path, category4,
            width, height, folder_path, project_name
        )
    if category5 is not None:
        window_path_pos_5 = generate_window(
            pklot_path, main_pos_path, category5,
            width, height, folder_path, project_name
        )

    # Generate roi window json
    roi_path_pos_1 = generate_window_pos(
        pklot_path, window_path_pos_1, position, folder_path, project_name
    )
    if category2 is not None:
        roi_path_pos_2 = generate_window_pos(
            pklot_path, window_path_pos_2, position, folder_path, project_name
        )
    if category3 is not None:
        roi_path_pos_3 = generate_window_pos(
            pklot_path, window_path_pos_3, position, folder_path, project_name
        )
    if category4 is not None:
        roi_path_pos_4 = generate_window_pos(
            pklot_path, window_path_pos_4, position, folder_path, project_name
        )
    if category5 is not None:
        roi_path_pos_5 = generate_window_pos(
            pklot_path, window_path_pos_5, position, folder_path, project_name
        )

    # Extract HoG
    hog_path_pos_1 = []
    hog_path_pos_2 = []
    hog_path_pos_3 = []
    hog_path_pos_4 = []
    hog_path_pos_5 = []

    for i, setting in enumerate(hog_dict["hog"]):
        temp_path = extract_hog_paths(
            pklot_path, roi_path_pos_1, width, height,
            folder_path, project_name, i
        )
        hog_path_pos_1.append(temp_path)
        if category2 is not None:
            temp_path = extract_hog_paths(
                pklot_path, roi_path_pos_2, width, height,
                folder_path, project_name, i
            )
            hog_path_pos_2.append(temp_path)
        if category3 is not None:
            temp_path = extract_hog_paths(
                pklot_path, roi_path_pos_3, width, height,
                folder_path, project_name, i
            )
            hog_path_pos_3.append(temp_path)
        if category4 is not None:
            temp_path = extract_hog_paths(
                pklot_path, roi_path_pos_4, width, height,
                folder_path, project_name, i
            )
            hog_path_pos_4.append(temp_path)
        if category5 is not None:
            temp_path = extract_hog_paths(
                pklot_path, roi_path_pos_5, width, height,
                folder_path, project_name, i
            )
            hog_path_pos_5.append(temp_path)

    # Generating for negative features

    # For the except features
    if except_category is not None:
        occ_neg_path = generate_occ_to_neg(
            main_pos_path, except_category,
            folder_path, project_name)

        window_path_neg_occ = generate_window(
            pklot_path, occ_neg_path, "ALL", 
            width, height, folder_path, project_name
            )
        roi_path_neg_occ = generate_window_pos(
            pklot_path, window_path_neg_occ, position, folder_path, project_name
        )
        hog_path_neg_occ = []
        for i, setting in enumerate(hog_dict["hog"]):
            temp_path = extract_hog_paths(
                pklot_path, roi_path_neg_occ, width, height, folder_path, 
                project_name, i
            )
            hog_path_neg_occ.append(temp_path)

    # For the indicated negative categories
    # Extrat hog
    hog_path_neg_1 = []
    for i, setting in enumerate(hog_dict["hog"]):
        temp_path = extract_hog_neg(
            pklot_path, negative_category, width, height,
            folder_path, project_name, i, "y", "y"
        )
        hog_path_neg_1.append(temp_path)

    # For Training
    # Going through each parameter change
    # Do both w div and wo div

    ## Training w/o except

    # Making model name
    pos_part_1 = "pos_" + category1 + "_"
    if category2 is not None:
        pos_part_1 = pos_part_1 + category2 + "_"
    if category3 is not None:
        pos_part_1 = pos_part_1 + category4 + "_"
    if category4 is not None:
        pos_part_1 = pos_part_1 + category5 + "_"
    if category5 is not None:
        pos_part_1 = pos_part_1 + category5 + "_"

    neg_part_1 = "neg_unocc_" + negative_category
    model_name_1 = pos_part_1 + neg_part_1

    model_path_wdiv = []
    result_path_wdiv = []

    # There are no except categories
    if except_category is None:
        for i, setting in enumerate(hog_dict["hog"]):
            if category5 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    pos_feat_path_5=hog_path_pos_5[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_1, width=width, height=height,
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category4 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_1, width=width, height=height,
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category3 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category2 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue

            temp_model_path, temp_result_path = train_svm_mult_w_div(
                pos_feat_path=hog_path_pos_1[i], neg_feat_path=hog_path_neg_1[i],
                model_name=model_name_1,
                width=width, height=height,
                project_name=project_name
            )
            model_path_wdiv.append(temp_model_path)
            result_path_wdiv.append(temp_result_path)
    else:
        for i, setting in enumerate(hog_dict["hog"]):
            if category5 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    pos_feat_path_5=hog_path_pos_5[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category4 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category3 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue
            if category2 is not None:
                temp_model_path, temp_result_path = train_svm_mult_w_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wdiv.append(temp_model_path)
                result_path_wdiv.append(temp_result_path)
                continue

            temp_model_path, temp_result_path = train_svm_mult_w_div(
                pos_feat_path=hog_path_pos_1[i], 
                neg_feat_path=hog_path_neg_1[i],
                neg_feat_path_2=hog_path_neg_occ[i], 
                model_name=model_name_1, width=width, height=height, 
                project_name=project_name
            )
            model_path_wdiv.append(temp_model_path)
            result_path_wdiv.append(temp_result_path)


    # For training w/o div
    pos_part_2 = pos_part_1
    neg_part_2 = "neg_unocc_" + negative_category
    model_name_2 = pos_part_2 + neg_part_2 + "wo_div"

    model_path_wo_div = []

    if except_category is not None:
        for i, setting in enumerate(hog_dict["hog"]):
            if category5 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    pos_feat_path_5=hog_path_pos_5[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category4 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category3 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category2 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    neg_feat_path=hog_path_neg_1[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue

            temp_model_path = train_svm_mult_wo_div(
                pos_feat_path=hog_path_pos_1[i], neg_feat_path=hog_path_neg_1[i], model_name=model_name_2, width=width, height=height, 
                project_name=project_name
            )
            model_path_wo_div.append(temp_model_path)
    else:
        for i, setting in enumerate(hog_dict["hog"]):
            if category5 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    pos_feat_path_5=hog_path_pos_5[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category4 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    pos_feat_path_4=hog_path_pos_4[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category3 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    pos_feat_path_3=hog_path_pos_3[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_2, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue
            if category2 is not None:
                temp_model_path = train_svm_mult_wo_div(
                    pos_feat_path=hog_path_pos_1[i],
                    pos_feat_path_2=hog_path_pos_2[i],
                    neg_feat_path=hog_path_neg_1[i],
                    neg_feat_path_2=hog_path_neg_occ[i],
                    model_name=model_name_1, width=width, height=height, 
                    project_name=project_name
                )
                model_path_wo_div.append(temp_model_path)
                result_path_wo_div.append(temp_result_path)
                continue

            temp_model_path = train_svm_mult_wo_div(
                pos_feat_path=hog_path_pos_1[i],
                neg_feat_path=hog_path_neg_1[i],
                neg_feat_path_2=hog_path_neg_occ[i], model_name=model_name_1,
                width=width, height=height,
                project_name=project_name
            )
            model_path_wo_div.append(temp_model_path)


    # Should be of the same category, just different HoG parameters
    # Generate list of resutls from diff parameters
    # Generate list of models from th diff parameters.

    # Generate list of python scripts to enter 

    model_description = "For finding optimal block size and cell size with HoG\n\n"

    parameters_list = (
        "pklot_path = {}\n"
        "main_pos_path = {}\n"
        "dataset_category = {}\n"
        "negative_category = {}\n"
        "category1 = {}\n"
        "category2 = {}\n"
        "category3 = {}\n"
        "category4 = {}\n"
        "category5 = {}\n"
        "except_category ={}\n" 
        "width = {}\n"
        "height = {}\n"
        "position = {}\n"
        "folder_path = {}\n"
        "project_name ={}\n"
        "hog_config_path = {}\n\n"

    ).format(
        pklot_path,
        main_pos_path,
        dataset_category,
        negative_category,
        category1,
        category2,
        category3,
        category4,
        category5,
        except_category,
        width,
        height,
        position,
        folder_path,
        project_name,
        hog_config_path,
    )
    print model_path_wdiv
    print result_path_wdiv 
    print model_path_wo_div

    return {"model_path_wdiv": model_path_wdiv, "result_path_wdiv": result_path_wdiv, "model_path_wo_div": model_path_wo_div}




if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-mp", "--main_pos_path",
        help="Main JSON file for paths for positive features",
        required=True)
    ap.add_argument(
        "-dc", "--dataset_category",
        help="Enter dataset category: UFPR04, UFPR05, PUC, or ALL",
        required=True
    )
    ap.add_argument(
        "-nc", "--negative_category",
        help="Enter negative dataset category: UFPR04, UFPR05, PUC, or ALL",
        required=True
    )
    ap.add_argument(
        "-c1", "--category1",
        help="Enter positive category name: left-1, left-2, etc. or ALL",
        required=True
    )
    ap.add_argument(
        "-c2", "--category2",
        help="Enter positive category name: left-1, left-2, etc. or ALL"
    )
    ap.add_argument(
        "-c3", "--category3",
        help="Enter positive category name: left-1, left-2, etc. or ALL"
    )
    ap.add_argument(
        "-c4", "--category4",
        help="Enter positive category name: left-1, left-2, etc. or ALL"
    )
    ap.add_argument(
        "-c5", "--category5",
        help="Enter positive category name: left-1, left-2, etc. or ALL"
    )
    ap.add_argument(
        "-ec", "--except_category",
        help="Enter category to exclude: left, right, center, left-1, none, etc."
    )
    ap.add_argument("-w", "--width", required=True, help="Width of window size")
    ap.add_argument("-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        "-pos", "--position",
        help="Enter position to crop from: upper-left, etc.",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    main_pos_path = args["main_pos_path"]
    dataset_category = args["dataset_category"]
    negative_category = args["negative_category"]
    category1 = args["category1"]
    category2 = args["category2"]
    category3 = args["category3"]
    category4 = args["category4"]
    category5 = args["category5"]
    except_category = args["except_category"]
    width = int(args["width"])
    height = int(args["height"])
    position = args["position"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    preprocess_training_hogparams(
        pklot_path,
        main_pos_path,
        dataset_category,
        negative_category,
        category1,
        category2,
        category3,
        category4,
        category5,
        except_category,
        width,
        height,
        position,
        folder_path,
        project_name
    )




