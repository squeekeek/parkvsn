"""
    Extracts HoG given paths file from generate-window.py

    Fixes:
    - changed to hog_list.json
    - fixed "hog_option" to hog_option

    Loads like hog-roi, but will resize image first before cropping.

    Example usage:
    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json -w 30 -ht 80

    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80.json -w 30 -ht 80

    python extract-hog-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json -w 30 -ht 80

    python extract_hog_paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -w 30 -ht 80 -n test

"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False

def extract_hog_paths(pklot_path, json_path, width, height, folder_path, project_name, hog_option, hog_dict):

    # Load json_path
    with open(json_path) as data_file:
        roi_dict = json.load(data_file)

    temp_feat_path =  "feats_" + json_path.split("/")[-1].split(".json")[0] + "_" + hog_dict["hog"][hog_option]["id"]


    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        feat_path = os.path.join(dir_main, "data", "feats_hog", project_name, temp_feat_path)
    else:
        feat_path = os.path.join(folder_path, temp_feat_path)

    # If feature directories don't exist, create them
    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)
    else:
        return feat_path

    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    # HoG parameters
    window_size = (width, height)
    orientations = hog_dict["hog"][hog_option]["orientations"]
    pixels_per_cell = hog_dict["hog"][hog_option]["pixels_per_cell"]
    cells_per_block = hog_dict["hog"][hog_option]["cells_per_block"]
    transform_sqrt = hog_dict["hog"][hog_option]["transform_sqrt"]
    block_norm = hog_dict["hog"][hog_option]["block_norm"]

    # Go thru every image
    # resize to window size
    # resize to window size 
    # extract HoG

    num_total = len(roi_dict)
    counter = 0

    for i, key in enumerate(roi_dict):
        for j, im_path in enumerate(roi_dict[key]):
            actual_path = actual_pklot_path + im_path
            orig_image = cv2.imread(actual_path)
            res_rows = roi_dict[key][im_path]["resized_dim"][0]
            res_cols = roi_dict[key][im_path]["resized_dim"][1]

            resized_image = cv2.resize(orig_image, (res_cols, res_rows))

            row_0 = roi_dict[key][im_path]["pt1"][0]
            row_1 = roi_dict[key][im_path]["pt2"][0]
            col_0 = roi_dict[key][im_path]["pt1"][1]
            col_1 = roi_dict[key][im_path]["pt2"][1]

            roi_image = resized_image[row_0:row_1, col_0:col_1]
            gray_resized = cv2.cvtColor(roi_image, cv2.COLOR_BGR2GRAY)
            if not ifVisualize:
                fd = feature.hog(
                    gray_resized,
                    orientations=orientations, 
                    pixels_per_cell=pixels_per_cell, 
                    cells_per_block=cells_per_block, 
                    transform_sqrt=transform_sqrt, 
                    visualise=False,
                    block_norm=block_norm
                )
            else:
                cv2.imshow("Gray image", gray_resized)
                fd, hogImage = feature.hog(
                    gray_resized, 
                    orientations=orientations, 
                    pixels_per_cell=pixels_per_cell, 
                    cells_per_block=cells_per_block, 
                    transform_sqrt=transform_sqrt, 
                    visualise=True,
                    block_norm=block_norm
                )
                hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
                hogImage = hogImage.astype("uint8")
                cv2.imshow("HOG Image", hogImage)
                cv2.waitKey(0)

            counter = counter + 1

            if (counter % 1000 == 0) or (counter == num_total):
                print "Extracted: {}".format(counter)

            fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
            fd_path = os.path.join(feat_path, fd_name)
            joblib.dump(fd, fd_path)
    
    print "HoG features saved in {}".format(feat_path)

    return feat_path

if __name__ == "__main__":
    # construct the argument parse and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json of images to extract hog",
        required=True)
    ap.add_argument(
        "-w", "--width", required=True, help="Width of window size")
    ap.add_argument(
        "-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    ap.add_argument(
        '-h', "--hog_option", help="Index of hog option to use", default=0
    )
    ap.add_argument(
        "-hj", "--hog_json",
        help="Enter path to hog json file to use",
        required=True
    )

    args = vars(ap.parse_args())

    # initialize the data matrix and labels
    print "[INFO] extracting features..."
    data = []
    labels = []

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    width = int(args["width"])
    height = int(args["height"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    hog_option = int(args["hog_option"])
    hog_json = args["hog_json"]

    # Load HoG setting
    hog_config_path = hog_json
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    extract_hog_paths(pklot_path, json_path, width, height, folder_path, project_name, hog_option, hog_dict)