"""
    Extracts lbp given paths file from generate-window.py

    Fixes:
    - changed to lbp_list.json
    - fixed "lbp_option" to lbp_option

    Loads like lbp-roi, but will resize image first before cropping.

    Example usage:
    python extract-lbp-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json -w 30 -ht 80

    python extract-lbp-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80.json -w 30 -ht 80

    python extract-lbp-paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j pos_upper-left_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json -w 30 -ht 80

    python extract_lbp_paths.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -w 30 -ht 80 -n test

"""

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import numpy as np

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from localbinarypatterns import LocalBinaryPatterns

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = False
ifVisualize = False

def extract_lbp_paths(pklot_path, json_path, width, height, folder_path, project_name, lbp_option, lbp_dict):

    # Load json_path
    with open(json_path) as data_file:
        roi_dict = json.load(data_file)

    temp_feat_path =  "feats_" + json_path.split("/")[-1].split(".json")[0] + "_" + lbp_dict["lbp"][lbp_option]["id"]


    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        feat_path = os.path.join(dir_main, "data", "feats_lbp", project_name, temp_feat_path)
    else:
        feat_path = os.path.join(folder_path, temp_feat_path)

    # If feature directories don't exist, create them
    if not os.path.isdir(feat_path):
        os.makedirs(feat_path)
    else:
        return feat_path

    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    # lbp parameters
    window_size = (width, height)
    lbp_P = lbp_dict["lbp"][lbp_option]["P"]
    lbp_R = lbp_dict["lbp"][lbp_option]["R"]
    lbp_rows = lbp_dict["lbp"][lbp_option]["rows"]
    lbp_cols = lbp_dict["lbp"][lbp_option]["cols"]

    lbp = LocalBinaryPatterns(numPoints=lbp_P, radius=lbp_R)

    # Go thru every image
    # resize to window size
    # resize to window size 
    # extract lbp

    num_total = len(roi_dict)
    counter = 0

    for i, key in enumerate(roi_dict):
        for j, im_path in enumerate(roi_dict[key]):
            actual_path = actual_pklot_path + im_path
            orig_image = cv2.imread(actual_path)
            res_rows = roi_dict[key][im_path]["resized_dim"][0]
            res_cols = roi_dict[key][im_path]["resized_dim"][1]

            resized_image = cv2.resize(orig_image, (res_cols, res_rows))

            row_0 = roi_dict[key][im_path]["pt1"][0]
            row_1 = roi_dict[key][im_path]["pt2"][0]
            col_0 = roi_dict[key][im_path]["pt1"][1]
            col_1 = roi_dict[key][im_path]["pt2"][1]

            roi_image = resized_image[row_0:row_1, col_0:col_1]
            gray_resized = cv2.cvtColor(roi_image, cv2.COLOR_BGR2GRAY)
            image_rows = gray_resized.shape[0]
            image_cols = gray_resized.shape[1]

            pixel_rows = image_rows/lbp_rows
            pixel_cols = image_cols/lbp_cols

            curr_row = 0

            for m in range(0, lbp_rows, 1):
                curr_col = 0
                for n in range(0, lbp_cols, 1):
                    curr_block = gray_resized[curr_row:curr_row+pixel_rows, curr_col:curr_col+pixel_cols]
                    fd_partial = lbp.describe(curr_block)
                    if (m == 0) and (n == 0):
                        fd = fd_partial
                    else:
                        fd = np.concatenate((fd, fd_partial))

                    curr_col = curr_col + pixel_cols

                curr_row = curr_row + pixel_rows


            #fd = lbp.describe(gray_resized)

            counter = counter + 1

            if (counter % 1000 == 0) or (counter == num_total):
                print "Extracted: {}".format(counter)

            fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
            fd_path = os.path.join(feat_path, fd_name)
            joblib.dump(fd, fd_path)
    
    print "LBP features saved in {}".format(feat_path)

    return feat_path

if __name__ == "__main__":
    # construct the argument parse and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        '-j', "--json_path", help="Path to json of images to extract lbp",
        required=True)
    ap.add_argument(
        "-w", "--width", required=True, help="Width of window size")
    ap.add_argument(
        "-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    ap.add_argument(
        '-lo', "--lbp_option", help="Index of lbp option to use", default=0
    )
    ap.add_argument(
        "-lj", "--lbp_json",
        help="Enter path to lbp json file to use",
        required=True
    )

    args = vars(ap.parse_args())

    # initialize the data matrix and labels
    print "[INFO] extracting features..."
    data = []
    labels = []

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    width = int(args["width"])
    height = int(args["height"])
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    lbp_option = int(args["lbp_option"])
    lbp_json = args["lbp_json"]

    # Load lbp setting
    lbp_config_path = lbp_json
    with open(lbp_config_path) as data_file:
        lbp_dict = json.load(data_file)

    extract_lbp_paths(pklot_path, json_path, width, height, folder_path, project_name, lbp_option, lbp_dict)