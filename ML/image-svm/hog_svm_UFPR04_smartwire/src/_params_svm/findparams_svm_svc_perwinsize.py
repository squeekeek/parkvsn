import numpy as np
from sklearn import svm

from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
import os
import glob
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report



"""
ValueError: 'f1-score' is not a valid scoring value. Valid options are ['accuracy', 'adjusted_rand_score', 'average_precision', 'f1', 'f1_macro', 'f1_micro', 'f1_samples', 'f1_weighted', 'neg_log_loss', 'neg_mean_absolute_error', 'neg_mean_squared_error', 'neg_median_absolute_error', 'precision', 'precision_macro', 'precision_micro', 'precision_samples', 'precision_weighted', 'r2', 'recall', 'recall_macro', 'recall_micro', 'recall_samples', 'recall_weighted', 'roc_auc']
"""
# Load input data
main_hog_folder = "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/per_winsize_wo_except/"
pos_folder_name_1 = "feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80_blk-2_cell-6"
pos_folder_name_2 = "feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80_blk-2_cell-6"
pos_folder_name_3 = "feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-30h-80_blk-2_cell-6"
neg_folder_name = "feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80_blk-2_cell-6"
pos_folder_1 = os.path.join(main_hog_folder, pos_folder_name_1)
pos_folder_2 = os.path.join(main_hog_folder, pos_folder_name_2)
pos_folder_3 = os.path.join(main_hog_folder, pos_folder_name_3)

neg_folder = os.path.join(main_hog_folder, neg_folder_name)

fds = []
labels = []
for feat_path in glob.glob(os.path.join(pos_folder_1, "*.feat")):
    fd = joblib.load(feat_path)
    fds.append(fd)
    labels.append(1)
for feat_path in glob.glob(os.path.join(pos_folder_2, "*.feat")):
    fd = joblib.load(feat_path)
    fds.append(fd)
    labels.append(1)
for feat_path in glob.glob(os.path.join(pos_folder_3, "*.feat")):
    fd = joblib.load(feat_path)
    fds.append(fd)
    labels.append(1)

for feat_path in glob.glob(os.path.join(neg_folder, "*.feat")):
    fd = joblib.load(feat_path)
    fds.append(fd)
    labels.append(0)

#input_file = 'data_multivar.txt'
#X, y = utilities.load_data(input_file)

###############################################
# Train test split

X_train, X_test, y_train, y_test = train_test_split(fds, labels, test_size=0.25, random_state=5)

# Set the parameters by cross-validation
parameter_grid = [  {'kernel': ['linear'], 'C': np.logspace(-3, 3, 7).tolist() },
                    {'kernel': ['rbf'], 'gamma': np.logspace(-3, 3, 7).tolist(), 'C': np.logspace(-3, 3, 7).tolist()},
                 ]


write_folder = "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/find_svm_params"
write_path = os.path.join(write_folder, "findparams_svc_perwinsize.txt")
write_file = open(write_path, 'w')
write_file.truncate()

metrics = ['precision', 'recall_weighted', 'recall', 'accuracy', 'f1' ]

for metric in metrics:
    str_a =  "\n#### Searching optimal hyperparameters for: {}\n".format(metric)
    print str_a
    write_file.write(str_a)

    classifier = GridSearchCV(svm.SVC(), 
            parameter_grid, cv=5, scoring=metric)
    classifier.fit(X_train, y_train)

    str_a = "\nScores across the parameter grid:\n"
    print str_a
    write_file.write(str_a)
    for params, avg_score, _ in classifier.grid_scores_:
        str_a = "{} --> {}".format(params, round(avg_score, 3))
        print str_a
        write_file.write(str_a)

    str_a = "\nHighest scoring parameter set: {}".format(classifier.best_params_)
    print str_a
    write_file.write(str_a)

    y_true, y_pred = y_test, classifier.predict(X_test)
    str_a = "\nFull performance report:\n"
    print str_a
    write_file.write(str_a)
    target_names = [
        "Empty",
        "Occupied"
    ]
    str_a =  (classification_report(y_true, y_pred, target_names=target_names, digits=4))
    print str_a
    write_file.write(str_a)
write_file.close()

