"""
    Trains and tests model
    -p1, -p2, -p3
    -n1, n2, -n3

    Creates own division of 60% training, 20% cv, 20% test

    Example usage:

    python train_svm_trainingset_mult.py \
    -m right-1-2-3-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -p2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
    -p3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n2 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n3 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-2_UFPR04_neg_paths_7677_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 

    python train_svm_trainingset_mult.py \
    -m right-1-pos-neg \
    -p1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_pos_paths_2001_w-40h-80 \
    -n1 /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_right-1_UFPR04_neg_paths_1789_w-40h-80\
    -n test \
    -w 40 \
    -ht 80 



"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse 
import glob
import os
import sys
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

def train_svm_mult_w_div(pos_feat_path, pos_feat_path_2, pos_feat_path_3, pos_feat_path_4, pos_feat_path_5, pos_feat_path_6, pos_feat_path_7, pos_feat_path_8, pos_feat_path_9, pos_feat_path_10, pos_feat_path_11, neg_feat_path, neg_feat_path_2, neg_feat_path_3, neg_feat_path_4, neg_feat_path_5, neg_feat_path_6, neg_feat_path_7, neg_feat_path_8, neg_feat_path_9, neg_feat_path_10, neg_feat_path_11, model_name, width, height, folder_path, project_name):

    # If feature directories don't exist, create them
    temp_model_path =  "w" + str(width) + "_h" + str(height) + "_" + model_name + '.model'
    results_filename = temp_model_path.split(".model")[0] + ".txt"
    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "models", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        model_path = os.path.join(data_folder, temp_model_path)
        results_path = os.path.join(data_folder, results_filename)
    else:
        model_path = os.path.join(folder_path, temp_model_path)
        results_path = os.path.join(folder_path, results_filename)

    write_file = open(results_path, 'w')
    write_file.truncate()

    str_a = "Positive folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = pos_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    fds = []
    labels = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(1)

    if pos_feat_path_2 is not None:
        print "Has 2nd pospaths folder"
        str_a = pos_feat_path_2  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_2, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 2nd pos paths folder"

    if pos_feat_path_3 is not None:
        print "Has 3rd pospaths folder"
        str_a = pos_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_3, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 3rd pos paths folder"

    if pos_feat_path_4 is not None:
        print "Has 4th pospaths folder"
        str_a = pos_feat_path_4  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_4, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 3rd pos paths folder"

    if pos_feat_path_4 is not None:
        print "Has 4th pospaths folder"
        str_a = pos_feat_path_4  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_4, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 4th pos paths folder"

    if pos_feat_path_5 is not None:
        print "Has 5th pospaths folder"
        str_a = pos_feat_path_5  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_5, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 5th pos paths folder"

    if pos_feat_path_6 is not None:
        print "Has 6th pospaths folder"
        str_a = pos_feat_path_6  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_6, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 6th pos paths folder"

    if pos_feat_path_7 is not None:
        print "Has 7th pospaths folder"
        str_a = pos_feat_path_7  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_7, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 7th pos paths folder"

    if pos_feat_path_8 is not None:
        print "Has 8th pospaths folder"
        str_a = pos_feat_path_8  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_8, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 8th pos paths folder"

    if pos_feat_path_9 is not None:
        print "Has 9th pospaths folder"
        str_a = pos_feat_path_9  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_9, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 9th pos paths folder"

    if pos_feat_path_10 is not None:
        print "Has 10th pospaths folder"
        str_a = pos_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_10, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 10th pos paths folder"

    if pos_feat_path_11 is not None:
        print "Has 11th pospaths folder"
        str_a = pos_feat_path_11  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(pos_feat_path_11, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(1)
    else:
        print "No 11th pos paths folder"

    str_a = "\n"
    print str_a
    write_file.write(str_a)
    
    str_a = "Negative folders: \n"
    print str_a
    write_file.write(str_a)
    str_a = neg_feat_path  + "\n"
    print str_a
    write_file.write(str_a)

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(0)

    if neg_feat_path_2 is not None:
        print "Has 2nd negpaths folder"
        str_a = neg_feat_path_2  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_2, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 2nd neg paths folder"

    if neg_feat_path_3 is not None:
        print "Has 3rd negpaths folder"
        str_a = neg_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_3, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 3rd neg paths folder"

    if neg_feat_path_4 is not None:
        print "Has 4th negpaths folder"
        str_a = neg_feat_path_4  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_4, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 3rd neg paths folder"

    if neg_feat_path_4 is not None:
        print "Has 4th negpaths folder"
        str_a = neg_feat_path_4  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_4, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 4th neg paths folder"

    if neg_feat_path_5 is not None:
        print "Has 5th negpaths folder"
        str_a = neg_feat_path_5  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_5, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 5th neg paths folder"

    if neg_feat_path_6 is not None:
        print "Has 6th negpaths folder"
        str_a = neg_feat_path_6  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_6, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 6th neg paths folder"

    if neg_feat_path_7 is not None:
        print "Has 7th negpaths folder"
        str_a = neg_feat_path_7  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_7, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 7th neg paths folder"

    if neg_feat_path_8 is not None:
        print "Has 8th negpaths folder"
        str_a = neg_feat_path_8  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_8, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 8th neg paths folder"

    if neg_feat_path_9 is not None:
        print "Has 9th negpaths folder"
        str_a = neg_feat_path_9  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_9, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 9th neg paths folder"

    if neg_feat_path_10 is not None:
        print "Has 10th negpaths folder"
        str_a = neg_feat_path_3  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_10, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 10th neg paths folder"

    if neg_feat_path_11 is not None:
        print "Has 11th negpaths folder"
        str_a = neg_feat_path_11  + "\n"
        print str_a
        write_file.write(str_a)
        for feat_path in glob.glob(os.path.join(neg_feat_path_11, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
    else:
        print "No 11th neg paths folder"

    str_a = "\n"
    print str_a
    write_file.write(str_a)

    # Perform test split
    # 60% - train, 20% - cross validation, 20% - test

    fds_train, fds_temp, labels_train, labels_temp = train_test_split(
        fds, labels, test_size=0.40, random_state=5)
    fds_test, fds_cv, labels_test, labels_cv = train_test_split(
        fds_temp, labels_temp, test_size=0.50, random_state=5)

    clf = LinearSVC()

    print "Training a Linear SVM Classifier"
    clf.fit(fds_train, labels_train)




    joblib.dump(clf, model_path)
    str_a =  "Classifier saved to {}\n".format(model_path)
    print str_a
    write_file.write(str_a)

    cv_predict = clf.predict(fds_cv)
    test_predict = clf.predict(fds_test)

    len_train = len(labels_train)
    len_cv = len(labels_cv)
    len_test = len(labels_test)
    len_total = len_train + len_cv + len_test

    


    print_total = (
        "Features in dataset (100%):\n"
        "Count: {}\n"
    ).format(len_total)

    print_train = (
        "Features in training dataset (60%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_train, Counter(labels_train))

    print_cv = (
        "Features in cross-validation dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_cv, Counter(labels_cv))

    print_test = (
        "Features in test dataset (20%):\n"
        "Count: {}\n"
        "{}\n"
    ).format(len_test, Counter(labels_test))

    write_file.write(print_total)
    print print_total
    write_file.write(print_train)
    print print_train
    write_file.write(print_cv)
    print print_cv
    write_file.write(print_test)
    print print_test

    target_names = [
        "Empty",
        "Occupied"
    ]
    
    str_a = "For cross-validation set (20%):\n"
    print str_a
    write_file.write(str_a)
    str_a =  (classification_report(
        labels_cv, cv_predict, target_names=target_names, digits=4))
    print str_a
    write_file.write(str_a)

    str_a =  "For test set (20%):\n"
    print str_a
    write_file.write(str_a)
    str_a =  (classification_report(
        labels_test, test_predict, target_names=target_names,digits=4))
    print str_a
    write_file.write(str_a)
    write_file.close()

    return (model_path, results_path)

if __name__ == "__main__":
    # Parse the command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-p1', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    ap.add_argument(
        "-p2", "--posfeat2",
        help="Path to 2nd positive features directory"
    )
    ap.add_argument(
        "-p3", "--posfeat3",
        help="Path to 3rd positive features directory"
    )
    ap.add_argument(
        "-p4", "--posfeat4",
        help="Path to 4th positive features directory"
    )
    ap.add_argument(
        "-p5", "--posfeat5",
        help="Path to 5th positive features directory"
    )
    ap.add_argument(
        "-p6", "--posfeat6",
        help="Path to 6th positive features directory"
    )
    ap.add_argument(
        "-p7", "--posfeat7",
        help="Path to 7th positive features directory"
    )
    ap.add_argument(
        "-p8", "--posfeat8",
        help="Path to 8th positive features directory"
    )
    ap.add_argument(
        "-p9", "--posfeat9",
        help="Path to 9th positive features directory"
    )
    ap.add_argument(
        "-p10", "--posfeat10",
        help="Path to 10th positive features directory"
    )
    ap.add_argument(
        "-p11", "--posfeat11",
        help="Path to 11th positive features directory"
    )

    ap.add_argument(
        '-n1', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    ap.add_argument(
        "-n2", "--negfeat2",
        help="Path to 2nd negative features directory"
    )
    ap.add_argument(
        "-n3", "--negfeat3",
        help="Path to 3rd negative features directory"
    )
    ap.add_argument(
        "-n4", "--negfeat4",
        help="Path to 4th negative features directory"
    )
    ap.add_argument(
        "-n5", "--negfeat5",
        help="Path to 5th negative features directory"
    )
    ap.add_argument(
        "-n6", "--negfeat6",
        help="Path to 6th negative features directory"
    )
    ap.add_argument(
        "-n7", "--negfeat7",
        help="Path to 7th negative features directory"
    )
    ap.add_argument(
        "-n8", "--negfeat8",
        help="Path to 8th negative features directory"
    )
    ap.add_argument(
        "-n9", "--negfeat9",
        help="Path to 9th negative features directory"
    )
    ap.add_argument(
        "-n10", "--negfeat10",
        help="Path to 10th negative features directory"
    )
    ap.add_argument(
        "-n11", "--negfeat11",
        help="Path to 11th negative features directory"
    )


    ap.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)
    ap.add_argument(
        '-w', "--width",
        help="width of the window size",
        required=True
    )
    ap.add_argument(
        '-ht', "--height",
        help="Height of the window size",
        required=True
    )
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")

    args = vars(ap.parse_args())

    pos_feat_path = args["posfeat"]
    pos_feat_path_2 = args["posfeat2"]
    pos_feat_path_3 = args["posfeat3"]
    pos_feat_path_4 = args["posfeat4"]
    pos_feat_path_5 = args["posfeat5"]
    pos_feat_path_6 = args["posfeat6"]
    pos_feat_path_7 = args["posfeat7"]
    pos_feat_path_8 = args["posfeat8"]
    pos_feat_path_9 = args["posfeat9"]
    pos_feat_path_10 = args["posfeat10"]
    pos_feat_path_11 = args["posfeat11"]

    neg_feat_path = args["negfeat"]
    neg_feat_path_2 = args["negfeat2"]
    neg_feat_path_3 = args["negfeat3"]
    neg_feat_path_4 = args["negfeat4"]
    neg_feat_path_5 = args["negfeat5"]
    neg_feat_path_6 = args["negfeat6"]
    neg_feat_path_7 = args["negfeat7"]
    neg_feat_path_8 = args["negfeat8"]
    neg_feat_path_9 = args["negfeat9"]
    neg_feat_path_10 = args["negfeat10"]
    neg_feat_path_11 = args["negfeat11"]

    model_name = args["model_name"]
    width = args["width"]
    height = args["height"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    
    model_path = train_svm_mult_w_div(pos_feat_path, pos_feat_path_2, pos_feat_path_3, pos_feat_path_4, pos_feat_path_5, pos_feat_path_6, pos_feat_path_7, pos_feat_path_8, pos_feat_path_9, pos_feat_path_10, pos_feat_path_11, neg_feat_path, neg_feat_path_2, neg_feat_path_3, neg_feat_path_4, neg_feat_path_5, neg_feat_path_6, neg_feat_path_7, neg_feat_path_8, neg_feat_path_9, neg_feat_path_10, neg_feat_path_11, model_name, width, height, folder_path, project_name)

    print "Model path:"
    print model_path
