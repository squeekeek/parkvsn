"""
    Tests a model given a folder of hog, expected label, and a model file

    Derived from train-svm-trainingset-mult.py

    Example usage:
    python eval_folder_hog.py \
    -f /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 \
    -m /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/models/UFPR04_halfway/w30_h80_pos_left-1neg_unocc_left-1_left.model  \
    -l 1


"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":

    parser = ap.ArgumentParser()
    parser.add_argument(
        '-f', "--hog_folder",
        help="Path to the hog folder to evaluate",
        required=True)
    parser.add_argument(
        '-m', "--model_path",
        help="Name of the model file",
        required=True)
    parser.add_argument(
        '-l', "--label",
        help="Output label of the hog foler",
        required=True
    )

    args = vars(parser.parse_args())

    hog_folder = args["hog_folder"]
    model_path = args["model_path"]
    label = int(args["label"])

    fds = []
    labels = []
    # Load the features
    for feat_path in glob.glob(os.path.join(hog_folder, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(label)

    # Load the classifier
    clf = joblib.load(model_path)

    predict_test = clf.predict(fds)
    len_total = len(labels)

    print_total = (
        "Features in dataset (100%):\n"
        "Count: {}\n"
    ).format(len_total)

    print print_total

    target_names = [
        "Empty",
        "Occupied"
    ]
    print "Classification Report:"
    #print (classification_report(
    #    labels, predict_test, target_names=target_names))
    #print str(predict_test)
    predict_list = predict_test.tolist()

    count_1 = predict_list.count(1)
    count_0 = predict_list.count(0)
    if label == 1:
        accuracy = (count_1/float(len_total))*100
        print "Accuracy for occupied: {}".format(accuracy)
        print "Count_1: {}".format(count_1)
        print "Count_0: {}".format(count_0)
    else:
        accuracy = (count_0/float(len_total))*100
        print "Accuracy for empty: {}".format(accuracy)
        print "Count_1: {}".format(count_1)
        print "Count_0: {}".format(count_0)


