"""
    Tests model

    Example usage:
    python test-svm-trainingset.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80  -m pos_left-1neg_unocc_left-1.model


    Example usage:
    python eval_posneg_hog.py \
    -p /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 \
    -n /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_tests_UFPR04/feats_pos_upper-left_genwin_left-2_UFPR04_neg_paths_2295_w-30h-80\
    -m /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/models/UFPR04_halfway/w30_h80_pos_left-1neg_unocc_left-1_left.model  


"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-p', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    parser.add_argument(
        '-n', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    parser.add_argument(
        "-n2", "--negfeat2",
        help="Path to 2nd negative features directory",
        default=""
    )
    parser.add_argument(
        '-m', "--model_path",
        help="Path of the model file",
        required=True)

    args = vars(parser.parse_args())

    pos_feat_path = args["posfeat"]
    neg_feat_path = args["negfeat"]
    neg_feat_path_2 = args["negfeat2"]
    model_path = args["model_path"]

    fds = []
    labels = []
    pos_nums = 0
    neg_nums = 0
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(1)
        pos_nums = pos_nums + 1

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(0)
        neg_nums = neg_nums + 1

    if len(neg_feat_path_2) > 0:
        print "Has 2nd negpaths folder"
        for feat_path in glob.glob(os.path.join(neg_feat_path_2, "*.feat")):
            fd = joblib.load(feat_path)
            fds.append(fd)
            labels.append(0)
            neg_nums = neg_nums + 1
    else:
        print "No 2nd neg paths folder"

    # Load the classifier
    clf = joblib.load(model_path)

    predict_test = clf.predict(fds)
    len_total = len(labels)

    print_total = (
        "Features in dataset (100%):\n"
        "Count: {}\n"
    ).format(len_total)

    print print_total

    target_names = [
        "Empty",
        "Occupied"
    ]
    print "Classification Report:"
    print (classification_report(
        labels, predict_test, target_names=target_names))
    #print str(predict_test)

    predict_list = predict_test.tolist()

    count_1 = predict_list.count(1)
    count_0 = predict_list.count(0)

    print "Positive features: {}".format(pos_nums)
    print "Negative features: {}".format(neg_nums)
    print "Count of occupied: {}".format(count_1)
    print "Count of empty: {}".format(count_0)
    """
    predict_list = predict_test.tolist()

    count_1 = predict_list.count(1)
    count_0 = predict_list.count(0)

    accuracy_occupied = (count_1/float(neg_nums))*100
    print "Accuracy for occupied: {}".format(accuracy)
    print "Count_1: {}".format(count_1)
    print "Count_0: {}".format(count_0)

    accuracy_empty = (count_0/float(len_total))*100
    print "Accuracy for empty: {}".format(accuracy)
    print "Count_1: {}".format(count_1)
    print "Count_0: {}".format(count_0)

    """
