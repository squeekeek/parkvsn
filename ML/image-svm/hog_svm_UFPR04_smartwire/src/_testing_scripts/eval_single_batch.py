
"""
    Evaluates images in a given folder
    python /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_testing_scripts/eval_single_batch.py \
    -f /Users/agatha/Desktop/eeei_4th_bridge \
    -mf /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_26_wo_except/blk-2_cell-6/wo_div/ \
    -mj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/models_UFPR04/perwinsize_26_wo_except/blk-2_cell-6/wo_div/models.json 

"""

from lib.p1_hog_svm import hog_svm
import cv2
import json
import os
import argparse
import glob
from timeit import default_timer as timer
def eval_batch(folder_path,model_folder, json_dict):
    eval_dict = {}
    for im_path in glob.glob(os.path.join(folder_path, "*.jpg")):
        print im_path

        image = cv2.imread(im_path)
        start_timer = timer()
        #pred = hog_svm(image, model_list, json_dict)
        pred = hog_svm(image, json_dict)
        end_timer = timer()
        print (end_timer - start_timer)
        print "Prediction: {}".format(pred)
        im_name = im_path.split("/")[-1]
        eval_dict[im_name] = pred

    return eval_dict
if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-f', "--folder_path",
        help="Path to the folder path of images",
        required=True
    )
    ap.add_argument(
        '-mf', "--model_folder_path",
        help="Path to the model folder path",
        required=True
    )
    ap.add_argument(
        '-mj', "--model_json",
        help="Path to the model json path",
        required=True
    )

    args = vars(ap.parse_args())
    folder_path = args["folder_path"]
    model_folder_path = args["model_folder_path"]
    model_json_path = args["model_json"]

    with open(model_json_path) as data_file:
        json_dict = json.load(data_file)
    eval_dict = eval_batch(folder_path, model_folder_path, model_json_path)
    print eval_dict
