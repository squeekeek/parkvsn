import imutils

def pyramid(image, scale, minSize):
    # yield the original image
    yield image

    # keep looping over the pyramid
    while True:
        # compute the new dimensions of the image and resize it
        print "image.shape[1]: {}, scale: {}".format(image.shape[1], scale)
        w = int(image.shape[1] / float(scale))
        print "new width: {}".format(w)
        image = imutils.resize(image, width=w)

        # if the resized image does not meet the supplied minimum
        # size, then stop constructing the pyramid
        if image.shape[0] < minSize[0] or image.shape[1] < minSize[1]:
            print "image.shape[0]: {}".format(image.shape[0])
            print "minSize[0]: {}".format(minSize[0])
            print "image.shape[1]: {}".format(image.shape[1])
            print "minSize[1]: {}".format(minSize[1])
           
            break

        # yield the next image in the pyramid
        yield image

def sliding_window(image, windowSize, stepSize):
    # slide a window across the image
    for y in xrange(0, image.shape[0], stepSize[0]):
        for x in xrange(0, image.shape[1], stepSize[1]):
            # yield the current window 
            yield (x, y, image[y:y + windowSize[0], x:x + windowSize[1]])