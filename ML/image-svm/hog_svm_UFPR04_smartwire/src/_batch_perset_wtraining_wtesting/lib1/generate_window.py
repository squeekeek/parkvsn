"""
    For json_adjusted_dim

    Generates category paths JSON adjusted for window sizes.

    Needs a given window size to be used for all images. 
    Crops an image from upper left to lower right given window size. 
        If the image has greater width than specified, the image is first resized to window width maintaining aspect ratio. 
        If the image has smaller width than specified, the image is first reszied to window width maintaining aspect ratio. 

        If image has smaller height than window size, image is resized with increasing window width, maintaining aspect ratio. Upon reaching an appropriate width, cropping is done. 

    Logs
        Store image paths still by cateogry
        each image path has details regarding resizing and cropping
        new_dim should be window size
        {
            "left-1":
                {
                    image-path: {
                        orig_dim: [cols, rows],
                        resized_dim: [cols, rows],
                        new_dim: [cols, row],
                        ifIncreasedWidth: 1,
                        ifIncreasedHeight: 0
                    },
                    image-path: {
                        orig_dim: [cols, rows],
                        resized_dim: [cols, rows]
                        new_dim: [cols, row],
                        ifIncreasedWidth: 1,
                        ifIncreasedHeight: 0
                    },
                }
        }
    Example usage: 
    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_pos_paths.json -c left-1 -w 30 -ht 80

    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j UFPR04_neg_paths.json -c ALL -w 30 -ht 80

    python generate-window.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json -c ALL -w 30 -ht 80 -n test

"""

import argparse
import glob
import os
import json
import io
import cv2
import sys

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


def generate_window(pklot_path, json_path, category, width, height, folder_path, project_name):

    ratio = height/float(width)

    with open(json_path) as data_file:
        json_paths = json.load(data_file)
    
    to_extract = []
    if category == "ALL":
        to_extract = json_paths.keys()
    else:
        to_extract.append(category)


    actual_pklot_path = pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"

    cropped_decoded = {}

    ifIncreasedWidth = 0
    ifIncreasedHeight = 0
    cropped_images = 0
    for i, key in enumerate(to_extract):
        cropped_decoded.update(
            {
                key: {}
            }
        )
        for j, im_path in enumerate(json_paths[key]):
            # crop picture to an appropriate ratio
            decoded_path = actual_pklot_path + im_path
            orig_image = cv2.imread(decoded_path)
            rows, cols = orig_image.shape[:2]
            orig_height = rows
            orig_width = cols
            orig_ratio = orig_height/float(orig_width)

            if orig_width < width:
                ifIncreasedWidth = 1
            else:
                ifIncreasedWidth = 0

            # Change width to that of window size
            res_width = width
            # might need ot change this to compensate for 1 pixel
            res_height = int(res_width*orig_ratio)
            if DEBUG:
                print "firstres_width: {}".format(res_width)
                print "firstres_height: {}".format(res_height)

            if res_height < height:
                ifIncreasedHeight = 1
                orig_ratio2 = orig_width/float(orig_height)
                res_height = height
                res_width = int(res_height*orig_ratio2)
                if DEBUG:
                    print "secres_width: {}".format(res_width)
                    print "secres_height: {}".format(res_height)
            else:
                ifIncreasedHeight = 0

            while True:
                if (res_width < width) or (res_height < height):
                    res_width = res_width + 1
                    res_height = int(res_width*orig_ratio)
                    if DEBUG:
                        print "loop_width: {}".format(res_width)
                        print "loop_height: {}".format(res_height)
                else:
                    break

            cropped_decoded[key].update(
                {
                    im_path: {
                        "orig_dim": [rows, cols],
                        "resized_dim": [res_height, res_width],
                        "win_size": [height, width],
                        "ifIncreasedWidth": ifIncreasedWidth,
                        "ifIncreasedHeight": ifIncreasedHeight
                    }
                }
            )


            cropped_images = cropped_images + 1
            if (cropped_images % 1000 == 0):
                print cropped_images

        print cropped_images

    cropped_filename = "genwin_" + category + "_" + json_path.split("/")[-1].split(".json")[0] + "_" + str(cropped_images) + "_w-" + str(width) + "h-" + str(height) + ".json"

    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "json_adjusted_dim", project_name)
        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)
        output_filename = os.path.join(data_folder, cropped_filename)
    else:
        output_filename = os.path.join(folder_path, cropped_filename)


    #output_filename = os.path.join(output_folder, cropped_filename)
    #output_filename = cropped_filename
    """
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    """
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(output_filename, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(
                        cropped_decoded,
                        indent=4, sort_keys=True,
                        separators=(',', ':'), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    print "Cropped details saved in {}".format(output_filename)

    return output_filename

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of paths to convert to window paths json format",
        required=True)
    ap.add_argument(
        "-c", "--category",
        help="Enter category name: left-1, left-2, etc. or ALL",
        required=True
    )
    ap.add_argument("-w", "--width", required=True, help="Width of window size")
    ap.add_argument("-ht", "--height", required=True, help="Height of window size")
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    width = int(args["width"])
    height = int(args["height"])
    category = args["category"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]
    #output_folder = args["output_folder"]

    generate_window(pklot_path, json_path, category, width, height, folder_path, project_name) 
