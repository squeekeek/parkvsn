#center
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_perset_wtraining_hog_lbp/lib1/train_svm_mult_wo_div.py \
-hp1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_flip_wo_except/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_blk-3_cell-6 \
-hp2 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_flip_wo_except/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_blk-3_cell-6 \
-hp3 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_flip_wo_except/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_blk-3_cell-6 \
-lp1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50_pr-18_rc-46 \
-lp2 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50_pr-18_rc-46 \
-lp3 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50_pr-18_rc-46 \
-hn1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/perwinsize_36_flip_wo_except/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_blk-3_cell-6  \
-ln1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_lbp/pw_lbp_18_findparams_1234_6/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50_pr-18_rc-46 \
-m hog_lbp_center \
-w 40 \
-ht 50 \
-n hog_lbp_concat_36_46_wo_div \
-sj /home/agatha/Documents/parkvsn/ML/image-svm/config/svm001.json

