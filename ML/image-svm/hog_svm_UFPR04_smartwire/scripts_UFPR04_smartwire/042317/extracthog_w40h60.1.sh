
# left-1, major-view-based
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-1" \
-nc "left-1" \
-ec "left-1" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60


# left-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-2" \
-nc "left-2" \
-ec "left-2" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60



# left-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-3" \
-nc "left-3" \
-ec "left-3" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60


# center-1, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-1" \
-nc "center-1" \
-ec "center-1" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60

# center-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-2" \
-nc "center-2" \
-ec "center-2" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60


# center-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-3" \
-nc "center-3" \
-ec "center-3" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60

# right-1, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-1" \
-nc "right-1" \
-ec "right-1" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60

# right-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-2" \
-nc "right-2" \
-ec "right-2" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60

# right-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-3" \
-nc "right-3" \
-ec "right-3" \
-w 40 \
-ht 60 \
-pos "upper-left" \
-n one-model_w40h60



