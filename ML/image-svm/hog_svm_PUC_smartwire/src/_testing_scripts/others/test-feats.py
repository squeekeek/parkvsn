"""
    For training dataset. Assumes same window size.
    Loads HoG features from folder.
    Outputs results with given filename.

    For quick testing.
    Assumption is that you know the labels of the feats in the folder.

    Example usage:
    python test-feats.py -f feats_roi_UFPR04_left-1 -m single14.model
"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-f', "--feat_path",
        help="Path to thefeatures directory",
        required=True)
    parser.add_argument(
        '-m', "--model_path",
        help="Path of the model file",
        required=True)

    args = vars(parser.parse_args())
    feat_path = args["feat_path"]
    model_path = args["model_path"]

    # Load model
    clf = joblib.load(model_path)

    fds = []
    fds_paths = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds_paths.append(feat_path)
        fds.append(fd)

    for i, fd in enumerate(fds):
        pred = clf.predict(fd.reshape(1, -1))
        print fds_paths[i]
        print "Pred: {}".format(pred)
        print ""

