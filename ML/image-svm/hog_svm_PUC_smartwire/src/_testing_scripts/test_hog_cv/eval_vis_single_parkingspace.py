"""
    For testing a single image and a model.

    Expects an input of an appropriately cropped parking space and a model.
    Progress:
    - [X] Would work for left-1
        - [ ] Need to negative hardmine for other stuff
        - [ ] Need to create master script to test all models

    Outputs detections.

    Example usage:
    python eval_vis_single_parkingspace.py \
    -i /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/testing_UFPR04/data/preprocess/img_marked_eeei_5th_solar_041017_a_morning_parkinglinebased/Segmented/Occupied/20170330_1030#8.jpg \
    -m /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/models/one-view-against-all_wo_except_wo_div/w50_h80_pos_right-1neg_unocc_ALL_wo_div.model \
    -w 50 -ht 80 -vd y \
    -v y -wr y \
    -f /Users/agatha/Desktop



"""


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from lib.nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from skimage.io import imread
from imutils.object_detection import non_max_suppression
from lib.helpers import pyramid, sliding_window

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 0


def eval_parkingspace(image_path, model_path, win_width, win_height, visualize_det, ifVisualize, ifWriteImage, output_folder):
    """
    Evaluates a parking space
    Passing image path and model path
    """
    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    config_path = os.path.join(top_dir, "config")

    # Load HoG setting
    hog_config_path = os.path.join("config_path", "hog.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    # Load model 
    clf = joblib.load(model_path)

    # Load image
    orig_image = cv2.imread(image_path)
    orig_height, orig_width = orig_image.shape[:2]
    orig_ratio = orig_width/float(orig_height)

    # Resize to height of 100 px maintaining aspect ratio
    res_height = 100
    res_width = int(res_height*orig_ratio)
    resized_image = cv2.resize(orig_image, (res_width, res_height))

    # Grayscale image
    gray_resized = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

    # Settings for HoG
    """
    window_size = (win_height, win_width)
    orientations = 9
    pixels_per_cell = [4, 4]
    cells_per_block = [4, 4]
    transform_sqrt = True
    """
    winSize = [win_height, win_width]
    blockSize = hog_dict["hog"][0]["blockSize"]
    blockStride = hog_dict["hog"][0]["blockStride"]
    cellSize = hog_dict["hog"][0]["cellSize"]
    nbins = hog_dict["hog"][0]["nbins"]
    derivAperture = hog_dict["hog"][0]["derivAperture"]
    winSigma = hog_dict["hog"][0]["winSigma"]
    histogramNormType = hog_dict["hog"][0]["histogramNormType"]
    L2HysThreshold = hog_dict["hog"][0]["L2HysThreshold"]
    gammaCorrection = hog_dict["hog"][0]["gammaCorrection"]
    nlevels = hog_dict["hog"][0]["nlevels"]
    signedGradients = hog_dict["hog"][0]["signedGradients"]

    # Load HoG Descriptor
    hog = cv2.HOGDescriptor(
        winSize, blockSize, blockStride, cellSize,
        nbins, derivAperture, winSigma, histogramNormType, L2HysThreshold,
        gammaCorrection, nlevels, signedGradients
    )


    # Settings for image pyramid
    #downscale = 1.1
    #scale = 1.01
    downscale = 1.01
    scale = 0
    detections = []
    step_size = (4, 4)

    # Settings for nms
    threshold = 0.3

    # Downscale the image and iterate
    # for im_scaled in pyramid(gray_resized, scale, window_size):
    #for im_scaled in pyramid_gaussian(gray_resized, downscale=downscale):
    for im_scaled in pyramid(gray_resized, scale=downscale, minSize=window_size):
        # This list contains detections at the current scale
        cd = []
        # If the width or height of the scaled image is less than the width
        # or height of the widnow, then end the iterations
        if im_scaled.shape[0] < window_size[0] or im_scaled.shape[1] < window_size[1]:
            """
            print "im_scaled.shape[0]: {}, im_scaled.shape[1]: {}".format(im_scaled.shape[0], im_scaled.shape[1])
            print "breaked"
            """
            break

        #  print "im_scaled.shape[:]: {}".format(im_scaled.shape[:])

        """
        print "im_window.shape[:]: {}".format(im_window.shape[:])
        print "im_window.shape[0]: {}".format(im_window.shape[0])
        print "im_window.shape[1]: {}".format(im_window.shape[1])
        """
        # sys.exit()
        # Calculate the HoG features
        """
        fd = feature.hog(
            im_window,
            orientations=orientations,
            pixels_per_cell=pixels_per_cell,
            cells_per_block=cells_per_block,
            transform_sqrt=transform_sqrt,
            visualise=False
        )
        """
   

        pred = clf.predict(fd.reshape(1, -1))
        if pred[0] > 0:
            test_df = clf.decision_function(fd.reshape(1, -1))
            """
            print "Detection:: Location -> ({}, {})".format(x, y)
            print "Scale ->  {} | Confidence Score {} \n".format(
                scale, clf.decision_function(fd.reshape(1, -1)))
            print "test_df.shape: {}".format(test_df.shape)
            """
            detections.append((
                x, y, test_df,
                int(window_size[1]*(downscale**scale)),
                int(window_size[0]*(downscale**scale))
                ))
            cd.append(detections[-1])
        if visualize_det == "y":
            clone = im_scaled.copy()
            for x1, y1, _, _, _ in cd:
                # Draw the detectionas at this scale
                cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + 
                    im_window.shape[0]), (0, 0,0), thickness=2)
            cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]),
                (255, 255, 255), thickness=2)
            #sliding_title = "({}, {}) Sliding Window in Progress".format(clone.shape[0], clone.shape[1])
            sliding_title = "Sliding Window in Progress"
            cv2.imshow(sliding_title, clone)
            cv2.waitKey(5)
        # Move the next scale
        #downscale = downscale + 1
        scale = scale + 1
    # Display the results before performing NMS
    clone = resized_image.copy()
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(
            resized_image, (x_tl, y_tl), (x_tl + w, y_tl + h), (0, 0, 0), thickness=2
        )
    if ifVisualize == "y":
        cv2.imshow("Raw Detections before NMS", resized_image)
        cv2.waitKey()



    # Perform Non Maxima Suppression
    # detections = non_max_suppression(detections, threshold)
    detections = nms(detections, threshold)
    num_detections = len(detections)

    # Display the results after performing NMS
    for (x_tl, y_tl, _, w, h) in detections:
        # Draw the detections
        cv2.rectangle(clone, (x_tl, y_tl), (x_tl+w,y_tl+h), (0, 0, 0), thickness=2)

    if ifWriteImage == "y":
        image_name = image_path.split("/")[-1]
        if output_folder is None:
            print "No output folder specified"
            sys.exit()
        if num_detections > 0:
            write_path = os.path.join(output_folder, "Occupied", image_name)
        else:
            write_path = os.path.join(output_folder, "Empty", image_name)
        cv2.imwrite(write_path, clone)

    if ifVisualize == "y":
        cv2.imshow("Final Detections after applying NMS", clone)
        cv2.waitKey()

    return num_detections

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-i', "--image_path",
        help="Path to the image file",
        required=True
    )
    ap.add_argument(
        '-m', "--model_path",
        help="Path to the model to use",
        required=True
    )
    ap.add_argument(
        '-w', "--width",
        help="Width of model",
        required=True
    )
    ap.add_argument(
        '-ht', "--height",
        help="Height of model",
        required=True
    )
    ap.add_argument(
        '-vd', "--visualize_detections",
        help="y to visualize detections",
        required=True
    )
    ap.add_argument(
        "-v", "--ifVisualize",
        help="y to visualize final detections",
        required=True
    )
    ap.add_argument(
        "-wr", "--ifWriteImage",
        help="y to write image",
        required=True
    )
    ap.add_argument(
        "-f", "--output_folder",
        help="Output folder to write images in",
        required=True
    )
    args = vars(ap.parse_args())

    image_path = args["image_path"]
    model_path = args["model_path"]
    win_width = int(args["width"])
    win_height = int(args["height"])
    visualize_det = args["visualize_detections"]
    ifVisualize = args["ifVisualize"]
    ifWriteImage = args["ifWriteImage"]
    output_folder = args["output_folder"]

    empty_folder = os.path.join(output_folder, "Empty")
    occupied_folder = os.path.join(output_folder, "Occupied")
    if not os.path.isdir(empty_folder):
        os.makedirs(empty_folder)
    if not os.path.isdir(occupied_folder):
        os.makedirs(occupied_folder)

    prediction = eval_parkingspace(
            image_path, model_path, win_width,
            win_height, visualize_det, ifVisualize, ifWriteImage, output_folder)
    print "# Occupied: {}".format(prediction)
