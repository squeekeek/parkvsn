"""
    Loads HoG features from a pos folder and a neg folder. 
    Outputs a trained model.

    Example usage:
    
    python train-model.py -p feats_pos_upper-left_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 -n feats_pos_upper-left_genwin_left-1_UFPR04_neg_paths_8680_w-30h-80 -m pos-left-1_neg-left-1
"""

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

import argparse as ap
import glob
import os
# from config import *
import cv2
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-p', "--posfeat",
        help="Path to the positive features directory",
        required=True)
    parser.add_argument(
        '-n', "--negfeat",
        help="Path to the negative features directory",
        required=True)
    parser.add_argument(
        '-m', "--model_name",
        help="Name of the model file",
        required=True)

    args = vars(parser.parse_args())

    pos_feat_path = args["posfeat"]
    neg_feat_path = args["negfeat"]
    model_name = args["model_name"]



    fds = []
    labels = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(1)

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path, "*.feat")):
        fd = joblib.load(feat_path)
        fds.append(fd)
        labels.append(0)

    # Perform test split
    # 60% - train, 20% - cross validation, 20% - test

    #fds_train, fds_temp, labels_train, labels_temp = train_test_split(
    #    fds, labels, test_size=0.40, random_state=5)
    #fds_test, fds_cv, labels_test, labels_cv = train_test_split(
    #    fds_temp, labels_temp, test_size=0.50, random_state=5)

    #clf = LinearSVC(class_weight="balanced")
    clf = LinearSVC()
    print "Training a Linear SVM Classifier"
    #clf.fit(fds_train, labels_train)
    clf.fit(fds, labels)
    # If feature directories don't exist, create them
    model_path =  model_name + '.model'
    joblib.dump(clf, model_path)
    print "Classifier saved to {}".format(model_path)