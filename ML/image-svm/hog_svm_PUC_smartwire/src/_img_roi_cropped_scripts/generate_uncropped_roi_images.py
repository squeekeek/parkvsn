"""
    Loads non-cropped images to a folder given an ROI json file.

    Example usage:
    python generate-uncropped-roi-images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j roi_UFPR04_left-1.json

    python generate_uncropped_roi_images.py -pk /Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/ -j /Users/agatha/Desktop/hog_svm_tests_UFPR04/data/json_cropped/UFPR04_halfway/pos_upper-left_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json -n test
"""


import argparse
import glob
import os
import cv2
import json
import io
import sys
import shutil


def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk',
        "--pklot_path", help="Path to PkLot dataset",
        required=True)
    ap.add_argument(
        "-j", "--json_path",
        help="JSON file of sorted numbers to load",
        required=True)
    ap.add_argument(
        '-f', "--folder_path", help="Folder path to store result")
    ap.add_argument(
        '-n', "--project_name", help="Project name")
    args = vars(ap.parse_args())

    test_pklot_path = args["pklot_path"]
    json_path = args["json_path"]
    folder_path = args["folder_path"]
    project_name = args["project_name"]

    if os.path.exists(json_path):
        print "Viewing images from: {}".format(json_path)
        with open(json_path) as data_file:
            roi_dict = json.load(data_file)
    else:
        print "Json path does not exist"

    pklot_path = test_pklot_path.split("PKLotSegmented")[0] + "PKLotSegmented"
    
    folder_name = json_path.split("/")[-1].split(".json")[0]
    temp_dest_path = "im_" + folder_name + "_uncropped"

    if folder_path is None:
        if project_name is None:
            print "You did not put a project name"
            sys.exit()
        parent_a = os.path.abspath("..")
        parent_b_temp = os.path.join(parent_a, "..")
        dir_main = os.path.abspath(parent_b_temp)
        data_folder = os.path.join(dir_main, "data", "img_uncropped_roi", project_name)

        if not os.path.isdir(data_folder):
            os.makedirs(data_folder)

        dest_path = os.path.join(data_folder, temp_dest_path)
    else:
        dest_path = os.path.join(folder_path, temp_dest_path)


    if os.path.isdir(dest_path):
        shutil.rmtree(dest_path)
   
    os.makedirs(dest_path)

    total_num = 0
    for i, key in enumerate(roi_dict):
        total_num = total_num + len(roi_dict[key])
    num_copied = 0
    for i, key in enumerate(roi_dict):
        for j, im_path in enumerate(roi_dict[key]):
            actual_path = pklot_path + im_path
            copyFile(actual_path, dest_path)
            num_copied = num_copied + 1
            if (num_copied % 1000 == 0 or num_copied == total_num):
                print "[{}%] {}/{} copied".format(
                    (num_copied/float(total_num))*100, num_copied, total_num)