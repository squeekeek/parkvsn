"""
    For HoG testing specifically.

    By set training.

    Note: Each of the id of hog_dict are the actual experiments, 
    so each of the indexes returned.

    Run different test as stated

    Use preprocess-training_hogparams.py to generate:
        - folder for models in testing_UFPR04
        - models.json in specified folder
        - script for copying models to specified folder
            - place in specified folder for models
            - to be run manaully but already has "cp" etc, and just need to replace relative folders with "replace with"
    Example Usage:
    python pipeline-individualviews_wo_except.py \
    -pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
    -mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
    -mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
    -dc UFPR04 \
    -hj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_210_212.json \
    -sj /Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/svm.json 

"""

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils
from collections import Counter

from datetime import datetime

from timeit import default_timer as timer


from preprocess_training_hogparams import preprocess_training_hogparams


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-pk', "--pklot_path", help="Path to PkLot dataset", 
        required=True)
    ap.add_argument(
        "-mp", "--main_pos_path",
        help="Main JSON file for paths for positive features",
        required=True)
    ap.add_argument(
        "-mn", "--main_neg_path",
        help="Main JSON file for paths for negative features",
        required=True)
    ap.add_argument(
        "-dc", "--dataset_category",
        help="Enter dataset category: UFPR04, UFPR05, PUC, or ALL",
        required=True
    )
    ap.add_argument(
        "-hj", "--hog_json",
        help="Enter path to hog json file to use",
        required=True
    )
    ap.add_argument(
        "-sj", "--svm_json",
        help="Enter path to svm json file to use",
        required=True
    )
    ap.add_argument(
        '-n', "--project_name", help="Project name",
        required=True
    )
    args = vars(ap.parse_args())
    pklot_path = args["pklot_path"]
    main_pos_path = args["main_pos_path"]
    main_neg_path = args["main_neg_path"]
    dataset_category = args["dataset_category"]
    hog_json = args["hog_json"]
    svm_json = args["svm_json"]
    project_name = args["project_name"]

    start_time = datetime.now()
    # Load HoG setting
    hog_config_path = hog_json
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)

    # Load SVM setting
    with open(svm_json) as data_file:
        svm_dict = json.load(data_file)
    # Test #1
    # Doing for each individual view without except
    test1_left_1_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="left-1",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=30,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_left_2_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="left-2",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=30,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_left_3_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="left-3",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_center_1_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="center-1",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=50,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_center_2_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="center-2",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=50,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_center_3_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="center-3",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=50,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_right_1_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="right-1",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=50,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_right_2_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="right-2",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )
    test1_right_3_dict = preprocess_training_hogparams(
        pklot_path=pklot_path,
        main_pos_path=main_pos_path,
        main_neg_path=main_neg_path,
        dataset_category=dataset_category,
        negative_category="ALL",
        category1="right-3",
        category2=None,
        category3=None,
        category4=None,
        category5=None,
        except_category=None,
        width=40,
        height=80,
        position="upper-left",
        folder_path=None,
        project_name=project_name,
        hog_dict=hog_dict,
        svm_dict=svm_dict
    )

    # Generate save script
    # Get models path
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    testing_path = os.path.join(top_dir, "testing_UFPR04")
    models_path = os.path.join(testing_path, "models")

    # Go through every experiment for w/ div
    for i, option in enumerate(hog_dict["hog"]):
        model_folder = project_name + "_" + option["id"] + "_w_div"
        model_folder_path = os.path.join(models_path, model_folder)
        if not os.path.isdir(model_folder_path):
            os.makedirs(model_folder_path)
        copy_path = os.path.join(model_folder_path, "copy_models.sh")

        write_copy = open(copy_path, 'w')
        write_copy.truncate()
        copy_script = (
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
        ).format(
            test1_left_1_dict["model_path_wdiv"][i], model_folder_path,
            test1_left_2_dict["model_path_wdiv"][i], model_folder_path,
            test1_left_3_dict["model_path_wdiv"][i], model_folder_path,
            test1_center_1_dict["model_path_wdiv"][i], model_folder_path,
            test1_center_2_dict["model_path_wdiv"][i], model_folder_path,
            test1_center_3_dict["model_path_wdiv"][i], model_folder_path,
            test1_right_1_dict["model_path_wdiv"][i], model_folder_path,
            test1_right_2_dict["model_path_wdiv"][i], model_folder_path,
            test1_right_3_dict["model_path_wdiv"][i], model_folder_path
        )
        write_copy.write(copy_script)
        write_copy.close()

        model_json_path = os.path.join(model_folder_path, "models.json")
        write_model = open(model_json_path, 'w')
        write_model.truncate()
        model_list = (
            "{'UFPR04': {"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} },"
            "{}: { 'rows': {}, 'cols': {} }"
            "}"
        ).format(
            test1_left_1_dict["model_path_wdiv"][i].split("/")[-1], 80, 30,
            test1_left_2_dict["model_path_wdiv"][i].split("/")[-1],80, 30,
            test1_left_3_dict["model_path_wdiv"][i].split("/")[-1], 80, 40,
            test1_center_1_dict["model_path_wdiv"][i].split("/")[-1], 50, 40,
            test1_center_2_dict["model_path_wdiv"][i].split("/")[-1], 50, 40,
            test1_center_3_dict["model_path_wdiv"][i].split("/")[-1], 50, 50,
            test1_right_1_dict["model_path_wdiv"][i].split("/")[-1], 80, 50,
            test1_right_2_dict["model_path_wdiv"][i].split("/")[-1], 80, 40,
            test1_right_3_dict["model_path_wdiv"][i].split("/")[-1], 80, 40,
        )
        write_model.write(model_list)
        write_model.close()

    # Go through every experiment for w/o divs
    for i, option in enumerate(hog_dict["hog"]):
        model_folder = project_name + "_" + option["id"] + "_wo_div"
        model_folder_path = os.path.join(models_path, model_folder)
        if not os.path.isdir(model_folder_path):
            os.makedirs(model_folder_path)
        copy_path = os.path.join(model_folder_path, "copy_models.sh")

        print "#############################################"
        print option["id"]
        print "#############################################"

        write_copy = open(copy_path, 'w')
        write_copy.truncate()
        copy_script = (
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
            "cp {} {}\n"
        ).format(
            test1_left_1_dict["model_path_wo_div"][i], model_folder_path,
            test1_left_2_dict["model_path_wo_div"][i], model_folder_path,
            test1_left_3_dict["model_path_wo_div"][i], model_folder_path,
            test1_center_1_dict["model_path_wo_div"][i], model_folder_path,
            test1_center_2_dict["model_path_wo_div"][i], model_folder_path,
            test1_center_3_dict["model_path_wo_div"][i], model_folder_path,
            test1_right_1_dict["model_path_wo_div"][i], model_folder_path,
            test1_right_2_dict["model_path_wo_div"][i], model_folder_path,
            test1_right_3_dict["model_path_wo_div"][i], model_folder_path
        )
        print copy_script
        write_copy.write(copy_script)
        write_copy.close()

        model_json_path = os.path.join(model_folder_path, "models.json")
        write_model.open(model_json_path, 'w')
        write_model.truncate()
        model_list = (
            "{'UFPR04': {"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} },"
            "'{}': { 'rows': {}, 'cols': {} }"
            "}"
        ).format(
            test1_left_1_dict["model_path_wo_div"][i].split("/")[-1], 80, 30,
            test1_left_2_dict["model_path_wo_div"][i].split("/")[-1], 80, 30,
            test1_left_3_dict["model_path_wo_div"][i].split("/")[-1], 80, 40,
            test1_center_1_dict["model_path_wo_div"][i].split("/")[-1], 50, 40,
            test1_center_2_dict["model_path_wo_div"][i].split("/")[-1], 50, 40,
            test1_center_3_dict["model_path_wo_div"][i].split("/")[-1], 50, 50,
            test1_right_1_dict["model_path_wo_div"][i].split("/")[-1], 80, 50,
            test1_right_2_dict["model_path_wo_div"][i].split("/")[-1], 80, 40,
            test1_right_3_dict["model_path_wo_div"][i].split("/")[-1], 80, 40,
        )
        print model_list
        write_model.write(model_list)
        write_model.close()

    end_time = datetime.now()
    print "Time elapsed: {}".format(end_time-start_time)

