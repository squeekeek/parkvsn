"""
========================
Plotting Learning Curves
========================

On the left side the learning curve of a naive Bayes classifier is shown for
the digits dataset. Note that the training score and the cross-validation score
are both not very good at the end. However, the shape of the curve can be found
in more complex datasets very often: the training score is very high at the
beginning and decreases and the cross-validation score is very low at the
beginning and increases. On the right side we see the learning curve of an SVM
with RBF kernel. We can see clearly that the training score is still around
the maximum and the validation score could be increased with more training
samples.
"""

import json
import os
import numpy as np
import glob
from sklearn.externals import joblib
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit

from lib1.generate_occ_to_neg import generate_occ_to_neg
from lib1.generate_window import generate_window
from lib1.generate_window_pos import generate_window_pos
from lib1.extract_hog_paths import extract_hog_paths
from lib1.extract_hog_neg import extract_hog_neg
from lib1.train_svm_mult_w_div import train_svm_mult_w_div
from lib1.train_svm_mult_wo_div import train_svm_mult_wo_div

from sklearn.svm import LinearSVC


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    """
    Generate a simple plot of the test and training learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : int, cross-validation generator or an iterable, optional
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:
          - None, to use the default 3-fold cross-validation,
          - integer, to specify the number of folds.
          - An object to be used as a cross-validation generator.
          - An iterable yielding train/test splits.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : integer, optional
        Number of jobs to run in parallel (default 1).
    """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt

if __name__ == "__main__":
    pklot_path = "/Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented"
    main_pos_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json"
    main_neg_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json"
    dataset_category = "UFPR04"
    negative_category = "left-1"
    category1 = "left-1"
    width = 80
    height = 30
    position = "upper-left"
    folder_path = None
    project_name = "find_learning_curve_left-1"
    hog_json = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/config/hog_list_36.json"

    # Load HoG setting
    with open(hog_json) as data_file:
        hog_dict = json.load(data_file)


    window_path_pos_1 = generate_window(
        pklot_path, main_pos_path, category1, width, height, folder_path, project_name
        )
    roi_path_pos_1 = generate_window_pos(
            pklot_path, window_path_pos_1, position, folder_path, project_name
        )

    # Extract HoG
    hog_path_pos_1 = []
    for i, setting in enumerate(hog_dict["hog"]):
        temp_path = extract_hog_paths(
            pklot_path, roi_path_pos_1, width, height,
            folder_path, project_name, i, hog_dict
        )
        hog_path_pos_1.append(temp_path)

    # For the indicated negative categories
    # Generate adjusted dim
    window_path_neg = generate_window(
        pklot_path, main_neg_path,
        negative_category, width, height,
        folder_path, project_name
    )
    # Genreaete roi json
    roi_path_neg = generate_window_pos(
        pklot_path, window_path_neg,
        position, folder_path, project_name
    )
    # Extract hog
    hog_path_neg_1 = []
    for i, setting in enumerate(hog_dict["hog"]):
        temp_path = extract_hog_paths(
            pklot_path, roi_path_neg, width, height,
            folder_path, project_name, i, hog_dict
        )
        hog_path_neg_1.append(temp_path)

    title = "Learning Curve (left-1 view, win=[80, 30], C=0.01)"
    cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)

    estimator = LinearSVC(class_weight="balanced", C=0.01)

    #X = hog_path_pos_1 + hog_path_neg_1
    X = []
    y = []
    for feat_path in glob.glob(os.path.join(hog_path_pos_1[0], "*.feat")):
        fd = joblib.load(feat_path)
        X.append(fd)
        y.append(1)
    for feat_path in glob.glob(os.path.join(hog_path_neg_1[0], "*.feat")):
        fd = joblib.load(feat_path)
        X.append(fd)
        y.append(0)


    plot_learning_curve(estimator, title, X, y, ylim=(0.7, 1.01), cv=cv, n_jobs=4)
    plt.show()

