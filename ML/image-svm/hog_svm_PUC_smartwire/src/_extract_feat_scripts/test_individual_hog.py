from extract_hog_paths_1 import extract_hog_paths
import os
import json
import sys

if __name__ == "__main__":
    pklot_path = "/Users/agatha/Documents/CoE198/datasets/PKLot/PKLotSegmented/"

    folder_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/notes/test_hog"
    project_name = "test_individual_hog"


    # Load configs
    parent_a = os.path.abspath("..")
    parent_b_temp = os.path.join(parent_a, "..")
    dir_main = os.path.abspath(parent_b_temp)
    parent_c_temp = os.path.join(dir_main, "..")
    top_dir = os.path.abspath(parent_c_temp)
    config_path = os.path.join(top_dir, "config")

    # Load HoG Setting
    hog_config_path = os.path.join(config_path, "hog_list.json")
    with open(hog_config_path) as data_file:
        hog_dict = json.load(data_file)


    # Testing 30x80 
    json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80.json"
    #json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80.json"
    width = 30
    height = 80
    print "###################################################################"
    print json_path
    print "###################################################################"
    for i, option in enumerate(hog_dict["hog"]):
        extract_hog_paths(
            pklot_path=pklot_path,
            json_path=json_path,
            width=width,
            height=height,
            folder_path=folder_path,
            project_name=project_name,
            hog_option=i
        )
    
    # Testing 40x50
    json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50.json"
    #json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50.json"
    width = 40
    height = 50
    print "###################################################################"
    print json_path
    print "###################################################################"
    for i, option in enumerate(hog_dict["hog"]):
        extract_hog_paths(
            pklot_path=pklot_path,
            json_path=json_path,
            width=width,
            height=height,
            folder_path=folder_path,
            project_name=project_name,
            hog_option=i
        )
    sys.exit()
    json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-50h-80.json"
    width = 50
    height = 80
    print "###################################################################"
    print json_path
    print "###################################################################"
    for i, option in enumerate(hog_dict["hog"]):
        extract_hog_paths(
            pklot_path=pklot_path,
            json_path=json_path,
            width=width,
            height=height,
            folder_path=folder_path,
            project_name=project_name,
            hog_option=i
        )
    json_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80.json"
    print "###################################################################"
    print json_path
    print "###################################################################"
    width = 40
    height = 80
    for i, option in enumerate(hog_dict["hog"]):
        extract_hog_paths(
            pklot_path=pklot_path,
            json_path=json_path,
            width=width,
            height=height,
            folder_path=folder_path,
            project_name=project_name,
            hog_option=i
        )