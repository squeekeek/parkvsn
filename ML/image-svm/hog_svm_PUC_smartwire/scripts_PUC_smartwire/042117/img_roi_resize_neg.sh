# negative images

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-50h-80.json \
-n one-view-against-all_except-major \
-g n

# the positive features that are except

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80.json \
-n one-view-against-all_except-major \
-g n

python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_img_roi_cropped_scripts/generate_cropped_roi_images_resize.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented/ \
-j /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/json_cropped/one-view-against-all_except-major/pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-50h-80.json \
-n one-view-against-all_except-major \
-g n
