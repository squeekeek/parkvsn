# copy models to testing_UFPR04
#cp -r /home/agatha/Documents/parkvsn/ML/image-svm/data/models/one-view-against-all_except-major /home/agatha/Documents/parkvsn/ML/image-svm/testing_UFPR04/models/


#### Cmd for occupied and unoccupied, and except center of center-1:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-50 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 \
-w 40 \
-ht 50 \
-m pos_center-1neg_unocc_ALL_center \
-n one-view-against-all_except-major 

#### Cmd for occupied and unoccupied, and except center of center-2:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-50 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 \
-w 40 \
-ht 50 \
-m pos_center-2neg_unocc_ALL_center \
-n one-view-against-all_except-major

#### Cmd for occupied and unoccupied, and except center of center-3:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-50 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-50 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_center_25464_w-40h-50 \
-w 40 \
-ht 50 \
-m pos_center-3neg_unocc_ALL_center \
-n one-view-against-all_except-major


#### Cmd for occupied and unoccupied, and except left of left-1:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-30h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 \
-w 30 \
-ht 80 \
-m pos_left-1neg_unocc_ALL_left \
-n one-view-against-all_except-major

#### Cmd for occupied and unoccupied, and except left of left-2:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-30h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-30h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-30h-80 \
-w 30 \
-ht 80 \
 -m pos_left-2neg_unocc_ALL_left \
-n one-view-against-all_except-major

#### Cmd for occupied and unoccupied, and except left of left-3:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-40h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_left_36151_w-40h-80 \
-w 40 \
-ht 80 \
-m pos_left-3neg_unocc_ALL_left \
-n one-view-against-all_except-major

#### Cmd for occupied and unoccupied, and except right of right-1:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-50h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-50h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-50h-80 \
-w 50 \
-ht 80 \
-m pos_right-1neg_unocc_ALL_right \
-n one-view-against-all_except-major 

#### Cmd for occupied and unoccupied, and except right of right-2:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 \
-w 40 \
-ht 80 \
-m pos_right-2neg_unocc_ALL_right \
-n one-view-against-all_except-major 

#### Cmd for occupied and unoccupied, and except right of right-3:
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-80 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_neg_paths_59718_w-40h-80 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/data/feats_hog/one-view-against-all_except-major/feats_pos_UL_genwin_ALL_UFPR04_pos_paths_except_right_30635_w-40h-80 \
-w 40 \
-ht 80 \
-m pos_right-3neg_unocc_ALL_right \
-n one-view-against-all_except-major 



