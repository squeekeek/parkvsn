python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_training_scripts/train_svm_mult_w_div.py \
-m pos_center-1neg_unocc_center-1 \
-p1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-1_UFPR04_pos_paths_11528_w-40h-60 \
-p2 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-2_UFPR04_pos_paths_7434_w-40h-60 \
-p3 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-3_UFPR04_pos_paths_1699_w-40h-60 \
-p4 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-1_UFPR04_pos_paths_6393_w-40h-60 \
-p5 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-2_UFPR04_pos_paths_1469_w-40h-60 \
-p6 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-3_UFPR04_pos_paths_2112_w-40h-60 \
-p7 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-1_UFPR04_pos_paths_2001_w-40h-60 \
-p8 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-2_UFPR04_pos_paths_7473_w-40h-60 \
-p9 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-3_UFPR04_pos_paths_6016_w-40h-60 \
-n1 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-1_UFPR04_neg_paths_14981_w-40h-60 \
-n2 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-2_UFPR04_neg_paths_11485_w-40h-60 \
-n3 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_center-3_UFPR04_neg_paths_2089_w-40h-60 \
-n4 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-1_UFPR04_neg_paths_8680_w-40h-60 \
-n5 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-2_UFPR04_neg_paths_2295_w-40h-60 \
-n6 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_left-3_UFPR04_neg_paths_1639_w-40h-60 \
-n7 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-1_UFPR04_neg_paths_1789_w-40h-60 \
-n8 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-2_UFPR04_neg_paths_7677_w-40h-60 \
-n9 /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/feats_hog/one-model_w40h60/feats_pos_UL_genwin_right-3_UFPR04_neg_paths_9083_w-40h-60 \
-w 40 \
-ht 60 \
-n one-model_w40h60 
