
# left-1, major-view-based
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-1" \
-nc "ALL" \
-ec "left" \
-w 30 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major


# left-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-2" \
-nc "ALL" \
-ec "left" \
-w 30 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major



# left-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "left-3" \
-nc "ALL" \
-ec "left" \
-w 40 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major


# center-1, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-1" \
-nc "ALL" \
-ec "center" \
-w 40 \
-ht 50 \
-pos "upper-left" \
-n one-view-against-all_except-major

# center-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-2" \
-nc "ALL" \
-ec "center" \
-w 40 \
-ht 50 \
-pos "upper-left" \
-n one-view-against-all_except-major


# center-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "center-3" \
-nc "ALL" \
-ec "center" \
-w 40 \
-ht 50 \
-pos "upper-left" \
-n one-view-against-all_except-major

# right-1, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-1" \
-nc "ALL" \
-ec "right" \
-w 50 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major

# right-2, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-2" \
-nc "ALL" \
-ec "right" \
-w 40 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major

# right-3, major-view based
## For training
## pipeline-per-category.py
###one-view-against-all
python /home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/src/_batch_scripts/pipeline-per-category.py \
-pk /home/agatha/Documents/datasets/PKLot/PKLotSegmented \
-mp "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_pos_paths.json" \
-mn "/home/agatha/Documents/parkvsn/ML/image-svm/hog_svm_UFPR04_smartwire/data/paths_sorted_posneg/UFPR04_halfway/UFPR04_neg_paths.json" \
-dc "UFPR04" \
-c "right-3" \
-nc "ALL" \
-ec "right" \
-w 40 \
-ht 80 \
-pos "upper-left" \
-n one-view-against-all_except-major



