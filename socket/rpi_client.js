var http = require('http'),
    express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    errorhandler = require('errorhandler'),
    bodyParser = require('body-parser');

var app = express();

app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(logger('combined'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static('public'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(request, response) {
    response.render('index');
});

app.use(errorhandler());

var server = http.createServer(app);


var io = require('socket.io-client'),
    socket = io.connect('http://localhost:4000');
//socket = io.connect('http://parakeet.sandnox.com');

var counter = 0;
var slots = [0, 0, 0, 0, 0, 0, 0, 0, 0];
var msg = "hello";
socket.on('connect', function() {
    console.log("socket connected");
    //var value = "Hello";
    //socket.emit('messageChange', { message: value });

    setInterval(function() {
            counter = counter + 1;
            var empty = counter;
            var total = 9;

            if (counter > 9) {
                counter = 0;
                empty = counter;
            } else {
                if (slots[counter] == 1) {
                    slots[counter] = 0;
                } else {
                    slots[counter] = 1;
                }

            }
            var msg = 'test_' + counter.toString();
            socket.emit('rpi_msg', {
                message: msg,
                empty: empty,
                total: total,
                slots: slots,
            })
        },
        3000);
});

socket.on('from_rpi', function(data) {
    console.log(data);
});

server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});